<?php

use app\modules\base\models\Setting;

/* @var $this yii\web\View */
/* @var $user app\modules\base\models\User */

/* Variables that are lengthy or used multiple times */
$blog_title = Setting::getSetting('blog_title');
$date = \Yii::$app->formatter->asDate(time(), 'php:l, F j, Y, g:i a');
$support_email = Setting::getSetting('support_email_address');
$register_url = Yii::$app->urlManager->createAbsoluteUrl(['/admin/default/register', 'token' => $user->security_token]);
//$register_link = Html::a(Html::encode($register_url), $register_url);

/* Title, subtitle, and body to be rendered */
$title = $blog_title . ' Invitation';
$subtitle = $date;
$body = <<<EOD
Greetings!

{$user->createdBy->firstNameLastName()} is inviting you to join {$blog_title}.
To participate, please visit the link below to register your account.

{$register_url}

If you have any questions or encounter problems registering your account,
you may contact {$user->createdBy->first_name} at {$user->createdBy->email}.

Welcome to {$blog_title}.
EOD;

echo $this->render('layouts/_content_text', [
    'title' => $title,
    'subtitle' => $subtitle,
    'body' => $body,
]);
