<?php

use app\modules\base\models\Setting;

/* @var $this yii\web\View */
/* @var $user app\modules\base\models\User */

/* Variables that are lengthy or used multiple times */
$blog_title = Setting::getSetting('blog_title');
$date = \Yii::$app->formatter->asDate(time(), 'php:l, F j, Y, g:i a');
$support_email = Setting::getSetting('support_email_address');
$reset_url = Yii::$app->urlManager->createAbsoluteUrl([
    '/admin/default/password-reset',
    'token' => $user->security_token
]);
//$reset_link = Html::a(Html::encode($reset_url), $reset_url);

/* Title, subtitle, and body to be rendered */
$title = $blog_title . ' Password Reset Request';
$subtitle = $date;
$body = <<<EOD
Hi {$user->first_name},

Please visit the link below to reset your password:

{$reset_url}

If you did not initiate this password reset please contact support
immediately at {$support_email}.
EOD;

echo $this->render('layouts/_content_text', [
    'title' => $title,
    'subtitle' => $subtitle,
    'body' => $body,
]);
