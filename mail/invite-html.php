<?php

use app\modules\base\models\Setting;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\modules\base\models\User */

/* Variables that are lengthy or used multiple times */
$blog_title = Setting::getSetting('blog_title');
$date = \Yii::$app->formatter->asDate(time(), 'php:l, F j, Y, g:i a');
$support_email = Setting::getSetting('support_email_address');
$register_link = Yii::$app->urlManager->createAbsoluteUrl(['/admin/default/register', 'token' => $user->security_token]);
$register_link = Html::a(Html::encode($register_link), $register_link);

/* Title, subtitle, and body to be rendered */
$title = $blog_title . ' Invitation';
$subtitle = $date;
$body = <<<EOD

    <p>Greetings!</p>
    
    <p>{$user->createdBy->firstNameLastName()} is inviting you to join {$blog_title}. To participate, please visit the link below to register your account.</p>
    
    <p>{$register_link}</p>
    
    <p>If you have any questions or encounter problems registering your account, you may contact {$user->createdBy->first_name} at {$user->createdBy->email}.</p>
    
    <p>Welcome to {$blog_title}.</p>

EOD;

echo $this->render('layouts/_content_html', [
    'title' => $title,
    'subtitle' => $subtitle,
    'body' => $body,
]);
