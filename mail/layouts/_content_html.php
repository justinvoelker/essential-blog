<?php

/* @var $title string */
/* @var $subtitle string */
/* @var $body string */

?>

<div style="color: #777; font-size: 1rem; line-height: 2rem;">

    <div style="border-top: 1px solid #ccc; border-bottom: 1px solid #ccc; padding: 40px 0; text-align: center;">
        <div style="font-size: 30px; color: #555; padding-bottom: 20px;">
            <?= $title ?>
        </div>
        <div>
            <?= $subtitle ?>
        </div>
    </div>

    <div style="border-bottom: 1px solid #ccc; padding: 20px 0;">
        <?= $body ?>
    </div>

</div>
