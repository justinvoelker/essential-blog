<?php

/* @var $title string */
/* @var $subtitle string */
/* @var $body string */

/* NOTE: The spacing/extra spacing between lines correctly produces single-space lines. */
?>
------------------------------------------------------------------------

<?= $title ?>

<?= $subtitle ?>


------------------------------------------------------------------------

<?= $body ?>


------------------------------------------------------------------------
