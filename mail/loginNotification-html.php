<?php

use app\modules\base\models\Setting;

/* @var $this yii\web\View */
/* @var $user app\modules\base\models\User */
/* @var $ip string */

/* Variables that are lengthy or used multiple times */
$blog_title = Setting::getSetting('blog_title');
$date = \Yii::$app->formatter->asDate(time(), 'php:l, F j, Y, g:i a');
$support_email = Setting::getSetting('support_email_address');

/* Title, subtitle, and body to be rendered */
$title = $blog_title . ' Login Notification';
$subtitle = $date;
$body = <<<EOD

    <p>Hi {$user->first_name},</p>
    
    <p>This email is to inform you that your {$blog_title} account has been successfully accessed as follows:</p>
    
    <p>
        Username: {$user->username}<br>
        IP Address: {$ip}<br>
        Time: {$date}
    </p>
    
    <p>If you did not initiate this login please contact support immediately at {$support_email}.</p>

EOD;

echo $this->render('layouts/_content_html', [
    'title' => $title,
    'subtitle' => $subtitle,
    'body' => $body,
]);
