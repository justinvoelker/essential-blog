<?php

use app\modules\base\models\Setting;

/* @var $this yii\web\View */
/* @var $user app\modules\base\models\User */

/* Variables that are lengthy or used multiple times */
$blog_title = Setting::getSetting('blog_title');
$date = \Yii::$app->formatter->asDate(time(), 'php:l, F j, Y, g:i a');
$support_email = Setting::getSetting('support_email_address');

/* Title, subtitle, and body to be rendered */
$title = $blog_title . ' Password Change';
$subtitle = $date;
$body = <<<EOD
Hi {$user->first_name},

This email is to inform you that your {$blog_title} password has been
successfully changed.

If you did not initiate this password change please contact support
immediately at {$support_email}.
EOD;

echo $this->render('layouts/_content_text', [
    'title' => $title,
    'subtitle' => $subtitle,
    'body' => $body,
]);
