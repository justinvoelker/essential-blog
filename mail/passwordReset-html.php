<?php

use app\modules\base\models\Setting;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\modules\base\models\User */

/* Variables that are lengthy or used multiple times */
$blog_title = Setting::getSetting('blog_title');
$date = \Yii::$app->formatter->asDate(time(), 'php:l, F j, Y, g:i a');
$support_email = Setting::getSetting('support_email_address');
$reset_url = Yii::$app->urlManager->createAbsoluteUrl([
    '/admin/default/password-reset',
    'token' => $user->security_token
]);
$reset_link = Html::a(Html::encode($reset_url), $reset_url);

/* Title, subtitle, and body to be rendered */
$title = $blog_title . ' Password Reset Request';
$subtitle = $date;
$body = <<<EOD

    <p>Hi {$user->first_name},</p>
    
    <p>Please visit the link below to reset your password:</p>
    
    <p>{$reset_link}</p>
    
    <p>If you did not initiate this password reset please contact support immediately at {$support_email}.</p>

EOD;

echo $this->render('layouts/_content_html', [
    'title' => $title,
    'subtitle' => $subtitle,
    'body' => $body,
]);
