# Test Cases

* Post Index (/admin/post)
  * Header should say Posts
  * Breadcrumb should say Posts
  * Menu button should be present and list Posts, Images, Tags, Categories, Users, Settings, line break, My Profile, and Logout
  * Help button should be present
  * Action Buttons should have single Add button
  * Grid should have Title and Created columns with default sort by created descending
  * Grid title should be title of most recent child post title
  * Grid sorting by title should sort post title according to most recent title (for edited and draft posts)
  * Draft posts should contain "(Draft)"
  * Published posts should contain title only
  * Edited published posts should contain "(Edited)"
* Settings
  * Changing admin posts per page should reflect in summary and pagination sections on post, image, tag, category, and user index pages
  * Changing admin posts per page should reflect in summary and pagination sections on post subgrids on tag, category, and user pages
   
