<?php
namespace app\rbac;

use yii\rbac\Rule;

/**
 * Checks if id does not match user passed via params (only used to update roles of non-self user)
 */
class SelfRuleFalse extends Rule
{
    public $name = 'isSelfFalse';

    /**
     * @param string|integer $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return boolean a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        return isset($params['model']) ? $params['model']->id != $user : false;
    }
}
