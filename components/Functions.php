<?php

namespace app\components;

use app\modules\base\models\Image;
use app\modules\base\models\Archive;
use app\modules\frontend\models\PostSearch;
use app\modules\base\models\Setting;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class Functions
{
    /**
     * Search for and return a list of recent posts
     * @param array $options
     * @return \app\modules\base\models\Post[]
     */
    public static function getPostList($options = [])
    {
        /* Default options if not provided */
        $count = isset($options['count']) ? $options['count'] : null;
        $orderby = isset($options['orderby']) ? $options['orderby'] : 'date';
        $order = isset($options['order']) ? $options['order'] : 'desc';

        /* Standard search model with ActiveDataProvider */
        $searchModel = new PostSearch();
        $searchModel->search(null);

        /* ActiveQuery from the ActiveDataProvider */
        $query = $searchModel->query;
        $posts = $query->all();

        /* Order the results */
        if ($orderby == 'title' && $order == 'asc') {
            ArrayHelper::multisort($posts, 'title', SORT_ASC, SORT_NATURAL | SORT_FLAG_CASE);
        } elseif ($orderby == 'title' && $order == 'desc') {
            ArrayHelper::multisort($posts, 'title', SORT_DESC, SORT_NATURAL | SORT_FLAG_CASE);
        } elseif ($orderby == 'date' && $order == 'asc') {
            ArrayHelper::multisort($posts, 'publish_date', SORT_ASC, SORT_NATURAL | SORT_FLAG_CASE);
        } elseif ($orderby == 'date' && $order == 'desc') {
            ArrayHelper::multisort($posts, 'publish_date', SORT_DESC, SORT_NATURAL | SORT_FLAG_CASE);
        }

        // Return a portion of the results (default should be high enough to include all results)
        return (!is_null($count)) ? array_slice($posts, 0, $count) : $posts;
    }

    /**
     * Search for and return a list of archive links
     * @param array $options
     * @return \app\modules\base\models\Archive[]
     */
    public static function getArchiveList($options = [])
    {
        /* Default options if not provided */
        $count = isset($options['count']) ? $options['count'] : null;
        $level = isset($options['level']) ? $options['level'] : Archive::LEVEL_MONTH;
        $format = isset($options['format']) ? $options['format'] : Archive::FORMAT_MONTH;

        /* Standard search model with ActiveDataProvider */
        $searchModel = new PostSearch();
        $searchModel->search(null);

        /* ActiveQuery from the ActiveDataProvider */
        $query = $searchModel->query;
        $posts = $query
            ->asArray()
            ->all();

        $archives = [];
        // Loop through every selected post to return only archive links as set in settings
        foreach ($posts as $post) {
            // Archive instance based on the post year, month, and day
            $archive = new Archive(
                [
                    'year' => $post['publish_year'],
                    'month' => $post['publish_month'],
                    'day' => $post['publish_day'],
                    'level' => $level,
                    'format' => $format,
                ]);

            // If the link is not already in the archive link list, add it
            if (!in_array($archive->ID, $archives)) {
                $archives[$archive->ID] = $archive;
            }
        }

        // Return a portion of the results (default should be high enough to include all results)
        return (!is_null($count)) ? array_slice($archives, 0, $count) : $archives;
    }

    /**
     * Search for and return a list of tags
     * @param array $options
     * @return \app\modules\base\models\Tag[]
     */
    public static function getTagList($options = [])
    {
        /* Default options if not provided */
        $count = isset($options['count']) ? $options['count'] : null;
        $orderby = isset($options['orderby']) ? $options['orderby'] : 'count';
        $order = isset($options['order']) ? $options['order'] : 'desc';

        /* Standard search model with ActiveDataProvider */
        $searchModel = new PostSearch();
        $searchModel->search(null);

        /* ActiveQuery from the ActiveDataProvider */
        $query = $searchModel->query;
        $posts = $query->all();

        $tags = [];
        // Loop through every post to return each tag in use
        foreach ($posts as $post) {
            /* @var $post \app\modules\base\models\Post */
            foreach ($post->tags as $tag) {
                if (!array_key_exists($tag->id, $tags)) {
                    $tags[$tag->id] = $tag;
                }
            }
        }

        /* Order the results */
        if ($orderby == 'name' && $order == 'asc') {
            ArrayHelper::multisort($tags, 'name', SORT_ASC, SORT_NATURAL | SORT_FLAG_CASE);
        } elseif ($orderby == 'name' && $order == 'desc') {
            ArrayHelper::multisort($tags, 'name', SORT_DESC, SORT_NATURAL | SORT_FLAG_CASE);
        } elseif ($orderby == 'count' && $order == 'asc') {
            ArrayHelper::multisort($tags, ['primary_posts_count','name'], [SORT_ASC, SORT_ASC], SORT_NATURAL | SORT_FLAG_CASE);
        } elseif ($orderby == 'count' && $order == 'desc') {
            ArrayHelper::multisort($tags, ['primary_posts_count','name'], [SORT_DESC, SORT_ASC], SORT_NATURAL | SORT_FLAG_CASE);
        }

        // Return a portion of the results (default should be high enough to include all results)
        return (!is_null($count)) ? array_slice($tags, 0, $count) : $tags;
    }

    /**
     * Search for and return a list of categories
     * @param array $options
     * @return \app\modules\base\models\Category[]
     */
    public static function getCategoryList($options = [])
    {
        /* Default options if not provided */
        $count = isset($options['count']) ? $options['count'] : null;
        $orderby = isset($options['orderby']) ? $options['orderby'] : 'count';
        $order = isset($options['order']) ? $options['order'] : 'desc';

        /* Standard search model with ActiveDataProvider */
        $searchModel = new PostSearch();
        $searchModel->search(null);

        /* ActiveQuery from the ActiveDataProvider */
        $query = $searchModel->query;
        $posts = $query->all();

        $categories = [];
        // Loop through every post to return each category in use
        foreach ($posts as $post) {
            /* @var $post \app\modules\base\models\Post */
            if (isset($post->category_id)) {
                if (!array_key_exists($post->category_id, $categories)) {
                    $categories[$post->category_id] = $post->category;
                }
            }
        }

        /* Order the results */
        if ($orderby == 'name' && $order == 'asc') {
            ArrayHelper::multisort($categories, 'name', SORT_ASC, SORT_NATURAL | SORT_FLAG_CASE);
        } elseif ($orderby == 'name' && $order == 'desc') {
            ArrayHelper::multisort($categories, 'name', SORT_DESC, SORT_NATURAL | SORT_FLAG_CASE);
        } elseif ($orderby == 'count' && $order == 'asc') {
            ArrayHelper::multisort($categories, ['primary_posts_count','name'], [SORT_ASC, SORT_ASC], SORT_NATURAL | SORT_FLAG_CASE);
        } elseif ($orderby == 'count' && $order == 'desc') {
            ArrayHelper::multisort($categories, ['primary_posts_count','name'], [SORT_DESC, SORT_ASC], SORT_NATURAL | SORT_FLAG_CASE);
        }

        // Return a portion of the results (default should be high enough to include all results)
        return (!is_null($count)) ? array_slice($categories, 0, $count) : $categories;
    }

    /**
     * Return the value for a given setting key if it exists. Return NULL if the key does not exist
     * @param $key string Setting key to find
     * @return array|NULL
     */
    public static function getSetting($key)
    {
        $setting = Setting::getSetting($key);

        return (isset($setting)) ? $setting : NULL;
    }

    public static function getAvatar($options = []) {

        /* Default options if not provided */
        $text = isset($options['text']) ? $options['text'] : null;
        $textAsIs = isset($options['textAsIs']) ? $options['textAsIs'] : false;
        $image = isset($options['image']) ? $options['image'] : null;
        $size = isset($options['size']) ? $options['size'] : null;

        /* If text is not provided in "as-is" format, use only the first character. */
        if (!$textAsIs) {
            $text = substr($text,0,1);
        }

        /* Calculate font size as half of image size */
        $sizeValue = preg_split('/(?=[a-z])/i', $size, 2)[0];
        $sizeUnits = preg_split('/(?=[a-z])/i', $size, 2)[1];
        $fontSize = ($sizeValue / 2) . $sizeUnits;

        $result = '';

        /* Begin avatar block */
        $result .= Html::beginTag('div', ['class' => ['avatar'], 'style' => "width: $size; height: $size;"]) . "\n";

        /* Only include an image if one is present and exists */
        $imageModel = Image::findModelByPath(rawurldecode($image));
        if ($image != null && $imageModel->imageFile()->exists) {
            $result .= $imageModel->imgTag([
                'sizes' => $size,
                    'class' => 'avatar__img',
                    'style' => "width: $size; height: $size;"
                ]) . "\n";
        }

        /* Always include letters as fallback. Will be hidden by css if image is present. */
        $result .= Html::tag('div', $text, [
            'class' => 'avatar__text',
                'style' => "width: $size; height: $size; line-height: $size; font-size: $fontSize;"
            ]) . "\n";

        /* End avatar block */
        $result .= Html::endTag('div') . "\n"; // .avatar

        return $result;

    }

    public static function getSelectableAvatar($options = []) {

        /* Duplicated logic from getAvatar */

        /* Default options if not provided */
        $size = isset($options['size']) ? $options['size'] : null;

        /* Calculate font size as half of image size */
        $sizeValue = preg_split('/(?=[a-z])/i', $size, 2)[0];
        $sizeUnits = preg_split('/(?=[a-z])/i', $size, 2)[1];
        $fontSize = ($sizeValue / 2) . $sizeUnits;

        /* Now to some custom stuff for the selectable avatar */

        /* Get normal avatar */
        $avatar = self::getAvatar($options);

        /* Checked avatar */
        $checked = Html::beginTag('div', ['class' => ['avatar'], 'style' => "width: $size; height: $size;"]) . "\n";
        $checked .= Html::tag('div', '<i class="fa fa-fw fa-check" aria-hidden="true"></i>', [
                'class' => 'avatar__check',
                'style' => "width: $size; height: $size; line-height: $size; font-size: $fontSize;"
            ]) . "\n";
        $checked .= Html::endTag('div') . "\n";


        $result = '';

        /* Begin avatar-flip-wrap block */
        $result .= Html::beginTag('div', ['class' => ['avatar-flip-wrap']]) . "\n";

        /* Add checkbox for selection */
        $result .= Html::checkbox('selection', false, [
                'value' => $options['record-id'],
                'class' => 'hidden',
                'data' => ['action-bar-group' => $options['action-bar-group']],
            ]) . "\n";

        /* Begin avatar-flip block */
        $result .= Html::beginTag('div', ['class' => ['avatar-flip']]) . "\n";

        /*  */
        $result .= Html::tag('div', $avatar, ['class' => 'avatar-flip-side']) . "\n";

        /*  */
        $result .= Html::tag('div', $checked, ['class' => 'avatar-flip-side avatar-flip-back']) . "\n";

        /* End flip block */
        $result .= Html::endTag('div') . "\n"; // .flip

        /* End flip-wrap block */
        $result .= Html::endTag('div') . "\n"; // .flip-wrap

        return $result;

    }


}
