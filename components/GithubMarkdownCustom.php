<?php

namespace app\components;

use app\modules\base\models\Image;
use cebe\markdown\GithubMarkdown;

class GithubMarkdownCustom extends GithubMarkdown
{
    public $imgSizes = null;
    public $imgClass = null;
    public $imgIsAmp = null;
    public $hrefClass = null;
    /**
     * Rather than typical img rendering, this custom rendering will return a responsive image tag
     * @param $block
     * @return string
     */
    protected function renderImage($block)
    {
        if (isset($block['refkey'])) {
            if (($ref = $this->lookupReference($block['refkey'])) !== false) {
                $block = array_merge($block, $ref);
            } else {
                return $block['orig'];
            }
        }

        // Get the responsive image tag
        // The url must be decoded since it was encoded by JavaScript when inserted into the content
        $image = Image::findModelByPath(rawurldecode($block['url']));
        /* Use alt text and title from image if not given. Title is likely never specified but alt always should be. */
        $text = empty($block['text']) ? $image->alternative_text : htmlspecialchars($block['text'], ENT_COMPAT | ENT_HTML401 | ENT_SUBSTITUTE, 'UTF-8');
        $title = empty($block['title']) ? $image->title : htmlspecialchars($block['title'], ENT_COMPAT | ENT_HTML401 | ENT_SUBSTITUTE, 'UTF-8');
        return $image->imgTag([
            'sizes' => $this->imgSizes,
            'title' => $title,
            'alt' => $text,
            'class' => $this->imgClass,
            'isAmp' => $this->imgIsAmp,
        ]);

//        return '<img src="' . htmlspecialchars($block['url'], ENT_COMPAT | ENT_HTML401, 'UTF-8') . '"'
//        . ' alt="' . htmlspecialchars($block['text'], ENT_COMPAT | ENT_HTML401 | ENT_SUBSTITUTE, 'UTF-8') . '"'
//        . (empty($block['title']) ? '' : ' title="' . htmlspecialchars($block['title'],
//                ENT_COMPAT | ENT_HTML401 | ENT_SUBSTITUTE, 'UTF-8') . '"')
//        . ($this->html5 ? '>' : ' />');
    }

    protected function renderLink($block)
    {
        if (isset($block['refkey'])) {
            if (($ref = $this->lookupReference($block['refkey'])) !== false) {
                $block = array_merge($block, $ref);
            } else {
                return $block['orig'];
            }
        }
        return '<a href="' . htmlspecialchars($block['url'], ENT_COMPAT | ENT_HTML401, 'UTF-8') . '"'
        . (empty($block['title']) ? '' : ' title="' . htmlspecialchars($block['title'], ENT_COMPAT | ENT_HTML401 | ENT_SUBSTITUTE, 'UTF-8') . '"')
        . (empty($this->hrefClass) ? '' : ' class="' . htmlspecialchars($this->hrefClass, ENT_COMPAT | ENT_HTML401 | ENT_SUBSTITUTE, 'UTF-8') . '"')
        . '>' . $this->renderAbsy($block['text']) . '</a>';
    }
}
