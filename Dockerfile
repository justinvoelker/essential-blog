FROM php:7.1-fpm-alpine

ENV NGINX_VERSION 1.10.3

#
# Setup the environment
#
RUN set -xe \
    # Export PHP flags (https://github.com/docker-library/php/issues/105#issuecomment-278114879)
    && export CFLAGS="$PHP_CFLAGS" CPPFLAGS="$PHP_CPPFLAGS" LDFLAGS="$PHP_LDFLAGS" \
    # Add permanent packages (icu for intl extension, imagemagick, nginx, supervisor)
    && apk add --no-cache \
        icu \
        imagemagick \
        # Even thought nginx is built below, installing it first ensures nginx is ready to run, just need to be rebuilt with added modules support.
        nginx \
        supervisor \
    # Add temporary packages ($PHPIZE_DEPS, imagemagick-dev libtool for imagemagick, icu-dev for intl extension)
    && apk add --no-cache --virtual .dependencies-build \
        $PHPIZE_DEPS \
        imagemagick-dev libtool \
        icu-dev \
#        # Memcached
#        zlib-dev libmemcached-dev cyrus-sasl-dev git \
    && apk add --no-cache \
        libpq \
        libmemcached \
        libssl1.1 \
    # Imagemagick needs to be built (https://github.com/docker-library/php/issues/105#issuecomment-287466839)
    && pecl install imagick-3.4.3 \
    && docker-php-ext-enable imagick \
    && docker-php-ext-configure intl --enable-intl \
    && docker-php-ext-configure pdo_mysql --with-pdo-mysql \
    && docker-php-ext-install \
        # for some datetime functions
        intl \
        # database connection
        mysqli \
        # database connection
        pdo_mysql \
        # image processing
        exif \
        # general multi-byte string support, not sure where/if this is truly needed
        mbstring \
#    # Memcached needs to be compiled from source directly
#    && git clone -b php7 https://github.com/php-memcached-dev/php-memcached /usr/src/php/ext/memcached \
#    && docker-php-ext-configure /usr/src/php/ext/memcached --disable-memcached-sasl \
#    && docker-php-ext-install /usr/src/php/ext/memcached \
#    && rm -rf /usr/src/php/ext/memcached \
    #
    # Nginx needs to be built to include ngx_http_realip_module (for detecting real visitor IP inside container)
    # Working example: https://github.com/LoicMahieu/alpine-nginx/blob/master/Dockerfile
    #
    && apk add --no-cache openssl-dev pcre-dev zlib-dev build-base \
    && cd /tmp \
    && wget http://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz \
    && tar -zxvf nginx-${NGINX_VERSION}.tar.gz \
    && cd /tmp/nginx-${NGINX_VERSION} \
    && ./configure --prefix=/var/lib/nginx --sbin-path=/usr/sbin/nginx --conf-path=/etc/nginx/nginx.conf --pid-path=/run/nginx/nginx.pid --lock-path=/run/nginx/nginx.lock --http-client-body-temp-path=/var/lib/nginx/tmp/client_body --http-proxy-temp-path=/var/lib/nginx/tmp/proxy --http-fastcgi-temp-path=/var/lib/nginx/tmp/fastcgi --http-uwsgi-temp-path=/var/lib/nginx/tmp/uwsgi --http-scgi-temp-path=/var/lib/nginx/tmp/scgi --user=nginx --group=nginx --with-ipv6 --with-pcre-jit --with-http_dav_module --with-http_ssl_module --with-http_stub_status_module --with-http_gzip_static_module --with-http_v2_module --with-http_auth_request_module --with-mail --with-mail_ssl_module --with-http_realip_module \
    && make \
    && make install \
    && apk del build-base \
    #
    # Remove build dependencies and clean up
    #
    && apk del .dependencies-build \
    && rm -rf /var/lib/apt/lists/* \
    #
    # Configure PHP
    #
    && mkdir -p /var/log/php \
    && echo "expose_php = Off" >> /usr/local/etc/php/php.ini \
    && echo "max_execution_time = 300" >> /usr/local/etc/php/php.ini \
    && echo "post_max_size = 128M" >> /usr/local/etc/php/php.ini \
    && echo "upload_max_filesize = 128M" >> /usr/local/etc/php/php.ini \
    && echo "log_errors = On" >> /usr/local/etc/php/php.ini \
    && echo "error_log = /var/log/php/error.log" >> /usr/local/etc/php/php.ini \
    #
    # Forward logs to Docker log collector
    #
    && ln -sf /dev/stderr /var/log/php/error.log \
    && ln -sf /dev/stderr /var/log/nginx/error.log

#
# Install website
#

# Copy project into image
COPY . /var/www/html/

# Setup project
RUN set -xe \
    # For pulling archive/composer packages over https
    && apk add --no-cache git \
    # Install composer and asset plugin, install vendor packages, uninstall composer
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer \
    && composer global require "fxp/composer-asset-plugin:^1.4.1" --no-plugins \
    && composer install -d /var/www/html --no-dev --prefer-dist \
    && composer dump-autoload -o \
    && rm /usr/bin/composer && rm -r /root/.composer \
    # Can be removed when aove apk add git is removed
    && apk del git
    # Remove all of the .git directories (no need for them)
    #&& find /var/www/html -type d -name .git -exec rm -rf {} + \

# Copy additional files into image
COPY docker/nginx.conf /etc/nginx/nginx.conf
COPY docker/default.conf /etc/nginx/conf.d/default.conf
COPY docker/supervisord.conf /etc/supervisord.conf
COPY docker/entrypoint.sh /usr/local/bin/entrypoint.sh
RUN chmod +x /usr/local/bin/entrypoint.sh

# Expose port 8 since base php image does not
EXPOSE 80

# Set entrypoint
ENTRYPOINT ["entrypoint.sh"]
