<?php

use yii\db\Migration;

/**
 * Handles the creation for table `user`.
 */
class m161020_012224_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(10)->unsigned(),
            'created_by' => $this->integer(10)->notNull()->unsigned(),
            'created_at' => $this->bigInteger()->notNull(),
            'updated_by' => $this->integer(10)->notNull()->unsigned(),
            'updated_at' => $this->bigInteger()->notNull(),
            'active_at' => $this->bigInteger(),
            'first_name' => $this->string(255),
            'last_name' => $this->string(255),
            'username' => $this->string(255)->unique(),
            'password_hash' => $this->string(64),
            'security_token' => $this->string(64)->unique(),
            'email' => $this->string(255)->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'status' => $this->integer(10)->unsigned()->notNull(),
            'short_biography' => $this->text(),
            'profile_photo' => $this->string(255),
            'two_factor_authentication' => $this->text(),
            'FOREIGN KEY (created_by) REFERENCES {{%user}} (id) ON DELETE RESTRICT ON UPDATE CASCADE',
            'FOREIGN KEY (updated_by) REFERENCES {{%user}} (id) ON DELETE RESTRICT ON UPDATE CASCADE',
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
