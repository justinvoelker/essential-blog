<?php

use yii\db\Migration;

/**
 * Handles the creation for table `page_tag`.
 */
class m170710_012317_create_page_tag_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%page_tag}}', [
            'page_id' => $this->integer(10)->notNull()->unsigned(),
            'tag_id' => $this->integer(10)->notNull()->unsigned(),
            'PRIMARY KEY (page_id,tag_id)',
            'FOREIGN KEY (page_id) REFERENCES {{%page}} (id) ON DELETE RESTRICT ON UPDATE CASCADE',
            'FOREIGN KEY (tag_id) REFERENCES {{%tag}} (id) ON DELETE RESTRICT ON UPDATE CASCADE',
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%page_tag}}');
    }
}
