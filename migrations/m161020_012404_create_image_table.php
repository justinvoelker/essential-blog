<?php

use yii\db\Migration;

/**
 * Handles the creation for table `image`.
 */
class m161020_012404_create_image_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%image}}', [
            'id' => $this->primaryKey(10)->unsigned(),
            'created_by' => $this->integer(10)->notNull()->unsigned(),
            'created_at' => $this->bigInteger()->notNull(),
            'updated_by' => $this->integer(10)->notNull()->unsigned(),
            'updated_at' => $this->bigInteger()->notNull(),
            'alternative_text' => $this->string(255)->notNull(),
            'title' => $this->string(255),
            'path' => $this->string(255)->notNull(),
            'FOREIGN KEY (created_by) REFERENCES {{%user}} (id) ON DELETE RESTRICT ON UPDATE CASCADE',
            'FOREIGN KEY (updated_by) REFERENCES {{%user}} (id) ON DELETE RESTRICT ON UPDATE CASCADE',
        ], $tableOptions);

        /* Creates index for `path` column */
        $this->createIndex(
            'image_ix_path',
            '{{%image}}',
            'path'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%image}}');
    }
}
