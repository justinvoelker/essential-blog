<?php

use yii\db\Migration;

/**
 * Handles the creation for table `page`.
 */
class m170710_012311_create_page_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%page}}', [
            'id' => $this->primaryKey(10)->unsigned(),
            'created_by' => $this->integer(10)->notNull()->unsigned(),
            'created_at' => $this->bigInteger()->notNull(),
            'updated_by' => $this->integer(10)->notNull()->unsigned(),
            'updated_at' => $this->bigInteger()->notNull(),
            'published_by' => $this->integer(10)->unsigned(),
            'published_at' => $this->bigInteger(),
            'revised_by' => $this->integer(10)->unsigned(),
            'revised_at' => $this->bigInteger(),
            'title' => $this->string(255),
            'slug' => $this->string(255),
            'excerpt' => $this->text(),
            'content' => 'mediumtext',
            'scheduled_for' => $this->bigInteger(),
            'category_id' => $this->integer(10)->unsigned(),
            'image' => $this->string(255),
            'status' => $this->integer(10)->unsigned(),
            'primary_id' => $this->integer(10)->unsigned(),
            'is_locked' => $this->integer(1)->unsigned(),
            'FOREIGN KEY (created_by) REFERENCES {{%user}} (id) ON DELETE RESTRICT ON UPDATE CASCADE',
            'FOREIGN KEY (updated_by) REFERENCES {{%user}} (id) ON DELETE RESTRICT ON UPDATE CASCADE',
            'FOREIGN KEY (published_by) REFERENCES {{%user}} (id) ON DELETE RESTRICT ON UPDATE CASCADE',
            'FOREIGN KEY (revised_by) REFERENCES {{%user}} (id) ON DELETE RESTRICT ON UPDATE CASCADE',
            'FOREIGN KEY (category_id) REFERENCES {{%category}} (id) ON DELETE RESTRICT ON UPDATE CASCADE',
        ], $tableOptions);

        /* Creates index for `slug` column */
        $this->createIndex(
            'page_ix_slug',
            '{{%page}}',
            'slug'
        );

        /* Creates index for `published_at` column */
        $this->createIndex(
            'page_ix_published_at',
            '{{%page}}',
            'published_at'
        );

        /* Creates index for `scheduled_for` column */
        $this->createIndex(
            'page_ix_scheduled_for',
            '{{%page}}',
            'scheduled_for'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%page}}');
    }
}
