<?php

use yii\base\InvalidConfigException;
use yii\rbac\DbManager;

class m160321_184433_populate_rbac extends \yii\db\Migration
{
    /**
     * @throws yii\base\InvalidConfigException
     * @return DbManager
     */
    protected function getAuthManager()
    {
        $authManager = Yii::$app->getAuthManager();
        if (!$authManager instanceof DbManager) {
            throw new InvalidConfigException('You should configure "authManager" component to use database before executing this migration.');
        }

        return $authManager;
    }

    public function up()
    {
        $auth = $this->getAuthManager();
        $this->db = $auth->db;

        /*
         * Rules
         */

        /* Add "Owner" rule */
        $ownerRule = new \app\rbac\OwnerRule;
        $auth->add($ownerRule);
        /* Add "Self" rule */
        $selfRule = new \app\rbac\SelfRule;
        $auth->add($selfRule);
        /* Add "Not Self" rule */
        $selfRuleFalse = new \app\rbac\SelfRuleFalse;
        $auth->add($selfRuleFalse);

        /*
         * Permissions
         */

        /* Post */

        /* Add "postCreate" permission */
        $postCreate = $auth->createPermission('postCreate');
        $postCreate->description = 'Create post';
        $auth->add($postCreate);

        /* Add "postUpdate" permission */
        $postUpdate = $auth->createPermission('postUpdate');
        $postUpdate->description = 'Update post';
        $auth->add($postUpdate);

        /* Add "postDelete" permission */
        $postDelete = $auth->createPermission('postDelete');
        $postDelete->description = 'Delete post';
        $auth->add($postDelete);

        /* Add "postPublish" permission */
        $postPublish = $auth->createPermission('postPublish');
        $postPublish->description = 'Publish post';
        $auth->add($postPublish);

        /* Add "postUpdateOwn" permission */
        $postUpdateOwn = $auth->createPermission('postUpdateOwn');
        $postUpdateOwn->description = 'Update own post';
        $postUpdateOwn->ruleName = $ownerRule->name;
        $auth->add($postUpdateOwn);

        /* Add "postDeleteOwn" permission */
        $postDeleteOwn = $auth->createPermission('postDeleteOwn');
        $postDeleteOwn->description = 'Delete own post';
        $postDeleteOwn->ruleName = $ownerRule->name;
        $auth->add($postDeleteOwn);

        /* Add "postPublishOwn" permission */
        $postPublishOwn = $auth->createPermission('postPublishOwn');
        $postPublishOwn->description = 'Publish own post';
        $postPublishOwn->ruleName = $ownerRule->name;
        $auth->add($postPublishOwn);

        /* The "actionOwn" permissions will be used from "action" permissions */
        $auth->addChild($postUpdateOwn, $postUpdate);
        $auth->addChild($postDeleteOwn, $postDelete);
        $auth->addChild($postPublishOwn, $postPublish);

        /* Page */

        /* Add "pageCreate" permission */
        $pageCreate = $auth->createPermission('pageCreate');
        $pageCreate->description = 'Create page';
        $auth->add($pageCreate);

        /* Add "pageUpdate" permission */
        $pageUpdate = $auth->createPermission('pageUpdate');
        $pageUpdate->description = 'Update page';
        $auth->add($pageUpdate);

        /* Add "pageDelete" permission */
        $pageDelete = $auth->createPermission('pageDelete');
        $pageDelete->description = 'Delete page';
        $auth->add($pageDelete);

        /* Add "pagePublish" permission */
        $pagePublish = $auth->createPermission('pagePublish');
        $pagePublish->description = 'Publish page';
        $auth->add($pagePublish);

        /* Add "pageUpdateOwn" permission */
        $pageUpdateOwn = $auth->createPermission('pageUpdateOwn');
        $pageUpdateOwn->description = 'Update own page';
        $pageUpdateOwn->ruleName = $ownerRule->name;
        $auth->add($pageUpdateOwn);

        /* Add "pageDeleteOwn" permission */
        $pageDeleteOwn = $auth->createPermission('pageDeleteOwn');
        $pageDeleteOwn->description = 'Delete own page';
        $pageDeleteOwn->ruleName = $ownerRule->name;
        $auth->add($pageDeleteOwn);

        /* Add "pagePublishOwn" permission */
        $pagePublishOwn = $auth->createPermission('pagePublishOwn');
        $pagePublishOwn->description = 'Publish own page';
        $pagePublishOwn->ruleName = $ownerRule->name;
        $auth->add($pagePublishOwn);

        /* The "actionOwn" permissions will be used from "action" permissions */
        $auth->addChild($pageUpdateOwn, $pageUpdate);
        $auth->addChild($pageDeleteOwn, $pageDelete);
        $auth->addChild($pagePublishOwn, $pagePublish);

        /* Tag */

        /* Add "tagCreate" permission */
        $tagCreate = $auth->createPermission('tagCreate');
        $tagCreate->description = 'Create tag';
        $auth->add($tagCreate);

        /* Add "tagUpdate" permission */
        $tagUpdate = $auth->createPermission('tagUpdate');
        $tagUpdate->description = 'Update tag';
        $auth->add($tagUpdate);

        /* Add "tagDelete" permission */
        $tagDelete = $auth->createPermission('tagDelete');
        $tagDelete->description = 'Delete tag';
        $auth->add($tagDelete);

        /* Add "tagUpdateOwn" permission */
        $tagUpdateOwn = $auth->createPermission('tagUpdateOwn');
        $tagUpdateOwn->description = 'Update own tag';
        $tagUpdateOwn->ruleName = $ownerRule->name;
        $auth->add($tagUpdateOwn);

        /* Add "tagDeleteOwn" permission */
        $tagDeleteOwn = $auth->createPermission('tagDeleteOwn');
        $tagDeleteOwn->description = 'Delete own tag';
        $tagDeleteOwn->ruleName = $ownerRule->name;
        $auth->add($tagDeleteOwn);

        /* The "actionOwn" permissions will be used from "action" permissions */
        $auth->addChild($tagUpdateOwn, $tagUpdate);
        $auth->addChild($tagDeleteOwn, $tagDelete);

        /* Category */

        /* Add "categoryCreate" permission */
        $categoryCreate = $auth->createPermission('categoryCreate');
        $categoryCreate->description = 'Create category';
        $auth->add($categoryCreate);

        /* Add "categoryUpdate" permission */
        $categoryUpdate = $auth->createPermission('categoryUpdate');
        $categoryUpdate->description = 'Update category';
        $auth->add($categoryUpdate);

        /* Add "categoryDelete" permission */
        $categoryDelete = $auth->createPermission('categoryDelete');
        $categoryDelete->description = 'Delete category';
        $auth->add($categoryDelete);

        /* Image */

        /* Add "imageCreate" permission */
        $imageCreate = $auth->createPermission('imageCreate');
        $imageCreate->description = 'Create image';
        $auth->add($imageCreate);

        /* Add "imageUpdate" permission */
        $imageUpdate = $auth->createPermission('imageUpdate');
        $imageUpdate->description = 'Update image';
        $auth->add($imageUpdate);

        /* Add "imageDelete" permission */
        $imageDelete = $auth->createPermission('imageDelete');
        $imageDelete->description = 'Delete image';
        $auth->add($imageDelete);

        /* Add "imageUpdateOwn" permission */
        $imageUpdateOwn = $auth->createPermission('imageUpdateOwn');
        $imageUpdateOwn->description = 'Update own image';
        $imageUpdateOwn->ruleName = $ownerRule->name;
        $auth->add($imageUpdateOwn);

        /* Add "imageDeleteOwn" permission */
        $imageDeleteOwn = $auth->createPermission('imageDeleteOwn');
        $imageDeleteOwn->description = 'Delete own image';
        $imageDeleteOwn->ruleName = $ownerRule->name;
        $auth->add($imageDeleteOwn);

        /* The "actionOwn" permissions will be used from "action" permissions */
        $auth->addChild($imageUpdateOwn, $imageUpdate);
        $auth->addChild($imageDeleteOwn, $imageDelete);

        /* Injection */

        /* Add "injectionCreate" permission */
        $injectionCreate = $auth->createPermission('injectionCreate');
        $injectionCreate->description = 'Create injection';
        $auth->add($injectionCreate);

        /* Add "injectionUpdate" permission */
        $injectionUpdate = $auth->createPermission('injectionUpdate');
        $injectionUpdate->description = 'Update injection';
        $auth->add($injectionUpdate);

        /* Add "injectionDelete" permission */
        $injectionDelete = $auth->createPermission('injectionDelete');
        $injectionDelete->description = 'Delete injection';
        $auth->add($injectionDelete);

        /* User */

        /* Add "userCreate" permission */
        $userCreate = $auth->createPermission('userCreate');
        $userCreate->description = 'Create user';
        $auth->add($userCreate);

        /* Add "userUpdate" permission */
        $userUpdate = $auth->createPermission('userUpdate');
        $userUpdate->description = 'Update user';
        $auth->add($userUpdate);

        /* Add "userDelete" permission */
        $userDelete = $auth->createPermission('userDelete');
        $userDelete->description = 'Delete user';
        $auth->add($userDelete);

        /* Add "userUpdateOwn" permission */
        $userUpdateOwn = $auth->createPermission('userUpdateOwn');
        $userUpdateOwn->description = 'Update own user';
        $userUpdateOwn->ruleName = $selfRule->name;
        $auth->add($userUpdateOwn);

        /* Add "userDeleteOwn" permission */
        $userDeleteOwn = $auth->createPermission('userDeleteOwn');
        $userDeleteOwn->description = 'Delete own user';
        $userDeleteOwn->ruleName = $selfRule->name;
        $auth->add($userDeleteOwn);

        /* The "actionOwn" permissions will be used from "action" permissions */
        $auth->addChild($userUpdateOwn, $userUpdate);
        $auth->addChild($userDeleteOwn, $userDelete);

        /* Setting */

        /* Add "settingUpdate" permission */
        $settingUpdate = $auth->createPermission('settingUpdate');
        $settingUpdate->description = 'Update setting';
        $auth->add($settingUpdate);

        /*
         * Roles
         */

        /*
         * Contributor
         * Create, update, delete own Posts (but cannot publish)
         * Create, update, delete own Pages (but cannot publish)
         * Create, update, delete own Tags
         * Create, update, delete own Images
         * Update, delete own User
         */

        /* Add "contributor" role */
        $contributor = $auth->createRole('contributor');
        $contributor->description = "Contributor";
        $auth->add($contributor);

        /* Post */
        $auth->addChild($contributor, $postCreate);
        $auth->addChild($contributor, $postUpdateOwn);
        $auth->addChild($contributor, $postDeleteOwn);
        /* Page */
        $auth->addChild($contributor, $pageCreate);
        $auth->addChild($contributor, $pageUpdateOwn);
        $auth->addChild($contributor, $pageDeleteOwn);
        /* Tag */
        $auth->addChild($contributor, $tagCreate);
        $auth->addChild($contributor, $tagUpdateOwn);
        $auth->addChild($contributor, $tagDeleteOwn);
        /* Image */
        $auth->addChild($contributor, $imageCreate);
        $auth->addChild($contributor, $imageUpdateOwn);
        $auth->addChild($contributor, $imageDeleteOwn);
        /* User */
        $auth->addChild($contributor, $userUpdateOwn);
        $auth->addChild($contributor, $userDeleteOwn);

        /*
         * Author
         * Contributor permissions
         * Publish own posts
         * Publish own pages
         */

        /* Add "author" role */
        $author = $auth->createRole('author');
        $author->description = "Author";
        $auth->add($author);

        /* Add "author" as parent of "contributor" (author can do everything contributor can do and more) */
        $auth->addChild($author, $contributor);
        /* Post */
        $auth->addChild($author, $postPublishOwn);
        /* Page */
        $auth->addChild($author, $pagePublishOwn);

        /*
         * Editor
         * Author permissions
         * Update, delete, publish all Posts
         * Update, delete, publish all Pages
         * Update, delete all Tags
         * Update, delete all Categories
         * Update, delete all Images
         */

        /* Add "editor" role */
        $editor = $auth->createRole('editor');
        $editor->description = "Editor";
        $auth->add($editor);

        /* Add "editor" as parent of "author" (editor can do everything author can do and more) */
        $auth->addChild($editor, $author);
        /* Post */
        $auth->addChild($editor, $postUpdate);
        $auth->addChild($editor, $postDelete);
        $auth->addChild($editor, $postPublish);
        /* Page */
        $auth->addChild($editor, $pageUpdate);
        $auth->addChild($editor, $pageDelete);
        $auth->addChild($editor, $pagePublish);
        /* Tag */
        $auth->addChild($editor, $tagUpdate);
        $auth->addChild($editor, $tagDelete);
        /* Category */
        $auth->addChild($editor, $categoryCreate);
        $auth->addChild($editor, $categoryUpdate);
        $auth->addChild($editor, $categoryDelete);
        /* Image */
        $auth->addChild($editor, $imageUpdate);
        $auth->addChild($editor, $imageDelete);
        /* Injection */
        $auth->addChild($editor, $injectionCreate);
        $auth->addChild($editor, $injectionUpdate);
        $auth->addChild($editor, $injectionDelete);

        /*
         * Administrator
         * Editor permissions
         * Create, update, delete all Users
         * Update Settings
         */

        /* Add "administrator" role */
        $administrator = $auth->createRole('administrator');
        $administrator->description = "Administrator";
        $auth->add($administrator);

        /* Add "administrator" as parent of "editor" (administrator can do everything editor can do and more) */
        $auth->addChild($administrator, $editor);
        /* User */
        $auth->addChild($administrator, $userCreate);
        $auth->addChild($administrator, $userUpdate);
        $auth->addChild($administrator, $userDelete);
        /* Setting */
        $auth->addChild($administrator, $settingUpdate);
    }

    public function down()
    {
        $authManager = $this->getAuthManager();
        $this->db = $authManager->db;

        $this->execute("SET foreign_key_checks = 0;");

        $this->truncateTable($authManager->assignmentTable);
        $this->truncateTable($authManager->itemChildTable);
        $this->truncateTable($authManager->itemTable);
        $this->truncateTable($authManager->ruleTable);
    }
}
