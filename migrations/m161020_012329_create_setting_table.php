<?php

use yii\db\Migration;

/**
 * Handles the creation for table `setting`.
 */
class m161020_012329_create_setting_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%setting}}', [
            'id' => $this->string(255)->notNull(),
            'value' => $this->text(),
            'PRIMARY KEY (id)',
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->truncateTable('{{%setting}}');
        $this->dropTable('{{%setting}}');
    }
}
