<?php

use yii\db\Migration;

/**
 * Handles the creation for table `user_auth_collection`.
 */
class m161020_012231_create_user_auth_connection_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_auth_connection}}', [
            'id' => $this->string(64)->notNull(),
            'user_id' => $this->integer(10)->unsigned()->notNull(),
            'connected_at' => $this->bigInteger()->notNull(),
            'PRIMARY KEY (id)',
            'FOREIGN KEY (user_id) REFERENCES {{%user}} (id) ON DELETE CASCADE ON UPDATE CASCADE',
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%user_auth_connection}}');
    }
}
