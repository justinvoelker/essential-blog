<?php

use yii\db\Migration;

/**
 * Handles the creation for table `category`.
 */
class m161020_012357_create_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%category}}', [
            'id' => $this->primaryKey(10)->unsigned(),
            'created_by' => $this->integer(10)->notNull()->unsigned(),
            'created_at' => $this->bigInteger()->notNull(),
            'updated_by' => $this->integer(10)->notNull()->unsigned(),
            'updated_at' => $this->bigInteger()->notNull(),
            'name' => $this->string(255)->notNull()->unique(),
            'slug' => $this->string(255)->notNull()->unique(),
            'image' => $this->string(255),
            'description' => $this->text(),
            'FOREIGN KEY (created_by) REFERENCES {{%user}} (id) ON DELETE RESTRICT ON UPDATE CASCADE',
            'FOREIGN KEY (updated_by) REFERENCES {{%user}} (id) ON DELETE RESTRICT ON UPDATE CASCADE',
        ], $tableOptions);

        /* Creates index for `slug` column */
        $this->createIndex(
            'category_ix_slug',
            '{{%category}}',
            'slug'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%category}}');
    }
}
