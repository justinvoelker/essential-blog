<?php

use yii\db\Migration;

/**
 * Handles the creation for table `injection`.
 */
class m170510_225149_create_injection_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%injection}}', [
            'id' => $this->primaryKey(10)->unsigned(),
            'created_by' => $this->integer(10)->notNull()->unsigned(),
            'created_at' => $this->bigInteger()->notNull(),
            'updated_by' => $this->integer(10)->notNull()->unsigned(),
            'updated_at' => $this->bigInteger()->notNull(),
            'name' => $this->string(255)->notNull(),
            'xpath' => $this->string(255)->notNull(),
            'action' => $this->integer(10)->unsigned()->notNull(),
            'content' => $this->text(),
            'parser' => $this->integer(10)->unsigned()->notNull(),
            'order' => $this->integer(10)->unsigned(),
            'status' => $this->integer(10)->unsigned()->notNull(),
            'FOREIGN KEY (created_by) REFERENCES {{%user}} (id) ON DELETE RESTRICT ON UPDATE CASCADE',
            'FOREIGN KEY (updated_by) REFERENCES {{%user}} (id) ON DELETE RESTRICT ON UPDATE CASCADE',
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%injection}}');
    }
}
