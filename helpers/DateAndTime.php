<?php

namespace app\helpers;


class DateAndTime
{
    /**
     * Return unix timestamp value from either a timestamp or a date time string.
     *
     * @param mixed $value
     * @param string $format
     * @return string
     */
    public static function toTimestamp($value, $format = 'Y-m-d H:i')
    {
        /* If the value contains a hyphen, it must be a formatted datetime string. */
        if (strpos($value, '-')) {
            /* Create DateTime object from the format that should be used. */
            $date = \DateTime::createFromFormat($format, $value);
            /* Format as seconds since Unix Epoch */
            $value = $date->format('U');
        }

        return $value;
    }
}
