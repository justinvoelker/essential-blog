<?php

Yii::setAlias('@tests', dirname(__DIR__) . '/tests/codeception');
Yii::setAlias('@webroot', dirname(__DIR__) . '/web');

/* Include custom params */
$params = require(__DIR__ . '/params.php');

/* Pull database connection params */
$dbhost = $params['databaseHost'];
$dbname = $params['databaseName'];
$dbuser = $params['databaseUser'];
$dbpass = $params['databasePass'];
$dbprfx = $params['databasePrfx'];
/* Remove database connection params just for ease of mind */
unset(
    $params['databaseHost'],
    $params['databaseName'],
    $params['databaseUser'],
    $params['databasePass'],
    $params['databasePrfx']
);

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
        'app\modules\base\models\Setting',
    ],
    'controllerNamespace' => 'app\commands',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host='.$dbhost.';dbname='.$dbname,
            'username' => $dbuser,
            'password' => $dbpass,
            'tablePrefix' => $dbprfx,
        ],
    ],
    'params' => $params,
    /*
    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
    ],
    */
];

return $config;
