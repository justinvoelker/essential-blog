<?php

return [

    /* Database configuration */
    'databaseHost' => 'database_host_here',
    'databaseName' => 'database_name_here',
    'databaseUser' => 'database_username_here',
    'databasePass' => 'database_password_here',
    'databasePrfx' => 'database_table_prefix_here',

    /* Allows the application to tell if a cookie has been modified on the client-side */
    'cookieValidationKey' => 'cookie_validation_key',

    /* Encryption key for encrypted database settings */
    'encryptionKey' => 'encryption_key_here',

];
