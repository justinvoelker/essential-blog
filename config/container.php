<?php

Yii::$container->set('yii\grid\GridView', [
    'layout' => "<div class='table__wrap'>{items}</div>\n{summary}\n{pager}",
    'tableOptions' => [
        'class' => 'table'
    ],
]);

Yii::$container->set('yii\widgets\DetailView', [
    'options' => [
        'class' => 'table'
    ],
]);
