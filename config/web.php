<?php

use yii\web\UrlNormalizer;

require __DIR__ . '/container.php';

/* Include custom params */
$params = require(__DIR__ . '/params.php');

/* Pull database connection params */
$dbhost = $params['databaseHost'];
$dbname = $params['databaseName'];
$dbuser = $params['databaseUser'];
$dbpass = $params['databasePass'];
$dbprfx = $params['databasePrfx'];
$cookieValidationKey = $params['cookieValidationKey'];
/* Remove not-needed-again params just for ease of mind */
unset(
    $params['databaseHost'],
    $params['databaseName'],
    $params['databaseUser'],
    $params['databasePass'],
    $params['databasePrfx'],
    $params['cookieValidationKey']
);

$config = [
    'id' => 'publish',
    'basePath' => dirname(__DIR__),
    'homeUrl'=> '/',
    'bootstrap' => [
        'log',
        'app\modules\base\models\Setting',
    ],
    'components' => [
        'request' => [
            'baseUrl' => '/',
            'cookieValidationKey' => $cookieValidationKey,
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\modules\base\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['/admin/default/login'],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'errorHandler' => [
            'errorAction' => 'frontend/site/error',
        ],
        /* 'mailer' is be added by the 'Setting' bootstrap */
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host='.$dbhost.';dbname='.$dbname,
            'username' => $dbuser,
            'password' => $dbpass,
            'tablePrefix' => $dbprfx,
            'enableSchemaCache' => true,
            'schemaCacheDuration' => 0,
            'schemaCache' => 'cache',
        ],
        'urlManager' => [
            'baseUrl' => '/',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'suffix' => '', /* Nothing after the end of the route, not even a trailing slash */
            'normalizer' => [
                'class' => 'yii\web\UrlNormalizer',
                'action' => UrlNormalizer::ACTION_REDIRECT_TEMPORARY, /* use temporary redirection instead of permanent */
            ],
            'rules' => [
                /*
                 * Admin interface rules
                 * Grouped as rules with a prefix of "admin" so that none of these rules are evaluated unless the URL
                 * begins with "/admin" which will only ever be the admin interface.
                 */
                [
                    'class' => 'yii\web\GroupUrlRule',
                    'prefix' => 'admin',
                    'rules' => [
                        /*
                         * Admin interface index page
                         * This forces "/admin" to the admin interface, rather than possibly directing it to the
                         * frontend in the case of a tag/category/etc having a URL structure of "/%slug%" with an
                         * actual slug of "admin"
                         */
                        '/' => '/',
                        /* Help pages */
                        '<controller:help>/<view:.*>' => '<controller>',
                        /* Setting pages */
                        '<controller:setting>/<group:(blog|admin)>' => '<controller>/index',
                        /* Index, install, login, logout, register, password reset, and error actions */
                        '<action:(index|install|login|logout|register|password-reset-request|password-reset|error)>' => 'default/<action>',
                        /* Standard model actions */
                        '<controller:(post|page|image|tag|category|injection|user)>' => '<controller>/index',
                        '<controller:(post|page|image|tag|category|injection|user)>/<action:(view|update|edit)>/<id:\d+>' => '<controller>/<action>',
                        '<controller:(post|page|image|tag|category|injection|user)>/<action:(delete)>/<ids:\d+(,\d+)*>' => '<controller>/<action>',
                        /* Unique model actions */
                        '<controller:(injection)>/<action:(update-status)>/<ids:\d+>' => '<controller>/<action>',
                        '<controller:(image)>/<action:(delete-variation|recreate-variations)>/<id:\d+>' => '<controller>/<action>',
                        /* Auth Connection actions */
                        'auth-connection-login/<authclient:(google|facebook)>' => 'default/auth-connection-login',
                        'user/auth-connection-connect/<authclient:(google|facebook)>' => 'user/auth-connection-connect',
                        'user/auth-connection-disconnect/<authclient:(google|facebook)>' => 'user/auth-connection-disconnect',
                        /*
                         * Default to catch all other admin controller/action combinations not explicitly identified
                         * above. This is pages such as tag/create post/generate-or-validate-slug
                         */
                        '<controller>/<action>' => '<controller>/<action>',
                    ],
                ],
                /*
                 * Frontend rules
                 * Few rules are actually set below. The majority of rules are added via the Setting bootstrap based on
                 * settings that are user configurable.
                 */
                /* Index page */
                '/' => 'frontend/site/index',
                /* Archive pages */
                '/<year:\d{4}>/<month:\d{2}>/<day:\d{2}>' => 'frontend/site/archive',
                '/<year:\d{4}>/<month:\d{2}>' => 'frontend/site/archive',
                '/<year:\d{4}>' => 'frontend/site/archive',
                /* Post page : Added by modules\base\models\Setting */
                /* Page page : Added by modules\base\models\Setting */
                /* Tag page : Added by modules\base\models\Setting */
                /* Category page : Added by modules\base\models\Setting */
            ],
        ],
        /* 'authClientCollection' is be added by the 'Setting' bootstrap */
        'assetManager' => [
            'appendTimestamp' => true,
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'sourcePath' => null,   // do not publish the bundle
                    'js' => [
                        '//ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js',
                    ]
                ],
                /* Completely disable Bootstrap Plugin Asset */
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'css' => [],
                    'js'=>[]
                ],
                /* Completely disable Bootstrap Asset */
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [],
                    'js'=>[]
                ],
            ],
        ],
    ],
    'modules' => [
        'admin' => [
            'class' => 'app\modules\backend\Module',
            'layout' => 'main',
            'defaultRoute' => 'post',
        ],
        'frontend' => [
            'class' => 'app\modules\frontend\Module',
            'layout' => 'main',
            'defaultRoute' => 'site',
        ],
    ],
    'params' => $params,
];

// configuration adjustments for 'dev' environment
if (YII_ENV_DEV) {
    $config['components']['db']['enableSchemaCache'] = false;
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['127.0.0.1', '::1', '10.*.*.*', '192.168.*.*', '172.*.*.*'],
    ];
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['127.0.0.1', '::1', '10.*.*.*', '192.168.*.*', '172.*.*.*'],
    ];
}

return $config;
