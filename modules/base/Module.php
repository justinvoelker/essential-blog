<?php

namespace app\modules\base;

use yii\base\Module as YiiBaseModule;

/**
 * Base module definition class
 *
 * Models: The Base module contains only the most basic model definitions for database interaction. Only model
 * properties and methods that represent actual database fields, calculated database fields, or database relationships
 * are included in the Base module's models.
 *
 * Views: None.
 *
 * Controllers: None.
 */
class Module extends YiiBaseModule
{
//    /**
//     * @inheritdoc
//     */
//    public $controllerNamespace = 'app\modules\base\controllers';
//
//    /**
//     * @inheritdoc
//     */
//    public function init()
//    {
//        parent::init();
//
//        // custom initialization code goes here
//    }
}
