<?php

namespace app\modules\base\models;

use app\helpers\DateAndTime;
use yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $two_factor_code;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
            /* two_factor_code required only when two-factor is enabled and username and password are already valid */
            [['two_factor_code'], 'required', 'when' => function($model) {
                /* @var $model LoginForm */
                $user = $model->getUser();
                return $user && $user->twoFactorEnabled() && !$model->hasErrors();
            }],
            /* two_factor_code is validated by validateTwoFactorCode() */
            ['two_factor_code', 'validateTwoFactorCode'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, /** @noinspection PhpUnusedParameterInspection */ $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Validates the two-factor authentication code.
     * This method serves as the inline validation for two_factor_code.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateTwoFactorCode($attribute, /** @noinspection PhpUnusedParameterInspection */ $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            /* validatePassword will ensure user exists. This only needs to be concerned about the two-factor code. */
            if ($user && $user->twoFactorEnabled() && !$user->validateTwoFactorCode($this->two_factor_code)) {
                $this->addError($attribute, 'Incorrect two-factor authentication code.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {

            /* Get the user */
            $user = $this->getUser();

            /* Send loginNotification email */
            $user->sendLoginNotificationEmail();

            /* Login the user */
            $user_session_duration = Setting::getSetting('user_session_duration');
            return Yii::$app->user->login($user, $user_session_duration);
        }

        return false;
    }

    /**
     * Finds user by [[username]] or email address
     *
     * @return User|null
     */
    public function getUser()
    {
        /* _user property is set to false by default. */
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        /* By this check, _user will either have a user or will be null if that username was not found. */
        if ($this->_user === null) {
            $this->_user = User::findByEmail($this->username);
        }

        return $this->_user;
    }
}
