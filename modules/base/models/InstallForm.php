<?php

namespace app\modules\base\models;

use yii;
use yii\base\Model;

/**
 * InstallForm is the model behind the install form.
 */
class InstallForm extends Model
{

    public $blogTitle = 'My New Blog';
    public $blogTagline = 'Blogging: Never before have so many people with so little to say said so much to so few.';

    public $firstName;
    public $lastName;
    public $emailAddress;

    public $username;
    public $password;

    public $supportEmailAddress;
    public $timeZone = 'UTC';

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            /* Required fields corresponding to settings */
            [['blogTitle', 'blogTagline', 'supportEmailAddress', 'timeZone'], 'required'],
            /* Required fields corresponding to user account */
            [['firstName', 'lastName', 'emailAddress', 'username', 'password'], 'required'],
            /* Email addresses */
            [['supportEmailAddress', 'emailAddress'], 'email'],
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function updateSettings()
    {

        if ($this->validate()) {

            /* Update settings */
            $settingForm = new SettingForm();
            $settingForm->blog_title = $this->blogTitle;
            $settingForm->blog_tagline = $this->blogTagline;
            $settingForm->support_email_address = $this->supportEmailAddress;
            $settingForm->time_zone = $this->timeZone;
            $results[] = $settingForm->validate() && $settingForm->saveSettings();

            /* Create user */
            $results[] = $this->createUser();

            /* If false is found in results list, result is false */
            $result = (in_array(false, $results, true)) ? false : true;

            /* Add false message if any prior steps returned false */
            if ($result === false) {
                Yii::$app->getSession()->addFlash('danger', 'Install has encountered an error. Please try again.');
                /* Ensure the user table is empty so install can be executed again */
                $results[] = User::deleteAll();
            } else {
                Yii::$app->getSession()->addFlash('success', 'Installation successful. Please log in.');
            }

            /* Refresh setting cache */
            Setting::refreshCache();

            return $result;
        } else {
            return false;
        }
    }

    public function createUser()
    {
        /* New user with required data */
        $model = new User();
        $model->generateAuthKey();
        $model->status = User::STATUS_ENABLED;

        // Detach TimestampBehavior so created_by and updated_at can be forcibly set
        $model->detachBehavior('BlameableBehavior');
        /* User data from install params */
        $model->created_by = '1';
        $model->created_at = time();
        $model->updated_by = '1';
        $model->updated_at = time();
        $model->first_name = $this->firstName;
        $model->last_name = $this->lastName;
        $model->username = $this->username;
        $model->password = $this->password;
        $model->email = $this->emailAddress;

        /* If the user successfully saves, add auth assignment*/
        if ($model->save(false)) {
            $auth = Yii::$app->getAuthManager();
            $auth->assign($auth->createRole('administrator'), $model->id);
        } else {
            return false;
        }

        return true;
    }
}
