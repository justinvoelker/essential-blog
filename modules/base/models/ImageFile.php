<?php

namespace app\modules\base\models;

use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;

/**
 *
 */
class ImageFile extends BaseImageFile
{
    /* Image types that allow the automatic creation of image variations */
    const IMAGE_TYPE_ALLOW_VARIATION_CREATION = [IMAGETYPE_JPEG];

    /**
     * List of all image variation paths based on the path of an image.
     *
     * @return array
     */
    public function variationPaths()
    {
        /*
         * Pattern to find image variations. Pattern is base name of original file, a hyphen, 2 to 5 digits, the
         * letter w, a period, then the extension
         */
        $pattern = preg_quote($this->baseName) . '\-((\d{2,5})w)\.' . $this->extension;

        /*
         * Loop through each file in the same directory as the original image, creating a list of all images paths
         * that match the pattern for image variations.
         */
        $images = [];
        $files = scandir($this->absoluteDirectory);
        foreach ($files as $file) {
            /* If the pattern matches (indicating the file is an image variation ...  */
            if (preg_match('/' . $pattern . '/', $file)) {
                /* ... save the image path for return. */
                $images[] = $this->directory . '/' . $file;
            }
        }

        return $images;
    }

    /**
     * List of all image variation ImageFiles based on the path of an image.
     *
     * @return ImageFile[]
     */
    public function variationImageFiles()
    {
        /* Loop through each variationPath, creating a list of ImageFile instances. */
        $imageFiles = [];
        foreach ($this->variationPaths() as $variationPath) {
            $imageFiles[] = new ImageFile($variationPath);
        }

        return $imageFiles;
    }

    /**
     * Upload an image to the file system, creating the parent directory if necessary.
     *
     * @param \yii\web\UploadedFile $uploadedFile
     * @return boolean
     */
    public function upload($uploadedFile)
    {
        /*
         * If absolute directory does not exist, try creating it, logging and error and returing false the create fails.
         *
         * Directories created by web process will be owned by web process and thus 755 allows write.
         */
        if (!file_exists($this->absoluteDirectory)) {
            try {
                FileHelper::createDirectory($this->absoluteDirectory, 0755, true);
            } catch (\Exception $e) {
                \Yii::error('Failed to create directory. ' . $e);

                return false;
            }
        }

        /*
         * Try saving the file but log an error and return false if something prevents the save.
         */
        try {
            $uploadedFile->saveAs($this->absolutePath);
        } catch (\Exception $e) {
            \Yii::error('Failed to save file. ' . $e);

            return false;
        }

        return true;
    }

    /**
     * Create multiple variations of an image for use as responsive images. Only allowed for certain image types.
     *
     * @return boolean
     */
    public function createVariations()
    {
        /* Only certain images image types are allowed to have variations automatically created */
        if (in_array($this->imageType, self::IMAGE_TYPE_ALLOW_VARIATION_CREATION)) {

            // Imagick instance for the image
            $im = new \Imagick($this->absolutePath);

            // Get the widths of images that need to be created
            $breakpoints = explode(',', Setting::getSetting('image_responsive_widths'));

            // Generate responsive images at each breakpoint, only if it is smaller than the original image
            foreach ($breakpoints as $breakpoint) {
                if ($breakpoint < $this->width) {
                    $saveFile = $this->absoluteDirectory . '/' . $this->baseName . '-' . $breakpoint . 'w.' . $this->extension;
                    // Different logic is required for animated GIF
                    if (!$this->isAnimatedGif) {
                        // Not an animated GIF, normal resize and saving
                        $im->resizeImage($breakpoint, null, $im::FILTER_LANCZOS, 1);
                        /* Only save the new image if the filesize is smaller than the original */
                        if (strlen($im) < $this->fileSize) {
                            $im->writeImage($saveFile);
                        }
                    } else {
                        // Animated GIF, need to be a little more complex
                        $imagick = $im->coalesceImages();
                        do {
                            $imagick->resizeImage($breakpoint, 0, \Imagick::FILTER_BOX, 1);
                        } while ($imagick->nextImage());
                        $imagick = $imagick->deconstructImages();
                        $imagick->writeImages($saveFile, true);
                    }
                }
            }
        }

        return true;
    }

    /**
     * Deletes all variations of a given Image from the filesystem
     *
     * @param $includeOriginal bool Whether or not the original should be included in the unlinking
     * @return bool
     */
    public function unlink($includeOriginal)
    {
        /* For each variation, unlink the file */
        foreach ($this->variationImageFiles() as $variationImageFile) {
            /* The file SHOULD exist, but just in case it doesn't, don't throw an error */
            if (file_exists($variationImageFile->absolutePath)) {
                unlink($variationImageFile->absolutePath);
            }
        }

        /* If the original is included, unlink it as well */
        if ($includeOriginal) {
            if (file_exists($this->absolutePath)) {
                unlink($this->absolutePath);
            }
        }

        return true;
    }

    /**
     * Compare the existing image variations against the setting determining how many variations should exist,
     * returning a list of the variations that do not exist.
     *
     * Used to identify which variations need to be manually created/uploaded (on image view) as well as indicate that
     * there is something "wrong" with a given image (image index).
     *
     * @return array
     */
    public function variationsMissing()
    {

        /* The image variation breakpoints that should exist. */
        $expectedVariations = explode(',', Setting::getSetting('image_responsive_widths'));

        /*
         * Since not all images have a width greater than the largest expected varation, expected varations should only
         * include those width that are less than the size of the original image.
         */
        $originalWidth = $this->width;
        /* The value in the array will be compared against the width of the original image. */
        $expectedVariations = array_filter($expectedVariations, function ($value) use ($originalWidth) {
            /* If the width of this variation is smaller than the original, is should be included in the result. */
            return $value < $originalWidth;
        });

        /* List of widths of each image variation that actually exists. */
        $found = ArrayHelper::getColumn($this->variationImageFiles(), 'width');

        /* Return only a list of the expected variations that are not found. */

        return array_diff($expectedVariations, $found);
    }

    /**
     * Compare the existing image variations against the original to determine if any variations have file sizes
     * larger than the original image.
     *
     * Used to identify which variations need to be manually created/uploaded (on image view) as well as indicate that
     * there is something "wrong" with a given image (image index).
     *
     * @return array
     */
    public function variationsLargerOrEqual()
    {

        /* Build list of variations whose file sizes are larger than the original image file size */
        $variations = array();
        foreach ($this->variationImageFiles() as $variationImageFile) {
            if ($variationImageFile->fileSize >= $this->fileSize) {
                $variations[] = $variationImageFile;
            }
        }

        return $variations;
    }

    /**
     * Compare the existing image variations against the original to determine if any variations have widths
     * larger than the original image.
     *
     * Used to identify which variations need to be manually created/uploaded (on image view) as well as indicate that
     * there is something "wrong" with a given image (image index).
     *
     * An automatically created variation will never be wider, but, if an image is uploaded then the origina image is
     * replaced with a smaller image, there may exist a variation that is wider than the original.
     *
     * @return array
     */
    public function variationsWiderOrEqual()
    {

        /* Build list of variations whose widths are wider than the original image file size */
        $variations = array();
        foreach ($this->variationImageFiles() as $variationImageFile) {
            if ($variationImageFile->width >= $this->width) {
                $variations[] = $variationImageFile;
            }
        }

        return $variations;
    }

    /**
     * Whether or not there exists at least one warning condition on at least one image variation.
     *
     * Used to identify which variations need to be manually created/uploaded (on image view) as well as indicate that
     * there is something "wrong" with a given image (image index).
     *
     * @return boolean
     */
    public function variationsHaveWarning()
    {
        return (count($this->variationsMissing()) > 0 || count($this->variationsLargerOrEqual()) > 0 || count($this->variationsWiderOrEqual()) > 0);
    }
}
