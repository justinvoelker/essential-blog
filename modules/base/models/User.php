<?php

namespace app\modules\base\models;

use app\modules\backend\helpers\DataHelper;
use PragmaRX\Google2FA\Google2FA;
use yii\base\NotSupportedException;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;
use yii\web\NotFoundHttpException;

/**
 * @property string $role
 */
class User extends BaseUser implements IdentityInterface
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'BlameableBehavior' => BlameableBehavior::className(),
            'TimestampBehavior' => TimestampBehavior::className(),
        ];
    }

    /**
     * Primary pages for this user, not all pages as returned by getPages()
     *
     * @return \yii\db\ActiveQuery
     */
    public function primaryPages()
    {
        return $this->hasMany(Page::className(), ['created_by' => 'id'])->where(['primary_id' => null]);
    }

    /**
     * Primary posts for this user, not all posts as returned by getPosts()
     *
     * @return \yii\db\ActiveQuery
     */
    public function primaryPosts()
    {
        return $this->hasMany(Post::className(), ['created_by' => 'id'])->where(['primary_id' => null]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public static function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested user does not exist.');
        }
    }

    /**
     * Before save, if password contains a value (only when provided on a form) set password hash from value entered.
     * Also, if the password_hash value is different (and was not blank) a password change email is sent. The check of
     * the old value against blank is to ensure that the password change email is not sent when a new account
     * registration is completed and the password is set for the first time.
     *
     * Exaple usage: When a user record is updated (and a password is set) via the administrative interface.
     *
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            // If password has been entered, set password hash
            if ($this->password) {
                $this->setPassword($this->password);
            }

            /**
             * Send password change email if the following two conditions are met:
             * 1. The password hash has changed
             * 2. The old password hash isset (meaning it contains a value and is not a new user registration)
             */
            if (array_key_exists('password_hash',
                    $this->dirtyAttributes) && isset($this->oldAttributes['password_hash'])
            ) {
                $this->sendPasswordChangeEmail();
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @return array
     */
    public static function allRoles()
    {
        return [
            '' => '',
            'contributor' => 'Contributor',
            'author' => 'Author',
            'editor' => 'Editor',
            'administrator' => 'Administrator',
        ];
    }

    /**
     * TODO find out where this is used and eliminate the getter/setter usage. Replace with standard methods as needed.
     *
     * @return array
     */
    public function getRole()
    {
        $userRoles = \Yii::$app->authManager->getRolesByUser($this->id);

        $result = null;
        if ($this->id && count($userRoles) <> 0) {
            $result = array_keys($userRoles)[0];
        }

        return $result;
    }

    /**
     * TODO find out where this is used and eliminate the getter/setter usage. Replace with standard methods as needed.
     *
     * @param string $value
     */
    public function setRole($value)
    {
        $this->role = $value;
    }

    /**
     * @return string
     */
    public function firstNameLastName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * @return string
     */
    public function firstInitialLastInitial()
    {
        return substr($this->first_name, 0, 1) . substr($this->last_name, 0, 1);
    }

    /**
     * Text for user avatar. Either first and last initial if a name is set or a question mark for users with no name
     * set (such as invited users that have not yet completed registration).
     *
     * @return string
     */
    public function avatarText()
    {
        return strlen(self::firstInitialLastInitial()) > 0 ? self::firstInitialLastInitial() : "?";
    }

    /**
     * Client names for connected user auth connections.
     *
     * Example usage: On User form, in a loop of all available auth clients, a "disconnect" button is shown if the
     * specific client is in the list of connected clients returned by this method.
     *
     * @return array
     */
    public function authConnectionClients()
    {
        $ids = ArrayHelper::getColumn($this->userAuthConnections, 'id');
        $clients = [];
        foreach ($ids as $id) {
            $clients[] = substr($id, 0, strpos($id, '-'));
        }

        return $clients;
    }

    /**
     * Connection dates for user auth connections
     *
     * Example usage: On User form, in a loop of all available auth clients, for connected clients, a date indicates
     * when that client a connected.
     *
     * @return array
     */
    public function authConnectionDates()
    {
        $authClients = ArrayHelper::map($this->userAuthConnections, 'id', 'connected_at');

        $clients = [];
        foreach ($authClients as $id => $connected_at) {
            $clientName = substr($id, 0, strpos($id, '-'));
            $clients[$clientName] = $connected_at;
        }

        return $clients;
    }

    /**
     * Update a user authorization role by removing existing roles then adding the new role.
     *
     * Example usage: In invite and user updates, revoke existing roles then reassign new role.
     */
    public function updateAuthRole()
    {
        $auth = \Yii::$app->authManager;
        $auth->revokeAll($this->id);
        $authRole = $auth->getRole($this->role);
        if ($authRole) {
            $auth->assign($authRole, $this->id);
        }
    }

    /**
     * Updates "active_at"
     */
    public function updateActiveAt()
    {
        $this->active_at = time();
        $this->detachBehaviors();
        $this->save(false);
    }

    /**
     * Sends an email to newly invited user.
     *
     * Example usage: User controller actions "invite" and "resendInvite" sent invitation emails.
     *
     * @return boolean whether the email was send
     */
    public function sendInviteEmail()
    {
        $blog_title = Setting::getSetting('blog_title');
        $support_email = Setting::getSetting('support_email_address');

        $subject = Setting::getSetting('blog_title') . ' Invitation';

        return \Yii::$app
            ->mailer
            ->compose(
                ['html' => 'invite-html', 'text' => 'invite-text'],
                ['user' => $this]
            )
            ->setFrom([$support_email => $blog_title . ' Support'])
            ->setTo($this->email)
            ->setSubject($subject)
            ->send();
    }

    /**
     * Generates password hash from password and sets it in the password_hash property of the model.
     *
     * Example usage: In beforeSave, if $password is set, setPassword is called to hash the value and set password_hash.
     * Also used via password reset process.
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = \Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Sends an email to the user that their password has been changed.
     *
     * Example usage: In beforeSave, if the password_hash has changed, a password change email is sent.
     *
     * @return boolean whether the email was send
     */
    public function sendPasswordChangeEmail()
    {
        $blog_title = Setting::getSetting('blog_title');
        $support_email = Setting::getSetting('support_email_address');

        $subject = $blog_title . ' Password Change';

        return \Yii::$app
            ->mailer
            ->compose(
                ['html' => 'passwordChange-html', 'text' => 'passwordChange-text'],
                ['user' => $this]
            )
            ->setFrom([$support_email => $blog_title . ' Support'])
            ->setTo($this->email)
            ->setSubject($subject)
            ->send();
    }

    /**
     * Sends an email to the user after successful login to their account.
     *
     * Example usage: In beforeSave, if the password_hash has changed, a password change email is sent.
     *
     * @return boolean whether the email was send
     */
    public function sendLoginNotificationEmail()
    {

        /* IP address of user (user as in visitor, not User model) */
        $ip = \Yii::$app->getRequest()->getUserIP();

        $blog_title = Setting::getSetting('blog_title');
        $support_email = Setting::getSetting('support_email_address');

        $subject = $blog_title . ' Login Notification';

        return \Yii::$app
            ->mailer
            ->compose(
                ['html' => 'loginNotification-html', 'text' => 'loginNotification-text'],
                ['user' => $this, 'ip' => $ip]
            )
            ->setFrom([$support_email => $blog_title . ' Support'])
            ->setTo($this->email)
            ->setSubject($subject)
            ->send();
    }

    /**
     * String of how long ago the user was active either as today, yesterday, X days ago or "never logged in." Does not
     * use Yii translation because it requires a higher ICU version that is not found on ASO shared hosting (just as a
     * reference that some things may not be available everywhere).
     *
     * Example usage: User index page shows how long ago a user was logged in.
     * TODO replace this one-off function with a generic helper that is also used for how long ago a post was published.
     *
     * @return string
     */
    public function lastActive()
    {
        if ($this->active_at) {
            /* Set "today" and "active" DateTime instances */
            $today = new \DateTime(date("Y-m-d"));
            $active = new \DateTime(\Yii::$app->formatter->asDate($this->active_at, 'php:Y-m-d'));
            /* Calculate days difference between today and active_at */
            $interval = $today->diff($active);
            $days = intval($interval->format('%a'));
            /* Output message as active today, yesterday, or X days ago */
            if ($days == 0) {
                $result = 'Active today';
            } elseif ($days == 1) {
                $result = 'Active yesterday';
            } else {
                $result = 'Active ' . $days . ' days ago';
            }
        } else {
            $result = 'Never logged in';
        }

        return $result;
    }

    /**
     * Whether or not user has two-factor authentication enabled
     */
    public function twoFactorEnabled()
    {
        return $this->two_factor_authentication !== null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ENABLED]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ENABLED]);
    }

    /**
     * Finds user by email address
     *
     * @param string $username
     * @return static|null
     */
    public static function findByEmail($username)
    {
        return static::findOne(['email' => $username, 'status' => self::STATUS_ENABLED]);
    }

    /**
     * Finds user by security token
     *
     * @param string $token security token
     * @param bool $checkIfValid where or not to check validity of token
     * @return null|static
     */
    public static function findBySecurityToken($token, $checkIfValid = true)
    {
        if ($checkIfValid && !static::isSecurityTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'security_token' => $token,
            'status' => [self::STATUS_ENABLED, self::STATUS_INVITED],
        ]);
    }

    /**
     * Finds out if security token is valid
     *
     * @param string $token security token
     * @return boolean
     */
    public static function isSecurityTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Setting::getSetting('user_security_token_expiration');

        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return \Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Validates two-factor authentication code
     *
     * @param string $two_factor_code two-factor authentication code to validate
     * @return boolean if two_factor_authentication secret provided is valid for current user
     */
    public function validateTwoFactorCode($two_factor_code)
    {
        /* Check the provided code against the two-factor secret */
        $google2fa = new Google2FA();
        if ($two_factor_code != self::currentTwoFactorLastCode() && $google2fa->verifyKey(self::currentTwoFactorSecret(), $two_factor_code)) {
            /* Update the last_code and return the result (if save fails, this will return false) */
            return self::updateTwoFactorLastCode($two_factor_code);
        } elseif (strlen($two_factor_code) == 8 && (in_array($two_factor_code, self::currentTwoFactorBackupCodes()))) {
            /* Update the backup_codes and return the result (if save fails, this will return false) */
            return self::updateTwoFactorBackupCodesRemovingCode($two_factor_code);
        }

        /* If not a valid generated code and not a valid backup code, code is not valid */
        return false;
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = \Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new security token
     */
    public function generateSecurityToken()
    {
        $this->security_token = \Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes security token
     */
    public function removeSecurityToken()
    {
        $this->security_token = null;
    }

    /**
     *
     */
    public function newTwoFactorAuthenticationSecret()
    {
        $google2fa = new Google2FA();
        return $google2fa->generateSecretKey();
    }

    /*
     *
     * Two-factor authentication handling
     *
     */

    /**
     * Current two-factor authentication secret
     *
     * @return string
     */
    public function currentTwoFactorSecret() {
        return DataHelper::decryptFromStorage($this->two_factor_authentication)['secret'];
    }

    /**
     * Current two-factor authentication enabled at timestamp
     *
     * @return integer
     */
    public function currentTwoFactorEnabledAt() {
        return DataHelper::decryptFromStorage($this->two_factor_authentication)['enabled_at'];
    }

    /**
     * Current two-factor authentication last code
     *
     * @return string
     */
    public function currentTwoFactorLastCode() {
        return DataHelper::decryptFromStorage($this->two_factor_authentication)['last_code'];
    }

    /**
     * Current two-factor authentication backup codes
     *
     * @return array
     */
    public function currentTwoFactorBackupCodes() {
        return DataHelper::decryptFromStorage($this->two_factor_authentication)['backup_codes'];
    }

    /**
     * Update two-factor authentication last code.
     *
     * During login, if the code entered does not equal the last_code and the code is a valid two-factor authentication
     * code, it needs to be set as the last_code so that another login cannot take place with the same code.
     *
     * @param $two_factor_code
     * @return boolean
     */
    public function updateTwoFactorLastCode($two_factor_code) {
        /* Current two-factor authentication data */
        $two_factor_authentication = DataHelper::decryptFromStorage($this->two_factor_authentication);
        /* Set the last_code */
        $two_factor_authentication['last_code'] = $two_factor_code;
        /* Encrypt the updated data for storage */
        $this->two_factor_authentication = DataHelper::encryptForStorage($two_factor_authentication);
        /* Detach BlameableBehavior and TimestampBehavior. This record update does not need to be tracked. */
        $this->detachBehaviors();
        /* Save the record and return the result of the save */
        return ($this->save());
    }

    /**
     * Update two-factor authentication last code.
     *
     * During login, if the code entered is 8 characters and exists in the list of current backup codes, it needs to be
     * removed from the list of backup_codes so that another login cannot take place with the same code.
     *
     * TODO perhaps this can be updated to use removeValue method in yii\helpers\BaseArrayHelper? Enh #12881
     *
     * @param $two_factor_code
     * @return boolean
     */
    public function updateTwoFactorBackupCodesRemovingCode($two_factor_code) {
        /* Current two-factor authentication data */
        $two_factor_authentication = DataHelper::decryptFromStorage($this->two_factor_authentication);
        /* Array of backup codes that will be manipulated */
        $backupCodes = self::currentTwoFactorBackupCodes();
        /* Find position of code in list of backup codes */
        $backupCodeKey = array_search($two_factor_code, $backupCodes);
        /* Remove the specified backup code */
        unset($backupCodes[$backupCodeKey]);
        /* Set the updated backup codes */
        $two_factor_authentication['backup_codes'] = $backupCodes;
        /* Encrypt the updated data for storage */
        $this->two_factor_authentication = DataHelper::encryptForStorage($two_factor_authentication);
        /* Detach BlameableBehavior and TimestampBehavior. This record update does not need to be tracked. */
        $this->detachBehaviors();
        /* Save the record and return the result of the save */
        return ($this->save());
    }
}
