<?php

namespace app\modules\base\models;

use yii\base\BaseObject;

/**
 * This is the model class for an image on the file system.
 *
 * @property bool $exists
 * @property string $path
 * @property string $absolutePath
 * @property string $directory
 * @property string $absoluteDirectory
 * @property string $fileName
 * @property string $baseName
 * @property string $extension
 * @property string $width
 * @property string $height
 * @property string $fileSize
 * @property int $imageType
 * @property bool $isAnimatedGif
 */
class BaseImageFile extends BaseObject
{
    /**
     * @var string the path of the file within the web directory (will start with "/uploads")
     */
    private $_path;

    /**
     * ImageFile constructor.
     *
     * @param string $path
     * @param array $config
     */
    public function __construct($path, $config = [])
    {
        $this->_path = $path;

        parent::__construct($config);
    }

    /**
     * Whether or not a file exists at this path.
     *
     * @return bool
     */
    public function getExists()
    {
        if (file_exists($this->absolutePath)) {
            return true;
        }
        return false;
    }

    /**
     * Returns the path for a given file path
     *
     * Example: Returns "/uploads/2016/02/_DSC7831.jpg" for path "/uploads/2016/02/_DSC7831.jpg"
     *
     * @return string
     */
    public function getPath()
    {
        return $this->_path;
    }

    /**
     * Returns the absolute path for a given file path
     *
     * Example: Returns "/var/www/html/web/uploads/2016/02/_DSC7831.jpg" for path "/uploads/2016/02/_DSC7831.jpg"
     *
     * @return string
     */
    public function getAbsolutePath()
    {
        return \Yii::getAlias('@webroot') . $this->_path;
    }

    /**
     * Returns the relative directory for a given file path.
     *
     * Example: Returns "/uploads/2016/02" for path "/uploads/2016/02/_DSC7831.jpg"
     *
     * @return string
     */
    public function getDirectory()
    {
        return pathinfo($this->_path, PATHINFO_DIRNAME);
    }

    /**
     * Returns the absolute directory for a given file path
     *
     * Example: Returns "/var/www/html/web/uploads/2016/02" for path "/uploads/2016/02/_DSC7831.jpg"
     *
     * @return string
     */
    public function getAbsoluteDirectory()
    {
        return pathinfo($this->absolutePath, PATHINFO_DIRNAME);
    }

    /**
     * Returns the file name for a given file path.
     *
     * Example: Returns "_DSC7831.jpg" for path "/uploads/2016/02/_DSC7831.jpg"
     *
     * Note: Purposefully opposes PHP's file name (which seems to be named incorrectly anyway)
     *
     * @return string
     */
    public function getFileName()
    {
        return pathinfo($this->_path, PATHINFO_BASENAME);
    }

    /**
     * Returns the base file name for a given file path (file name without extension).
     *
     * Example: Returns "_DSC7831" for path "/uploads/2016/02/_DSC7831.jpg"
     *
     * Note: Purposefully opposes PHP's base name (which seems to be named incorrectly anyway)
     *
     * @return string
     */
    public function getBaseName()
    {
        return pathinfo($this->_path, PATHINFO_FILENAME);
    }

    /**
     * Returns the extension for a given file path
     *
     * Example: Returns "jpg" for path "/uploads/2016/02/_DSC7831.jpg"
     *
     * @return string
     */
    public function getExtension()
    {
        return pathinfo($this->_path, PATHINFO_EXTENSION);
    }

    /**
     * Returns the width of the image, in pixels.
     *
     * @return array
     */
    public function getWidth()
    {
        return getimagesize($this->absolutePath)[0];
    }

    /**
     * Returns the height of the image, in pixels.
     *
     * @return array
     */
    public function getHeight()
    {
        return getimagesize($this->absolutePath)[1];
    }

    /**
     * Returns the file size of the image, in bytes.
     *
     * @return array
     */
    public function getFileSize()
    {
        return filesize($this->absolutePath);
    }

    /**
     * Whether or not a file is an animated GIF.
     *
     * @return int
     */
    public function getImageType()
    {
        return exif_imagetype($this->absolutePath);
    }

    /**
     * Whether or not a file is an animated GIF.
     *
     * @return bool
     */
    public function getIsAnimatedGif()
    {
        return (bool)preg_match('#(\x00\x21\xF9\x04.{4}\x00\x2C.*){2,}#s', file_get_contents($this->absolutePath));
    }
}
