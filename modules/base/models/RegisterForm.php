<?php
namespace app\modules\base\models;

use yii\base\InvalidParamException;
use yii\base\Model;

/**
 * Password reset form
 */
class RegisterForm extends Model
{
    public $first_name;
    public $last_name;
    public $username;
    public $password;
    public $security_token;

    /**
     * @var \app\modules\base\models\User
     */
    private $_user;

    /**
     * Creates a form model given a token.
     *
     * @param  string $token
     * @param  array $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($token, $config = [])
    {
        $this->security_token = $token;
        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException('Security token cannot be blank.');
        }
        $this->_user = User::findBySecurityToken($token, false);
        if (!$this->_user) {
            throw new InvalidParamException('Invalid security token.');
        }
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'username', 'password'], 'required'],
            ['username', 'unique', 'targetClass' => '\app\modules\base\models\User', 'targetAttribute' => 'username'],
        ];
    }

    /**
     * Registers user
     *
     * @return boolean if password was reset.
     */
    public function register()
    {
        $user = $this->_user;

        $user->first_name = $this->first_name;
        $user->last_name = $this->last_name;
        $user->username = $this->username;
        $user->setPassword($this->password);
        $user->status = $user::STATUS_ENABLED;
        $user->removeSecurityToken();

        /*
         * Blameable Behavior needs to be detached so updated_by can be explicitly set.
         * Since the user is not logged in, updated_by would be set to NULL when the record is saved/updated.
         */
        $user->detachBehavior('BlameableBehavior');
        $user->updated_by = $user->id;

        return $user->save();
    }
}
