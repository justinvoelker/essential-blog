<?php
namespace app\modules\base\models;

use yii\base\InvalidParamException;
use yii\base\Model;

/**
 * Password reset form
 */
class PasswordResetForm extends Model
{
    public $password;

    /**
     * @var \app\modules\base\models\User
     */
    private $_user;


    /**
     * Creates a form model given a token.
     *
     * @param  string $token
     * @param  array $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($token, $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException('Security token cannot be blank.');
        }
        $this->_user = User::findBySecurityToken($token);
        if (!$this->_user) {
            throw new InvalidParamException('Invalid security token.');
        }
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['password', 'required'],
        ];
    }

    /**
     * Resets password.
     *
     * @return boolean if password was reset.
     */
    public function resetPassword()
    {
        $user = $this->_user;
        $user->setPassword($this->password);
        $user->removeSecurityToken();

        /*
         * Blameable Behavior needs to be detached so updated_by can be explicitly set.
         * Since the user is not logged in, updated_by would be set to NULL when the record is saved/updated.
         */
        $user->detachBehavior('BlameableBehavior');
        $user->updated_by = $user->id;

        return $user->save(false);
    }
}
