<?php

namespace app\modules\base\models;

use yii\base\BaseObject;

/**
 * This is the model class for a theme on the file system.
 *
 * @property string $name
 * @property string $directory
 * @property bool $exists
 * @property string $blogTheme
 */
class BlogTheme extends BaseObject
{
    public $name;

    public function __construct($name, $config = [])
    {
        $this->name = $name;

        parent::__construct($config);
    }

    /**
     * File system path of the theme
     * @return bool
     */
    public function getDirectory()
    {
        return \Yii::getAlias('@webroot/themes/') . $this->name;
    }

    /**
     * Whether or not the theme directory exists
     * @return bool
     */
    public function getExists()
    {
        return file_exists($this->directory);
    }

    /**
     * Whether or not the theme directory exists
     * @return bool
     */
    public static function getBlogTheme()
    {
        $settingBlogTheme = new BlogTheme(Setting::getSetting('blog_theme'));

        return ($settingBlogTheme->exists) ? $settingBlogTheme->name : null;
    }
}
