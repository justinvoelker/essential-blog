<?php

namespace app\modules\base\models;

use app\modules\frontend\models\PageSearch as FrontendPageSearch;
use app\components\GithubMarkdownCustom;
use app\helpers\DateAndTime;
use yii\base\InvalidCallException;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 *
 */
class Page extends BasePage
{
    /*
     * Constants for referencing the various page statuses
     */
    const STATUS_DRAFT = 0;
    const STATUS_PUBLISHED = 1;
    const IS_LOCKED = 1; /* This is also set in js/page/action-edit.js */

    /**
     * Replace each piece of the URL structure with the appropriate piece of data.
     *
     * Only replaces variable values (text surrounded with %) leaving static values (text without %) to return null
     * which leaves them as a string of static text in the URL.
     *
     * @param $piece
     * @return int|string
     */
    public function urlStructureValue($piece)
    {
        $result = null;
        switch ($piece) {
            case '%year%':
                $result = $this->publish_year;
                break;
            case '%month%':
                $result = sprintf('%02d', $this->publish_month);
                break;
            case '%day%':
                $result = sprintf('%02d', $this->publish_day);
                break;
            case '%slug%':
                $result = ($this->slug) ? $this->slug : '';
                break;
            case '%id%':
                $result = $this->id;
                break;
        }

        return $result;
    }

    /**
     * Get the route for a page link (as array of url pieces).
     *
     * Example: A page with slug "new-page" and a url structure of "/page/%slug%" returns:
     *
     * [
     *     "/frontend/site/page",
     *     "page" => NULL,
     *     "slug" => "new-page"
     * ]
     *
     * The first item in the returned array is the absolute route to the action. The second item is the string
     * "page" (since it is a key with no value). The third item is the query parameter "slug" with "new-page"
     * as its value.
     *
     * @return mixed
     */
    public function route()
    {
        /* Get setting for page URL structure */
        $urlStructure = Setting::getSetting('url_structure_page');

        /* Remove the first slash. With an opening slash, the first array item will be blank. */
        $urlPieces = explode('/', substr($urlStructure, 1));

        /* Route starts with controller/action */
        $route[0] = '/frontend/site/page';

        /* Append each piece url structure to the route being built */
        foreach ($urlPieces as $urlPiece) {
            $route[str_replace('%', '', $urlPiece)] = $this->urlStructureValue($urlPiece);
        }

        return $route;
    }

    /**
     * Parse supplied content as Markdown.
     *
     * Example usage: Parsing page content or excerpt.
     *
     * @param $content
     * @param array $options
     * @return string
     */
    public function parseMarkdown($content, $options = [])
    {
        // Create instance or parsing utility
        $parser = new GithubMarkdownCustom();

        // Set parser properties for configuring the parser result
        $parser->imgSizes = isset($options['imgSizes']) ? $options['imgSizes'] : null;
        $parser->imgClass = isset($options['imgClass']) ? $options['imgClass'] : null;
        $parser->imgIsAmp = isset($options['imgIsAmp']) ? $options['imgIsAmp'] : null;
        $parser->hrefClass = isset($options['hrefClass']) ? $options['hrefClass'] : null;

        // Return the parsed content
        return $parser->parse($content);
    }

    /**
     * Page excerpt parsed as markdown.
     *
     * Returns the page excerpt if it is available or the first paragraph of the content if excerpt is blank.
     *
     * @param array $options
     * @return string
     */
    public function parsedExcerpt($options = [])
    {
        /* Use the actual excerpt if it exists. Otherwise, first paragraph of content */
        if ($this->excerpt) {
            $result = $this->excerpt;
        } else {
            if (strpos($this->content, PHP_EOL) !== false) {
                $result = substr($this->content, 0, strpos($this->content, PHP_EOL));
            } else {
                $result = $this->content;
            }
        };

        /*
         * Presents an interesting dilemma. Pages without excerpts could start with a big header which means an ugly
         * excerpt. However, if the page starts with a normal paragraph or a true excerpt exists, simple markdown such
         * as emphasis or bold text will be parsed appropriately. It is for this reason that pages should only ever
         * begin with an opening paragraph following by H2 headings in the content.
         */

        return $this->parseMarkdown($result, $options);
    }

    /**
     * Page content parsed as markdown.
     *
     * @param array $options
     * @return string
     */
    public function parsedContent($options = [])
    {
        // Return the parsed content
        return $this->parseMarkdown($this->content, $options);
    }

    /*
     * When a page is saved, by default, the beforeSave() method create a new primary page when primary_id is
     * blank. However, some scenarios (updating the primary during publish and unpublish, the actual creation of the
     * primary) require that the beforeSave logic that creates the primary must NOT create a primary despite having a blank
     * primary_id. Setting this property to false prevents beforeSave() from creating a primary.
     */
    private $_createPrimaryIfNull = true;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'BlameableBehavior' => BlameableBehavior::className(),
            'TimestampBehavior' => TimestampBehavior::className(),
        ];
    }

    /**
     * Finds the Page model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Page the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public static function findModel($id)
    {
        if (($model = Page::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Depending on the various states of a page (published, unpublished, locked, etc.) the various action buttons
     * will have different text and enabled/disabled states.
     *
     * Only to be called on a restore point, not a primary page.
     *
     * @return array
     */
    public function buttons()
    {
        /* Throw error if called from the primary. Must be called from a restore point. */
        if (!$this->isNewRecord && $this->primary_id == null) {
            throw new InvalidCallException('Page::actionBarButtons should only be called from restore point instance.');
        } else {
            return [
                'save' => [
                    'locator' => '[data-submit-type="save"]',
                    'text' => 'Save',
                    /*
                     * Basically, this always returns false since it is always called from a save, publish, or other
                     * command. Save will be enabled as soon as data is edited, but, whenever a controller determines
                     * enabled status, it will be false.
                     */
                    'enabled' => false,
                ],
                'saverestorepoint' => [
                    'locator' => '[data-submit-type="saverestorepoint"]',
                    'text' => 'Save Restore Point',
                    /*
                     * If not a new record and the newest restore point is not locked, save restore point should be enabled.
                     */
                    'enabled' => (!$this->isNewRecord && !$this->is_locked),
                ],
                'publish' => [
                    'locator' => '[data-action="publish"]',
                    'text' => ($this->hasFuturePublishDate()) ? 'Schedule' : ((!$this->isNewRecord && $this->primaryPage()->status == self::STATUS_PUBLISHED) ? 'Republish' : 'Publish'),
                    /*
                     * Publishing is enabled if the page is not a new record and 1) The primary page is not currently published or 2) it is
                     * published but the updated_at dates on the newest restore point and the published primary are different (indicating the
                     * page has been updated since it was published).
                     */
                    'enabled' => (!$this->isNewRecord && ($this->primaryPage()->status != self::STATUS_PUBLISHED || ($this->primaryPage()->status == self::STATUS_PUBLISHED && $this->updated_at != $this->primaryPage()->updated_at))),
                ],
                'unpublish' => [
                    'locator' => '[data-action="unpublish"]',
                    'text' => ($this->hasFuturePublishDate()) ? 'Unschedule' : 'Unpublish',
                    /*
                     * Unpublishing is enabled if the page is not a new record and the primary page is currently published.
                     */
                    'enabled' => (!$this->isNewRecord && ($this->primaryPage()->status == self::STATUS_PUBLISHED)),
                ],
            ];
        }
    }

    /**
     * For a given restore point page, return the primary page.
     *
     * @return Page|array|\yii\db\ActiveRecord
     */
    public function primaryPage()
    {
        return Page::find()->where(['id' => $this->primary_id])->one();
    }

    /**
     * For a given primary page, return all restore point pages.
     *
     * Example usage: For displaying a list of all page restore points when viewing a primary page in the administrative
     * interface or deleting all page restore points when deleting the primary page.
     *
     * @return \yii\db\ActiveQuery
     */
    public function pageRestorePoints()
    {
        return $this->hasMany(Page::className(), ['primary_id' => 'id']);
    }

    /**
     * For a given primary page, return the newest restore point. Newest determined by selecting all restore points, sorting the
     * newest first, and returning only the first result.
     *
     * Example usage: Most actions within the administrative interface only show the newest restore point (for example when
     * viewing a page, editing a page, or determining which actionn bar buttons to show).
     *
     * @return Page|array|\yii\db\ActiveRecord
     */
    public function newestRestorePoint()
    {
        return Page::find()->where(['primary_id' => $this->id])->orderBy(['created_at' => SORT_DESC])->one();
    }

    /**
     * Return a text description of the page status.
     *
     * @return mixed
     */
    public function statusText()
    {
        if ($this->status == self::STATUS_PUBLISHED && ($this->newestRestorePoint() != null && $this->newestRestorePoint()->is_locked == false)) {
            $result = 'Edited';
        } else if ($this->status == self::STATUS_PUBLISHED && DateAndTime::toTimestamp($this->scheduled_for) > time()) {
            $result = 'Scheduled';
        } else if ($this->status == self::STATUS_PUBLISHED) {
            $result = 'Published';
        } else {
            $result = 'Draft';
        }

        return $result;
    }

    /**
     * Boolean indicating whether or not a page has a published date that is in the future (anytime after right now).
     *
     * Example usage: Flash alert after publish a page will either indicate that the page has been "publish" or
     * "scheduled" depending on whether or not the schedule date is in the future. Also, action bar buttons depend on
     * whether or not a schedule date is set.
     *
     * @return bool
     */
    public function hasFuturePublishDate()
    {
        return isset($this->scheduled_for) && DateAndTime::toTimestamp($this->scheduled_for) > time();
    }

    /**
     * Regardless of how a Page is saved (autosave, manual, or save restore point) the newest restore point will be used to save
     * the _POST data as long as it is not locked (save and save restore point will both overwrite an existing, unlocked
     * records but a save restore point will additionally set is_locked to true). Basically, if the newest restore point is
     * locked, a new restore point needs to be created to preserve the existing, locked restore point. Unless, of course,
     * $restore_points_enabled is false, then no new restore point will ever be created.
     *
     * Another instance in which a new restore point will be created is if the user currently editing the record is not
     * the user that created that restore point. For example, if a post is being reviewed by an editor, any updates
     * that user makes will automatically be saved to a new restore point by locking the current restore point.
     *
     * @return Page|\yii\db\ActiveRecord
     */
    public function returnUnlockedPage()
    {
        /* If page restore points are enabled, new restore points can be created, otherwise, no new restore points, just overwriting the one non-primary restore point. */
        $restore_points_enabled = filter_var(Setting::getSetting('restore_points_enabled'), FILTER_VALIDATE_BOOLEAN);

        /*
         * If restore points are enabled and the user currently updating the record is not the user that created that
         * restore point, lock the existing record and create a new restore point, preserving the content updated by the
         * previous user and allowing the new users' updates to save to a new restore point (below).
         */
        if (!$this->isNewRecord && $restore_points_enabled && \Yii::$app->user->id != $this->created_by) {
            /* Detach Behavior so updated attributes will not be set */
            $this->detachBehavior('BlameableBehavior');
            $this->detachBehavior('TimestampBehavior');
            /* Set the record as locked as save it*/
            $this->is_locked = self::IS_LOCKED;
            $this->save();
        }

        /* If current instance is a locked restore point, instantiate a new restore point (if page restore points are enabled) */
        if ($this->is_locked == self::IS_LOCKED && $restore_points_enabled) {
            /* Copy the existing restore point data to a new restore point */
            $data = $this->attributes;
            //Yii::error(var_dump($data));
            $model = new Page();
            $model->setAttributes($data, false);
            /* Clear the ID so it can be auto incremented */
            $model->id = null;
            /* Clear is_locked so the default (not locked) can be set */
            $model->is_locked = null;

            return $model;
        } else {
            /* Default is to return $this instance */
            return $this;
        }
    }

    /**
     * Publish the page. Executed against a restore point page, not the primary page.
     */
    public function publish()
    {
        /* Throw exception if called from a new record. Should never happen from controller action, just a safety measure. */
        if ($this->isNewRecord) {
            throw new InvalidCallException('Page::publish can only be called on existing records.');
        }

        /* Throw exception if called from the primary. Must be called from a restore point. */
        if ($this->primary_id == null) {
            throw new InvalidCallException('Page::publish can only be called on a page restore point.');
        }

        /* Throw exception if called from a restore point other than the newest restore point. */
        if ($this->id != $this->primaryPage()->newestRestorePoint()->id) {
            throw new InvalidCallException('Page::publish can only be called on the newest page restore point.');
        }

        /* Ensure title contains data */
        if (strlen($this->title) == 0) {
            $this->addError('title', 'Title is required to publish a page.');
        }

        /* Ensure slug contains data */
        if (strlen($this->slug) == 0) {
            $this->addError('slug', 'Slug is required to publish a page.');
        }

        /* Only continue with publishing if there are no errors at this point */
        if (!$this->hasErrors()) {

            /* Start transaction so that newest restore point and primary page will either both be updated or neither will be. */
            $transaction = Page::getDb()->beginTransaction();
            try {
                /* Current timezone independent time */
                $time = time();

                /* Step 1, update the newest restore point to set it as published */

                /* Set published attributes if not already set (will already be set if a page is being republished */
                $this->published_by = ($this->published_by) ? $this->published_by : \Yii::$app->user->id;
                $this->published_at = ($this->published_at) ? $this->published_at : $time;
                /* Always set the revised attributes (just like updated attributes with BlameableBehavior) */
                $this->revised_by = \Yii::$app->user->id;
                $this->revised_at = $time;
                /* Set status to published */
                $this->status = Page::STATUS_PUBLISHED;
                /* Lock the restore point */
                $this->is_locked = Page::IS_LOCKED;
                /*
                 * Note: In contrast to Category where "category_id" is the field and "category" is the related record,
                 * "tags" represents both the field and the related records. When a record is saved (so during the
                 * $model->load() inside actionEdit) the tag IDs are loaded from the multi-select dropdown on the form
                 * into the entity. Thus, during the save action, "tags" contains an array of tag IDs. However, when it
                 * comes to publishing, "$this->tags" references the "tags" related records (which are full models)
                 * since it was not populated/overwritten with as simple ID array from the form data. As such, that
                 * list of models needs to be converted to just the tag IDs so that afterSave() can work with a list of
                 * IDs just as it does when tags are loaded from the form.
                 */
                $this->tags = ArrayHelper::getColumn($this->tags, 'id');
                /* Save the restore point. */
                if (!$this->save()) {
                    /* No need to "addError" here as with primary below since this is "this" model and each attribute already threw its own error if needed. */
                    /* Throw an exception so the transaction will be rolled back */
                    throw new \Exception('Failed to save newest page restore point as published.');
                }

                /* Step 2, update the primary with data from the newest restore point */

                /* Get primary page to update */
                $primary = $this->primaryPage();
                /* Updated attributes are always set on update */
                $primary->updated_by = $this->updated_by;
                $primary->updated_at = $this->updated_at;
                /* Published attributes will either keep an existing non-null value or will be set from newest restore point */
                $primary->published_by = ($primary->published_by) ? $primary->published_by : $this->published_by;
                $primary->published_at = ($primary->published_at) ? $primary->published_at : $this->published_at;
                /* Revised attributes are always set on update */
                $primary->revised_by = $this->revised_by;
                $primary->revised_at = $this->revised_at;
                /* Title is obviously needed */
                $primary->title = $this->title;
                /* Definitely need slug, excerpt, and content */
                $primary->slug = $this->slug;
                $primary->excerpt = $this->excerpt;
                $primary->content = $this->content;
                /* May or may not be set, but needs to be copied to primary */
                $primary->scheduled_for = $this->scheduled_for;
                /* Category, tags, and image are copied */
                $primary->category_id = $this->category_id;
                $primary->tags = $this->tags;
                $primary->image = $this->image;
                /* Could set status to published, but, copying from newest restore point for consistency */
                $primary->status = $this->status;
                /* Detach Behavior so updated attributes can be forcibly set as above */
                $primary->detachBehavior('BlameableBehavior');
                $primary->detachBehavior('TimestampBehavior');
                /* Do NOT create a primary if null (since it will be null, being this is the primary). */
                $primary->_createPrimaryIfNull = false;
                /* Save the primary page. */
                if (!$primary->save()) {
                    /* Add all primary errors to a non-existent attribute so they will get returned on "this" instance. */
                    $this->addError('*primary', implode('<br>', $primary->firstErrors));
                    /* Throw an exception so the transaction will be rolled back */
                    throw new \Exception('Failed to save primary page as published.');
                }

                /* Commit the transaction */
                $transaction->commit();
            } /** @noinspection PhpUndefinedClassInspection */ catch (\Throwable $e) {
                /* If either save above failed and threw an exception, roll back the entire transaction. */
                $transaction->rollBack();
            }
        }

        /* Return result is based on whether or not model has errors */

        return ($this->hasErrors()) ? false : true;
    }

    /**
     * Unpublish the page. Executed against a restore point page, not the primary page.
     */
    public function unpublish()
    {
        /* Throw exception if called from a new record. Should never happen from controller action, just a safety measure. */
        if ($this->isNewRecord) {
            throw new InvalidCallException('Page::unpublish can only be called on existing records.');
        }

        /* Throw exception if called from the primary. Must be called from a restore point. */
        if ($this->primary_id == null) {
            throw new InvalidCallException('Page::unpublish can only be called on a page restore point.');
        }

        /* Throw exception if called from a restore point other than the newest restore point. */
        if ($this->id != $this->primaryPage()->newestRestorePoint()->id) {
            throw new InvalidCallException('Page::publish can only be called on the newest page restore point.');
        }

        /* Only continue with unpublishing if there are no errors at this point */
        if (!$this->hasErrors()) {

            /* Start transaction so that newest restore point and primary page will either both be updated or neither will be. */
            $transaction = Page::getDb()->beginTransaction();
            try {

                /* Step 1, instantiate a new page restore point if necessary (if the newest restore point is locked) */

                $newestRestorePoint = $this->returnUnlockedPage();

                /* Step 2, update the newest restore point to set it as unpublished */

                /* Clear published attributes */
                $newestRestorePoint->published_by = null;
                $newestRestorePoint->published_at = null;
                /* Clear revised attributes */
                $newestRestorePoint->revised_by = null;
                $newestRestorePoint->revised_at = null;
                /* Set status back to draft */
                $newestRestorePoint->status = self::STATUS_DRAFT;
                /* Save the restore point */
                if (!$newestRestorePoint->save()) {
                    /* Add all newest restore point errors to a non-existent attribute so they will get returned on "this" instance. */
                    $this->addError('*newestRestorePoint', implode('<br>', $newestRestorePoint->firstErrors));
                    /* Throw an exception so the transaction will be rolled back */
                    throw new \Exception('Failed to save newest page restore point as unpublished.');
                }

                /* Step 4, update the primary with data from the newest restore point (most of which is null) */

                /* Get primary page to update */
                $primary = $this->primaryPage();
                /* Clear updated attributes */
                $primary->updated_by = $newestRestorePoint->updated_by;
                $primary->updated_at = $newestRestorePoint->updated_at;
                /* Clear published attributes */
                $primary->published_by = $newestRestorePoint->published_by;
                $primary->published_at = $newestRestorePoint->published_at;
                /* Clear revised attributes */
                $primary->revised_by = $newestRestorePoint->revised_by;
                $primary->revised_at = $newestRestorePoint->revised_at;
                /* Set status back to draft */
                $primary->status = $newestRestorePoint->status;
                /* Detach Behavior so updated attributes can be forcibly set as above */
                $primary->detachBehavior('BlameableBehavior');
                $primary->detachBehavior('TimestampBehavior');
                /* Since this IS the primary, do NOT create a primary if null (since it will be null, being the primary. */
                $primary->_createPrimaryIfNull = false;
                /* Save the primary page. */
                if (!$primary->save()) {
                    /* Add all primary errors to a non-existent attribute so they will get returned on "this" instance. */
                    $this->addError('*primary', implode('<br>', $primary->firstErrors));
                    /* Throw an exception so the transaction will be rolled back */
                    throw new \Exception('Failed to save primary page as unpublished.');
                }

                /* Commit the transaction */
                $transaction->commit();

                return true;
            } /** @noinspection PhpUndefinedClassInspection */ catch (\Throwable $e) {
                /* Add exception message to model errors so it can be shown as flash alert */
                $this->addError('*exception', $e->getMessage());
                /* If either save above failed and threw an exception, roll back the entire transaction. */
                $transaction->rollBack();

                return false;
            }
        }

        /*
         * Though the try/catch of the transaction returns true/false, the transaction will never have been executed
         * if there were errors in the model.
         */

        return ($this->hasErrors()) ? false : true;
    }

    /*
     * Handle deletion of related records before the page is deleted.
     *
     * If the page being deleted in a primary page, deleted all page_tags related to primary or restore points then
     * delete restore points. If the page being deleted is a restore point, only delete the related page_tags
     */
    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }

        /* Start transaction to prevent errors from causing abandoned records */
        $transaction = Page::getDb()->beginTransaction();
        try {
            /* Logic differs between deleting a primary page and a restore point */
            if ($this->primary_id == null) {
                /*
                 * This is a primary page, page and page restore point page_tags need to be deleted as well as page restore points.
                 */

                /* Delete page tags for primary page and restore points (comparing expected to delete to ensure success). */
                $pages[] = $this->id;
                $pages = array_merge($pages, ArrayHelper::getColumn($this->pageRestorePoints()->asArray()->all(), 'id'));
                $countExpect = PageTag::find()->where(['page_id' => $pages])->count();
                $countActual = PageTag::deleteAll(['page_id' => $pages]);
                if ($countActual != $countExpect) {
                    throw new \Exception('Failed to delete all related page tags.');
                }

                /* Delete page restore points */
                $restorepoints = ArrayHelper::getColumn($this->pageRestorePoints()->asArray()->all(), 'id');
                $countExpect = Page::find()->where(['id' => $restorepoints])->count();
                $countActual = Page::deleteAll(['id' => $restorepoints]);
                if ($countActual != $countExpect) {
                    throw new \Exception('Failed to delete all page restore points.');
                }

            } else {
                /*
                 * This is a page restore point, only the related page_tags need to be deleted.
                 */

                /* Delete page tags for this page restore point (comparing expected to delete to ensure success). */
                $countExpect = PageTag::find()->where(['page_id' => $this->id])->count();
                $countActual = PageTag::deleteAll(['page_id' => $this->id]);
                if ($countActual != $countExpect) {
                    throw new \Exception('Failed to delete all page tags related to this restore point.');
                }
            }

            /* Commit the transaction */
            $transaction->commit();

            return true;
        } /** @noinspection PhpUndefinedClassInspection */ catch (\Throwable $e) {
            /* Add exception message to model errors so it can be shown as flash alert */
            $this->addError('*exception', $e->getMessage());
            /* If either save above failed and threw an exception, roll back the entire transaction. */
            $transaction->rollBack();

            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            /**
             * If a primary page is not specified and _createprimaryIfNull has not been changed to false, create one.
             * This must happen after validation to ensure that if validation of the page restore point fails, a primary
             * is not left existing without the restore point that prompted its creation in the first place.
             */
            if (empty($this->primary_id) && $this->_createPrimaryIfNull) {
                /* Create a new primary, preventing the creation of its own primary which would start an infinite loop */
                $primary = new Page();
                $primary->_createPrimaryIfNull = false;
                $primary->save();
                /* Set the primary_id on the new page */
                $this->primary_id = $primary->id;
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * After page save has successfully completed, set the page tags for this page
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        /* Remove existing tags */
        $this->unlinkAll('tags', true);
        /* Save new tags */
        foreach ($this->tags as $id){
            $this->link('tags', Tag::findOne($id));
        }
    }

    /**
     * Generate AND set the slug for the page. Only proceeds if title is set. Will loop through slug generation,
     * appending incremental numbers until a nique slug is found (based on validating the slug attribute).
     */
    public function generateAndSetSlug()
    {
        /* Only create a slug if title is not blank */
        if (strlen($this->title) > 0) {
            /* Create initial slug based on title */
            $this->slug = Inflector::slug($this->title);

            /* Validate the slug */
            if (!$this->validate('slug')) {
                /* Slug failed validation, make it unique */
                $baseSlug = $this->slug;
                $iteration = 0;
                while (!$this->validate('slug')) {
                    $iteration++;
                    $this->slug = $baseSlug . '-' . ($iteration + 1);
                }
            }
        }
    }

    /**
     * Custom validation for the slug based on the full url path. This means that not only will the slug be validated
     * against other slugs but against the entire path that includes any additional values in the URL structure.
     *
     * Yii 2.0.11 expects three parameters for validate methods: $attribute, $params, $validator
     *
     * @param $attribute
     * @param $params
     */
    public function validateSlug(
        $attribute,
        /** @noinspection PhpUnusedParameterInspection */
        $params
    ) {
        /*
         * Search for pages (published) with the current url route. Explicitly using FrontendPageSearch since it
         * already includes all of the necessary logic to check only published, primary pages. When validating that a
         * slug is not already in use, that is all that truly matters anyway.
         */
        $searchModel = new FrontendPageSearch(['url_path' => Url::toRoute($this->route())]);
        $dataProvider = $searchModel->search(null);

        /*
         * Depending on when validation is executed (either from editing page restore point or unpublishing a primary
         * page), this validation could be happening on a restore point or the primary page. Only the primary page is
         * checked for slug validation so if this is a restore point, the primary_id is used. If this is the primary
         * page, then its own ID will be used.
         */
        $page_id = isset($this->primary_id) ? $this->primary_id : $this->id;

        /* Count of existing pages with this url route */
        $count = $dataProvider->getTotalCount();
        /* Whether or not the primary page is in the list of  */
        $isSelf = in_array($page_id, $dataProvider->getKeys());

        /*
         * If more than one (should never happen) OR exactly one and page is not itself (the only truly accepted
         * possibility), throw error.
         */
        if ($count > 1 || ($count == 1 && !$isSelf)) {
            $this->addError($attribute,
                'Slug "' . $this->slug . '" has already been taken according to the configured URL structure.');
        }
    }
}
