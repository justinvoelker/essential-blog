<?php
namespace app\modules\base\models;

use Yii;
use yii\base\Model;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            [
                'email',
                'exist',
                'targetClass' => '\app\modules\base\models\User',
                'filter' => ['status' => User::STATUS_ENABLED],
                'message' => 'There is no user with that email.'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ENABLED,
            'email' => $this->email,
        ]);

        if (!$user) {
            return false;
        }

        if (!User::isSecurityTokenValid($user->security_token)) {
            $user->generateSecurityToken();
        }

        /*
         * Blameable Behavior needs to be detached so updated_by can be explicitly set.
         * Since the user is not logged in, updated_by would be set to NULL when the record is saved/updated.
         * The assumption is made that the user requested their own password reset since updated_by must be set to something.
         */
        $user->detachBehavior('BlameableBehavior');
        $user->updated_by = $user->id;

        if (!$user->save()) {
            return false;
        }

        $blog_title = Setting::getSetting('blog_title');
        $support_email = Setting::getSetting('support_email_address');

        $subject = $blog_title . ' Password Reset Request';

        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'passwordReset-html', 'text' => 'passwordReset-text'],
                ['user' => $user]
            )
            ->setFrom([$support_email => $blog_title . ' Support'])
            ->setTo($this->email)
            ->setSubject($subject)
            ->send();
    }
}
