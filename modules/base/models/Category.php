<?php

namespace app\modules\base\models;

use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\web\NotFoundHttpException;

/**
 *
 */
class Category extends BaseCategory
{
    /**
     * Replace each piece of the URL structure with the appropriate piece of data.
     *
     * Only replaces variable values (text surrounded with %) leaving static values (text without %) to return null
     * which leaves them as a string of static text in the URL.
     *
     * @param $piece
     * @return int|string
     */
    public function urlStructureValue($piece)
    {
        $result = null;
        switch ($piece) {
            case '%slug%':
                $result = ($this->slug) ? $this->slug : '';
                break;
            case '%id%':
                $result = $this->id;
                break;
        }

        return $result;
    }

    /**
     * Get the route for a category link (as array of url pieces).
     *
     * Example: A category with slug "new-category" and a url structure of "/category/%slug%" returns:
     *
     * [
     *     "/frontend/site/category",
     *     "category" => NULL,
     *     "slug" => "new-category"
     * ]
     *
     * The first item in the returned array is the absolute route to the action. The second item is the string
     * "category" (since it is a key with no value). The third item is the query parameter "slug" with "new-category"
     * as its value.
     *
     * @return mixed
     */
    public function route()
    {
        /* Get setting for category URL structure */
        $urlStructure = Setting::getSetting('url_structure_category');

        /* Remove the first slash. With an opening slash, the first array item will be blank. */
        $urlPieces = explode('/', substr($urlStructure, 1));

        /* Route starts with controller/action */
        $route[0] = '/frontend/site/category';

        /* Append each piece url structure to the route being built */
        foreach ($urlPieces as $urlPiece) {
            $route[str_replace('%', '', $urlPiece)] = $this->urlStructureValue($urlPiece);
        }

        return $route;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
            'SluggableBehavior' => [
                'class' => SluggableBehavior::className(),
                'attribute' => ['name'],
                'immutable' => true,
                'ensureUnique' => true,
            ],
        ];
    }

    /**
     * Finds the category model based on its primary key value.
     *
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public static function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested category does not exist.');
        }
    }

    /**
     * Primary posts that reference this category.
     *
     * Example usage: On category view page, show all primary posts using this category.
     *
     * @return \yii\db\ActiveQuery
     */
    public function primaryPosts()
    {
        return $this->hasMany(Post::className(), ['category_id' => 'id'])
            ->andWhere(['primary_id' => null]);
    }

    /**
     * Primary pages that reference this category.
     *
     * Example usage: On category view page, show all primary pages using this category.
     *
     * @return \yii\db\ActiveQuery
     */
    public function primaryPages()
    {
        return $this->hasMany(Page::className(), ['category_id' => 'id'])
            ->andWhere(['primary_id' => null]);
    }

    /**
     * Before deletion of a category, check for primary posts.
     *
     * If the count of primary posts referencing this category is 0 (zero), deletion is allowed to continue.
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            /* Prevent delete if any posts or pages are using this category */
            if ($this->primary_posts_count != 0 || $this->primary_pages_count != 0) {
                return false;
            }

            return true;
        } else {
            return false;
        }
    }
}
