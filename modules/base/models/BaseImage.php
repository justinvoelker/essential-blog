<?php

namespace app\modules\base\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%image}}".
 *
 * @property integer $id
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 * @property string $alternative_text
 * @property string $title
 * @property string $path
 *
 * @property User $createdBy
 * @property User $updatedBy
 */
class BaseImage extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%image}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        /* Max image size in MB multiplied by 1024^2 to get bytes */
        $maxImageSize = Setting::getSetting('maximum_upload_size') * 1048576;

        return [
            [['alternative_text'], 'required'],
            [['alternative_text', 'title'], 'string', 'max' => 255],
            [['path'], 'unique', 'message' => 'Image already exists.'],
            [['uploadedFiles'], 'image', 'extensions' => 'jpg, png, gif', 'maxFiles' => 0, 'maxSize' => $maxImageSize],
            [['title'], 'default'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'alternative_text' => 'Alternative Text',
            'title' => 'Title',
            'path' => 'Path',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
