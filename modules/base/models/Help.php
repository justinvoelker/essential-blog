<?php

namespace app\modules\base\models;

use app\modules\backend\widgets\Modal;
use yii\base\BaseObject;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Markdown;

/**
 * Help button and modal
 *
 * @author Justin Voelker <justin@justinvoelker.com>
 */
class Help extends BaseObject
{

    public static function button()
    {
        return Html::label('<i class="fa fa-fw fa-question-circle" aria-label="Help"></i>'
            , 'help-modal'
            , ['class' => 'action-bar__list-item-button']
        );
    }

    public static function modal()
    {

        /* Controller/action are the default path for the help title and path*/
        $controller = \Yii::$app->controller->id;
        $action = \Yii::$app->controller->action->id;

        /* Set title either from params or by building it from controller/action */
        if (isset(\Yii::$app->view->params['helpTitle'])) {
            $helpTitle =  \Yii::$app->view->params['helpTitle'];
        } else {
            $helpTitle = Inflector::humanize($controller) . '/' . Inflector::humanize($action) . ' Help';
        }

        /* Set path either from params or by building it from controller/action */
        if (isset(\Yii::$app->view->params['helpPath'])) {
            $helpPath = \Yii::$app->view->params['helpPath'];
        } else {
            $helpPath = '/' . $controller . '/' . $action;
        }

        /* Filesystem path to help file that should exist */
        $helpFile = \Yii::$app->modules['admin']->basePath . '/help' . $helpPath . '.md';

        /* If actual help file exists, parse it. Else, error */
        if (is_readable($helpFile)) {
            $helpContent = Markdown::process(\Yii::$app->controller->renderFile($helpFile), 'gfm-comment');
        } else {
            $helpContent = 'No help exists for ' . $helpPath;
        }

        /* Display help modal*/
        Modal::begin([
            'id' => 'help-modal',
            'title' => $helpTitle,
        ]);
        echo $helpContent;
        Modal::end();
    }
}
