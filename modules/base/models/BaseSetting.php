<?php

namespace app\modules\base\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%setting}}".
 *
 * @property string $id
 * @property string $value
 */
class BaseSetting extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%setting}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['id', 'safe'],
            ['value', 'default'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'value' => 'Value',
        ];
    }
}
