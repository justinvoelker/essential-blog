<?php

namespace app\modules\base\models;

use yii\base\BaseObject;

/**
 * This is the model class for a post archive which represents both the individual archive page as well as a link to
 * that archive.
 *
 * @property string $year
 * @property string $month
 * @property string $day
 * @property string $level
 * @property string $format
 * @property string $date
 * @property string $ID
 * @property string $title
 * @property string $url
 */
class Archive extends BaseObject
{
    public $year;
    public $month;
    public $day;
    public $level;
    public $format;

    const LEVEL_YEAR = 'year';
    const LEVEL_MONTH = 'month';
    const LEVEL_DAY = 'day';

    const FORMAT_YEAR = 'Y';
    const FORMAT_MONTH = 'F Y';
    const FORMAT_DAY = 'F d, Y';

    public function __construct($options = [], $config = [])
    {
        /* Set local year, month, and day from parameters */
        $this->year = isset($options['year']) ? $options['year'] : null;
        $this->month = isset($options['month']) ? $options['month'] : null;
        $this->day = isset($options['day']) ? $options['day'] : null;

        /* Set level based on year, month, and day parameters as provided */
        if (isset($this->year)) {
            $this->level = Archive::LEVEL_YEAR;
        }
        if (isset($this->month)) {
            $this->level = Archive::LEVEL_MONTH;
        }
        if (isset($this->day)) {
            $this->level = Archive::LEVEL_DAY;
        }
        /* Immediately override level if it was set as an option */
        $this->level = isset($options['level']) ? $options['level'] : $this->level;

        /* Set format based on level */
        if ($this->level == Archive::LEVEL_YEAR) {
            $this->format = Archive::FORMAT_YEAR;
        }
        if ($this->level == Archive::LEVEL_MONTH) {
            $this->format = Archive::FORMAT_MONTH;
        }
        if ($this->level == Archive::LEVEL_DAY) {
            $this->format = Archive::FORMAT_DAY;
        }
        /* Immediately override format if it was set as an option */
        $this->format = isset($options['format']) ? $options['format'] : $this->format;

        parent::__construct($config);
    }

    /**
     * Timestamp substituting January and day 01 based on level
     * @return bool
     */
    public function getDate()
    {
        $year = $this->year;
        $month = ($this->level == Archive::LEVEL_MONTH || $this->level == Archive::LEVEL_DAY) ? $this->month : '01';
        $day = ($this->level == Archive::LEVEL_DAY) ? $this->day : '01';

        return mktime(0, 0, 0, $month, $day, $year);
    }

    /**
     * Date formatted as YYYYMMDD for use, if needed, identifying uniqueness of this Archive in a list
     * @return bool
     */
    public function getID()
    {
        return date('Ymd', $this->date);
    }

    /**
     * Formatted date for use as link title
     * @return bool
     */
    public function getTitle()
    {
        return date($this->format, $this->date);
    }


    /**
     * URL of archive, only containing year, month, and day if set
     * @return array
     */
    public function getUrl()
    {
        // Start of URL
        $url = ['/frontend/site/archive'];

        // Year is always present
        $url['year'] = date("Y", $this->date);

        // If level is month or day, include month in the URL
        if ($this->level == Archive::LEVEL_MONTH || $this->level == Archive::LEVEL_DAY) {
            $url['month'] = date("m", $this->date);
        }
        // If level is day, include it in the URL
        if ($this->level == Archive::LEVEL_DAY) {
            $url['day'] = date("d", $this->date);
        }

        return $url;
    }
}
