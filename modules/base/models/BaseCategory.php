<?php

namespace app\modules\base\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%category}}".
 *
 * @property integer $id
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 * @property string $name
 * @property string $slug
 * @property string $image
 * @property string $description
 *
 * @property User $createdBy
 * @property User $updatedBy
 * @property Page[] $pages
 * @property Post[] $posts
 *
 * @property integer $primary_pages_count
 * @property integer $primary_posts_count
 */
class BaseCategory extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'slug'], 'required'],
            [['description'], 'string'],
            [['name', 'slug', 'image'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['slug'], 'unique'],
            [['image', 'description'], 'default'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'name' => 'Name',
            'slug' => 'Slug',
            'image' => 'Image',
            'description' => 'Description',
            'primary_pages_count' => 'Page Count',
            'primary_posts_count' => 'Post Count',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * Defines the relationship to Page. Used in search model's join statement.
     *
     * Method name must contain "get" prefix to be used in search model's join statement.
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPages()
    {
        return $this->hasMany(Page::className(), ['category_id' => 'id']);
    }

    /**
     * Defines the relationship to Post. Used in search model's join statement.
     *
     * Method name must contain "get" prefix to be used in search model's join statement.
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Post::className(), ['category_id' => 'id']);
    }

    /**
     * Getter method for the primary_pages_count. Calculated in search model for use in sorting.
     *
     * Method name must contain "get" prefix to be used as class property.
     *
     * @return integer
     */
    public function getPrimary_pages_count()
    {
        return $this->hasMany(Page::className(), ['category_id' => 'id'])
            ->andWhere(['primary_id' => null])
            ->count();
    }

    /**
     * Getter method for the primary_posts_count. Calculated in search model for use in sorting.
     *
     * Method name must contain "get" prefix to be used as class property.
     *
     * @return integer
     */
    public function getPrimary_posts_count()
    {
        return $this->hasMany(Post::className(), ['category_id' => 'id'])
            ->andWhere(['primary_id' => null])
            ->count();
    }
}
