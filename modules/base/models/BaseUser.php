<?php

namespace app\modules\base\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property integer $id
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 * @property integer $active_at
 * @property string $first_name
 * @property string $last_name
 * @property string $username
 * @property string $password_hash
 * @property string $security_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property string $short_biography
 * @property string $profile_photo
 * @property string $two_factor_authentication
 *
 * @property User $createdBy
 * @property Category[] $categories
 * @property Image[] $images
 * @property Injection[] $injections
 * @property User[] $invitedUsers
 * @property Page[] $pages
 * @property Post[] $posts
 * @property Tag[] $tags
 * @property UserAuthConnection[] $userAuthConnections
 */
class BaseUser extends ActiveRecord
{
    const STATUS_DISABLED = 1;
    const STATUS_INVITED = 5;
    const STATUS_ENABLED = 9;

    /*
     * @var string Password as entered by the user which will need to be hashed and stored in password_hash.
     */
    public $password;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            /* Allow invited users to have role and email updated without requiring name and username */
            [['first_name', 'last_name', 'username'], 'required', 'when' => function($model) {
                /* @var $model User */
                return $model->status != self::STATUS_INVITED;
            }, 'whenClient' => "function (attribute, value) {
                return $('[name=\"_user-status\"]').val() != '".self::STATUS_INVITED."';
            }"],
            [['email', 'auth_key', 'status', 'role'], 'required'],
            [['status'], 'integer'],
            [['short_biography', 'profile_photo', 'password'], 'string'],
            [['short_biography', 'profile_photo'], 'default'],
            [['first_name', 'last_name', 'username', 'email'], 'string', 'max' => 255],
            [['password_hash', 'security_token'], 'string', 'max' => 64],
            [['auth_key'], 'string', 'max' => 32],
            [['email'], 'email'],
            [['email'], 'unique'],
            [['username'], 'unique'],
            [['role'], 'in', 'range' => array_keys(User::allRoles())],
            [['security_token'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'active_at' => 'Active At',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'username' => 'Username',
            'password_hash' => 'Password Hash',
            'security_token' => 'Security Token',
            'email' => 'Email',
            'auth_key' => 'Auth Key',
            'status' => 'Status',
            'short_biography' => 'Short Biography',
            'profile_photo' => 'Profile Photo',
            'two_factor_authentication'=> 'Two-Factor Authentication',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(Image::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInjections()
    {
        return $this->hasMany(Injection::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvitedUsers()
    {
        return $this->hasMany(User::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPages()
    {
        return $this->hasMany(Page::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Post::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserAuthConnections()
    {
        return $this->hasMany(UserAuthConnection::className(), ['user_id' => 'id']);
    }
}
