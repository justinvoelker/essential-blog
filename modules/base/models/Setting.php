<?php

namespace app\modules\base\models;

use app\modules\backend\helpers\DataHelper;
use Yii;
use yii\base\BootstrapInterface;
use yii\base\InvalidValueException;
use yii\caching\TagDependency;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;

/**
 *
 */
class Setting extends BaseSetting implements BootstrapInterface
{
    /**
     * This controls the actual encryption/decryption on write/read and adds an indicator icon to the label (performed
     * in actionIndex in SettingController). Though the encryption/decryption and label indicator are automatically
     * added, these settings need to be manually updated on their respective setting group pages to use a password
     * input and/or custom password wiget which allows the toggling of input type to show/hide the value.
     *
     * @var array List of settings that will be encrypted in the database.
     */
    const ENCRYPTED_SETTINGS = [
        'authclient_facebook_secret',
        'authclient_google_secret',
        'mail_smtp_password',
    ];

    /**
     * Bootstrap method to be called during application bootstrap stage.
     * Loads all the settings into the Yii::$app->params array
     *
     * http://stackoverflow.com/a/27782895/3344794
     * http://www.yiiframework.com/doc-2.0/guide-caching-data.html#query-caching
     *
     * @param \yii\web\Application $app the application currently running
     */
    public function bootstrap($app)
    {
        /* Empty array of settings must always exist */
        Yii::$app->params['settings'] = [];

        /*
         * The settings table will not exist when initial migrations are running. This is in place to avoid errors
         * when the boostrap process attempts to run when not settings exist to bootstrap
         */
        $tableSchema = Yii::$app->db->schema->getTableSchema('{{%setting}}');
        if ($tableSchema !== null) {

            /*
             * Retrieve settings from database (or cache), decrypt (if necessary), and add to the "params" array.
             */

            /* Get settings from database (or cache if available) */
            $resultSettings = Setting::getDb()->cache(function () {
                return Setting::find()->asArray()->all();
            }, 0, new TagDependency(['tags' => ['settings']]));

            /* Format settings results as array of id=>value */
            $settings = ArrayHelper::map($resultSettings, 'id', 'value');

            /* Now load the settings into the global params array */
            foreach ($settings as $id => $value) {
                Yii::$app->params['settings'][$id] = $value;
            }

            /*
             * Perform additional setup based on settings such as updating urlManager, set mailer configuration, set
             * auth client configurations, and timezone.
             */

            /*
             * URL Manager
             * Order matters! Keep these in the same order as shown on the settings page for simplicity and consistency.
             * When a frontend URL is parsed, the first matching rule found is used. Meaning, if, for example, tag and
             * category both have URL structure set to "/%slug%" and the URL is "example.com/new" only the tag model
             * will be used. Even if a "new" tag does not exist but a "new" category does. This is because %slug% is
             * simply a route parameter and the first rule that matches that pattern is for tag. Maybe, possibly,
             * someday, some logic can be implemented that will search subsequent models if a model is not found for
             * the given route but that is largely unnecessary. Something should be used to differentiate the various
             * record types in their URL structures. Otherwise, post could be given a structure of /%slug% and all
             * content could be created as posts.
             */

            /* Add urlManager route for post */
            $url_structure_post = $this->replaceURLPattern(Setting::getSetting('url_structure_post'));
            Yii::$app->getUrlManager()->addRules([
                $url_structure_post => 'frontend/site/post',
            ]);

            /* Add urlManager routes for tag */
            $url_structure_tag = $this->replaceURLPattern(Setting::getSetting('url_structure_tag'));
            Yii::$app->getUrlManager()->addRules([
                $url_structure_tag => 'frontend/site/tag',
            ]);

            /* Add urlManager routes for category */
            $url_structure_category = $this->replaceURLPattern(Setting::getSetting('url_structure_category'));
            Yii::$app->getUrlManager()->addRules([
                $url_structure_category => 'frontend/site/category',
            ]);

            /* Add urlManager route for page */
            $url_structure_page = $this->replaceURLPattern(Setting::getSetting('url_structure_page'));
            Yii::$app->getUrlManager()->addRules([
                $url_structure_page => 'frontend/site/page',
            ]);

            /*
             * Mailer
             */

            /* Set mailer configuration based on delivery method setting */
            $mail_delivery_method = Setting::getSetting('mail_delivery_method');
            $mailer_config = [];
            /* All mailer options using Mailer class from swiftmailer */
            $mailer_config['class'] = 'yii\swiftmailer\Mailer';
            if ($mail_delivery_method == 'file') {
                /* Method is "file" - Save emails as files */
                $mailer_config['useFileTransport'] = true;
            } else {
                /* Sending emails via php mail() or SMTP */
                $mailer_config['useFileTransport'] = false;
                if ($mail_delivery_method == 'smtp') {
                    /* Defining a transport is what separates mail() from SMTP */
                    $mailer_config['transport'] = [
                        'class' => 'Swift_SmtpTransport',
                        'host' => Setting::getSetting('mail_smtp_host'),
                        'username' => Setting::getSetting('mail_smtp_username'),
                        'password' => Setting::getSetting('mail_smtp_password'),
                        'port' => Setting::getSetting('mail_smtp_port'),
                        'encryption' => Setting::getSetting('mail_smtp_encryption'),
                    ];
                }
            }
            Yii::$app->setComponents(['mailer' => $mailer_config]);

            /*
             * Auth Clients
             */

            /* Set authClientCollection clients based on populated settings */
            $authClientCollection_clients = [];
            /* Facebook */
            if (($authclient_facebook_id = Setting::getSetting('authclient_facebook_id')) !== null
                && ($authclient_facebook_secret = Setting::getSetting('authclient_facebook_secret')) !== null
            ) {
                $authClientCollection_clients['facebook'] = [
                    'class' => 'yii\authclient\clients\Facebook',
                    'clientId' => $authclient_facebook_id,
                    'clientSecret' => $authclient_facebook_secret,
                ];
            }
            /* Google */
            if (($authclient_google_id = Setting::getSetting('authclient_google_id')) !== null
                && ($authclient_google_secret = Setting::getSetting('authclient_google_secret')) !== null
            ) {
                $authClientCollection_clients['google'] = [
                    'class' => 'yii\authclient\clients\Google',
                    'clientId' => $authclient_google_id,
                    'clientSecret' => $authclient_google_secret,
                ];
            }
            /* If any clients are set, set the config */
            if (!empty($authClientCollection_clients)) {
                $authClientCollection_config = [];
                $authClientCollection_config['class'] = 'yii\authclient\Collection';
                $authClientCollection_config['clients'] = $authClientCollection_clients;
                Yii::$app->setComponents(['authClientCollection' => $authClientCollection_config]);
            }

            /*
             * Timezone
             */

            /* Add timezone if valid, else UTC */
            $time_zone_is_valid = in_array(Setting::getSetting('time_zone'), timezone_identifiers_list());
            Yii::$app->timeZone = ($time_zone_is_valid) ? Setting::getSetting('time_zone') : 'UTC';
        }

        /* Figure out why Gii breaks when CSRF is enabled. Only un-comment this when using Gii */
        //Yii::$app->request->enableCsrfValidation = false;
    }

    /**
     * Replace each piece of the URL structure with the appropriate pattern to match that data. Only selected patterns
     * are replaced. Any other pattern will be left as-is (e.g. "/post/%slug%" will only replace "%slug%" and will leave
     * "post" alone as static data.
     * @param $urlStructure
     * @return mixed
     */
    public function replaceURLPattern($urlStructure)
    {
        $result = $urlStructure;

        /*
         * Basic replacement values
         */
        $result = str_replace("%year%", "<year:\d{4}>", $result);
        $result = str_replace("%month%", "<month:\d{2}>", $result);
        $result = str_replace("%day%", "<day:\d{2}>", $result);
        /*
         * Default value when specifying <slug> would produce a regex that allows everything except forward slashes.
         * The value below allows forward slashes. The forward slash may be used for pages where a pseudo-page-heiarchy
         * is desired such as in "example.com/about-us/john-doe." Using a * rather than + allows blanks which is needed
         * for showing initial/empty slug previews on new post/page pages.
         */
        $result = str_replace("%slug%", "<slug:.*>", $result);
        $result = str_replace("%id%", "<id:\d+>", $result);

        return $result;
    }

    /**
     * Save an individual setting, encrypting the value if necessary.
     *
     * @param $id
     * @param $value
     * @return bool
     */
    public static function saveSetting($id, $value)
    {
        /* Encrypt and encode if necessary */
        if (in_array($id, Setting::ENCRYPTED_SETTINGS)) {
            $value = DataHelper::encryptForStorage($value);
        }

        /* Find existing model or create new instance */
        if (($model = Setting::findOne($id)) === null) {
            $model = new Setting();
            $model->id = $id;
        }

        /* Set the value */
        $model->value = $value;

        return $model->save();
    }

    /**
     * The only appropriate way to retrieve the value for a given setting. Only pulls cached values, decrypts values
     * that need to be decrypted, returns default values if setting values is not in the cached settings (meaning it
     * does not exist in the database).
     *
     * @param $key
     * @return string
     */
    public static function getSetting($key)
    {
        /* Start with a null value in case the requested key is not a valid setting */
        $value = null;

        /* If this setting exists in the params array of database settings, use that value. Otherwise, default value. */
        if (array_key_exists($key, Yii::$app->params['settings'])) {
            $value = Yii::$app->params['settings'][$key];
            if (in_array($key, self::ENCRYPTED_SETTINGS)) {
                $value = DataHelper::decryptFromStorage($value);
            }
        } else {
            /* Pull the default value from a new instance of SettingForm */
            $settingForm = new SettingForm();
            if ($settingForm->hasProperty($key)) {
                $value = $settingForm->$key;
            } else {
                throw new InvalidValueException("Setting '$key' does not exist.");
            }
        }


        return $value;
    }

    /**
     * Delete deprecated settings.
     *
     * This method will find all settings that exist in the database but do not exist as attributes of the SettingForm
     * model and will delete them from the database.
     */
    public static function deleteDeprecatedSettings()
    {
        /* Retrieve list of settings from the database saving only the IDs */
        $settings = Setting::find()->all();
        $settingsIds = ArrayHelper::getColumn($settings, 'id');

        /* Retrieve list of attributes from the SettingForm model saving only the attribute IDs */
        $settingsForm = new SettingForm();
        $settingsFormIds = array_keys($settingsForm->attributes);

        /* Calculate list of deprecated settings (those that exist in the database but not the SettingForm model) */
        $settingsInvalid = array_diff($settingsIds, $settingsFormIds);

        /* If more than one deprecated setting exists, delete it */
        if (count($settingsInvalid) > 0) {
            Setting::deleteAll(['in', 'id', $settingsInvalid]);
        }
    }

    /**
     * Retrieve list of themes by reading themes directory on filesystem.
     *
     * Example usage: Settings page that lists all available themes available for selection.
     *
     * @return array All themes found in themes directory
     */
    public static function getThemes()
    {
        // Set themes directory
        $themeDir = \Yii::getAlias('@webroot/themes');

        // Get contents of directory except for .. and .
        $themes = array_diff(scandir($themeDir), array('.', '..', '.gitignore'));

        $result = [];
        // Each directory in the themes directory is a theme
        foreach ($themes as $theme) {
            $result[$theme] = Inflector::id2camel($theme);
        }

        return $result;
    }

    /**
     * Return list of PHP time zones, broken down into sub-array by region (https://gist.github.com/Xeoncross/1204255)
     *
     * Example usage: Install or settings page for selection of blog timezone (determines dates/times on frontend).
     *
     * @return array Timezones broken into sub-arrays by region
     */
    public static function getTimeZones()
    {
        // Regions
        $regions = [
            'Africa' => \DateTimeZone::AFRICA,
            'America' => \DateTimeZone::AMERICA,
            'Antarctica' => \DateTimeZone::ANTARCTICA,
            'Arctic' => \DateTimeZone::ARCTIC,
            'Asia' => \DateTimeZone::ASIA,
            'Atlantic' => \DateTimeZone::ATLANTIC,
            'Australia' => \DateTimeZone::AUSTRALIA,
            'Europe' => \DateTimeZone::EUROPE,
            'Indian' => \DateTimeZone::INDIAN,
            'Pacific' => \DateTimeZone::PACIFIC,
            'UTC' => \DateTimeZone::UTC
        ];

        // Timezones within each region
        $result = [];
        foreach ($regions as $region => $mask) {
            $timezones = \DateTimeZone::listIdentifiers($mask);
            foreach ($timezones as $timezone) {
                // Lets sample the time there right now
                $time = new \DateTime(null, new \DateTimeZone($timezone));
                // Remove region name and add a sample time
                //$result[$region][$timezone] = substr($timezone, strlen($region) + 1) . ' (' . $time->format('H:i') . ')';
                $result[$region][$timezone] = $timezone . ' (' . $time->format('g:i a') . ')';
            }
        }

        return $result;
    }

    /**
     * Return list of pre-defined admin color schemes.
     *
     * Color names are postfixed with the intensity number from Google's Material Design color palette here:
     * https://material.io/guidelines/style/color.html#color-color-palette. Postfix number added for two reasons:
     * 1) PhpStorm was displaying a color block in the margin when editing the SCSS that was not the correct color
     * (and it was just confusing) and 2) Adding the postfix number will make adding more colors later (if desired)
     * much easier.
     *
     * @return array Color schemes
     */
    public static function getAdminColorSchemes()
    {
        return [
            'red-700' => 'Red 700',
            'pink-700' => 'Pink 700',
            'purple-700' => 'Purple 700',
            'deep-purple-700' => 'Deep Purple 700',
            'indigo-700' => 'Indigo 700',
            'blue-700' => 'Blue 700',
            'light-blue-700' => 'Light Blue 700',
            'cyan-700' => 'Cyan 700',
            'teal-700' => 'Teal 700',
            'green-700' => 'Green 700',
            'light-green-700' => 'Light Green 700',
            'lime-700' => 'Lime 700',
            'yellow-700' => 'Yellow 700',
            'amber-700' => 'Amber 700',
            'orange-700' => 'Orange 700',
            'deep-orange-700' => 'Deep Orange 700',
        ];
    }

    /**
     * Clear existing cached settings and retrieve updated values from database, caching the new values.
     *
     * Example usage: When settings are updated (either in admin interface or installer), settings are cached so that
     * future settings references are pulled from quicker cache, not database.
     */
    public static function refreshCache()
    {
        // Invalidate current cache with 'settings' tag
        TagDependency::invalidate(Yii::$app->cache, ['settings']);

        // Execute and cache (with 'settings' tag) the query that retrieves all settings as array
        Setting::getDb()->cache(function () {
            return Setting::find()->asArray()->all();
        }, 0, new TagDependency(['tags' => ['settings']]));
    }
}
