<?php

namespace app\modules\base\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\caching\TagDependency;
use yii\web\NotFoundHttpException;

/**
 *
 */
class Injection extends BaseInjection
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
        ];
    }

    /**
     * Finds the Injection model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Injection the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public static function findModel($id)
    {
        if (($model = Injection::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested injection does not exist.');
        }
    }

    /**
     * Text representation of the action integer of a record
     */
    public function actionText()
    {
        return self::$actions[$this->action];
    }

    /**
     * Text representation of the parser integer of a record
     */
    public function parserText()
    {
        return self::$parsers[$this->parser];
    }

    /**
     * Text representation of the status integer of a record
     */
    public function statusText()
    {
        return self::$statuses[$this->status];
    }

    /**
     * Whether or not the PHP can be successfully parsed.
     */
    public function parseSuccessPhp()
    {
        /* Assume successful parsing */
        $result = true;

        /* Only try parsing PHP if this injections is meant to be parsed as PHP */
        if ($this->parser == self::PARSER_PHP) {
            /* Try parsing as PHP, setting result to false if it fails */
            try {
                eval($this->content);
            } catch (\Throwable $t) {
                $result = false;
            }
        }

        return $result;
    }

    /**
     * Whether or not there exists at least one warning condition.
     *
     * @return boolean
     */
    public function hasWarning()
    {
        return (!$this->parseSuccessPhp());
    }


    /**
     * Clear the cached injections and retrieve updated values from database.
     */
    public static function refreshCache()
    {
        // Invalidate current cache with 'injections' tag
        TagDependency::invalidate(Yii::$app->cache, ['injections']);

        // Execute and cache (with 'injections' tag) the query that retrieves all active injections as array
        Injection::getDb()->cache(function () {
            return Injection::find()->asArray()->all();
        }, 0, new TagDependency(['tags' => ['injections']]));
    }

    /**
     * After the record is saved, refresh the cache.
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        /* Call parent implementation of afterSave() */
        parent::afterSave($insert, $changedAttributes);

        /* Refresh cached Injections */
        self::refreshCache();
    }

    /**
     * After a record is deleted, refresh the cache.
     */
    public function afterDelete()
    {
        /* Call parent implementation of afterDelete() */
        parent::afterDelete();

        /* Refresh cached Injections */
        self::refreshCache();
    }

    /**
     * Set the status of the injection to enabled. Cache will be updated via afterSave.
     */
    public function updateStatusEnable()
    {
        /* Set the status to enabled and save the record */
        $this->status = self::STATUS_ENABLED;
        $this->save();
    }

    /**
     * Set the status of the injection to disabled. Cache will be updated via afterSave.
     */
    public function updateStatusDisable()
    {
        /* Set the status to enabled and save the record */
        $this->status = self::STATUS_DISABLED;
        $this->save();
    }
}
