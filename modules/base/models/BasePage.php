<?php

namespace app\modules\base\models;

use app\helpers\DateAndTime;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%page}}".
 *
 * @property integer $id
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 * @property integer $published_by
 * @property integer $published_at
 * @property integer $revised_by
 * @property integer $revised_at
 * @property string $title
 * @property string $slug
 * @property string $excerpt
 * @property string $content
 * @property integer $scheduled_for
 * @property integer $category_id
 * @property string $image
 * @property integer $status
 * @property integer $primary_id
 * @property integer $is_locked
 *
 * @property User $createdBy
 * @property User $updatedBy
 * @property User $publishedBy
 * @property User $revisedBy
 * @property Category $category
 * @property PageTag[] $pageTags
 * @property Tag[] $tags
 *
 * @property integer $publish_date
 * @property integer $publish_year
 * @property integer $publish_month
 * @property integer $publish_day
 * @property integer $title_revised
 */
class BasePage extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        /**
         * Some values, such as is_locked, are set to numbers when manually creating a record and a string when
         * set via the form. Removing the "strict" would allow that to be valid, but, then values such as "aaa"
         * would validate to true since they contain data (or so it seems was happening). For now, just allow numbers
         * and string values of 0 and 1 and leave "strict" in place.
         */
        return [
            [['title', 'slug'], 'string', 'max' => 255],
            [['primary_id'], 'safe'],
            [['title', 'slug', 'excerpt', 'content', 'scheduled_for', 'category_id', 'image', 'status'], 'default'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['tags'], 'each', 'rule' => ['exist', 'targetClass' => Tag::className(), 'targetAttribute' => ['tags' => 'id']]],
            [['scheduled_for'], 'date', 'format' => 'php:Y-m-d H:i', 'timestampAttribute' => 'scheduled_for'],
            [['slug'], 'validateSlug'],
            [['status'], 'default', 'value' => 0],
            [['status'], 'in', 'range' => [0, 1, "0", "1"], 'strict' => true],
            [['is_locked'], 'default', 'value' => 0],
            [['is_locked'], 'in', 'range' => [0, 1, "0", "1"], 'strict' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'published_by' => 'Published By',
            'published_at' => 'Published At',
            'revised_by' => 'Revised By',
            'revised_at' => 'Revised At',
            'title' => 'Title',
            'slug' => 'Slug',
            'excerpt' => 'Excerpt',
            'content' => 'Content',
            'scheduled_for' => 'Scheduled For',
            'category_id' => 'Category',
            'image' => 'Image',
            'status' => 'Status',
            'primary_id' => 'Primary ID',
            'is_locked' => 'Is Locked',
            /* Related models */
            'tags' => 'Tags',
            /* Custom attributes */
            'publish_date' => 'Publish Date',
            'publish_year' => 'Publish Year',
            'publish_month' => 'Publish Month',
            'publish_day' => 'Publish Day',
            'title_revised' => 'Title Revised',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublishedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'published_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRevisedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'revised_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageTags()
    {
        return $this->hasMany(PageTag::className(), ['page_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])->viaTable('{{%page_tag}}', ['page_id' => 'id']);
    }

    /**
     * @param $value
     */
    public function setTags($value)
    {
        /* Ensure that the value is at least an empty array */
        $this->tags = !empty($value) ? $value : [];
    }

    /**
     * Getter method for the publish_date as calculated within PageSearch.
     */
    public function getPublish_date()
    {
        if ($this->scheduled_for) {
            return DateAndTime::toTimestamp($this->scheduled_for);
        } elseif ($this->published_at) {
            return DateAndTime::toTimestamp($this->published_at);
        } else {
            return time();
        }
    }

    /**
     * Getter method for the publish_year as calculated within PageSearch.
     */
    public function getPublish_year()
    {
        return date('Y', $this->publish_date);
    }

    /**
     * Getter method for the publish_month as calculated within PageSearch.
     */
    public function getPublish_month()
    {
        return date('m', $this->publish_date);
    }

    /**
     * Getter method for the publish_day as calculated within PageSearch.
     */
    public function getPublish_day()
    {
        return date('d', $this->publish_date);
    }

    /**
     * Getter method for the title_revised as calculated within PageSearch.
     *
     * Used to display/sort page title from newest restore point on page index.
     */
    public function getTitle_revised()
    {
        /* @var $page Page */
        $page = Page::find()->where(['primary_id' => $this->id])->orderBy(['created_at' => SORT_DESC])->one();

        return $page->title;
    }
}
