<?php

namespace app\modules\base\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%injection}}".
 *
 * @property integer $id
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 * @property string $name
 * @property string $xpath
 * @property integer $action
 * @property string $content
 * @property integer $parser
 * @property integer $order
 * @property integer $status
 *
 * @property User $createdBy
 * @property User $updatedBy
 */
class BaseInjection extends ActiveRecord
{
    /*
     * Constants for referencing actions, parsers, and statuses
     */
    const ACTION_INSERT_BEFORE_ELEMENT = 1;
    const ACTION_INSERT_AFTER_ELEMENT = 2;
    const ACTION_PREPEND_TO_CONTENT = 3;
    const ACTION_APPEND_TO_CONTENT = 4;
    const ACTION_REPLACE_CONTENT = 5;
    const ACTION_REMOVE_CONTENT = 6;
    const ACTION_REPLACE_ELEMENT = 7;
    const ACTION_REMOVE_ELEMENT = 8;
    const PARSER_NONE = 1;
    const PARSER_MARKDOWN = 2;
    const PARSER_PHP = 3;
    const STATUS_DISABLED = 0;
    const STATUS_ENABLED = 1;

    /**
     * List of possible actions
     */
    public static $actions = [
        '1' => 'Insert before element',
        '2' => 'Insert after element',
        '3' => 'Prepend to content',
        '4' => 'Append to content',
        '5' => 'Replace content',
        '6' => 'Remove content',
        '7' => 'Replace element',
        '8' => 'Remove element',
    ];

    /**
     * List of possible parsers
     */
    public static $parsers = [
        '1' => 'Do not parse',
        '2' => 'Markdown',
        '3' => 'PHP',
    ];

    /**
     * List of possible statuses
     */
    public static $statuses = [
        '0' => 'Disabled',
        '1' => 'Enabled',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%injection}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'xpath', 'action', 'parser', 'status'], 'required'],
            [['action', 'parser', 'order', 'status'], 'integer'],
            [['name', 'xpath'], 'string', 'max' => 255],
            [['content'], 'string'],
            [['content'], 'default'],
            [['action'], 'in', 'range' => array_keys(self::$actions)],
            [['parser'], 'default', 'value' => 1],
            [['parser'], 'in', 'range' => array_keys(self::$parsers)],
            [['status'], 'default', 'value' => self::STATUS_ENABLED],
            [['status'], 'in', 'range' => array_keys(self::$statuses)],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'name' => 'Name',
            'xpath' => 'Xpath',
            'action' => 'Action',
            'content' => 'Content',
            'parser' => 'Parser',
            'order' => 'Order',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
