<?php

namespace app\modules\base\models;

use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 *
 */
class Image extends BaseImage
{
    /**
     * Instance of the ImageFile for this Image
     *
     * @return ImageFile
     */
    public function imageFile()
    {
        return new ImageFile($this->path);
    }

    /**
     * The responsive image tag.
     *
     * @param array $options the tag options in terms of name-value pairs. The following options are specially handled:
     *
     * - sizes: string, the value associated with the sizes attribute of a responsive image tag. Any occurrences of
     *   `%width%` will be replaced with the width of the original image. If not provided, sizes will be set to
     *   `(max-width: %width%px) 100vw, %width%px`. However, sizes should be included to ensure sidebars, fixed-width
     *   pages, etc. are properly calculated. Sizes will be theme-dependant!
     * - isAmp: boolean, used to toggle the tag as being `img` or `amp-img`.
     *
     * The rest of the options will be rendered as the attributes of the resulting tag. The values will
     * be HTML-encoded using [[Html::encode()]]. If a value is null, the corresponding attribute will not be rendered.
     *
     * @return string
     */
    public function imgTag($options = [])
    {
        /* Set default sizes if not specified */
        $sizes = isset($options['sizes']) ? $options['sizes'] : '(max-width: %width%px) 100vw, %width%px';
        unset($options['sizes']);

        /* Set isAmp to false if not specified */
        $isAmp = isset($options['isAmp']) ? $options['isAmp'] : false;
        unset($options['isAmp']);

        /* Image alt text from model if not provided */
        $options['alt'] = isset($options['alt']) ? $options['alt'] : $this->alternative_text;

        /* Image title from model if not provided */
        $options['title'] = isset($options['title']) ? $options['title'] : $this->title;

        /* Remove title attribute if empty */
        if (strlen($options['title']) == 0) {
            unset($options['title']);
        }

        /* If the path is not null (image not found) and file does not exist, display a very basic img tag */
        if ($this->imageFile()->path == null) {
            return Html::img('', $options);
        } elseif (!$this->imageFile()->exists) {
            return Html::img($this->path, $options);
        } else {
            /* Otherwise, the file exists, file data can be read and the regular img tag can be created */

            /* Start with a list of all image variations */
            $variationImageFiles = $this->imageFile()->variationImageFiles();

            /* The largest responsive image width settings */
            $max = (integer) max(explode(',', Setting::getSetting('image_responsive_widths')));

            /* Include the original image if its width is less than the largest responsive image width settings */
            if ($this->imageFile()->width < $max) {
                array_push($variationImageFiles, $this->imageFile());
            }

            /* Ensure images are sorted largest width to smallest */
            ArrayHelper::multisort($variationImageFiles, 'width', SORT_DESC);

            /* Smallest responsive image (to be used as default src). Fallback to actual image if no variations. */
            if (count($variationImageFiles) > 0) {
                $smallest_img = $this->pathEscape($variationImageFiles[count($variationImageFiles) - 1]->path);
            } else {
                $smallest_img = $this->pathEscape($this->imageFile()->path);
            }

            /* Largest width (for replacement in sizes attribute). Fallback to actual image width if no variations. */
            if (count($variationImageFiles) > 0) {
                $largest_width = $variationImageFiles[0]->width;
            } else {
                $largest_width = $this->imageFile()->width;
            }

            /* Only add sizes and srcset attribute if variations exist */
            if (count($variationImageFiles) > 0) {
                /* In sizes option, replace %width% with largest image width */
                $options['sizes'] = str_replace("%width%", $largest_width, $sizes);

                /* Create srcset from all variations */
                $srcsetArray = [];
                foreach ($variationImageFiles as $variationImageFile) {
                    $srcsetArray[] = $this->pathEscape($variationImageFile->path) . ' ' . $variationImageFile->width . 'w';
                }
                $options['srcset'] = implode(', ', $srcsetArray);
            }

            /*
             * Need to provide a width and height for amp pages AND to ensure images with a width smaller than
             * specified sizes do not expand beyond their max width.
             */
            $options['width'] = $this->imageFile()->width;
            $options['height'] = $this->imageFile()->height;

            /* Change result based on isAmp */
            if ($isAmp) {
                /* Need to replicate what Html::img is doing to set the src attribute */
                $options['src'] = Url::to($smallest_img);
                return Html::tag('amp-img', '', $options);
            } else {
                return Html::img($smallest_img, $options);
            }
        }
    }

    /**
     * Escape a given path so it can be used in a url (essentially, replace spaces with %20).
     *
     * @param string $path
     * @return string
     */
    private function pathEscape($path)
    {
        return str_replace(' ', '%20', $path);
    }

    /**
     * Finds the image model based on its path.
     *
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param string $path
     * @return Image the loaded model
     */
    public static function findModelByPath($path)
    {
        if (strlen($path) > 0 && ($model = Image::findOne(['path' => $path])) !== null) {
            return $model;
        } else {
            return new Image();
        }
    }

    /**
     * When images are submitted for upload, they need to be stored in a property so that they can be validated.
     *
     * @var \yii\web\UploadedFile[]
     */
    public $uploadedFiles;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
        ];
    }

    /**
     * Finds the Image model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Image the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public static function findModel($id)
    {
        if (($model = Image::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested image does not exist.');
        }
    }

    /**
     * Before validation, use attributes of uploaded file to set properties of image instance (for new records).
     *
     * @return bool
     */
    public function beforeValidate()
    {
        /* Call parent implementation to invoke events */
        parent::beforeValidate();

        /*
         * Upload logic is only executed before saving new records which should have data in uploadedFiles. Also,
         * uploading variations will populate uploadedFiles and this logic will be set, but, the model will never be
         * saved so it does not matter that the alternative_text and path are being set here and could be changed.
         */
        if (!empty($this->uploadedFiles)) {

            /* jQuery File Upload submits individual requests so there is only ever one item in "uploadedFiles" */
            $uploadedFile = $this->uploadedFiles[0];

            /* Set alternative_text as uploaded file baseName (filename without file extension) */
            $this->alternative_text = $uploadedFile->baseName;

            /* Set path with directory including year and month and uploaded filename */
            $this->path = '/uploads/' . gmdate("Y") . '/' . gmdate("m") . '/' . $uploadedFile->name;
        }

        return true;
    }


    /**
     * After image save has successfully completed, upload the actual image file.
     *
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        /* Call parent implementation to invoke events */
        parent::afterSave($insert, $changedAttributes);

        /* Upload and resize logic is only performed for new records */
        if ($insert) {
            /* Upload the original file */
            if ($this->imageFile()->upload($this->uploadedFiles[0])) {
                /* If upload was successful, create image variations or add errors/warnings as appropriate */
                $this->imageFile()->createVariations();
            } else {
                $this->addError('*error', 'Image failed to upload. See error log for details.');
            }
        }
    }
}
