<?php

namespace app\modules\base\models;

use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\web\NotFoundHttpException;

/**
 *
 */
class Tag extends BaseTag
{
    /**
     * Replace each piece of the URL structure with the appropriate piece of data.
     *
     * Only replaces variable values (text surrounded with %) leaving static values (text without %) to return null
     * which leaves them as a string of static text in the URL.
     *
     * @param $piece
     * @return int|string
     */
    public function urlStructureValue($piece)
    {
        $result = null;
        switch ($piece) {
            case '%slug%':
                $result = ($this->slug) ? $this->slug : '';
                break;
            case '%id%':
                $result = $this->id;
                break;
        }

        return $result;
    }

    /**
     * Get the route for a tag link (as array of url pieces).
     *
     * Example: A tag with slug "new-tag" and a url structure of "/tag/%slug%" returns:
     *
     * [
     *     "/frontend/site/tag",
     *     "tag" => NULL,
     *     "slug" => "new-tag"
     * ]
     *
     * The first item in the returned array is the absolute route to the action. The second item is the string
     * "tag" (since it is a key with no value). The third item is the query parameter "slug" with "new-tag"
     * as its value.
     *
     * @return mixed
     */
    public function route()
    {
        /* Get setting for category URL structure */
        $urlStructure = Setting::getSetting('url_structure_tag');

        /* Remove the first slash. With an opening slash, the first array item will be blank. */
        $urlPieces = explode('/', substr($urlStructure, 1));

        /* Route starts with controller/action */
        $route[0] = '/frontend/site/tag';

        /* Append each piece url structure to the route being built */
        foreach ($urlPieces as $urlPiece) {
            $route[str_replace('%', '', $urlPiece)] = $this->urlStructureValue($urlPiece);
        }

        return $route;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
            'SluggableBehavior' => [
                'class' => SluggableBehavior::className(),
                'attribute' => ['name'],
                'immutable' => true,
                'ensureUnique' => true,
            ],
        ];
    }

    /**
     * Finds the tag model based on its primary key value.
     *
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return Tag the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public static function findModel($id)
    {
        if (($model = Tag::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested tag does not exist.');
        }
    }

    /**
     * Primary posts that reference this tag.
     *
     * Example usage: On tag view page, show all primary posts using this tag.
     *
     * @return \yii\db\ActiveQuery
     */
    public function primaryPosts()
    {
        return $this->hasMany(Post::className(), ['id' => 'post_id'])
            ->viaTable('{{%post_tag}}', ['tag_id' => 'id'])
            ->andWhere(['primary_id' => null]);
    }

    /**
     * Primary pages that reference this tag.
     *
     * Example usage: On tag view page, show all primary pages using this tag.
     *
     * @return \yii\db\ActiveQuery
     */
    public function primaryPages()
    {
        return $this->hasMany(Page::className(), ['id' => 'page_id'])
            ->viaTable('{{%page_tag}}', ['tag_id' => 'id'])
            ->andWhere(['primary_id' => null]);
    }

    /**
     * Before deletion of a tag, check for primary posts.
     *
     * If the count of primary posts referencing this tag is 0 (zero), deletion is allowed to continue.
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {

            /* Prevent delete if any posts or pages are using this tag */
            if ($this->primary_posts_count != 0 || $this->primary_pages_count != 0) {
                return false;
            }

            /* Delete all post_tag records that relate this tag to posts */
            $countExpect = PostTag::find()->where(['tag_id' => $this->id])->count();
            $countActual = PostTag::deleteAll(['tag_id' => $this->id]);
            if ($countActual != $countExpect) {
                return false;
            }

            return true;
        } else {
            return false;
        }
    }
}
