<?php

namespace app\modules\base\models;

use app\modules\backend\helpers\DataHelper;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Settings form
 *
 * @property string $admin_color_scheme
 * @property string $admin_items_per_page
 * @property string $allow_indexing
 * @property string $authclient_facebook_id
 * @property string $authclient_facebook_secret
 * @property string $authclient_google_id
 * @property string $authclient_google_secret
 * @property string $autosave_seconds
 * @property string $blog_tagline
 * @property string $blog_theme
 * @property string $blog_title
 * @property string $frontend_items_per_page
 * @property string $image_responsive_widths
 * @property string $mail_delivery_method
 * @property string $mail_smtp_encryption
 * @property string $mail_smtp_host
 * @property string $mail_smtp_password
 * @property string $mail_smtp_port
 * @property string $mail_smtp_username
 * @property string $maximum_upload_size
 * @property string $restore_points_enabled
 * @property string $support_email_address
 * @property string $time_zone
 * @property string $url_structure_category
 * @property string $url_structure_page
 * @property string $url_structure_post
 * @property string $url_structure_tag
 * @property string $user_security_token_expiration
 * @property string $user_session_duration
 */
class SettingForm extends Model
{
    const ENCRYPTED_FIELD_INDICATOR = '<i class="fa fa-fw fa-lock" aria-hidden="true"></i>';

    public $admin_color_scheme = 'blue-700';
    public $admin_items_per_page = '10';
    public $allow_indexing = 'true';
    public $authclient_facebook_id = null;
    public $authclient_facebook_secret = null;
    public $authclient_google_id = null;
    public $authclient_google_secret = null;
    public $autosave_seconds = '10';
    public $blog_tagline = 'Blogging: Never before have so many people with so little to say said so much to so few.';
    public $blog_theme = null;
    public $blog_title = 'My New Blog';
    public $frontend_items_per_page = '10';
    public $image_responsive_widths = '1140,940,720,480,240,120';
    public $mail_delivery_method = 'file';
    public $mail_smtp_encryption = null;
    public $mail_smtp_host = null;
    public $mail_smtp_password = null;
    public $mail_smtp_port = null;
    public $mail_smtp_username = null;
    public $maximum_upload_size = '100';
    public $restore_points_enabled = 'true';
    public $support_email_address = null;
    public $time_zone = 'UTC';
    public $url_structure_category = '/category/%slug%';
    public $url_structure_page = '/%slug%';
    public $url_structure_post = '/%year%/%month%/%slug%';
    public $url_structure_tag = '/tag/%slug%';
    public $user_security_token_expiration = '600';
    public $user_session_duration = '0';

    public function rules()
    {
        return [
            /* Admin color scheme */
            ['admin_color_scheme', 'required'],
            ['admin_color_scheme', 'in', 'range' => array_keys(Setting::getAdminColorSchemes())],

            /* Admin items per page */
            ['admin_items_per_page', 'required'],
            ['admin_items_per_page', 'integer', 'min' => 1],

            /* Allow search engine indexing */
            ['allow_indexing', 'required'],
            ['allow_indexing', 'boolean', 'trueValue' => "true", 'falseValue' => "false", 'strict' => true],

            /* Authorization client (Facebook) ID */
            ['authclient_facebook_id', 'default', 'value' => null],

            /* Authorization client (Facebook) secret */
            ['authclient_facebook_secret', 'default', 'value' => null],

            /* Authorization client (Google) ID */
            ['authclient_google_id', 'default', 'value' => null],

            /* Authorization client (Google) secret */
            ['authclient_google_secret', 'default', 'value' => null],

            /* Autosave seconds */
            ['autosave_seconds', 'required'],
            ['autosave_seconds', 'integer', 'min' => 1],

            /* Blog tagline */
            ['blog_tagline', 'required'],

            /* Blog theme */
            ['blog_theme', 'default', 'value' => null],
            ['blog_theme', 'in', 'range' => array_keys(Setting::getThemes())],

            /* Blog title */
            ['blog_title', 'required'],

            /* Frontend items per page */
            ['frontend_items_per_page', 'required'],
            ['frontend_items_per_page', 'integer', 'min' => 1],

            /* Image responsive widths */
            ['image_responsive_widths', 'required'],

            /* Mail delivery method */
            ['mail_delivery_method', 'default', 'value' => null],
            ['mail_delivery_method', 'in', 'range' => ['mail', 'smtp', 'file']],

            /* Mail, SMTP encryption */
            ['mail_smtp_encryption', 'default', 'value' => null],
            ['mail_smtp_encryption', 'in', 'range' => ['ssl', 'tls']],

            /* Mail, SMTP host */
            ['mail_smtp_host', 'default', 'value' => null],

            /* Mail, SMTP password */
            ['mail_smtp_password', 'default', 'value' => null],

            /* Mail, SMTP port */
            ['mail_smtp_port', 'default', 'value' => null],
            ['mail_smtp_port', 'integer'],

            /* Mail, SMTP username */
            ['mail_smtp_username', 'default', 'value' => null],

            /* Maximum upload size */
            ['maximum_upload_size', 'required'],
            ['maximum_upload_size', 'integer', 'min' => 1],

            /* Restore points enabled */
            ['restore_points_enabled', 'required'],
            ['restore_points_enabled', 'boolean', 'trueValue' => "true", 'falseValue' => "false", 'strict' => true],

            /* Support email address */
            ['support_email_address', 'required'],
            ['support_email_address', 'email'],

            /* Time zone */
            ['time_zone', 'required'],
            ['time_zone', 'in', 'range' => \DateTimeZone::listIdentifiers()],

            /* URL structure, category */
            ['url_structure_category', 'required'],
            ['url_structure_category', 'string'],

            /* URL structure, page */
            ['url_structure_page', 'required'],
            ['url_structure_page', 'string'],

            /* URL structure, post */
            ['url_structure_post', 'required'],
            ['url_structure_post', 'string'],

            /* URL structure, tag */
            ['url_structure_tag', 'required'],
            ['url_structure_tag', 'string'],

            /* User security token expiration */
            ['user_security_token_expiration', 'required'],
            ['user_security_token_expiration', 'integer'],

            /* User session duration */
            ['user_session_duration', 'required'],
            ['user_session_duration', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'admin_color_scheme' => 'Color Scheme',
            'admin_items_per_page' => 'Items Per Page',
            'allow_indexing' => 'Allow search engine indexing',
            'authclient_facebook_id' => 'Facebook Client ID',
            'authclient_facebook_secret' => 'Facebook Client Secret',
            'authclient_google_id' => 'Google Client ID',
            'authclient_google_secret' => 'Google Client Secret',
            'autosave_seconds' => 'Autosave Seconds',
            'blog_tagline' => 'Blog Tagline',
            'blog_theme' => 'Theme',
            'blog_title' => 'Blog Title',
            'frontend_items_per_page' => 'Items Per Page',
            'image_responsive_widths' => 'Responsive Image Widths',
            'mail_delivery_method' => 'Delivery method',
            'mail_smtp_encryption' => 'Encryption',
            'mail_smtp_host' => 'Host',
            'mail_smtp_password' => 'Password',
            'mail_smtp_port' => 'Port',
            'mail_smtp_username' => 'Username',
            'maximum_upload_size' => 'Maximum Upload Size (MB)',
            'restore_points_enabled' => 'Enable Restore Points',
            'support_email_address' => 'Support Email Address',
            'time_zone' => 'Time Zone',
            'url_structure_category' => 'Category URL Structure',
            'url_structure_page' => 'Page URL Structure',
            'url_structure_post' => 'Post URL Structure',
            'url_structure_tag' => 'Tag URL Structure',
            'user_security_token_expiration' => 'User Security Token Expiration',
            'user_session_duration' => 'Session Duration',
        ];
    }

    /**
     * When the SettingForm class is instantiated, it needs to be populated with the setting values as they exist in
     * the database. This ensures that there is no chance for an out-of-date values to be cached and carried forward or
     * for direct database edits to be missed.
     *
     * Since all properties of this class are declared with defauly values, if a given setting does not exist in the
     * database, it will end up with the default value. Only values set in the database will overwrite the defaults.
     *
     * @param  array $config name-value pairs that will be used to initialize the object properties
     */
    public function __construct($config = [])
    {
        /* Retrieve all settings directly from the database */
        $settings = Setting::find()->asArray()->all();

        /* Extract just the id and value from the settings as key/value pairs */
        $settings = ArrayHelper::map($settings, 'id', 'value');

        /* For each setting, populate this model with data */
        foreach ($settings as $id => $value) {
            /* Only set attribute value if the attribute exists. (Database may have deprecated setting.) */
            if ($this::hasProperty($id)) {
                /* Set the value exactly as defined in the database. */
                $this->$id = $value;
                /* Decrypt that value if necessary */
                if (in_array($id, Setting::ENCRYPTED_SETTINGS) && $value != null) {
                    $this->$id = DataHelper::decryptFromStorage($value);
                }
            }
        }

        parent::__construct($config);
    }

    /*
     * Overridden so that a lock can be appended to encrypted settings. Note, by default, all labels are encoded. In
     * order for this lock icon to be appropriately displayed, the label must be manually specified as follows:
     * `$form->field($model, 'attribute')->textInput()->label($model->getAttributeLabel('attribute'))`
     */
    public function getAttributeLabel($attribute)
    {
        /* Start with label as determined by parent implementation */
        $label = parent::getAttributeLabel($attribute);

        /* Add indicator icon to encrypted fields */
        if (in_array($attribute, Setting::ENCRYPTED_SETTINGS)) {
            $label = $label . self::ENCRYPTED_FIELD_INDICATOR;
        }

        return $label;
    }

    /**
     * Save the settings as posted to the database. In the event that a value does not already exist in the database,
     * it will be created as a new record. Deprecated settings are also removed from the database if found.
     */
    public function saveSettings()
    {
        /* Save each setting */
        foreach ($this->attributes as $id => $value) {
            Setting::saveSetting($id, $value);
        }

        /* Delete any deprecated settings that may exist in the database but not this SettingForm model */
        Setting::deleteDeprecatedSettings();

        /* Refresh the settings cache */
        Setting::refreshCache();

        return true;
    }
}
