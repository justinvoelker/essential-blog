<?php

namespace app\modules\backend;

use app\helpers\DateAndTime;
use app\modules\base\models\Setting;
use app\modules\base\models\User;
use yii;
use yii\base\Module as YiiBaseModule;
use yii\web\Cookie;

/**
 * Backend module definition class
 *
 * The Backend module contains all of the class definitions for administrative interaction.
 *
 * Models: Only model properties and methods that are used EXCLUSIVELY for administrative functionality are included
 * in the Backend module's models. If a property or method is necessary for the Frontend, it will be included in the
 * Frontend module. All properties and methods included in the Frontend module are accessible within the Backend module
 * as the Backend models extend the Frontend models.
 *
 * Views: All views for administrative functionality.
 *
 * Controllers: All controllers for administrative functionality.
 */
class Module extends YiiBaseModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\backend\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        /*
         * Use error action found within administrative interface rather than default. The default is set within
         * config/web.php and is an action found within frontend module.
         */
        Yii::$app->errorHandler->errorAction = 'admin/default/error';

        /* As long as the user is not a guest ... */
        if (!Yii::$app->user->isGuest) {
            /* ... find the logged in user */
            $user = User::findOne(\Yii::$app->user->id);
            /* ... update that users' last active timestamp */
            $user->updateActiveAt();
            /*
             * ... switch to a new identity cookie with the currently configured session duration setting. Without
             * updating this identity, if session duration was set to 1 month at the time a user logged in but then was
             * changed to be 1 day, that user would still remain logged in for 1 month. Updating this identity on each
             * request ensures that the user will only stay logged in for the duration set in the session duration
             * setting. Since we cannot compare the current identity duration to the setting, it just needs to be
             * updated/switchd/set on each request.
             */
            $user_session_duration = Setting::getSetting('user_session_duration');
            Yii::$app->user->switchIdentity($user, $user_session_duration);
            /*
             * ... update long-lasting admin_user_exists cookie that exists so analytics can ignore views from any
             * device where an admin user exists -> http://andrewmiguelez.com/exclude-yourself-from-google-analytics/
             */
            $cookie = new Cookie([
                'name' => 'admin_user_exists',
                'value' => true,
                'expire' => time() + (86400 * 365), /* one year */
                'path' => '/',
            ]);
            Yii::$app->response->cookies->add($cookie);
        }
    }
}
