<?php

namespace app\modules\backend\widgets;

use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\widgets\InputWidget;

/**
 * SelectImageInput generates an image selection input.
 *
 * SelectImageInput transforms a standard text attribute into a hidden input for storing the path of a given image.
 * In addition to the hidden input, an image preview is generated along with buttons to select and clear the image.
 *
 * To use MaskedInput, you should set the [[sizes]] property to indicate the sizes property of the responsive image tag.
 * The following example shows how to use SelectImageInput to select an image:
 *
 * ```php
 * echo SelectImageInput::widget([
 *     'name' => 'header_image',
 *     'sizes' => '(max-width: 543px) calc((100vw * .75) - 30px), 370px',
 * ]);
 * ```
 *
 * You can also use this widget in an [[yii\widgets\ActiveForm|ActiveForm]] using the [[yii\widgets\ActiveField::widget()|widget()]]
 * method, for example like this:
 *
 * ```php
 * <?= $form->field($model, 'image', [
 *     'options' => ['class' => 'form-group form-group-image'],
 *     'template' => "{label}\n{hint}\n{error}\n{input}",
 * ])->widget(\app\modules\backend\widgets\SelectImageInput::className(), [
 *     'sizes' => '(max-width: 543px) calc((100vw * .75) - 30px), 370px'
 * ]) ?>
 * ```
 *
 * Note in the above that the template is not necessary but recommended to keep the label, hint, and error all above the
 * rendered image preview and buttons (which replace the input). The addition of the form-group-image class is so that
 * styles can be added (such as the following) to shrink spacing between elements.
 *
 * ```css
 * .form-group-image label { margin-bottom: 0; }
 * .form-group-image .hint-block { margin-top: 0; margin-bottom: 5px; }
 * .form-group-image .help-block { display: none; }
 * .form-group-image .btn-list { margin-top: 5px; }
 * .form-group-image .btn-list .btn { margin-right: 5px; }
 * ```
 */
class SelectImageInput extends InputWidget
{
    /**
     * @var string the sizes attribute to be passed to the image tag for responsive images
     */
    public $sizes = '100vw';
    /**
     * @var array the HTML attributes for the input tag.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = ['class' => 'form-control'];

    /**
     * @inheritdoc
     */
    public function run()
    {
        $this->registerClientScript();

        if ($this->hasModel()) {
            echo Html::activeInput('hidden', $this->model, $this->attribute, $this->options);
        } else {
            echo Html::input('hidden', $this->name, $this->value, $this->options);
        }

        $id = $this->options['id'];
        echo '<div class="image-preview-container">';
        echo '<div id="' . $id . '-preview" class="image-preview-img"></div>';
        echo '<div class="btn-list">';
        echo Html::label('Select', 'image-modal', [
            'id' => $id . '-change',
            'class' => 'btn btn-scheme',
        ]);
        echo Html::button('Clear', [
            'id' => $id . '-clear',
            'class' => 'btn btn-default clear_image hidden'
        ]);
        echo '</div>'; // .btn-list
        echo '</div>'; // .image-preview-container

    }

    /**
     * Registers the needed client script and options.
     */
    public function registerClientScript()
    {
        $js = '';
        $view = $this->getView();
        $id = $this->options['id'];

        $idCamel = Inflector::id2camel($id);

        // Use input value to retrieve the img tag for the image at that path
        $js .= 'function update' . $idCamel . '() {' . "\n";
        $js .= '    "use strict";' . "\n";
        $js .= '    var $sizes = "' . $this->sizes . '";' . "\n";
        $js .= '    var $path = $("#' . $id . '").val();' . "\n";
        $js .= '    $.ajax({' . "\n";
        $js .= '        url: "/admin/image/img-from-path?path=" + $path + "&sizes=" + $sizes + "&classes=img-responsive",' . "\n";
        $js .= '        type: "POST",' . "\n";
        $js .= '        processData: false,' . "\n";
        $js .= '        contentType: false,' . "\n";
        $js .= '        success: function (data) {' . "\n";
        $js .= '            $("#' . $id . '-preview").html(data.img);' . "\n";
        $js .= '        },' . "\n";
        $js .= '        error: function () {' . "\n";
        $js .= '            $("#' . $id . '-preview").html("Error retrieving image");' . "\n";
        $js .= '        },' . "\n";
        $js .= '        complete: function () {' . "\n";
        $js .= '            $("#' . $id . '-preview").trigger("updated");' . "\n";
        $js .= '        }' . "\n";
        $js .= '    });' . "\n";
        $js .= '}' . "\n";

        // Initial population of image
        $js .= 'if ($("#' . $id . '").val().length > 0) {' . "\n";
        $js .= '    update' . $idCamel . '();' . "\n";
        $js .= '}' . "\n";
        $js .= 'updateButtons' . $idCamel . '();' . "\n";

        // When the select button is clicked, update the trigger-field input in the modal
        $js .= '$("#' . $id . '-change").click(function () {' . "\n";
        $js .= '    "use strict";' . "\n";
        $js .= '    $("#image-modal-trigger-field").val("' . $id . '");' . "\n";
        $js .= '});' . "\n";

        // When the clear button is clicked, clear the input and the preview
        $js .= '$("#' . $id . '-clear").click(function () {' . "\n";
        $js .= '    "use strict";' . "\n";
        $js .= '    $("#' . $id . '").val("");' . "\n";
        $js .= '    $("#' . $id . '-preview").html("");' . "\n";
        $js .= '    $("#' . $id . '-preview").trigger("updated");' . "\n";
        $js .= '    updateButtons' . $idCamel . '();' . "\n";
        $js .= '});' . "\n";

        // When the modal is closed via the accept button, find selected image and insert it into the input
        // If no image was selected, nothing happens
        $js .= '$(".image-modal .modal-accept").click(function (e) {' . "\n";
        $js .= '    "use strict";' . "\n";
        $js .= '    if ($("#image-modal-trigger-field").val() === "' . $id . '") {' . "\n";
        $js .= '        var imgValue = $("#image-modal-selected-image-path").val();' . "\n";
        $js .= '        if (imgValue.length === 0) {' . "\n";
        $js .= '            e.stopPropagation();' . "\n";
        $js .= '        } else {' . "\n";
        $js .= '            $("#' . $id . '").val(imgValue);' . "\n";
        $js .= '            update' . $idCamel . '();' . "\n";
        $js .= '        }' . "\n";
        $js .= '    }' . "\n";
        $js .= '    updateButtons' . $idCamel . '();' . "\n";
        $js .= '});' . "\n";

        // Update select/clear buttons as appropriate
        $js .= 'function updateButtons' . $idCamel . '() {' . "\n";
        $js .= '    "use strict";' . "\n";
        $js .= '    if ($("#' . $id . '").val().length > 0) {' . "\n";
        $js .= '        $("#' . $id . '-change").html("Change")' . "\n";
        $js .= '        $("#' . $id . '-clear").removeClass("hidden")' . "\n";
        $js .= '    } else {' . "\n";
        $js .= '        $("#' . $id . '-change").html("Select")' . "\n";
        $js .= '        $("#' . $id . '-clear").addClass("hidden")' . "\n";
        $js .= '    }' . "\n";
        $js .= '}' . "\n";

        $view->registerJs($js);
    }
}
