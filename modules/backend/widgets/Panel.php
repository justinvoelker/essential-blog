<?php

namespace app\modules\backend\widgets;

use yii\base\Widget;
use yii\helpers\Html;

class Panel extends Widget
{
    /**
     * @var string Header title
     */
    public $title;

    /**
     * @var boolean If panel is collapsable via icon in header
     */
    public $collapsable;

    /**
     * @var boolean If panel is rendered in an already collapsed state
     */
    public $collapsed;

    /**
     * @var array the HTML attributes for the panel.
     */
    public $options = [];

    /**
     * Initializes the widget.
     */
    public function init()
    {
        parent::init();

        $this->initOptions();

        Html::addCssClass($this->options, 'panel');
        if ($this->collapsable) {
            Html::addCssClass($this->options, 'panel_collapsable');
        }
        if ($this->collapsed) {
            Html::addCssClass($this->options, 'panel_minimized');
        }

        echo Html::beginTag('div', $this->options) . "\n";
        echo $this->renderHeader() . "\n";
        echo $this->renderBodyBegin() . "\n";
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        echo "\n" . $this->renderBodyEnd();
//        echo "\n" . $this->renderFooter();
        echo "\n" . Html::endTag('div'); // .panel
    }

    /**
     * Renders the header HTML markup of the modal
     * @return string the rendering result
     */
    protected function renderHeader()
    {
        $iconMinimize = '<i class="fa fa-fw fa-chevron-up" aria-hidden="true"></i>';

        $result = '';
        $result .= Html::beginTag('div', ['class' => 'panel__header']) . "\n";
        $result .= Html::tag('div', $this->title, ['class' => 'panel__title']) . "\n";
        $result .= Html::beginTag('div', ['class' => 'panel__actions']) . "\n";
        if ($this->collapsable) {
            $result .= Html::tag('div', $iconMinimize, ['class' => 'panel__action panel__action-minimize']) . "\n";
        }
        $result .= "\n" . Html::endTag('div'); // .panel__actions
        $result .= "\n" . Html::endTag('div'); // .panel__header

        return $result;
    }

    /**
     * Renders the opening tag of the modal body.
     * @return string the rendering result
     */
    protected function renderBodyBegin()
    {
        $options = [];
        $options['class'] = 'panel__body';

        if ($this->collapsed) {
            $options['style'] = 'display: none;';
        }

        return Html::beginTag('div', $options);
    }

    /**
     * Renders the closing tag of the modal body.
     * @return string the rendering result
     */
    protected function renderBodyEnd()
    {
        return Html::endTag('div');
    }

    /**
     * Renders the HTML markup for the footer of the modal
     * @return string the rendering result
     */
//    protected function renderFooter()
//    {
//        return Html::tag('div', $this->footer, ['class' => 'lightbox__footer']) . "\n";
//    }

    /**
     * Initializes the widget options.
     * This method sets the default values for various options.
     */
    protected function initOptions()
    {
        /* Collapsed is false by default */
        if (!isset($this->collapsed)) {
            $this->collapsed = false;
        }

        /* Collapsable is false by default */
        if (!isset($this->collapsed)) {
            $this->collapsed = false;
        }

    }
}
