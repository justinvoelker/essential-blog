<?php

namespace app\modules\backend\widgets;

use yii\helpers\Html;
use yii\widgets\InputWidget;

/**
 * PasswordInput generates a standard password input with the option to show/hide the password.
 *
 * To be used in an [[yii\widgets\ActiveForm|ActiveForm]] using the [[yii\widgets\ActiveField::widget()|widget()]]
 * method. Example:
 *
 * ```php
 * <?= $form->field($model, 'password')->widget(\app\modules\backend\widgets\PasswordInput::className()) ?>
 * ```

 */
class PasswordInput extends InputWidget
{
    /**
     * @inheritdoc
     */
    public $options = ['class' => 'form-control'];

    /**
     * @inheritdoc
     */
    public function run()
    {
        $this->registerClientScript();

        /* Wrapped input in relatively position div so that show/hide buttons can be position absolutely */
        echo Html::beginTag('div', ['class' => 'password-visibility-container']);

        /* Display actual input */
        if ($this->hasModel()) {
            echo Html::activeInput('password', $this->model, $this->attribute, $this->options);
        } else {
            echo Html::input('password', $this->name, $this->value, $this->options);
        }

        /* Show "button" */
        echo Html::tag('span', 'Show', [
            'class' => 'password-visibility-toggle password-show hidden',
            'style' => 'display:block;'
        ]);
        /* Hide "button" */
        echo Html::tag('span', 'Hide', [
            'class' => 'password-visibility-toggle password-hide hidden',
            'style' => 'display:none;'
        ]);

        /* Close relatively position div */
        echo Html::endTag('div');
    }

    /**
     * Registers the needed client script and options.
     */
    public function registerClientScript()
    {
        /* View is needed for registering JS and CSS */
        $view = $this->getView();

        /* ID is used for targeting the exact field. Allows for multiple password inputs without interference. */
        $id = $this->options['id'];

        /* Form group containing input and password visibility toggles */
        $formGroupClass = 'field-'.$id;

        /* On click of a password-visibility-toggle element, change input type and show/hide corresponding toggles */
        $js = '';
        $js .= '$(".' . $formGroupClass . ' .password-visibility-toggle").click(function () {' . "\n";
        $js .= '    if ($(this).hasClass("password-show")) {' . "\n";
        $js .= '        $(".' . $formGroupClass . ' input").prop("type", "input")' . "\n";
        $js .= '        $(".' . $formGroupClass . ' .password-show").hide()' . "\n";
        $js .= '        $(".' . $formGroupClass . ' .password-hide").show()' . "\n";
        $js .= '    } else {' . "\n";
        $js .= '        $(".' . $formGroupClass . ' input").prop("type", "password")' . "\n";
        $js .= '        $(".' . $formGroupClass . ' .password-hide").hide()' . "\n";
        $js .= '        $(".' . $formGroupClass . ' .password-show").show()' . "\n";
        $js .= '    }' . "\n";
        $js .= '});' . "\n";

        /* On any change of the input value, toggle 'hidden' class of toggles if content exists */
        $js .= '$(".' . $formGroupClass . ' input").on("keyup change", function () {' . "\n";
        $js .= '    $(".' . $formGroupClass . ' .password-visibility-toggle").toggleClass("hidden", $(this).val().length == 0);' . "\n";
        $js .= '});' . "\n";
        /* When page is ready, also peform the check to account for fields with existing data */
        $js .= '$(".' . $formGroupClass . ' .password-visibility-toggle").toggleClass("hidden", $(".' . $formGroupClass . ' input").val().length == 0);' . "\n";

        /* Register JavaScript */
        $view->registerJs($js);

        /* Basic CSS for positioning the container and actual toggles. Styling handled in normal site stylesheets. */
        $css = '';
        $css .= '.password-visibility-container { position: relative; }';
        $css .= '.password-visibility-toggle { position: absolute; bottom: 0; right: 0; }';

        /* Register CSS */
        $view->registerCss($css);
    }
}
