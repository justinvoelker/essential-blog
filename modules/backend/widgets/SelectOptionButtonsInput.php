<?php

namespace app\modules\backend\widgets;

use yii\helpers\Html;
use yii\widgets\InputWidget;

/**
 * SelectOptionButtonsInput generates a hidden dropdown list input with and displays the options as selectable buttons.
 *
 * To be used in an [[yii\widgets\ActiveForm|ActiveForm]] using the [[yii\widgets\ActiveField::widget()|widget()]]
 * method. Example:
 *
 * ```php
 * <?= $form->field($model, 'category_id')->widget(SelectOptionButtonsInput::className(), ['items' => $categoryArray]) ?>
 * ```
 * or
 * ```php
 * <?= $form->field($model, 'tags')->widget(SelectOptionButtonsInput::className(), ['items' => $tagsArray, 'options' => ['class' => 'form-control', 'multiple' => true]]) ?>
 * ```
 */
class SelectOptionButtonsInput extends InputWidget
{
    /**
     * Items available to be used in the hidden dropdown and as buttons.
     */
    public $items = [];

    /**
     * Custom item to be appended to end of items. For example, a label to trigger a modal window.
     */
    public $appendItem;

    /**
     * Items available to be used in the hidden dropdown and as buttons.
     */
    private $_selected = [];

    /**
     * @inheritdoc
     */
    public $options = ['class' => 'form-control hidden'];

    /**
     * @inheritdoc
     */
    public function run()
    {
        $this->registerClientScript();

        /* Display actual input */
        if ($this->hasModel()) {
            echo Html::activeDropDownList($this->model, $this->attribute, $this->items, $this->options);
            $this->_selected = Html::getAttributeValue($this->model, $this->attribute);
        } else {
            echo Html::dropDownList($this->name, $this->value, $this->items, $this->options);
            $this->_selected = $this->value;
        }

        /* Ensure the selected items are represented as an array */
        $this->_selected = is_array($this->_selected) ? $this->_selected : [$this->_selected];

        /* Begin button list */
        echo Html::beginTag('div', ['class' => 'btn-list']);

        /* For each item in the supplied list, display a button, optionally marking it as selected if appropriate. */
        foreach ($this->items as $value => $label) {
            /* Determine if button should indicate the value is selected */
            $class = (in_array($value, $this->_selected)) ? 'btn-scheme select-option__selected' : null;
            /* Display button with appropriate classes and data-value attribute */
            echo Html::button($label, [
                'class' => ['btn', 'select-option', 'btn-default', $class],
                'data' => ['value' => $value],
            ]);
        }

        /* If an append item is specified, such as an add button, append it to the list now */
        if ($this->appendItem) {
            echo $this->appendItem;
        }

        /* Close button list */
        echo Html::endTag('div');
    }

    /**
     * Registers the needed client script and options.
     */
    public function registerClientScript()
    {
        /* View is needed for registering JS */
        $view = $this->getView();

        /* ID is used for targeting the exact field. Allows for multiple select option button inputs without interference. */
        $id = $this->options['id'];

        /* Form group containing input and select option buttons */
        $formGroupClass = 'field-'.$id;

        /* Begin JavaScript */
        $js = '';

        /*
         * On click of a select-option button, change the selected option based on one of three scenarios:
         * 1) Multiple selection allowed. Toggle the 'selected' attribute of the chosen item.
         * 2) Single selection allowed and chosen value != current value. Set the chosen value as selected.
         * 3) Single selection allowed and chosen value == current value. Clear selected value if a default option exists.
         */
        $js .= '$(".' . $formGroupClass . ' .select-option").click(function () {' . "\n";
        $js .= '    if ($("#' . $id . '[multiple]").length) {' . "\n";
        $js .= '        var dataValue = $(this).data("value");' . "\n";
        $js .= '        var select = $("#' . $id . ' option[value="+dataValue+"]")[0];' . "\n";
        $js .= '        select.selected = !select.selected;' . "\n";
        $js .= '    } else {' . "\n";
        $js .= '        if ($(this).data("value") != $("#' . $id . ' option:selected").val()) {' . "\n";
        $js .= '            $("#' . $id . '").val($(this).data("value"))' . "\n";
        $js .= '        } else {' . "\n";
        $js .= '            if ($("#' . $id . ' option[value=\'\'").length > 0) {' . "\n";
        $js .= '                $("#' . $id . '").val("");' . "\n";
        $js .= '            }' . "\n";
        $js .= '        }' . "\n";
        $js .= '    }' . "\n";
        $js .= '    $("#' . $id . '").trigger("change");' . "\n";
        $js .= '});' . "\n";

        /*
         * On change of selected options, update the buttons to show which options are now selected.
         * This is being done on change rather than witin the button click merely to ensure that the intended options
         * are indeed selected. If the button toggled on click but setting the selected option failed, the buttons
         * would reflect inaccurate data. This ensure the buttons only show what is truly set in the data.
         * The check for val length is to eliminate options without a value such as a default/prompt option.
         */
        $js .= '$(".' . $formGroupClass . ' select").on("change", function () {' . "\n";
        $js .= '    $("#' . $id . ' option").each(function() {' . "\n";
        $js .= '        if ($(this).val().length > 0) {' . "\n";
        $js .= '            $(".' . $formGroupClass . ' .select-option[data-value="+$(this).val()+"]").toggleClass("select-option__selected btn-scheme", $(this).is(":selected"))' . "\n";
        $js .= '        }' . "\n";
        $js .= '    });' . "\n";
        $js .= '});' . "\n";

        /* Register JavaScript */
        $view->registerJs($js);
    }
}
