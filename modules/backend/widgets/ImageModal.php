<?php

namespace app\modules\backend\widgets;

use app\assets\JqueryFileUploadAsset;
use app\assets\LazysizesAsset;
use app\modules\base\models\Image;
use \yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 *
 * @author Justin Voelker <justin@justinvoelker.com>
 */
class ImageModal extends Widget
{
    /**
     * @var boolean whether or not image selection is allowed
     * Upload page does not allow selection
     * Settings page allows selection (for blog header image)
     */
    public $selectable = true;

    /*
     * @var string The controller action to which the uploaded images will be sent. The default of '/admin/image/upload'
     * is because all of the image selection widgets also allow upload and this is the action they need to be sent to.
     * Other, unique actions can be set if necessary such as upload-variations or replace-original on the image update
     * page.
     */
    public $actionUrl = ['/admin/image/upload'];

    public $headerText = 'Select Image';

    public $acceptButtonText = 'Insert';
    public $cancelButtonText = 'Cancel';

    public function init()
    {
        parent::init();

        JqueryFileUploadAsset::register($this->view);
        LazysizesAsset::register($this->view);

        // A couple buttons in the footer
        $footer = '';
        if ($this->selectable) {
            $footer .= '<label for="image-modal" class="btn btn-default modal-close">' . $this->cancelButtonText . '</label>';
            $footer .= '<label for="image-modal" class="btn btn-scheme modal-accept">' . $this->acceptButtonText . '</label>';
        } else {
            $footer .= '<label for="image-modal" class="btn btn-scheme modal-close">' . $this->cancelButtonText . '</label>';
        }

        /* Tabbed container class must have 'tabbed' but may also disable those tabs if not selectable */
        $tabbedClasses[] = 'tabbed';
        if (!$this->selectable) {
            $tabbedClasses[] = 'tabbed_hidden-tabs';
        }
        $tabbedClass = implode($tabbedClasses, ' ');

        /* Display image modal*/
        Modal::begin([
            'id' => 'image-modal',
            'title' => $this->headerText,
            'footer' => $footer,
        ]);
        ?>
        <!-- Tab panes - Library is hidden on non-selectable pages -->
        <div class="<?= $tabbedClass ?>">
            <input name="tabbed" id="tabbed-upload" type="radio" class="tabbed__input" value="upload" checked>
            <div class="tabbed__tab">
                <div class="tabbed__header">
                    <label class="tabbed__label" for="tabbed-upload">Upload</label>
                </div>
                <div class="tabbed__content">
                    <div class="image-upload">
                        <div id="drop">
                            <div class="drop-text">Drag and drop here</div>
                            Or, if you prefer...<br>
                            <?= Html::a('Browse to a file', false, ['class' => 'btn btn-scheme']) ?>
                            <?= Html::fileInput('files[]', null, [
                                'id' => 'files',
                                'multiple' => true,
                                'accept' => 'image/*',
                                'data-url' => Url::to($this->actionUrl),
                            ]);?>
                        </div>
                        <ul id="uploaded-files-progress"></ul>
                    </div>
                </div>
            </div>
            <input name="tabbed" id="tabbed-library" type="radio" class="tabbed__input" value="library">
            <div class="tabbed__tab">
                <div class="tabbed__header">
                    <label class="tabbed__label" for="tabbed-library">Library</label>
                </div>
                <div class="tabbed__content">
                    <div class="image-list">
                        <?php
                        if ($this->selectable) {
                            $images = Image::find()
                                ->orderBy('created_at DESC, id DESC')
                                ->all();
                            foreach ($images as $image) {
                                /* @var $image \app\modules\base\models\Image */
                                $img = $image->imgTag(['class' => 'img-responsive center-and-crop square-50 lazyload', 'sizes' => '50px']);
                                // Make the image tag lazyload
                                $img = str_replace(' sizes=', ' data-sizes=', $img);
                                $img = str_replace(' src=', ' data-src=', $img);
                                $img = str_replace(' srcset=', ' data-srcset=', $img);
                                $img = str_replace('<img', '<img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="', $img);
                                // Display the tag
                                echo Html::tag('div', $img, [
                                    'class' => 'item',
                                    'data' => ['alternative_text' => $image->alternative_text, 'path' => $image->path],
                                ]);
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" id="image-modal-selected-image-alternative_text">
        <input type="hidden" id="image-modal-selected-image-path">
        <input type="hidden" id="image-modal-trigger-field">

        <?php
        Modal::end();

        /* Instruct lazysizes to NOT expand viewing area to load additional images */
        $this->view->registerJs("window.lazySizesConfig = window.lazySizesConfig || {};window.lazySizesConfig.expand = 1;", 1);

        $script = <<< JS
            // PHP variable of whether or not this modal instance is selectable
            var selectable = '$this->selectable';

            /**
             * Uploading of images
             *
             * Heavily customized from http://tutorialzine.com/2013/05/mini-ajax-file-upload-form/
             */

            // Show browse dialog when the button is clicked
            $('#drop a').click(function(){
                // Simulate a click on the file input button to show the file browser dialog
                $(this).parent().find('input').click();
            });

            // Initialize the jQuery File Upload plugin
            //noinspection JSUnusedGlobalSymbols
            $('#files').fileupload({

                // This element will accept file drag/drop uploading
                dropZone: $('#drop'),

                // This function is called when a file is added to the queue;
                // either via the browse button, or via drag/drop:
                add: function (e, data) {

                    var tpl = $('<li class="working"><p><span class="filename"></span><span class="message"></span></p><div class="progress"><div class="progress-bar" style=""></div></div></li>');

                    // Append the file name
                    tpl.find('p span:first-child').text(data.files[0].name);
                    tpl.find('p span:nth-child(2)').text('uploading');

                    // Add the HTML to the UL element
                    data.context = tpl.appendTo( $('.image-modal ul#uploaded-files-progress') );

                    // Automatically upload the file once it is added to the queue
                    var jqXHR = data.submit();
                },

                progress: function(e, data){

                    // Calculate the completion percentage of the upload
                    var progress = parseInt(data.loaded / data.total * 100, 10);

                    // Update the progress bar
                    data.context.find('.progress-bar').attr("style", "width:" + progress + "%");
                    data.context.find('.progress-bar').text(progress + "%");

                    if(progress == 100){
                        data.context.removeClass('working');
                        data.context.find('p span:nth-child(2)').text('processing');
                    }
                },

                fail:function(e, data){
                    // Something has gone wrong!
                    data.context.addClass('error');
                    data.context.find('p span:first-child').append("<br />" + data.errorThrown);
                    data.context.find('p span:nth-child(2)').text('error');
                    data.context.find('.progress-bar').addClass('progress-bar-danger');
                },

                done: function (e, data) {
                    if (data.result.error) {
                        /* Add 'error' class to the entire LI element */
                        data.context.addClass('error');
                        /* Add danger class to the progress bar */
                        data.context.find('.progress-bar').addClass('progress-bar-danger');
                        /* Append the error to the currently-displayed filename */
                        data.context.find('p span:first-child').append("<br />" + data.result.error);
                        /* Change 'processing' text to 'error' */
                        data.context.find('p span:nth-child(2)').text('error');
                    } else {
                        if (data.result.warning) {
                            /* Add 'warning' class to the entire LI element */
                            data.context.addClass('warning');
                            /* Add warning class to the progress bar */
                            data.context.find('.progress-bar').addClass('progress-bar-warning');
                            /* Append the error to the currently-displayed filename */
                            data.context.find('p span:first-child').append("<br />" + data.result.warning);
                            /* Change 'processing' text to 'done' */
                            data.context.find('p span:nth-child(2)').text('done');
                        } else {
                            /* Add success class to the progress bar */
                            data.context.find('.progress-bar').addClass('progress-bar-success');
                            /* Change 'processing' text to 'done' */
                            data.context.find('p span:nth-child(2)').text('done');
                        }
                        // If selectable, there wil be a list of the images that need to be added to/selected from
                        if (selectable) {
                            // Prepend the new image to the existing list
                            $('.image-list').prepend( '<div class="item to-be-selected" data-alternative_text="'+ data.result.alternative_text +'" data-path="'+ data.result.path +'">'+ data.result.img +'</div>' );
                            // Trigger a click on the just-added image to simulate selection of that image...
                            $('.to-be-selected').click().removeClass('to-be-selected');
                            // ... so that it will be the selected image when the modal is closed via the accept button
                            $('.image-modal .modal-accept').click();
                        }
                    }

                }

            }).bind('fileuploadadd', function (e, data) {
                /*
                 * Even though the URL should be set automatically from the data-url attribute, that attribute is being
                 * dynamically changed in some places. For example, the image update page allows the image modal to be
                 * for two different actions: replace-original and upload-variations. This ensures that if the data-url
                 * attribute changed dynamically, it is pulled each time a file is added to the upload, not necessarily
                 * only when the page first loads.
                 */
                data.url = $('input#files').attr('data-url');
            });

            // Prevent the default action when a file is dropped on the window
            $(document).on('drop dragover', function (e) {
                e.preventDefault();
            });

            // Add some styling for dragover event
            $(document).bind('dragover', function (e) {
                var dropZone = $('#drop'),
                    timeout = window.dropZoneTimeout;
                if (!timeout) {
                    dropZone.addClass('in');
                } else {
                    clearTimeout(timeout);
                }
                var found = false,
                    node = e.target;
                do {
                    if (node === dropZone[0]) {
                        found = true;
                        break;
                    }
                    node = node.parentNode;
                } while (node != null);
                if (found) {
                    dropZone.addClass('hover');
                } else {
                    dropZone.removeClass('hover');
                }
                window.dropZoneTimeout = setTimeout(function () {
                    window.dropZoneTimeout = null;
                    dropZone.removeClass('in hover');
                }, 100);
            });

            /**
             * Selectable widget only
             */
            // The accept button should only be enabled on the "library" tab
            var modalAccept = $('.image-modal .modal-accept');
            /* Upload tab is default, so disable the accept button */
            modalAccept.addClass("disabled");
            /* When tabs are toggles, if the library tab is selected, enable the accept button */
            $('.image-modal input[name=tabbed]').on('change', function() {
                var tab = $('input[name=tabbed]:checked').val(); 
                modalAccept.toggleClass("disabled", tab !== 'library');
            });

            /*
             * Image selection for insertion into a post, settings page, or user profile
             * Inspired by http://stackoverflow.com/questions/3339013/jquery-image-picker
             * Needs to use delegated binding (.on) since new images will be added
             * and just using the .click event wouldn't acknowledge new items.
             */
            $('.image-modal .image-list').on("click dblclick", ".item", function(e){
                // Remove selected class from all .item then add to clicked .item
                $('.image-modal .image-list .item').removeClass("selected");
                $(this).closest('.item').addClass("selected");
                // Populated the selected image into the hidden input that can be read by pages that use this modal
                $('#image-modal-selected-image-alternative_text').val($(this).data('alternative_text'));
                $('#image-modal-selected-image-path').val($(this).data('path'));
                // If this was a double click, auto-click the accept button
                if (e.type == 'dblclick') {
                    $('.image-modal .modal-accept').click();
                }
            });

            /**
             * When the modal is closed, de-select any selected image and clear the selected image field
             */
            $('#image-modal').on('modalHidden', function() {
                $('.image-modal .image-list .item').removeClass("selected");
                $('#image-modal-selected-image-alternative_text').val('');
                $('#image-modal-selected-image-path').val('');
            });
JS;
        $this->view->registerJs($script, View::POS_READY);

    }
}
