<?php

namespace app\modules\backend\widgets;

use app\modules\backend\models\PageSearch;
use app\modules\backend\models\PostSearch;
use app\modules\backend\models\TagSearch;
use app\modules\backend\models\CategorySearch;
use yii;
use \yii\base\Widget;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;

/**
 *
 * @author Justin Voelker <justin@justinvoelker.com>
 */
class LinkModal extends Widget
{
    public $headerText = 'Select Link';

    public $acceptButtonText = 'Insert';
    public $cancelButtonText = 'Cancel';

    public function init()
    {
        parent::init();

        // A couple buttons in the footer
        $footer = '';
        $footer .= '<label for="link-modal" class="btn btn-default modal-close">' . $this->cancelButtonText . '</label>';
        $footer .= '<label for="link-modal" class="btn btn-scheme modal-accept">' . $this->acceptButtonText . '</label>';

        /* Display image modal*/
        Modal::begin([
            'id' => 'link-modal',
            'title' => $this->headerText,
            'footer' => $footer,
        ]);
        ?>
        <div class="tabbed">
            <input name="tabbed-link" id="tabbed-external" type="radio" class="tabbed__input" value="external" checked>
            <div class="tabbed__tab">
                <div class="tabbed__header">
                    <label class="tabbed__label" for="tabbed-external">External</label>
                </div>
                <div class="tabbed__content">
                    <div class="link-external">
                        To link to an external page, simple enter the Title and URL below
                    </div>
                </div>
            </div>
            <input name="tabbed-link" id="tabbed-post" type="radio" class="tabbed__input" value="post">
            <div class="tabbed__tab">
                <div class="tabbed__header">
                    <label class="tabbed__label" for="tabbed-post">Post</label>
                </div>
                <div class="tabbed__content">
                    <div class="link-post">
                        <?php
                        $searchModel = new PostSearch();
                        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                        Pjax::begin(['enablePushState' => false, 'id' => 'link-modal-sort-post']);
                        echo GridView::widget([
                            'id' => 'link-modal-grid-post',
                            'dataProvider' => $dataProvider,
                            //'filterModel' => $searchModel,
                            'options' => ['class' => 'grid-view table-responsive'],
                            'rowOptions' => function ($model) {
                                /* @var $model \app\modules\base\models\Post */
                                return [
                                    'data' => [
                                        'title' => $model->title_revised,
                                        'url' => Url::toRoute($model->route()),
                                    ]
                                ];
                            },
                            'columns' => [
                                [
                                    'attribute' => 'title_revised',
                                    'label' => 'Title',
                                    'format' => 'raw',
                                    'value' => function ($data) {
                                        /* @var $data \app\modules\base\models\Post */
                                        $titleText = Html::encode($data->title_revised);
                                        if ($data->statusText() != 'Published') {
                                            $titleText .= ' <strong>(' . $data->statusText() . ')</strong>';
                                        }
                                        return $titleText;
                                    },
                                ],
                                [
                                    'attribute' => 'created_at',
                                    'label' => 'Created',
                                    'value' => function ($data) {
                                        return Yii::$app->formatter->asDate($data->created_at, 'medium');
                                    }
                                ],
                            ],
                        ]);
                        Pjax::end();
                        ?>
                    </div>
                </div>
            </div>
            <input name="tabbed-link" id="tabbed-page" type="radio" class="tabbed__input" value="page">
            <div class="tabbed__tab">
                <div class="tabbed__header">
                    <label class="tabbed__label" for="tabbed-page">Page</label>
                </div>
                <div class="tabbed__content">
                    <div class="link-page">
                        <?php
                        $searchModel = new PageSearch();
                        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                        Pjax::begin(['enablePushState' => false, 'id' => 'link-modal-sort-page']);
                        echo GridView::widget([
                            'id' => 'link-modal-grid-page',
                            'dataProvider' => $dataProvider,
                            //'filterModel' => $searchModel,
                            'options' => ['class' => 'grid-view table-responsive'],
                            'rowOptions' => function ($model) {
                                /* @var $model \app\modules\base\models\Page */
                                return [
                                    'data' => [
                                        'title' => $model->title_revised,
                                        'url' => Url::toRoute($model->route()),
                                    ]
                                ];
                            },
                            'columns' => [
                                [
                                    'attribute' => 'title_revised',
                                    'label' => 'Title',
                                    'format' => 'raw',
                                    'value' => function ($data) {
                                        /* @var $data \app\modules\base\models\Page */
                                        $titleText = Html::encode($data->title_revised);
                                        if ($data->statusText() != 'Published') {
                                            $titleText .= ' <strong>(' . $data->statusText() . ')</strong>';
                                        }
                                        return $titleText;
                                    },
                                ],
                                [
                                    'attribute' => 'created_at',
                                    'label' => 'Created',
                                    'value' => function ($data) {
                                        return Yii::$app->formatter->asDate($data->created_at, 'medium');
                                    }
                                ],
                            ],
                        ]);
                        Pjax::end();
                        ?>
                    </div>
                </div>
            </div>
            <input name="tabbed-link" id="tabbed-tag" type="radio" class="tabbed__input" value="tag">
            <div class="tabbed__tab">
                <div class="tabbed__header">
                    <label class="tabbed__label" for="tabbed-tag">Tag</label>
                </div>
                <div class="tabbed__content">
                    <div class="link-tag">
                        <?php
                        $searchModel = new TagSearch();
                        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                        Pjax::begin(['enablePushState' => false, 'id' => 'link-modal-sort-tag']);
                        echo GridView::widget([
                            'id' => 'link-modal-grid-tag',
                            'dataProvider' => $dataProvider,
                            //'filterModel' => $searchModel,
                            'options' => ['class' => 'grid-view table-responsive'],
                            'rowOptions' => function ($model) {
                                return [
                                    'data' => [
                                        'title' => $model->name,
                                        'url' => '/tag/' . $model->slug,
                                    ]
                                ];
                            },
                            'columns' => [
                                'name',
                                'primary_posts_count',
                                'primary_pages_count',
                            ],
                        ]);
                        Pjax::end();
                        ?>
                    </div>
                </div>
            </div>
            <input name="tabbed-link" id="tabbed-category" type="radio" class="tabbed__input" value="category">
            <div class="tabbed__tab">
                <div class="tabbed__header">
                    <label class="tabbed__label" for="tabbed-category">Category</label>
                </div>
                <div class="tabbed__content">
                    <div class="link-category">
                        <?php
                        $searchModel = new CategorySearch();
                        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                        Pjax::begin(['enablePushState' => false, 'id' => 'link-modal-sort-category']);
                        echo GridView::widget([
                            'id' => 'link-modal-grid-category',
                            'dataProvider' => $dataProvider,
                            //'filterModel' => $searchModel,
                            'options' => ['class' => 'grid-view table-responsive'],
                            'rowOptions' => function ($model) {
                                return [
                                    'data' => [
                                        'title' => $model->name,
                                        'url' => '/category/' . $model->slug,
                                    ]
                                ];
                            },
                            'columns' => [
                                'name',
                                'primary_posts_count',
                                'primary_pages_count',
                            ],
                        ]);
                        Pjax::end();
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <div id="link-modal-form" class="form-horizontal">
            <div class="form-group">
                <label class="control-label col-sm-2" for="link-modal-link-title">Title</label>
                <div class="col-sm-8">
                    <?= Html::textInput("link-modal-link-title", null, [
                        'id' => 'link-modal-link-title',
                        'class' => ['form-control']
                    ]); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="link-modal-link-url">URL</label>
                <div class="col-sm-8">
                    <?= Html::textInput("link-modal-link-url", null, [
                        'id' => 'link-modal-link-url',
                        'class' => ['form-control']
                    ]); ?>
                </div>
            </div>
        </div>

        <input type="hidden" id="image-modal-selected-image-title">
        <input type="hidden" id="image-modal-selected-image-path">
        <input type="hidden" id="link-modal-trigger-field">

        <?php
        Modal::end();


        $script = <<< JS

            // Upon click of a post, tag, or category, insert the appropriate title and url into the input fields
            // Uses 'on' event since it needs to bind to new elements (after sorting, pagination, and filtering)
            $('.link-modal').on('click', 'tr[data-title]', function () {
                $('#link-modal-link-title').val($(this).data('title'));
                $('#link-modal-link-url').val($(this).data('url'));
            });

            // When the modal is closed, clear the link title and url
            $('#link-modal').on('modalHidden', function() {
                $('#link-modal-link-title').val('');
                $('#link-modal-link-url').val('');
            });

JS;
        $this->view->registerJs($script, View::POS_READY);

    }
}
