<?php

namespace app\modules\backend\widgets;

use app\modules\base\models\Help;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * Render the main action bar found on every page of the administrative interface.
 *
 * Buttons are set by adding elements to the params['action-buttons'] array as follows:
 *
 * $this->params['action-buttons'] = [
 *     [
 *         'icon' => 'fa-plus',
 *         'text' => 'New Post',
 *         'url' => ['edit'],
 *         'options' => null,
 *         'authorized' => (\Yii::$app->user->can('postCreate')),
 *     ],
 * ];
 *
 * The following keys may be set:
 * * icon: The Font Awesome icon for the action.
 * * text: Text to display next to the icon.
 * * form: Optional. The ID of a form to be submitted. Renders as button element.
 * * for: Optional. The ID of a form input to be clicked (such as checkboxes that control modals). Renders as label element.
 * * url: Optional. Default rendering as an anchor element to a give URL.
 * * options: Options such as CSS classes for the action.
 * * authorized: Boolean value indicating whether or not this action should be shown.
 *
 * @author Justin Voelker <justin@justinvoelker.com>
 */
class ActionBar extends Widget
{
    public function run()
    {
        $this->registerClientScript();
        ?>

        <div class="action-bar__container">

            <?php if (isset($this->view->params['action-buttons'])): ?>
                <?php foreach ($this->view->params['action-buttons'] as $actionButtonGroup): ?>
                    <?php
                    /* Determine which CSS classes should be applied to the action bar group */
                    $groupClasses = [];
                    /* Always add the block element class and modifier class that includes the name of this group */
                    $groupClasses[] = 'action-bar__group';
                    $groupClasses[] = 'action-bar__group_' . $actionButtonGroup['name'];
                    /* Add "primary" and "active" modifier if set */
                    if (isset($actionButtonGroup['primary']) && $actionButtonGroup['primary'] === true) {
                        $groupClasses[] = 'action-bar__group_primary action-bar__group_active';
                    }
                    ?>
                    <div class="<?= implode(' ', $groupClasses) ?>">
                        <ul class="action-bar__list clearfix">
                            <?php foreach ($actionButtonGroup['items'] as $actionButton): ?>
                                <?php if ($actionButton['authorized']): /* List item not shown if not authorized */ ?>
                                    <li class="action-bar__list-item">
                                        <?= $this->renderButton($actionButton) ?>
                                    </li>
                                <?php endif; /* if authorized */ ?>
                            <?php endforeach; /* each action button */ ?>
                        </ul>
                    </div>

                <?php endforeach; /* each action button group */ ?>
            <?php endif; /* if action-buttons isset */ ?>

            <div class="action-bar__help">
                <?= Help::button(); ?>
            </div>
        </div>
        <?php
    }

    /**
     * Render a button
     *
     * @param $actionButton
     * @return string
     */
    public function renderButton($actionButton)
    {
        $result = '';

        /* Build icon and text for item */
        $icon = '<i class="fa fa-fw ' . $actionButton['icon'] . '" aria-hidden="true"></i>';
        $text = '<span class="action-bar__list-item-link-text">' . $actionButton['text'] . '</span>';

        /* Item will either be a link or a form submit button */
        if (isset($actionButton['form'])) {
            /* Buttons as form submit buttons */
            Html::addCssClass($actionButton['options'], 'action-bar__list-item-button');
            $result .= Html::submitButton($icon . $text,
                ['form' => $actionButton['form'], 'class' => $actionButton['options']['class']]);
        } elseif (isset($actionButton['for'])) {
            /* Buttons as modal trigger labels */
            Html::addCssClass($actionButton['options'], 'action-bar__list-item-label');
            $result .= Html::label($icon . $text, $actionButton['for'], $actionButton['options']);
        } else {
            /* Buttons as standard links */
            Html::addCssClass($actionButton['options'], 'action-bar__list-item-link');
            $result .= Html::a($icon . $text, $actionButton['url'], $actionButton['options']);
        }

        return $result;
    }

    /**
     * Registers the needed client script and options.
     */
    public function registerClientScript()
    {
        /* View is needed for registering JS and CSS */
        $view = $this->getView();

        $js = '';
        /* Change visible action bar group if necessary (if that group is not already active) */
        $js .= 'function setActionBarActiveGroup(group) {' . "\n";
        $js .= '    "use strict";' . "\n";
        $js .= '    group = group.length > 0 ? group : "primary";' . "\n";
        $js .= '    if (!$(".action-bar__group_"+group).hasClass("action-bar__group_active")) {;' . "\n";
        $js .= '        $(".action-bar__group").removeClass("action-bar__group_active");' . "\n";
        $js .= '        $(".action-bar__group_"+group).addClass("action-bar__group_active");' . "\n";
        $js .= '    }' . "\n";
        $js .= '};' . "\n";
        /* Update a specific button text */
        $js .= 'function updateActionBarButtonText(locator, text) {' . "\n";
        $js .= '    "use strict";' . "\n";
        $js .= '    console.info("Update action bar button \"" + locator + "\" with text \"" + text + "\"");' . "\n";
        $js .= '    $(locator).children(".action-bar__list-item-link-text").text(text);' . "\n";
        $js .= '};' . "\n";
        /* Update a specific button state (enabled/disabled) */
        $js .= 'function updateActionBarButtonState(locator, state) {' . "\n";
        $js .= '    "use strict";' . "\n";
        $js .= '    console.info("Update action bar button \"" + locator + "\" with state \"" + state + "\"");' . "\n";
        $js .= '    $(locator).toggleClass("disabled", !state);' . "\n";
        $js .= '};' . "\n";

        /* Register JavaScript */
        $view->registerJs($js, $view::POS_HEAD);
    }

}
