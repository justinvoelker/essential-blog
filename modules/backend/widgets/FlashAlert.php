<?php

namespace app\modules\backend\widgets;

use app\assets\GrowlAsset;
use Yii;
use yii\base\Widget;
use yii\web\View;

/**
 *
 * @author Justin Voelker <justin@justinvoelker.com>
 */
class FlashAlert extends Widget
{
    public function run()
    {
        /*
         * Wrapper function for flash alert messages. Rather than calling the external library
         * functions directly, this function is called throughout the admin interface so that
         * the external library could be changed and only this would need to change.
         */
        GrowlAsset::register($this->view);

        $script = <<< JS

            function flashAlert(params) {
                var title = params.title;
                var type = (params.type != null) ? params.type : "default";
                var message = (params.message != null) ? params.message : "";
                var style = "default";
                
                switch(type) {
                    case "danger":
                        title = (title != null) ? title : "Error";
                        style = "error";
                        break;
                    case "warning":
                        title = (title != null) ? title : "Warning";
                        style = "warning";
                        break;
                    case "success":
                        title = (title != null) ? title : "Success";
                        style = "notice";
                        break;
                    case "default":
                    default:
                        title = (title != null) ? title : "Success";
                        style = "default";
                        break;
                }
                $.growl({
                    title: title,
                    message: message,
                    style: style,
                    location: 'br',
                    size: 'large'
                });
            }
JS;
        $this->view->registerJs($script, View::POS_END);

        /*
         * Read all flash messages and display them.
         */
        if (!empty(Yii::$app->session->allFlashes)) {
            foreach (Yii::$app->session->getAllFlashes() as $key => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        $this->view->registerJs('flashAlert({type: "' . $key . '", message: "' . addslashes($message) . '"})', View::POS_READY);
                    }
                } else {
                    $this->view->registerJs('flashAlert({type: "' . $key . '", message: "' . addslashes($messages) . '"})', View::POS_READY);
                }
            }
        }
    }
}
