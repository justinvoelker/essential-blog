<?php

namespace app\modules\backend\widgets;

use yii\base\Widget;
use yii\web\View;

/**
 *
 * @author Justin Voelker <justin@justinvoelker.com>
 */
class CreateRecordModal extends Widget
{
    /**
     * @var string the title to appear in the header of the modal window.
     */
    public $modalId;
    /**
     * @var string the title to appear in the header of the modal window.
     */
    public $title;
    /**
     * @var \yii\db\ActiveRecord the model instance to be passed to the form.
     */
    public $model;
    /**
     * @var string the form view name.
     */
    public $formView;
    /**
     * @var array|string the URL for performing AJAX-based validation.
     */
    public $validationUrl;
    /**
     * @var array|string the form action URL.
     */
    public $action;
    /**
     * @var string the id of the pjax container to be refreshed after a new record is created.
     */
    public $pjaxContainer;

    public function init()
    {
        parent::init();

        /* A couple buttons in the footer */
        $footer = '';
        $footer .= '<label class="btn btn-default modal-close" for="'.$this->modalId.'">Cancel</label>';
        $footer .= '<button class="btn btn-scheme" type="submit" form="'.$this->modalId.'-form">Save</button>';

        /* Display modal */
        Modal::begin([
            'id' => $this->modalId,
            'title' => $this->title,
            'footer' => $footer,
        ]);

        /* Render the form, passing necessary parameters as supplied to this modal */
        echo $this->render($this->formView, [
            'model' => $this->model,
            'formId' => $this->modalId . '-form',
            'enableAjaxValidation' => true,
            'validationUrl' => $this->validationUrl,
            'action' => $this->action,
        ]);

        Modal::end();

        /*
         * JavaScript handling actions within modal
         *
         * If a Pjax container is specified, that likely indicates that the record created via this modal needs to be
         * shown, in some way, on the page from which it was created (for example, in a select list of categories or
         * tags on the page/post edit pages). In this example, newly created tags need to be included in the select list
         * so they can be selected. Manually adding the new record to the page would require knowledge about how the
         * new record is being used/displayed. By wrapping the content-to-be-update in a Pjax container, that portion
         * of the page can be refreshed without knowing anything about its contents. In the example of tags on the
         * page/post edit page, the tags are populated in a select list which is then transformed into clickable
         * buttons for selection. Without wrapping that content in Pjax, this modal would need to add the new tag in
         * alphabetical order to the select list as well as generate and insert the selection button into the existing
         * button list. Rather, with a Pjax container, the selected value(s) of a select list can be saved, the Pjax
         * container can be refreshed, and the selected value(s) can be restored (while also selecting the new tag)
         * which makes this code far more generic and able to be used in more than one place.
         */
        $script = <<< JS

            $('#{$this->modalId}').on('modalShown', function() {
                /* When the modal is opened, send focus to the name field */
                $('.{$this->modalId} form :input:enabled:visible:first').focus();
            }).on('modalHidden', function() {
                /* When the modal is closed, reset the form (clear field(s) and validation errors) */
                $('.{$this->modalId} form').trigger('reset');
            });

            /* Catch form submit and perform ajax submit instead */
            $('#{$this->modalId}-form').on('beforeSubmit', function() {
                /* global flashAlert */
                //noinspection JSUnusedGlobalSymbols
                $.ajax({
                    url: $(this).attr('action'),
                    type: 'POST',
                    data: new FormData(this),
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    /**
                    * @param data Data object returned from ajax submission
                    * @param data.notify Notification message
                    * @param data.notifyType Notification type (success, error, etc)
                    * @param data.createdId The ID of the newly created record so that it can be selected
                    */
                    success: function (data) {
                        if (data.notifyType === 'success') {
                            /*
                             * If a Pjax container is provided (test the length of the class property value, if not 0
                             * a value must be set) and that container actually exists (meaning it is a valid element
                             * on the page), refresh the container, maintaining the set value(s).
                             */
                            if ('{$this->pjaxContainer}'.length !== 0 && $('#{$this->pjaxContainer}').length !== 0) {
    
                                /*
                                 * If a select field is found, save the current values so they can be restored after the
                                 * container is refreshed. If no select is found, nothing needs to be saved. Different logic
                                 * applies to multiple and single select fields.
                                 */
                                if ($('#{$this->pjaxContainer}').find('select[multiple]').length > 0) {
                                    /* Save currently selected values */
                                    var dataValue = $('#{$this->pjaxContainer}').find('select').val();
                                    /* If no values were selected, declare as empty array so pushing new value will work */
                                    dataValue = (dataValue === null) ? [] : dataValue;
                                    /* Add the newly created option as a selected value */
                                    dataValue.push(data.createdId);
                                } else if ($('#{$this->pjaxContainer}').find('select').length > 0) {
                                    /* Not a multiple select, simply set the value to the newly created record */
                                    dataValue = data.createdId;
                                }
                                /* Refresh the container */
                                $.pjax.reload({container:'#{$this->pjaxContainer}'});
                                /* After Pjax refresh is complete, restore values and trigger change event */
                                $(document).on("pjax:success", '#{$this->pjaxContainer}',  function(){
                                    $('#{$this->pjaxContainer}').find('select').val(dataValue).change();
                                });
                            }
                            /* Close the modal and display success alert */
                            $('#{$this->modalId}').trigger('modalHide');
                            flashAlert({type: data.notifyType, message: data.notify});
                        } else {
                            /* Display returned message in error summary */
                            $('.error-summary-custom').empty().append("<p>" + data.notify  + "</p>").show();
                        }
                     }
                 });
                /* Prevent default form submission action */
                return false;
            });
JS;
        $this->view->registerJs($script, View::POS_READY);

    }
}
