<?php

namespace app\modules\backend\widgets;

use yii\web\View;
use yii\base\Widget;
use yii\helpers\Html;

class Modal extends Widget
{
    /**
     * @var string HTML ID attribute of the input that controls the modal visibility. Also added as class to parent div.
     */
    public $id;
    public $title;
    public $footer;

    /**
     * Initializes the widget.
     */
    public function init()
    {
        parent::init();

        $this->initOptions();

        echo Html::beginTag('div', ['class' => ['lightbox', $this->id]]) . "\n";
        echo Html::input('checkbox', null, null, ['id' => $this->id, 'class' => 'lightbox__state']);
        echo Html::beginTag('div', ['class' => 'lightbox__content']) . "\n";
        echo $this->renderHeader() . "\n";
        echo $this->renderBodyBegin() . "\n";

        $this->registerScripts();
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        echo "\n" . $this->renderBodyEnd();
        echo "\n" . $this->renderFooter();
        echo "\n" . Html::endTag('div'); // .lightbox__content
        echo "\n" . Html::label(null, $this->id, ['class' => 'lightbox__backdrop']);
        echo "\n" . Html::endTag('div'); // .lightbox
    }

    /**
     * Renders the header HTML markup of the modal
     * @return string the rendering result
     */
    protected function renderHeader()
    {
        $closeIcon = '<i class="fa fa-fw fa-times lightbox__close-icon" aria-hidden="true"></i>';

        $result = '';
        $result .= Html::beginTag('div', ['class' => 'lightbox__header']) . "\n";
        $result .= Html::tag('div', $this->title, ['class' => 'lightbox__title']) . "\n";
        $result .= Html::label($closeIcon, $this->id, ['class' => 'lightbox__header-close']);
        $result .= "\n" . Html::endTag('div'); // .lightbox__header

        return $result;
    }

    /**
     * Renders the opening tag of the modal body.
     * @return string the rendering result
     */
    protected function renderBodyBegin()
    {
        return Html::beginTag('div', ['class' => 'lightbox__main']);
    }

    /**
     * Renders the closing tag of the modal body.
     * @return string the rendering result
     */
    protected function renderBodyEnd()
    {
        return Html::endTag('div');
    }

    /**
     * Renders the HTML markup for the footer of the modal
     * @return string the rendering result
     */
    protected function renderFooter()
    {
        return Html::tag('div', $this->footer, ['class' => 'lightbox__footer']) . "\n";
    }

    /**
     * Initializes the widget options.
     * This method sets the default values for various options.
     */
    protected function initOptions()
    {
        if (!isset($this->id)) {
            $this->id = 'lightbox-' . microtime();
        }

        if (!isset($this->title)) {
            $this->title = 'Title';
        }

        if (!isset($this->footer)) {
            $this->footer = Html::label('Close', $this->id, ['class' => 'btn btn-scheme']);
        }
    }

    protected function registerScripts()
    {
        /** @noinspection JSJQueryEfficiency */
        $script = <<< JS
            /* Trigger events when modal is shown or hidden. */
            $('#{$this->id}').on('change', function() {
                /* "Checked" indicates that the change event was the the modal opening, not closing */
                if (this.checked) {
                    $(this).trigger("modalShown");
                } else {
                    $(this).trigger("modalHidden");
                }
            });
            /* When modalShow event is triggered, show the modal by checking the checkbox and trigger the change event */
            $('#{$this->id}').on('modalShow', function() {
                $('#{$this->id}').prop('checked', true).change();
            });
            /* When modalHide event is triggered, hide the modal by unchecking the checkbox and trigger the change event */
            $('#{$this->id}').on('modalHide', function() {
                $('#{$this->id}').prop('checked', false).change();
            });
JS;
        $this->view->registerJs($script, View::POS_END);

    }
}
