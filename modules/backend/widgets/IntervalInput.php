<?php

namespace app\modules\backend\widgets;

use yii\helpers\Html;
use yii\widgets\InputWidget;

/**
 * IntervalInput generates a standard text input filed grouped with a dropdown containing units of time.
 *
 * To be used in an [[yii\widgets\ActiveForm|ActiveForm]] using the [[yii\widgets\ActiveField::widget()|widget()]]
 * method. Example:
 *
 * ```php
 * <?= $form->field($model, 'duration')->widget(\app\modules\backend\widgets\IntervalInput::className()) ?>
 * ```
 *
 * For the purposes of setting intervals, seconds, minutes, hours, and days are exact value. However, months and years
 * start the deviate. One month is always 30 days and one year is always 360 days which is 12, 30-day months. This
 * allows values such as "0.5 years" to be equivalent to "6 months" and "36 months" to be equivalent to "3 years."
 * If one "true" year is desired, simply set the interval to "365 days." Note that this is still not 100% correct as
 * leap years will have 366 days.
 */
class IntervalInput extends InputWidget
{
    /**
     * @inheritdoc
     */
    public function run()
    {
        $this->registerClientScript();

        // The actual value will be seconds and needs to be parsed to a unit-based value
        $value = Html::getAttributeValue($this->model, $this->attribute);
        if ($value == 0) {
            $units = 's';
        } elseif (($value % (60 * 60 * 24 * 30 * 12)) == 0) {
            $value = (int)$value / (60 * 60 * 24 * 30 * 12);
            $units = 'y';
        } elseif (($value % (60 * 60 * 24 * 30)) == 0) {
            $value = (int)$value / (60 * 60 * 24 * 30);
            $units = 'm';
        } elseif (($value % (60 * 60 * 24)) == 0) {
            $value = (int)$value / (60 * 60 * 24);
            $units = 'd';
        } elseif (($value % (60 * 60)) == 0) {
            $value = (int)$value / (60 * 60);
            $units = 'h';
        } elseif (($value % (60)) == 0) {
            $value = (int)$value / (60);
            $units = 'i';
        } else {
            $value = (int)$value;
            $units = 's';
        }

        // Hidden input containing actual data
        if ($this->hasModel()) {
            echo Html::activeInput('hidden', $this->model, $this->attribute, $this->options);
        } else {
            echo Html::input('hidden', $this->name, $this->value, $this->options);
        }

        // Displayed input group for data collection
        echo Html::beginTag('div', ['class' => 'input-group input-group-interval']);
        echo Html::input('text', null, $value, ['id' => $this->attribute . '_value', 'class' => 'form-control']);
        echo Html::beginTag('div', ['class' => 'input-group-addon']);
        echo Html::dropDownList(null, $units,
            ['s' => 'Seconds', 'i' => 'Minutes', 'h' => 'Hours', 'd' => 'Days', 'm' => 'Months', 'y' => 'Years'],
            ['id' => $this->attribute . '_units', 'class' => 'form-control']);
        echo Html::endTag('div');
        echo Html::endTag('div');
    }

    /**
     * Registers the needed client script and options.
     */
    public function registerClientScript()
    {
        /* View is needed for registering JS and CSS */
        $view = $this->getView();

        /* ID is used for targeting the exact field. Allows for multiple password inputs without interference. */
        $id = $this->options['id'];

        /* Form group containing input and password visibility toggles */
        $fieldValue = $this->attribute . '_value';
        $fieldUnits = $this->attribute . '_units';

        /* On click of a password-visibility-toggle element, change input type and show/hide corresponding toggles */
        $js = '';
        $js .= '$("#' . $fieldValue . ', #' . $fieldUnits . '").change(function () {' . "\n";
        $js .= '    var value = $("#' . $fieldValue . '").val();' . "\n";
        $js .= '    var units = $("#' . $fieldUnits . '").val();' . "\n";
        $js .= '    var multiplier = 0' . "\n";
        $js .= '    if (units == "s") {' . "\n";
        $js .= '        multiplier = 1' . "\n";
        $js .= '    } else if (units == "i") {' . "\n";
        $js .= '        multiplier = 60' . "\n";
        $js .= '    } else if (units == "h") {' . "\n";
        $js .= '        multiplier = 60 * 60' . "\n";
        $js .= '    } else if (units == "d") {' . "\n";
        $js .= '        multiplier = 60 * 60 * 24' . "\n";
        $js .= '    } else if (units == "m") {' . "\n";
        $js .= '        multiplier = 60 * 60 * 24 * 30' . "\n";
        $js .= '    } else if (units == "y") {' . "\n";
        $js .= '        multiplier = 60 * 60 * 24 * 30 * 12' . "\n";
        $js .= '    }' . "\n";
        $js .= '    $("#' . $id . '").val(value * multiplier)' . "\n";
        $js .= '});' . "\n";

        /* Register JavaScript */
        $view->registerJs($js);
    }
}
