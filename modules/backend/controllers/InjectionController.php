<?php

namespace app\modules\backend\controllers;

use app\modules\base\models\Injection;
use app\modules\backend\models\InjectionSearch;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\HttpException;

/**
 * InjectionController implements the CRUD actions for Injection model.
 */
class InjectionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index',
                            'view',
                            'create',
                            'update',
                            'delete',
                            'update-status',
                        ],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Injection models.
     * @return mixed
     */
    public function actionIndex()
    {
        Url::remember();

        $searchModel = new InjectionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Injection model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        Url::remember();

        return $this->render('view', [
            'model' => Injection::findModel($id),
        ]);
    }

    /**
     * Creates a new Injection model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws HttpException
     */
    public function actionCreate()
    {
        $model = new Injection();

        /* Set default status */
        $model->status = Injection::STATUS_ENABLED;

        if (\Yii::$app->user->can('injectionCreate')) {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            throw new HttpException(403, 'You do not have permission to create injections.');
        }
    }

    /**
     * Updates an existing Injection model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionUpdate($id)
    {
        $model = Injection::findModel($id);

        // Check permissions
        if (\Yii::$app->user->can('injectionUpdate', ['model' => $model])) {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        } else {
            throw new HttpException(403, 'You do not have permission to update this record.');
        }
    }

    /**
     * Deletes an existing Injection model (or models).
     * After delete, the browser will be redirected to the 'index' page.
     * @param string $ids
     * @return mixed
     */
    public function actionDelete($ids)
    {
        /* Split the list of comma-separated ids into an array */
        $ids = explode(',', $ids);

        /* Retrieve all models based on the primary keys */
        $models = Injection::findAll($ids);

        /*
         * Loop through each model, checking for:
         * 1) Permission to delete
         * Permission must be granted in order to allow deletion. If condition is not met, an error message is added.
         */
        $errorMessages = [];
        foreach ($models as $model) {
            /* Add error if permission to delete is denied */
            if (!\Yii::$app->user->can('injectionDelete', ['model' => $model])) {
                $errorMessages[] = '<b>' . $model->name . '</b>, permission denied.';
            }
        }

        /*
         * If there were any errors, $errorMessages will contain values.
         * If there are values in the error message list, add a flash alert and do not delete. Else, continue with
         * deletion of each record.
         */
        if (count($errorMessages) > 0) {
            $message = 'Delete cancelled due to the following:<br>' . implode('<br>', $errorMessages);
            Yii::$app->getSession()->addFlash('warning', $message);
        } else {

            /* Start transaction so that if any deletion fails, entire transaction can be rolled back. */
            $transaction = Injection::getDb()->beginTransaction();
            try {

                /* Loop through each model and delete it. Failures throw exceptions that roll back the transaction. */
                foreach ($models as $model) {
                    $model->delete();
                }

                /* Since no exceptions were thrown during deletions, commit the transaction. */
                $transaction->commit();

                /* Add success flash alert. */
                $message = (count($models) == 1) ? 'Injection deleted.' : 'Injections deleted.';
                Yii::$app->getSession()->addFlash('success', $message);

            } /** @noinspection PhpUndefinedClassInspection */ catch(\Throwable $e) {
                /* If any exceptions where thrown during deletions, roll back the transaction. */
                $transaction->rollBack();

                /* Add flash alert with error from thrown exception. */
                Yii::$app->getSession()->addFlash('danger', $e->getMessage());
            }
        }

        return $this->redirect(['index']);
    }

    /**
     * Enable or disable an existing Injection model (or models).
     *
     * @param string $ids
     * @param string $status
     * @return mixed
     */
    public function actionUpdateStatus($ids, $status)
    {
        /* Split the list of comma-separated ids into an array */
        $ids = explode(',', $ids);

        /* Retrieve all models based on the primary keys */
        $models = Injection::findAll($ids);

        /*
         * Loop through each model, checking for:
         * 1) Permission to update
         * Permission must be granted in order to allow update. If condition is not met, an error message is added.
         */
        $errorMessages = [];
        foreach ($models as $model) {
            /* Add error if permission to delete is denied */
            if (!\Yii::$app->user->can('injectionUpdate', ['model' => $model])) {
                $errorMessages[] = '<b>' . $model->name . '</b>, permission denied.';
            }
        }

        /*
         * Validate the status since it is now technically 'user entered' data. Declare new instance of Injection,
         * populate the status and validate that attribute only. If there is an error, add it to the error message to
         * be returned.
         */
        $injectionStatusValidate = new Injection();
        $injectionStatusValidate->status = $status;
        $injectionStatusValidate->validate(['status']);
        if ($injectionStatusValidate->hasErrors()) {
            $errorMessages[] = $injectionStatusValidate->getFirstError('status');
        }

        /*
         * If there were any errors, $errorMessages will contain values.
         * If there are values in the error message list, add a flash alert and do not continue. Else, continue with
         * update of each record.
         */
        if (count($errorMessages) > 0) {
            $message = 'Action cannot be completed due to the following:<br>' . implode('<br>', $errorMessages);
            Yii::$app->getSession()->addFlash('warning', $message);
        } else {

            /* Start transaction so that if any update fails, entire transaction can be rolled back. */
            $transaction = Injection::getDb()->beginTransaction();
            try {

                /* Loop through each model and update it. Failures throw exceptions that roll back the transaction. */
                foreach ($models as $model) {
                    if ($status == $model::STATUS_ENABLED) {
                        $model->updateStatusEnable();
                    } elseif ($status == $model::STATUS_DISABLED) {
                        $model->updateStatusDisable();
                    }
                }

                /* Since no exceptions were thrown during updates, commit the transaction. */
                $transaction->commit();

                /* Add success flash alert. */
                $statusText = strtolower($models[0]->statusText());
                $message = (count($models) == 1) ? 'Injection '.$statusText.'.' : 'Injections '.$statusText.'.';
                Yii::$app->getSession()->addFlash('success', $message);

            } /** @noinspection PhpUndefinedClassInspection */ catch(\Throwable $e) {
                /* If any exceptions where thrown during deletions, roll back the transaction. */
                $transaction->rollBack();

                /* Add flash alert with error from thrown exception. */
                Yii::$app->getSession()->addFlash('danger', $e->getMessage());
            }
        }

        return $this->redirect(Url::previous());
    }

}
