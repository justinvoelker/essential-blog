<?php

namespace app\modules\backend\controllers;

use app\modules\base\models\Tag;
use app\modules\backend\models\TagSearch;
use yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * TagController implements the CRUD actions for Tag model.
 */
class TagController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index',
                            'view',
                            'create',
                            'update',
                            'delete',
                            'ajax-create',
                            'ajax-validate',
                        ],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Tag models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TagSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tag model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = Tag::findModel($id);
        $postsDataProvider = new ActiveDataProvider([
            'query' => $model->primaryPosts(),
        ]);
        $pagesDataProvider = new ActiveDataProvider([
            'query' => $model->primaryPages(),
        ]);

        return $this->render('view', [
            'model' => $model,
            'posts' => $postsDataProvider,
            'pages' => $pagesDataProvider,
        ]);
    }

    /**
     * Creates a new Tag model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws HttpException
     */
    public function actionCreate()
    {
        $model = new Tag();

        if (\Yii::$app->user->can('tagCreate')) {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            throw new HttpException(403, 'You do not have permission to create tags.');
        }
    }

    /**
     * Updates an existing Tag model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionUpdate($id)
    {
        $model = Tag::findModel($id);

        if (\Yii::$app->user->can('tagUpdate', ['model' => $model])) {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        } else {
            throw new HttpException(403, 'You do not have permission to update this record.');
        }
    }

    /**
     * Deletes an existing Tag model (or models).
     * After delete, the browser will be redirected to the 'index' page.
     * @param string $ids
     * @return mixed
     */
    public function actionDelete($ids)
    {
        /* Split the list of comma-separated ids into an array */
        $ids = explode(',', $ids);

        /* Retrieve all models based on the primary keys */
        $models = Tag::findAll($ids);

        /*
         * Loop through each model, checking for:
         * 1) Permission to delete
         * 2) Whether or not the tag is in use
         * Permission must be granted and the tag must not be in use in order to allow deletion. If either
         * condition is not met, an error message is added.
         */
        $errorMessages = [];
        foreach ($models as $model) {
            /* Add error if permission to delete is denied */
            if (!\Yii::$app->user->can('tagDelete', ['model' => $model])) {
                $errorMessages[] = '<b>' . $model->name . '</b>, permission denied.';
            }
            /* Add error if tag is in use and thus cannot be deleted */
            if ($model->primary_posts_count != 0 || $model->primary_pages_count != 0) {
                $errorMessages[] = '<b>' . $model->name . '</b>, in use and cannot be deleted.';
            }
        }

        /*
         * If there were any errors, $errorMessages will contain values.
         * If there are values in the error message list, add a flash alert and do not delete. Else, continue with
         * deletion of each record.
         */
        if (count($errorMessages) > 0) {
            $message = 'Delete cancelled due to the following:<br>' . implode('<br>', $errorMessages);
            Yii::$app->getSession()->addFlash('warning', $message);
        } else {

            /* Start transaction so that if any deletion fails, entire transaction can be rolled back. */
            $transaction = Tag::getDb()->beginTransaction();
            try {

                /* Loop through each model and delete it. Failures throw exceptions that roll back the transaction. */
                foreach ($models as $model) {
                    $model->delete();
                }

                /* Since no exceptions were thrown during deletions, commit the transaction. */
                $transaction->commit();

                /* Add success flash alert. */
                $message = (count($models) == 1) ? 'Tag deleted.' : 'Tags deleted.';
                Yii::$app->getSession()->addFlash('success', $message);

            } /** @noinspection PhpUndefinedClassInspection */ catch(\Throwable $e) {
                /* If any exceptions where thrown during deletions, roll back the transaction. */
                $transaction->rollBack();

                /* Add flash alert with error from thrown exception. */
                Yii::$app->getSession()->addFlash('danger', $e->getMessage());
            }
        }

        return $this->redirect(['index']);
    }

    /**
     * Creates a new Tag model via Ajax request
     *
     * @return mixed
     */
    public function actionAjaxCreate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /* Check permissions */
        if (\Yii::$app->user->can('tagCreate')) {

            /* Create new instance */
            $model = new Tag();

            /*
             * Even though Ajax validation occurred during data entry, the call to the save method does not skip
             * validation since it is during validation that SluggableBehavior will generate a slug.
             */
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                $result['notify'] = 'Tag created.';
                $result['notifyType'] = 'success';
                $result['createdId'] = $model->id;
            } else {
                $result['notify'] = implode('<br>', $model->firstErrors);
                $result['notifyType'] = 'warning';
            }
        } else {
            $result['notify'] = 'You do not have permission to create tags.';
            $result['notifyType'] = 'warning';
        }

        return $result;
    }

    /**
     * Validate data via Ajax.
     *
     * Example usage, the basic create form in modal window on page/post edit form.
     *
     * @return mixed
     */
    public function actionAjaxValidate()
    {
        $model = new Tag();

        $model->load(Yii::$app->request->post());

        Yii::$app->response->format = Response::FORMAT_JSON;

        return ActiveForm::validate($model);
    }
}
