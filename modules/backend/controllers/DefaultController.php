<?php

namespace app\modules\backend\controllers;

use app\helpers\DateAndTime;
use app\modules\base\models\InstallForm;
use app\modules\base\models\LoginForm;
use app\modules\base\models\PasswordResetForm;
use app\modules\base\models\PasswordResetRequestForm;
use app\modules\base\models\RegisterForm;
use app\modules\base\models\Setting;
use app\modules\base\models\User;
use app\modules\base\models\UserAuthConnection;
use yii;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;

class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['install', 'error'],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'login',
                            'auth-connection-login',
                            'register',
                            'register-ajax-validation',
                            'password-reset-request',
                            'password-reset'
                        ],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'logout'],
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'auth-connection-login' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'authConnectionLoginCallback'],
            ],

        ];
    }

    public function actionInstall()
    {
        $this->layout = 'minimal';

        /* If user table exists and any users exist, install is complete and should not be allowed to continue */
        $userTableSchema = Yii::$app->db->schema->getTableSchema('{{%user}}');
        if ($userTableSchema != null && User::find()->count() > 0) {
            return $this->redirect(['/admin']);
        }

        $model = new InstallForm();
        if ($model->load(Yii::$app->request->post()) && $model->updateSettings()) {
            /* So that when the user logs in the call to goBack() goes to the admin */
            Yii::$app->user->returnUrl = '/admin';
            return $this->redirect(['/admin/default/login']);
        } else {
            /* Set flash message if model failed validation (meaning the install did not complete) */
            if ($model->hasErrors()) {
                Yii::$app->getSession()->addFlash('warning', 'Install failed, please review data entered in Installation Wizard.');
            }
            return $this->render('install', [
                'model' => $model,
            ]);
        }
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        /* If user is logged in go back to previous page. Authenticated users do not have access to login page. */
        if (!Yii::$app->user->isGuest) {
            return $this->goBack();
        }

        $model = new LoginForm();

        /* If Ajax request, perform login checks and log in if everything checks out */
        if (Yii::$app->request->isAjax) {

            /* Response will be Json */
            Yii::$app->response->format = Response::FORMAT_JSON;

            /* Load the posted data and attempted login */
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return $this->goBack();
            } else {
                /* Return errors */
                $result['notify'] = implode('. ', $model->getFirstErrors());
                $result['notifyType'] = 'warning';
                /*
                 * This is used by the action-login.js to determine if the user needs to be shown the two-factor code
                 * or not. The two-factor code will not be required or validated until username and password are
                 * valid so if the two-factor code has an error, that means it is either required and blank or invalid.
                 * There will only ever be two errors for two-factor code: A) it is blank and required, B) it is wrong.
                 * If it is blank and required, that may simply be because it has not been shown yet such as when the
                 * user first enters a valid username and password. In this case, the JS can show the two-factor code
                 * and not display the error. If it is blank again, the JS can determine that it is already shown and
                 * the user just did not enter a value in which case the error can be shown. The next option is that it
                 * is invalid. This can only happen after the two-factor code has been shown and populated. In this
                 * case, the JS will simply clear the field and display an error.
                 */
                $result['twoFactorCodeError'] = $model->hasErrors('two_factor_code');
            }

            return $result;

        } else {
            $this->layout = 'minimal';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @param $client \yii\authclient\ClientInterface
     */
    public function authConnectionLoginCallback($client)
    {
        $attributes = $client->getUserAttributes();

        // Cloud ID for User Cloud Account
        $authClientId = $client->getId() . '-' . $attributes['id'];

        // If User Cloud Account exists, login
        if ($userAuthClient = UserAuthConnection::findOne(['id' => $authClientId])) {
            $user = $userAuthClient->user;
            /* Send loginNotification email */
            $user->sendLoginNotificationEmail();
            // Login User
            $user_session_duration = Setting::getSetting('user_session_duration');
            Yii::$app->user->login($user, $user_session_duration);
        } else {
            Yii::$app->getSession()->addFlash('warning',
                'The ' . $client->getTitle() . ' account you have selected is not associated with any user on this site. Login with your username and password to connect your user account to this ' . $client->getTitle() . ' account.');
        }
    }

    /**
     * Logs out a user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        /* Remove long-lasting admin_user_exists cookie. Only removed by manual logging out (or one year passes) */
        Yii::$app->response->cookies->remove('admin_user_exists');

        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Registers an invited user.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionRegister($token)
    {
        $this->layout = 'minimal';

        try {
            $model = new RegisterForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->register()) {
            Yii::$app->session->setFlash('success', 'Registration successful. Please login.');
            // Set the return url as the main admin page
            Yii::$app->user->returnUrl = ['/admin'];

            return $this->redirect(['/admin/default/login']);
        }

        return $this->render('register', [
            'model' => $model,
        ]);
    }

    /**
     * Ajax validation for the register form
     *
     * @param string $token
     * @return mixed
     */
    public function actionRegisterAjaxValidation($token)
    {
        $model = new RegisterForm($token);

        $model->load(Yii::$app->request->post());

        Yii::$app->response->format = Response::FORMAT_JSON;

        return ActiveForm::validate($model, ['username']);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionPasswordResetRequest()
    {
        $this->layout = 'minimal';

        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->redirect(['/admin/default/login']);
            } else {
                Yii::$app->session->setFlash('warning', 'Error sending password reset email.');
            }
        }

        return $this->render('passwordResetRequest', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionPasswordReset($token)
    {
        $this->layout = 'minimal';

        try {
            $model = new PasswordResetForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->redirect(['/admin/default/login']);
        }

        return $this->render('passwordReset', [
            'model' => $model,
        ]);
    }
}
