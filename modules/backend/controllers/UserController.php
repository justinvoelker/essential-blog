<?php

namespace app\modules\backend\controllers;

use app\modules\backend\helpers\DataHelper;
use app\modules\backend\models\PostSearch;
use app\modules\backend\models\UserSearch;
use app\modules\base\models\User;
use app\modules\base\models\UserAuthConnection;
use PragmaRX\Google2FA\Google2FA;
use yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index',
                            'view',
                            'invite',
                            'invite-ajax-validation',
                            'resend-invite',
                            'update',
                            'delete',
                            'two-factor-verify-setup',
                            'two-factor-enable',
                            'two-factor-disable',
                            'auth-connection-connect',
                            'auth-connection-disconnect',
                            'auth-connection-refresh',
                        ],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'auth-connection-connect' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'authConnectionConnectCallback'],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     * @throws HttpException
     */
    public function actionIndex()
    {
        $inviteModel = new User();
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'inviteModel' => $inviteModel,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tag model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = User::findModel($id);
        $searchModel = new PostSearch(['created_by' => $id]);
        $postsCreatedByDataProvider = $searchModel->search(null);

        return $this->render('view', [
            'model' => $model,
            'postsCreatedBy' => $postsCreatedByDataProvider,
        ]);
    }

    /**
     * Creates a new User model and sends an invite.
     * @return mixed
     */
    public function actionInvite()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        // Check permissions
        if (\Yii::$app->user->can('userCreate')) {

            $model = new User();

            // Populate fields necessary for every new user
            $model->generateSecurityToken();
            $model->generateAuthKey();
            $model->status = User::STATUS_INVITED;

            // Ajax validation already occurred, can skip validation on save
            if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
                $result['successUser'] = true;

                // Save auth role
                $model->updateAuthRole();

                if ($model->sendInviteEmail()) {
                    $result['successEmail'] = true;
                    $result['message'] = 'Invitation for ' . $model->email . ' sent.';
                } else {
                    $result['successEmail'] = false;
                    $result['message'] = 'The user has been saved but the invite failed to send.';
                }
            } else {
                $result['successUser'] = false;
                $result['message'] = 'An unknown error has prevented the user from being saved.';
            }
        } else {
            $result['successUser'] = false;
            $result['message'] = 'You do not have permission to invite users.';
        }

        return $result;
    }

    /**
     * Ajax validation for the user invite form
     * @return mixed
     */
    public function actionInviteAjaxValidation()
    {
        $model = new User();

        $model->load(Yii::$app->request->post());

        Yii::$app->response->format = Response::FORMAT_JSON;

        return ActiveForm::validate($model, ['email', 'role']);
    }

    /**
     * Resend the new user invite email.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionResendInvite($id)
    {
        // Check permissions
        if (\Yii::$app->user->can('userCreate')) {

            $model = User::findModel($id);

            // Only resend invite if status is invited
            if ($model->status == User::STATUS_INVITED) {
                if ($model->sendInviteEmail()) {
                    Yii::$app->getSession()->addFlash('success', 'Invitation for ' . $model->email . ' resent.');
                } else {
                    Yii::$app->getSession()->addFlash('warning', 'Error resending invitation for ' . $model->email);
                }
            } else {
                Yii::$app->getSession()->addFlash('warning',
                    'Invitation for ' . $model->email . ' cannot be resent.');
            }

            return $this->redirect(['index']);
        } else {
            throw new HttpException(403, 'You do not have permission to resend user invites.');
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionUpdate($id)
    {
        $model = User::findModel($id);

        // Check permissions
        if (\Yii::$app->user->can('userUpdate', ['model' => $model])) {

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                /* If this user being updated is not the user performing the action, update authorization role. */
                if ($model->id != Yii::$app->user->id) {
                    $model->updateAuthRole();
                }

                // Set flash message and redirect back to index
                Yii::$app->getSession()->addFlash('success', 'User saved.');

                return $this->redirect(['index']);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        } else {
            throw new HttpException(403, 'You do not have permission to update this user.');
        }
    }

    /**
     * Deletes an existing User model (or models).
     * After delete, the browser will be redirected to the 'index' page.
     *
     * If a user deletes their own record, they will technically be "logged out" which means they will be directed to
     * the login page. A flash alert will still display the successfull deletion of the user.
     *
     * @param string $ids
     * @return mixed
     */
    public function actionDelete($ids)
    {
        /* Split the list of comma-separated ids into an array */
        $ids = explode(',', $ids);

        /* Retrieve all models based on the primary keys */
        $models = User::findAll($ids);

        /*
         * Loop through each model, checking for:
         * 1) Permission to delete
         * 2) User must not have any other records (post, page, image, tag, category, injection, user)
         * Permission must be granted in order to allow deletion. If condition is not met, an error message is added.
         */
        $errorMessages = [];
        foreach ($models as $model) {
            /* @var $model \app\modules\base\models\User */
            /* Add error if permission to delete is denied */
            if (!\Yii::$app->user->can('userDelete', ['model' => $model])) {
                $errorMessages[] = '<b>' . $model->firstNameLastName() . '</b>, permission denied.';
            }
            /* Add errors if user has created other records */
            /* TODO delete should still fail if user has updated records that they did not create, but, that's another problem for another day */
            if (($countPosts = $model->primaryPosts()->count()) > 0) {
                $errorMessages[] = '<b>' . $model->firstNameLastName() . '</b>, has ' . $countPosts . ' ' . (($countPosts == 1) ? 'post' : 'posts') . '.';
            }
            if (($countPages = $model->primaryPages()->count()) > 0) {
                $errorMessages[] = '<b>' . $model->firstNameLastName() . '</b>, has ' . $countPages . ' ' . (($countPages == 1) ? 'page' : 'pages') . '.';
            }
            if (($countImages = count($model->images)) > 0) {
                $errorMessages[] = '<b>' . $model->firstNameLastName() . '</b>, has ' . $countImages . ' ' . (($countImages == 1) ? 'image' : 'images') . '.';
            }
            if (($countTags = count($model->tags)) > 0) {
                $errorMessages[] = '<b>' . $model->firstNameLastName() . '</b>, has ' . $countTags . ' ' . (($countTags == 1) ? 'tag' : 'tags') . '.';
            }
            if (($countCategories = count($model->categories)) > 0) {
                $errorMessages[] = '<b>' . $model->firstNameLastName() . '</b>, has ' . $countCategories . ' ' . (($countCategories == 1) ? 'category' : 'categories') . '.';
            }
            if (($countInjections = count($model->injections)) > 0) {
                $errorMessages[] = '<b>' . $model->firstNameLastName() . '</b>, has ' . $countInjections . ' ' . (($countInjections == 1) ? 'injection' : 'injections') . '.';
            }
            if (($countInvitedUsers = count($model->invitedUsers)) > 0) {
                $errorMessages[] = '<b>' . $model->firstNameLastName() . '</b>, has ' . $countInvitedUsers . ' ' . (($countInvitedUsers == 1) ? 'invited user' : 'invited users') . '.';
            }
        }

        /*
         * If there were any errors, $errorMessages will contain values.
         * If there are values in the error message list, add a flash alert and do not delete. Else, continue with
         * deletion of each record.
         */
        if (count($errorMessages) > 0) {
            $message = 'Delete cancelled due to the following:<br>' . implode('<br>', $errorMessages);
            Yii::$app->getSession()->addFlash('warning', $message);
        } else {

            /* Start transaction so that if any deletion fails, entire transaction can be rolled back. */
            $transaction = User::getDb()->beginTransaction();
            try {

                /* Loop through each model and delete it. Failures throw exceptions that roll back the transaction. */
                foreach ($models as $model) {
                    /*
                     * Because the created_by field is a self-referencing foreign key, once a user updates their own
                     * record, that record can no longer be deleted without violating the foreign key constraint.
                     * Rather than blindly disabling all foreign key constrains, the updated_by field is simply set to
                     * the value of created_by. This will eliminate the self-referencing foreign key restriction and
                     * will allow the record to be deleted (provided there are no other foreign key constraints).
                     * If the delete fails, the transaction will be rolled back and updated_by will be restored.
                     */
                    $model->updated_by = $model->created_by;
                    $model->detachBehaviors();
                    $model->save();

                    /* Delete the user. If delete fails, transaction will be rolled back, reverting updated_by. */
                    $model->delete();

                    /* Remove assigned roles */
                    $auth = Yii::$app->authManager;
                    $auth->revokeAll($model->id);

                }

                /* Since no exceptions were thrown during deletions, commit the transaction. */
                $transaction->commit();

                /* Add success flash alert. */
                $message = (count($models) == 1) ? 'User deleted.' : 'Users deleted.';
                Yii::$app->getSession()->addFlash('success', $message);

            } /** @noinspection PhpUndefinedClassInspection */ catch(\Throwable $e) {
                /* If any exceptions where thrown during deletions, roll back the transaction. */
                $transaction->rollBack();

                /* Add flash alert with error from thrown exception. */
                Yii::$app->getSession()->addFlash('danger', json_encode($e->getmessage()));
            }
        }

        return $this->redirect(['index']);
    }

    /**
     * Enable two-factor authentication
     */
    public function actionTwoFactorEnable() {
        /* Result starts off null */
        $result = null;

        $secret = Yii::$app->request->post('two_factor_secret');
        $code = Yii::$app->request->post('two_factor_code');

        /* Verify that the provided code is valid for the given secret */
        $google2fa = new Google2FA();
        if ($google2fa->verifyKey($secret, $code)) {
            /* Generate backup codes */
            $backupCodes = [];
            for ($i = 0; $i < 10; $i++) {
                while (in_array($backupCode = mt_rand(10000000, 99999999), $backupCodes));
                $backupCodes[] = $backupCode;
            }

            /* Setup two-factor data for saving - secret, enabled date, last code (empty for now), and backup codes */
            $twoFactorData = [];
            $twoFactorData['secret'] = $secret;
            $twoFactorData['enabled_at'] = time();
            $twoFactorData['last_code'] = '';
            $twoFactorData['backup_codes'] = $backupCodes;

            /* Encrypt, encode, and save two-factor data to user */
            $user = User::findModel(Yii::$app->user->id);
            $user->two_factor_authentication = DataHelper::encryptForStorage($twoFactorData);
            if ($user->save()) {
                /* Save successful, return backup codes and success message */
                $result['twoFactorBackupCodes'] = json_encode($backupCodes);
                $result['notify'] = 'Two-factor authentication enabled.';
                $result['notifyType'] = 'success';
            } else {
                /* Error saving, return error message*/
                $result['notify'] = 'Failed to save enable two-factor authentication.';
                $result['notifyType'] = 'warning';
            }
        } else {
            $result['notify'] = 'Invalid code, ensure two-factor application is configure properly.';
            $result['notifyType'] = 'warning';
        }

        /* Response will be JSON */
        Yii::$app->response->format = Response::FORMAT_JSON;

        /* Return notification */
        return $result;
    }

    /**
     * Disable two-factor authentication
     *
     * @return Response
     */
    public function actionTwoFactorDisable()
    {
        /* Retrieve user model */
        $model = User::findModel(Yii::$app->user->id);

        /* Result starts off null */
        $result = null;

        /* Update User with two-factor authentication set to null */
        $model->two_factor_authentication = null;
        if ($model->save()) {
            /* Return notification type and message. */
            $result['notify'] = 'Two-factor authentication disabled.';
            $result['notifyType'] = 'success';
        } else {
            /* Error saving, return error message*/
            $result['notify'] = 'Failed to disable two-factor authentication.';
            $result['notifyType'] = 'warning';
        }

        /* Response will be JSON */
        Yii::$app->response->format = Response::FORMAT_JSON;

        /* Return notification */
        return $result;
    }

    /**
     * @param $client \yii\authclient\ClientInterface
     * @return Response
     */
    public function authConnectionConnectCallback($client)
    {
        $attributes = $client->getUserAttributes();

        // Cloud ID for User Cloud Account
        $authClientId = $client->getId() . '-' . $attributes['id'];

        // If User Cloud Account does not exist, create User and User Cloud Account
        if (!$userAuthClient = UserAuthConnection::findOne(['id' => $authClientId])) {
            // Create User Auth Client
            $userAuthClient = new UserAuthConnection();
            $userAuthClient->id = $authClientId;
            $userAuthClient->user_id = Yii::$app->user->id;
            $userAuthClient->connected_at = time();
            $userAuthClient->save();
            Yii::$app->getSession()->addFlash('success', $client->getTitle() . ' account successfully connected.');
        } else {
            Yii::$app->getSession()->addFlash('warning',
                $client->getTitle() . ' account already connected to another user.');
        }

        /* @var $authAction \yii\authclient\AuthAction */
        $authAction = $this->action;

        /* Redirect closes the popup, but there is no need to actually redirect */

        return $authAction->redirect(null, false);
    }

    public function actionAuthConnectionDisconnect($authclient)
    {
        /* Contains notify and notifyType to be returned to page calling the action */
        $result = null;

        /* Find the auth user connection for the authenticated user and the provided auth client */
        $userAuthConnection = UserAuthConnection::find()
            ->where(['user_id' => Yii::$app->user->id])
            ->andWhere(['like', 'id', $authclient])
            ->one();

        /* If no user auth connection was found, set a warning notification */
        if ($userAuthConnection == null) {
            $result['notify'] = 'UserAuthConnection connection not found.';
            $result['notifyType'] = 'warning';
        } else {
            /* User auth connection found, continue with delete */
            $userAuthConnection->delete();
            $result['notify'] = 'Client disconnected.';
            $result['notifyType'] = 'success';
        }

        /* Response will be JSON */
        Yii::$app->response->format = Response::FORMAT_JSON;

        /* Return notification */
        return $result;
    }

    /**
     * This action is called via a loop upon click of any auth client "connect" button. This action simply reads the
     * session flash messages. If any flash messages are found, they are returned to the looping ajax call so that the
     * loop can be exited, the pjax container refreshed, and the notification displayed.
     */
    public function actionAuthConnectionRefresh()
    {
        /* Contains notify and notifyType to be returned to page calling the action */
        $result = null;

        /* The authConnectionConnectCallback method will set flash messages as appropriate. Once those me */
        if (!empty(Yii::$app->session->allFlashes)) {
            $result['notifyFound'] = true;

            /* Find the flash message set by the success callback */
            foreach (Yii::$app->session->getAllFlashes() as $key => $messages) {
                foreach ($messages as $message) {
                    $result['notify'] = addslashes($message);
                    $result['notifyType'] = $key;
                }
            }

        } else {
            /* No flash was found. This will allow the loop to contiue. */
            $result['notifyFound'] = false;
        }

        /* Response will be JSON */
        Yii::$app->response->format = Response::FORMAT_JSON;

        /* Return notification */
        return $result;
    }

}
