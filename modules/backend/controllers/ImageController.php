<?php

namespace app\modules\backend\controllers;

use app\modules\base\models\Image;
use app\modules\base\models\ImageFile;
use app\modules\backend\models\ImageFileSearch;
use app\modules\backend\models\ImageSearch;
use yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * ImageController implements the CRUD actions for Image model.
 */
class ImageController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['img-from-path'],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'index',
                            'view',
                            'update',
                            'delete',
                            'upload',
                            'upload-variations',
                            'delete-variation',
                            'recreate-variations',
                            'replace-original'
                        ],
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'upload' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Ajax action to return an image tag for a given image based on an image path. Seemingly only used in select
     * image widget.
     *
     * @param string $path file path of image
     * @param string $sizes
     * @param string $classes
     * @return mixed
     */
    public function actionImgFromPath($path, $sizes, $classes)
    {
        $image = Image::findModelByPath($path);
        Yii::$app->response->format = Response::FORMAT_JSON;

        return ['img' => $image->imgTag([
            'sizes' => $sizes,
            'class' => $classes,
        ])];

    }

    /**
     * Lists all Image models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ImageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Image model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = Image::findModel($id);

        /* Search model for retrieving image variations */
        $searchModel = new ImageFileSearch(['path' => $model->path]);
        $dataProvider = $searchModel->search();

        return $this->render('view', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Updates an existing Image model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionUpdate($id)
    {
        $model = Image::findModel($id);

        /* Search model for retrieving image variations */
        $searchModel = new ImageFileSearch(['path' => $model->path]);
        $dataProvider = $searchModel->search();

        // Check permissions
        if (\Yii::$app->user->can('imageUpdate', ['model' => $model])) {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'dataProvider' => $dataProvider,
                ]);
            }
        } else {
            throw new HttpException(403, 'You do not have permission to update this record.');
        }
    }

    /**
     * Deletes an existing Image model (or models).
     * After delete, the browser will be redirected to the 'index' page.
     * @param string $ids
     * @return mixed
     */
    public function actionDelete($ids)
    {
        /* Split the list of comma-separated ids into an array */
        $ids = explode(',', $ids);

        /* Retrieve all models based on the primary keys */
        $models = Image::findAll($ids);

        /*
         * Loop through each model, checking for:
         * 1) Permission to delete
         * Permission must be granted in order to allow deletion. If condition is not met, an error message is added.
         */
        $errorMessages = [];
        foreach ($models as $model) {
            /* Add error if permission to delete is denied */
            if (!\Yii::$app->user->can('imageDelete', ['model' => $model])) {
                $errorMessages[] = '<b>' . $model->alternative_text . '</b>, permission denied.';
            }
        }

        /*
         * If there were any errors, $errorMessages will contain values.
         * If there are values in the error message list, add a flash alert and do not delete. Else, continue with
         * deletion of each record.
         */
        if (count($errorMessages) > 0) {
            $message = 'Delete cancelled due to the following:<br>' . implode('<br>', $errorMessages);
            Yii::$app->getSession()->addFlash('warning', $message);
        } else {

            /* Start transaction so that if any deletion fails, entire transaction can be rolled back. */
            $transaction = Image::getDb()->beginTransaction();
            try {

                /* Loop through each model and delete it. Failures throw exceptions that roll back the transaction. */
                foreach ($models as $model) {
                    $model->delete();
                }

                /* Now that the database models have been deleted, remove the files from the filesystem. */
                foreach ($models as $model) {
                    /* @var $model \app\modules\base\models\Image */
                    $model->imageFile()->unlink(true);
                }

                /* Since no exceptions were thrown during deletions, commit the transaction. */
                $transaction->commit();

                /* Add success flash alert. */
                $message = (count($models) == 1) ? 'Image deleted.' : 'Images deleted.';
                Yii::$app->getSession()->addFlash('success', $message);

            } /** @noinspection PhpUndefinedClassInspection */ catch(\Throwable $e) {
                /* If any exceptions where thrown during deletions, roll back the transaction. */
                $transaction->rollBack();

                /* Add flash alert with error from thrown exception. */
                Yii::$app->getSession()->addFlash('danger', $e->getMessage());
            }
        }

        return $this->redirect(['index']);
    }

    /**
     * Ajax action to upload a file. First creates Image model then uploads file to filesystem
     *
     * @return mixed
     * @throws \yii\web\HttpException
     */
    public function actionUpload()
    {
        // Check permissions
        if (\Yii::$app->user->can('imageCreate')) {

            $return = array();

            /*
             * Create new instance and set uploaded files. Setting the uploaded files as a property of the class
             * allows validation to occur, ensuring file size, file type, etc. are valid.
             */
            $model = new Image();
            $model->uploadedFiles = UploadedFile::getInstancesByName('files');

            // Save model, returning an error if validation errors occur.
            if ($model->save()) {

                /*
                 * The afterSave logic is responsible for uploading and resizing images. Based on the status of the
                 * upload and type of file uploaded, the model may now contain errors/warnings that needs to be sent
                 * back to the calling script. Upload/resize errors and warnings are set on a fake attributes named
                 * '*error' and '*warning'
                 */
                if ($model->hasErrors() && $model->getFirstError('*error') !== null) {
                    /*
                     * An error was found. Return the error as defined and delete the database model. Model is
                     * being deleted since errors are reserved for unrecoverable issues such as the original image
                     * failing to save.
                     */
                    $return['error'] = $model->getFirstError('*error');
                    $model->delete();
                } else {
                    /*
                     * A warning was found. Likely, the image upload was successful but a something happened that
                     * requires manual clean up (such as manually uploading image variations).
                     */
                    if ($model->hasErrors() && $model->getFirstError('*warning') !== null) {
                        $return['warning'] = $model->getFirstError('*warning');
                    }

                    /*
                     * Since the action was at least partially successful, this data should exist and is needed
                     * by the calling script
                     */
                    $return['alternative_text'] = $model->alternative_text;
                    $return['path'] = $model->path;
                    $return['img'] = $model->imgTag([
                        'sizes' => '50px',
                        'class' => 'img-responsive center-and-crop square-50'
                    ]);
                }
            } else {
                /*
                 * Model not saved. There was likely a validation error such as invalid file extension or file size.
                 * Return the first error.
                 */
                $return['error'] = implode(' ', array_values($model->firstErrors));
            }

            Yii::$app->response->format = Response::FORMAT_JSON;

            return $return;
        } else {
            throw new HttpException(403, 'You do not have permission to upload.');
        }
    }

    /**
     * Ajax action upload variations of an image.
     *
     * @param integer $id
     * @return mixed
     * @throws \yii\web\HttpException
     */
    public function actionUploadVariations($id)
    {
        /* Loaded model is only used for checking permissions and reading existing image data. No save takes place. */
        $model = Image::findModel($id);

        // Check permissions
        if (\Yii::$app->user->can('imageUpdate', ['model' => $model])) {

            $return = array();

            /*
             * Populate the uploadedFiles property of a new model so it can be validated, ensuring file size,
             * file type, etc. are valid. By using a new model to represent the uploaded image, if an image with the
             * exact same filename as the original file is included in the set of uploaded files, it will throw an
             * error because the path is not unique.
             */
            $modelUploaded = new Image();
            $modelUploaded->uploadedFiles = UploadedFile::getInstancesByName('files');

            /*
             * Validate the uploaded file to ensure valid file size and type. Only validating uploadedFiles attribute
             * since the path may fail 'unique' validation and all we really care about is file size, type, etc.
             */
            if ($modelUploaded->validate(['uploadedFiles'])) {

                /* Read the width of the uploaded file so it can be used in the new file name. */
                $width = getimagesize($modelUploaded->uploadedFiles[0]->tempName)[0];

                /* If width of uploaded variation is equal to or larger than original, throw error */
                if ($width >= $model->imageFile()->width) {
                    $return['error'] = "Variation must be smaller than original image.";
                } else {

                    /* Build path of file to be saved */
                    $path = $model->imageFile()->directory . '/' . $model->imageFile()->baseName . '-' . $width . 'w.' . $model->imageFile()->extension;

                    /*
                     * Create a new instance of ImageFile since the file being uploaded is a stand alone instance and is
                     * not part of the image model.
                     */
                    $imageFile = new ImageFile($path);

                    /* Now that the imageFile has the proper path for the new file, it can be uploaded. */
                    $imageFile->upload($modelUploaded->uploadedFiles[0]);
                }

            } else {
                /*
                 * Model not valid. There was likely a validation error such as invalid file extension or file size.
                 * Return the first error.
                 */
                $return['error'] = implode(' ', array_values($modelUploaded->firstErrors));
            }

            Yii::$app->response->format = Response::FORMAT_JSON;

            return $return;
        } else {
            throw new HttpException(403, 'You do not have permission to update this image.');
        }
    }

    /**
     * Deletes an existing image variation.
     *
     * After delete, the browser will be redirected to the 'update' page.
     *
     * Rather than passing in the full name of the image variations to be deleted, the variation is provided as the
     * width of the variation so that the file name can be determined within the method. This will eliminate the
     * possibility of deleting a variation for the wrong image.
     *
     * @param string $id
     * @param string $variations
     * @return mixed
     * @throws HttpException
     */
    public function actionDeleteVariation($id, $variations)
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->get('_pjax') === null) {

            /* Loaded model is only used for checking permissions. */
            $model = Image::findModel($id);

            // Check permissions
            if (\Yii::$app->user->can('imageUpdate', ['model' => $model])) {

                /* Contains notify and notifyType to be returned (as well as other data) to page calling the action */
                $result = null;

                /* Split provided variations into array */
                $variationsArray = explode(',', $variations);

                /* Loop through each provided variation and unlink the actual file */
                foreach ($variationsArray as $variation) {
                    /* Path of this image variation */
                    $file = $model->imageFile()->absoluteDirectory . '/' . $model->imageFile()->baseName . '-' . $variation . 'w.' . $model->imageFile()->extension;
                    /* The file SHOULD exist, but just in case it doesn't, don't throw an error */
                    if (file_exists($file)) {
                        unlink($file);
                    }
                }

                /* Post failed to validate. Return errors. */
                $result['notify'] = (count($variationsArray) == 1) ? 'Image variation deleted.' : 'Image variations deleted.';
                $result['notifyType'] = 'success';

                /* Response will be JSON */
                Yii::$app->response->format = Response::FORMAT_JSON;

                /* Return data */

                return $result;

            } else {
                throw new HttpException(403, 'You do not have permission to delete variations.');
            }
        } else {
            return false;
        }
    }

    /**
     * Ajax action to recreate the variations of an image.
     *
     * @param integer $id
     * @return mixed
     * @throws \yii\web\HttpException
     */
    public function actionRecreateVariations($id)
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->get('_pjax') === null) {

            /* Loaded model is only used for checking permissions. */
            $model = Image::findModel($id);

            // Check permissions
            if (\Yii::$app->user->can('imageUpdate', ['model' => $model])) {

                /* Contains notify and notifyType to be returned (as well as other data) to page calling the action */
                $result = null;

                /* Perform check(s) and remove/recreate if applicable */
                if (!in_array($model->imageFile()->imageType, ImageFile::IMAGE_TYPE_ALLOW_VARIATION_CREATION)) {
                    /* If image type is not in the list of image types that allow variation creation, return error */
                    $result['notify'] = 'Variations cannot be created for '.$model->imageFile()->extension.' images.';
                    $result['notifyType'] = 'warning';
                } elseif ($model->imageFile()->unlink(false)) {
                    /* If removal of all variations (not including original) was successfuly, recreate them */
                    if ($model->imageFile()->createVariations()) {
                        /* If creation of variations was successfuly, return success */
                        $result['notify'] = 'Successfully recreated variations.';
                        $result['notifyType'] = 'success';
                    } else {
                        $result['notify'] = 'Failed to recreate variations.';
                        $result['notifyType'] = 'danger';
                    }
                } else {
                    $result['notify'] = 'Could not delete all variations.';
                    $result['notifyType'] = 'warning';
                }

                /* Response will be JSON */
                Yii::$app->response->format = Response::FORMAT_JSON;

                /* Return data */

                return $result;

            } else {
                throw new HttpException(403, 'You do not have permission to update this image.');
            }
        } else {
            return false;
        }
    }

    /**
     * Ajax action to replace the original image.
     *
     * @param integer $id
     * @return mixed
     * @throws \yii\web\HttpException
     */
    public function actionReplaceOriginal($id)
    {
        /* Loaded model is only used for checking permissions and reading existing image data. No save takes place. */
        $model = Image::findModel($id);

        // Check permissions
        if (\Yii::$app->user->can('imageUpdate', ['model' => $model])) {

            $return = array();

            /*
             * Populate the uploadedFiles property of a new model so it can be validated, ensuring file size,
             * file type, etc. are valid. Only the uploadedFiles property will be validated since validations such as
             * 'unique' need to be skipped
             */
            $modelUploaded = new Image();
            $modelUploaded->uploadedFiles = UploadedFile::getInstancesByName('files');

            /*
             * Validate the uploaded file to ensure valid file size and type. Only validating uploadedFiles attribute
             * since the path may fail 'unique' validation and all we really care about is file size, type, etc.
             */
            if ($modelUploaded->validate(['uploadedFiles'])) {

                /*
                 * Additional check(s). Uploaded file must be of same type (have same extension) as original.
                 */
                if ($modelUploaded->uploadedFiles[0]->extension != $model->imageFile()->extension) {
                    $return['error'] = 'Image type does not match original (replacement image must be '.$model->imageFile()->extension.')';
                } else {
                    /*
                     * Create a new instance of ImageFile since the file being uploaded is a stand alone instance and is
                     * not part of the image model.
                     */
                    $imageFile = new ImageFile($model->path);

                    /* Now that the imageFile has the proper path for the new file, it can be uploaded. */
                    $imageFile->upload($modelUploaded->uploadedFiles[0]);
                }
            } else {
                /*
                 * Model not valid. There was likely a validation error such as invalid file extension or file size.
                 * Return the first error.
                 */
                $return['error'] = implode(' ', array_values($modelUploaded->firstErrors));
            }

            Yii::$app->response->format = Response::FORMAT_JSON;

            return $return;
        } else {
            throw new HttpException(403, 'You do not have permission to update this image.');
        }
    }
}
