<?php

namespace app\modules\backend\controllers;

use app\modules\base\models\Page;
use app\modules\backend\models\PageSearch;
use yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\Response;

/**
 * PageController implements the CRUD actions for Page model.
 */
class PageController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index',
                            'view',
                            'edit',
                            'publish',
                            'unpublish',
                            'delete',
                            'generate-or-validate-slug',
                        ],
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'publish' => ['POST'],
                    'unpublish' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Page models.
     * Include the most recent page or currently selected page (if provided) for preview functionality.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Page model. Only used on small screens. Larger screens show page in preview pane.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        /* Retrieve all page restore points for this primary page */
        $pageRestorePointsDataProvider = new ActiveDataProvider([
            'query' => Page::findModel($id)->pageRestorePoints(),
        ]);

        /* Return the newest restore point as the model and all page restore points */
        return $this->render('view', [
            'model' => Page::findModel($id)->newestRestorePoint(),
            'pageRestorePoints' => $pageRestorePointsDataProvider,
        ]);
    }

    /**
     * Creates a new Page model if it does not exist or updates an existing Page model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param null $id Primary page ID
     * @return mixed
     * @throws HttpException
     *
     */
    public function actionEdit($id = null)
    {
        /* If an ID is provided, load the modal for an update. Else, instantiate new Page for create. */
        if ($id) {
            $model = Page::findModel($id)->newestRestorePoint();
        } else {
            $model = new Page();
        }

        /* Is user authorized to create a page or update this page? */
        if ((!$id && \Yii::$app->user->can('pageCreate'))
            || ($id && \Yii::$app->user->can('pageUpdate', ['model' => $model->primaryPage()]))
        ) {

            /*
             * On this page edit page, the LinkModal displays pages/tags/categories for insertion into page content.
             * In the LinkModal, record grids are wrapped in a Pjax widget so that they may be sorted/paginated. Also
             * on this edit page, every save uses Pjax to update the header__body with an updated page title. Since
             * that those Pjax actions are accomplished via Ajax, sorting/paging through any grid inside the LinkModal
             * widget or updating the header after a save will trigger an Ajax call to this edit action. Simply checking
             * to see if this request is an Ajax request means the logic that follows will be trigger not only when the
             * form is submitted via Ajax but also when any grid in the LinkModal is sorted/paged or the header is
             * updated after a save. To prevent this, the $_GET param of "_pjax" and is also retrieved and checked. If
             * the request contains the _pjax $_GET parameter, then that particular request must be from the LinkModal
             * sorting/paging or updating of the header and the save logic below is not executed. If the _pjax $_GET
             * parameter is null, the Ajax request must be from a form submit and the save logic is executed.
             *
             * So, if this is an Ajax request that does not contain a _pjax $_GET paramter, perform the save logic.
             * Otherwise, just load the page edit form.
             */
            if (Yii::$app->request->isAjax && Yii::$app->request->get('_pjax') === null) {

                /*
                 * STEP 1: Load $_POST data into an instance of an unlocked restore point.
                 *
                 * Will either be the restore point loaded at the start of this action or one that is newly instantiated
                 * inside returnUnlockedPage().
                 */

                /*
                 * Use the existing restore point or a newly instantiated restore point, whichever is necessary to be
                 * working with and unlocked restore point. */
                $model = $model->returnUnlockedPage();

                /*
                 * Load the $_POST data into the model (will either be the existing model or the just instantiated model
                 * if the existing was locked).
                 */
                $model->load(Yii::$app->request->post());

                /*
                 * STEP 2: Save page restore point
                 *
                 * First, ensure the restore point will validate. After validation occurs, the beforeSave will fire
                 * which will create a new primary if needed (if primary_id is empty and _createPrimaryIfNull has not
                 * been changed to false). Creating the new primary takes place after validation to ensure failed
                 * validation does not result in a new primary without a child. An alternative would be to create the
                 * primary regardless then delete it if validation fails but that is just unnecessary and terrible.
                 */

                /* Contains notify and notifyType to be returned (as well as other data) to page calling the action */
                $result = null;

                /*
                 * Validate the submitted $_POST data that has been loaded into the existing restore point or a new
                 * instance.
                 */
                if (!$model->validate()) {
                    /* Page failed to validate. Return errors. */
                    $result['notify'] = implode('<br>', $model->firstErrors);
                    $result['notifyType'] = 'warning';
                } else {
                    /*
                     * Now that validation has succeeded, the page restore point can be save (which will trigger
                     * beforeSave which creates a new primary if necessary.
                     */

                    /* Save the page (either updating the existing restore point or inserting the new instance) */
                    if ($model->save()) {
                        $result['notify'] = 'Page saved.';
                        $result['notifyType'] = 'success';
                        /* Button text and enabled status for all of the action bar buttons */
                        $result['buttons'] = $model->buttons(); /* Since entire result is json, no need to json encod */
                        /* Additional result attributes */
                        $result['primary_id'] = $model->primary_id;
                        $result['action'] = Url::to(['edit', 'id' => $model->primary_id]);
                        /* Returns "first-page" */
                        $result['slug'] = $model->slug;
                        /* Returns "/2017/01/first-page" */
                        $result['url'] = Url::toRoute($model->route());
                        /* Returns "/2017/01/first-page?preview=true" */
                        $result['urlPreview'] = Url::toRoute($model->route()) . '?preview=true';
                    } else {
                        $result['notify'] = implode('<br>', $model->firstErrors);
                        $result['notifyType'] = 'warning';
                    }
                }

                /* Response will be JSON */
                Yii::$app->response->format = Response::FORMAT_JSON;

                /* Return data */
                return $result;

            } else {
                /**
                 * Before rendering the edit page, regenerate and set the slug for non-published pages.
                 * Scenario 1: Two pages were in draft status with the same slug. When Page A was published, the slug
                 * for Page B stopped being unique and cannot be used.
                 * Scenario 2: Page A was created and published. Page B was created with the same title and received a
                 * unique slug. Page A is unpublished. Page B is edited and the slug can be returned to the original
                 * value since the Page A was the reason it was changed but now Page A is no longer published.
                 */
                /* Do not automatically generate and set the slug if the page is published. */
                if ($model->status != Page::STATUS_PUBLISHED) {
                    $model->generateAndSetSlug();
                }

                /*
                 * Regardless of whether or not this restore point is currently locked, set it back to empty so that
                 * subsequent form submits are not locked unless a restore point is being saved.
                 */
                $model->is_locked = null;

                return $this->render('edit', ['model' => $model]);
            }
        } else {
            /* Decide which error message is thrown based on whether or not an ID is present (create vs update) */
            if ($id) {
                throw new HttpException(403, 'You do not have permission to update this page.');
            } else {
                throw new HttpException(403, 'You do not have permission to create pages.');
            }
        }
    }

    /**
     * Publish a single Page. This publish action accepts a primary page id. It then retrieves the newest
     * restore point which is what is actually sent to the publish method.
     * @param integer $id
     * @return mixed
     */
    public function actionPublish($id)
    {
        /* Retrieve newest restore point model */
        $model = Page::findModel($id)->newestRestorePoint();

        /* Contains notify and notifyType to be returned (as well as other pieces of data) to page calling the action */
        $result = null;

        /* Is user authorized to publish this page? */
        if (\Yii::$app->user->can('pagePublish', ['model' => $model->primaryPage()])) {
            /* Attempt publish, returning a warning if there is a problem */
            if ($model->publish()) {
                $result['notify'] = (!$model->hasFuturePublishDate()) ? 'Page published.' : 'Page scheduled.';
                $result['notifyType'] = 'success';
            } else {
                $result['notify'] = implode('<br>', $model->firstErrors);
                $result['notifyType'] = 'warning';
            }
            /*
             * Button text and enabled status for all of the action bar buttons. Not using the previously retrieved
             * model since it has possibly been updated as part of the publish process. Re-retrieving the newest
             * restore point ensures that whatever is in the database is what the buttons will be based on.
             */
            $result['buttons'] = Page::findModel($id)->newestRestorePoint()->buttons(); /* Since entire result is json, no need to json encode */
        } else {
            $result['notify'] = 'You do not have permission to publish this page.';
            $result['notifyType'] = 'warning';
        }

        /* Response will be JSON */
        Yii::$app->response->format = Response::FORMAT_JSON;

        /* Return notification */
        return $result;
    }

    /**
     * Unpublish a single Page. This unpublish action is provided a primary page id. It then retrieves the newest
     * restore point which is what is actually sent to the unpublish method.
     * @param integer $id
     * @return mixed
     */
    public function actionUnpublish($id)
    {
        /* Retrieve model */
        $model = Page::findModel($id)->newestRestorePoint();

        /* Contains notify and notifyType to be returned to page calling the action (unpublish does not have a separate permission) */
        $result = null;

        /* Is user authorized to publish this page? */
        if (\Yii::$app->user->can('pagePublish', ['model' => $model->primaryPage()])) {
            /* Attempt unpublish, returning a warning if there is a problem */
            if ($model->unpublish()) {
                $result['notify'] = (!$model->hasFuturePublishDate()) ? 'Page unpublished.' : 'Page unscheduled.';
                $result['notifyType'] = 'success';
            } else {
                $result['notify'] = implode('<br>', $model->firstErrors);
                $result['notifyType'] = 'warning';
            }
            /*
             * Button text and enabled status for all of the action bar buttons. Not using the previously retrieved
             * model since it has possibly been updated as part of the publish process. Re-retrieving the newest
             * restore point ensures that whatever is in the database is what the buttons will be based on.
             */
            $result['buttons'] = Page::findModel($id)->newestRestorePoint()->buttons(); /* Since entire result is json, no need to json encod */
        } else {
            $result['notify'] = 'You do not have permission to unpublish this page.';
            $result['notifyType'] = 'warning';
        }

        /* Response will be JSON */
        Yii::$app->response->format = Response::FORMAT_JSON;

        /* Return notification */
        return $result;
    }

    /**
     * Deletes an existing Page model (or models).
     * After delete, the browser will be redirected to the 'index' page.
     * @param string $ids
     * @return mixed
     */
    public function actionDelete($ids)
    {
        /* Split the list of comma-separated ids into an array */
        $ids = explode(',', $ids);

        /* Retrieve all models based on the primary keys */
        $models = Page::findAll($ids);

        /*
         * Loop through each model, checking for:
         * 1) Permission to delete
         * 2) Whether or not the page is a primary page
         * Permission must be granted and the page must be a primary page in order to allow deletion. If either
         * condition is not met, an error message is added.
         */
        $errorMessages = [];
        foreach ($models as $model) {
            /* Add error if permission to delete is denied */
            if (!\Yii::$app->user->can('pageDelete', ['model' => $model])) {
                $errorMessages[] = '<b>' . $model->title . '</b>, permission denied.';
            }
            /* Add error if this is not a primary page */
            if ($model->primary_id != null) {
                $errorMessages[] = '<b>' . $model->title . '</b>, not a primary page.';
            }
        }

        /*
         * If there were any errors, $errorMessages will contain values.
         * If there are values in the error message list, add a flash alert and do not delete. Else, continue with
         * deletion of each record.
         */
        if (count($errorMessages) > 0) {
            $message = 'Delete cancelled due to the following:<br>' . implode('<br>', $errorMessages);
            Yii::$app->getSession()->addFlash('warning', $message);
        } else {

            /* Start transaction so that if any deletion fails, entire transaction can be rolled back. */
            $transaction = Page::getDb()->beginTransaction();
            try {

                /* Loop through each model and delete it. Failures throw exceptions that roll back the transaction. */
                foreach ($models as $model) {
                    $model->delete();
                }

                /* Since no exceptions were thrown during deletions, commit the transaction. */
                $transaction->commit();

                /* Add success flash alert. */
                $message = (count($models) == 1) ? 'Page deleted.' : 'Pages deleted.';
                Yii::$app->getSession()->addFlash('success', $message);

            } /** @noinspection PhpUndefinedClassInspection */ catch(\Throwable $e) {
                /* If any exceptions where thrown during deletions, roll back the transaction. */
                $transaction->rollBack();

                /* Add flash alert with error from thrown exception. */
                Yii::$app->getSession()->addFlash('danger', $e->getMessage());
            }
        }

        return $this->redirect(['index']);
    }

    /**
     * Generate a page slug taking the title, existing slug, scheduled_for, and
     * page status into consideration. Can be called on any page to generate a slug but will only return a different
     * slug if the page is not already published.
     * @param null $primary_id
     * @param null $trigger
     * @return array
     */
    public function actionGenerateOrValidateSlug($primary_id = null, $trigger = null)
    {
        /* Response will be JSON */
        Yii::$app->response->format = Response::FORMAT_JSON;

        /* Load the primary model or just instantiate a new page if there is no primary (as on a new page) */
        $model = ($primary_id) ? Page::findModel($primary_id) : new Page();

        /* Load $_POST data so the title and scheduled_for can be used to generate the slug */
        $model->load(Yii::$app->request->post());

        /**
         * If triggered via the slug field, no generation should occur, only validation. If triggered via any other
         * field, a new slug can be generated (but only if the page is not published).
         */
        if ($trigger == 'slug') {
            /* Validate only the slug field */
            if ($model->validate('slug')) {
                /* Slug is valid */
                $return['notify'] = 'Slug validated.';
                $return['notifyType'] = 'success';
            } else {
                /* Validation failed meaning the slug is not unique */
                $return['notify'] = implode(' ', $model->firstErrors);
                $return['notifyType'] = 'warning';
            }
        } else {
            /* If the page is published or there is no title, a slug should not be generated. */
            if ($model->status == Page::STATUS_PUBLISHED) {
                /* Page published, slug should not be generated */
                $return['notify'] = 'Slug not generated. Page is published.';
                $return['notifyType'] = 'success';
            } elseif (strlen($model->title) == 0) {
                /* Title is empty, slug should not be generated (but an empty slug should be returned) */
                $model->slug = '';
                $return['notify'] = 'Slug not generated. Title is not set.';
                $return['notifyType'] = 'success';
            } else {
                /* Page is not published and there is a title. Generate a unique slug */
                $model->generateAndSetSlug();
                $return['notify'] = 'Slug generated.';
                $return['notifyType'] = 'success';
            }
        }

        $return['slug'] = $model->slug;
        $return['url'] = Url::toRoute($model->route());

        return $return;
    }
}
