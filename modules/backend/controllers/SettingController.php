<?php

namespace app\modules\backend\controllers;

use app\modules\base\models\Setting;
use app\modules\base\models\SettingForm;
use yii;
use yii\base\Model;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\HttpException;

/**
 * SettingController implements the CRUD actions for Setting model.
 */
class SettingController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index',
                        ],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists individual setting pages
     * @param string $group
     * @return mixed
     * @throws HttpException
     */
    public function actionIndex($group = null)
    {
        /**
         * Scattered through the code below are returns or redirects, depending on what is happening. If settings are
         * posted and saved successfully, redirect is used so that an entirely new request can be started, possibly
         * reflecting updated settings (such as blog title). If a return was used, the bootstrap would have already
         * take place which means the settings would have already been read from the cache by the time this return was
         * called.
         *
         * The return us used in every other place. Either as the original, non-form-post return, or a return when a
         * validation error exists and the form needs to be returned with its data intact and error messages included.
         */

        // Check permissions
        if (\Yii::$app->user->can('settingUpdate')) {

            /* New instance of setting form which contains all settings as attributes and performs validation */
            $model = new SettingForm();

            /* Load posted data, validate, and save  */
            if ($model->load(Yii::$app->request->post())) {
                if ($model->validate()) {
                    if ($model->saveSettings()) {
                        /* No errors, save was successful */
                        Yii::$app->session->setFlash('success', 'Settings saved.');
                        return $this->redirect([$group]);
                    } else {
                        /* There was an error during saving */
                        Yii::$app->session->setFlash('danger', implode('<br>', $model->firstErrors));
                    }
                } else {
                    /* There was an error during valiation */
                    Yii::$app->session->setFlash('warning', implode('<br>', $model->firstErrors));
                }
            }

            return $this->render($group, ['model' => $model]);
        } else {
            throw new HttpException(403, 'You do not have permission to update settings.');
        }
    }
}
