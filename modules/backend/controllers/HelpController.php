<?php

namespace app\modules\backend\controllers;

use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * HelpController displaying static help pages
 *
 * http://stackoverflow.com/a/33097117/3344794
 */
class HelpController extends Controller
{
    /* Set defaultAction to the only action in this controller, page */
    public $defaultAction = 'page';

    /* Set layout to help to include help navigation */
    public $layout = 'help';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['page'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Single action, specified above as $defaultAction, to serve static content help pages
     *
     * @return array
     */
    public function actions()
    {
        return [
            'page' => array(
                'class' => 'yii\web\ViewAction',
                'viewPrefix' => null,
            ),
        ];
    }
}
