<?php

namespace app\modules\backend\helpers;

use yii;

class DataHelper
{
    /**
     * Encrypt a value using the provided key then UTF-8 encode the result.
     *
     * @param $value
     * @param $key
     * @return string
     */
    public static function encryptAndUtf8EncodeByKey($value, $key)
    {
        return ($value != null) ? utf8_encode(Yii::$app->getSecurity()->encryptByKey($value, $key)) : null;
    }

    /**
     * Decode a UTF-8 value then decrypt the result using the configured site encryption key.
     *
     * @param $value
     * @param $key
     * @return string
     */
    public static function utf8DecodeAndDecryptByKey($value, $key)
    {
        return ($value != null) ? Yii::$app->getSecurity()->decryptByKey(utf8_decode($value), $key) : null;
    }

    /**
     * Encrypt a value using the configured site encryption key then UTF-8 encode the result.
     *
     * @param $value
     * @return string
     */
    public static function encryptAndUtf8Encode($value)
    {
        return self::encryptAndUtf8EncodeByKey($value, \Yii::$app->params['encryptionKey']);
    }

    /**
     * Decode a UTF-8 value then decrypt the result using the configured site encryption key.
     *
     * @param $value
     * @return string
     */
    public static function utf8DecodeAndDecrypt($value)
    {
        return self::utf8DecodeAndDecryptByKey($value, \Yii::$app->params['encryptionKey']);
    }

    /**
     * Encode a value to a JSON array. Only JSON encode if value to encode is a PHP array.
     *
     * @param $value
     * @return string
     */
    public static function jsonEncode($value)
    {
        return (is_array($value)) ? json_encode($value) : $value;
    }

    /**
     * Decode a JSON value. If there was an error decoding (such as bad JSON or value was not JSON at all, e.g. a plain
     * string) return the value as provided.
     *
     * @param $value
     * @return mixed
     */
    public static function jsonDecode($value)
    {
        $decoded = json_decode($value, true);

        return (json_last_error() === JSON_ERROR_NONE) ? $decoded : $value;
    }

    /**
     * Encrypt a value for storage (such as in a database). Value will be JSON encoded, encrypted with site encryption
     * key, then UTF8 encoded.
     *
     * @param $value
     * @return string
     */
    public static function encryptForStorage($value)
    {
        return self::encryptAndUtf8Encode(self::jsonEncode($value));
    }

    /**
     * Decrypt a value from storage (such as in a database). Value will be decoded from UTF8, decrypted with site
     * encryption key, then JSON decoded.
     *
     * @param $value
     * @return mixed
     */
    public static function decryptFromStorage($value)
    {
        return self::jsonDecode(self::utf8DecodeAndDecrypt($value));
    }
}
