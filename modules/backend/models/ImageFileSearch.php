<?php

namespace app\modules\backend\models;

use app\modules\base\models\Image;
use app\modules\base\models\Setting;
use yii\data\ArrayDataProvider;

/**
 * ImageFileSearch represents the model behind the search form about `app\modules\base\models\ImageFile`.
 */
class ImageFileSearch extends Image
{
    /**
     * @var string The name of the setting that indicates items per page (differs between backend and frontend)
     */
    public $pageSizeSetting = 'admin_items_per_page';

    /**
     * Creates data provider instance with search query applied
     *
     * @return ArrayDataProvider
     */
    public function search()
    {
        /* Retrieve all variations for the searched path */
        $image = Image::findModelByPath($this->path);
        $allModels = $image->imageFile()->variationImageFiles();

        /* Setup ArrayDataProvider to allow sorting */
        $dataProvider = new ArrayDataProvider([
            'allModels' => $allModels,
            'pagination' => [
                'pageSize' => Setting::getSetting($this->pageSizeSetting),
            ],
            'sort' => [
                'attributes' => [
                    'exists',
                    'path',
                    'absolutePath',
                    'directory',
                    'absoluteDirectory',
                    'fileName',
                    'baseName',
                    'extension',
                    'width',
                    'height',
                    'fileSize',
                    'isAnimatedGif'
                ],
                'defaultOrder' => ['width' => SORT_ASC],
            ],
        ]);

        return $dataProvider;
    }
}
