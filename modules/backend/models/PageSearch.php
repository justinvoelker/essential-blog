<?php

namespace app\modules\backend\models;

use app\modules\base\models\Page;
use app\modules\base\models\Setting;
use app\modules\frontend\models\PageSearch as PageSearchFrontend;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PageSearch represents the model behind the search form about `app\modules\base\models\Page`.
 */
class PageSearch extends Page
{
    /**
     * @var string The name of the setting that indicates items per page (differs between backend and frontend)
     */
    public $pageSizeSetting = 'admin_items_per_page';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'created_at', 'updated_by', 'updated_at', 'published_at', 'revised_at'], 'integer'],
            [['title', 'slug', 'content', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        /**
         * Page publish date is either the scheduled date (if set) or the published date (if set). If neither are set,
         * then current timestamp. Current time stamp is used as the "else" since that is the date/time that will be
         * used in generating slugs for newly created pages. Besides, that date won't matter on the frontend anyway
         * since only published pages are shown.
         */
        $queryPublishDate = 'IF(scheduled_for IS NOT NULL, scheduled_for, IF(published_at IS NOT NULL, published_at, UNIX_TIMESTAMP()))';

        $query = Page::find()
            ->select([
                /* All page fields */
                '{{%page}}.*',
                /* Calculated fields for publish_date and publish date parts */
                $queryPublishDate . ' AS publish_date',
                PageSearchFrontend::queryUrlPiece('%year%') . ' AS publish_year',
                PageSearchFrontend::queryUrlPiece('%month%') . ' AS publish_month',
                PageSearchFrontend::queryUrlPiece('%day%') . ' AS publish_day',
                /* Calculated field for configured url structure (for validating slug uniqueness) */
                PageSearchFrontend::queryUrlPath() . ' AS url_path',
                /* Calculated field for title (for titles that are different on child pages and not-yet-published pages) */
                '(SELECT title FROM {{%page}} p WHERE p.primary_id={{%page}}.id ORDER BY created_at DESC LIMIT 1) AS title_revised'
            ]);

        /**
         * The following conditions will ALWAYS apply:
         * 1) Page is primary
         */
        $query->andWhere(['primary_id' => null]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => Setting::getSetting($this->pageSizeSetting),
            ],
            'sort' => [
                'defaultOrder' => ['created_at' => SORT_DESC]
            ],
        ]);

        $dataProvider->sort->attributes['publish_date'] = [
            'asc' => ['publish_date' => SORT_ASC],
            'desc' => ['publish_date' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['publish_year'] = [
            'asc' => ['publish_year' => SORT_ASC],
            'desc' => ['publish_year' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['publish_month'] = [
            'asc' => ['publish_month' => SORT_ASC],
            'desc' => ['publish_month' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['publish_day'] = [
            'asc' => ['publish_day' => SORT_ASC],
            'desc' => ['publish_day' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['title_revised'] = [
            'asc' => ['title_revised' => SORT_ASC],
            'desc' => ['title_revised' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
            'published_at' => $this->published_at,
            'revised_at' => $this->revised_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
