<?php

use app\modules\backend\widgets\ImageModal;
use app\modules\backend\widgets\Panel;
use app\modules\base\models\Setting;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \app\modules\base\models\SettingForm */

$this->title = 'Blog Settings';
$this->params['breadcrumbs'][] = $this->title;

$this->params['helpTitle'] = $this->title . ' Help';
$this->params['helpPath'] = '/setting/blog';

$this->registerJsFile('/backend/js/setting/action-index.js', ['depends' => '\app\assets\AdminAsset']);

$this->params['action-buttons'] = [
    [
        'name' => 'main',
        'primary' => true,
        'items' => [
            [
                'icon' => 'fa-floppy-o',
                'text' => 'Save',
                'form' => 'setting-form',
                'options' => null,
                'authorized' => \Yii::$app->user->can('settingUpdate'),
            ],
        ]
    ],
];
?>

<div class="setting-index-blog content-wrapper">

    <?php $form = ActiveForm::begin(['id' => 'setting-form']); ?>

    <?= Html::beginTag('div', ['class' => 'form-group']) ?>
    <?= Html::input('text', 'setting-search', null,
        ['id' => 'setting-search', 'class' => 'form-control', 'autofocus' => true, 'placeholder' => 'Search']) ?>
    <?= Html::endTag('div') ?>

    <p id="no-matching-settings" class="hidden">No settings match the provided search criteria.</p>

    <?php Panel::begin(['title' => 'General']); ?>

    <?= $form->field($model, 'blog_title')->textInput()
        ->label($model->getAttributeLabel('blog_title'), ['data-additional-search-terms' => 'name header']) ?>

    <?= $form->field($model, 'blog_tagline')->textInput()
        ->label($model->getAttributeLabel('blog_tagline'), ['data-additional-search-terms' => 'description']) ?>

    <?= $form->field($model, 'allow_indexing')->dropDownList([
        'true' => 'Yes, allow indexing',
        'false' => 'No, attempt to prevent indexing'
    ])
        ->label($model->getAttributeLabel('allow_indexing')) ?>

    <?php Panel::end(); ?>

    <?php Panel::begin(['title' => 'Appearance']); ?>

    <?php
    /* Add default, delivered theme to beginning of theme list */
    $themes = [];
    $themes[''] = 'Default';
    $themes = array_merge($themes, Setting::getThemes());
    ?>
    <?= $form->field($model, 'blog_theme')->dropDownList($themes)
        ->label($model->getAttributeLabel('blog_theme'), ['data-additional-search-terms' => 'scheme layout']) ?>

    <?= $form->field($model, 'frontend_items_per_page')->textInput()
        ->label($model->getAttributeLabel('frontend_items_per_page'),
            ['data-additional-search-terms' => 'posts pages tags categories category']) ?>

    <?= $form->field($model, 'time_zone')->dropDownList(Setting::getTimeZones())
        ->label($model->getAttributeLabel('time_zone'), ['data-additional-search-terms' => 'timezone']) ?>

    <?php Panel::end(); ?>

    <?php Panel::begin(['title' => 'Links']); ?>

    <?= $form->field($model, 'url_structure_post')->textInput()
        ->label($model->getAttributeLabel('url_structure_post'), ['data-additional-search-terms' => 'links posts']) ?>

    <?= $form->field($model, 'url_structure_page')->textInput()
        ->label($model->getAttributeLabel('url_structure_page'), ['data-additional-search-terms' => 'links pages']) ?>

    <?= $form->field($model, 'url_structure_tag')->textInput()
        ->label($model->getAttributeLabel('url_structure_tag'), ['data-additional-search-terms' => 'links tags']) ?>

    <?= $form->field($model, 'url_structure_category')->textInput()
        ->label($model->getAttributeLabel('url_structure_category'),
            ['data-additional-search-terms' => 'links categories']) ?>

    <?php Panel::end(); ?>

    <?php ActiveForm::end(); ?>

</div>

<?= ImageModal::widget() ?>
