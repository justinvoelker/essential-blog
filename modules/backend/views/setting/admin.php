<?php

use app\modules\backend\widgets\ImageModal;
use app\modules\backend\widgets\Panel;
use app\modules\backend\widgets\PasswordInput;
use app\modules\backend\widgets\IntervalInput;
use app\modules\base\models\Setting;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \app\modules\base\models\SettingForm */

$this->title = 'Admin Settings';
$this->params['breadcrumbs'][] = $this->title;

$this->params['helpTitle'] = $this->title . ' Help';
$this->params['helpPath'] = '/setting/admin';

$this->registerJsFile('/backend/js/setting/action-index.js', ['depends' => '\app\assets\AdminAsset']);

$this->params['action-buttons'] = [
    [
        'name' => 'main',
        'primary' => true,
        'items' => [
            [
                'icon' => 'fa-floppy-o',
                'text' => 'Save',
                'form' => 'setting-form',
                'options' => null,
                'authorized' => \Yii::$app->user->can('settingUpdate'),
            ],
        ]
    ],
];
?>

    <div class="setting-index-admin content-wrapper">

        <?php $form = ActiveForm::begin(['id' => 'setting-form']); ?>

        <?= Html::beginTag('div', ['class' => 'form-group']) ?>
        <?= Html::input('text', 'setting-search', null,
            ['id' => 'setting-search', 'class' => 'form-control', 'autofocus' => true, 'placeholder' => 'Search']) ?>
        <?= Html::endTag('div') ?>

        <p id="no-matching-settings" class="hidden">No settings match the provided search criteria.</p>

        <?php Panel::begin(['title' => 'Interface']); ?>

        <?php
        $colorSchemeBoxes = '<div class="color-scheme-selector">';
        foreach (Setting::getAdminColorSchemes() as $value => $description) {
            $colorSchemeBoxes .= \yii\helpers\Html::tag('div', null, [
                'class' => [
                    'color-scheme-selector__color',
                    'color-scheme-selector__color_' . $value,
                    ($value == $model->admin_color_scheme) ? 'color-scheme-selector__color_active' : null
                ],
                'title' => $description,
                'data' => ['scheme-value' => $value],
            ]);
        }
        $colorSchemeBoxes .= '</div>';
        ?>
        <?= $form->field($model, 'admin_color_scheme', [
            'template' => "{label}\n{input}\n" . $colorSchemeBoxes . "\n{hint}\n{error}",
        ])->hiddenInput()
            ->label($model->getAttributeLabel('admin_color_scheme'), ['data-additional-search-terms' => 'theme']) ?>

        <?= $form->field($model, 'admin_items_per_page')->textInput()
            ->label($model->getAttributeLabel('admin_items_per_page'), ['data-additional-search-terms' => 'posts pages images tags categories category users injections']) ?>

        <?php Panel::end(); ?>

        <?php Panel::begin(['title' => 'Content']); ?>

        <?= $form->field($model, 'autosave_seconds')->textInput()
            ->label($model->getAttributeLabel('autosave_seconds'), ['data-additional-search-terms' => 'saving drafts revisions versions']) ?>

        <?= $form->field($model, 'restore_points_enabled')->dropDownList([
            'true' => 'Yes, enable restore points',
            'false' => 'No, disable restore points'
        ])
            ->label($model->getAttributeLabel('restore_points_enabled'), ['data-additional-search-terms' => 'saving save drafts revisions versions']) ?>

        <?= $form->field($model, 'maximum_upload_size')->textInput()
            ->label($model->getAttributeLabel('maximum_upload_size'), ['data-additional-search-terms' => 'images files limit']) ?>

        <?= $form->field($model, 'image_responsive_widths')->textInput()
            ->label($model->getAttributeLabel('image_responsive_widths'), ['data-additional-search-terms' => 'images sizes heights']) ?>

        <?php Panel::end(); ?>

        <?php Panel::begin(['title' => 'Outgoing Email']); ?>

        <?= $form->field($model, 'support_email_address')->textInput()
            ->label($model->getAttributeLabel('support_email_address'), ['data-additional-search-terms' => '']) ?>

        <?= $form->field($model, 'mail_delivery_method')->dropDownList([
            /*'mail' => 'Send emails via PHP mail() function',*/
            'smtp' => 'Send emails via external SMTP server',
            'file' => 'Do not send emails--only save emails locally in runtime/mail directory'
        ])
            ->label($model->getAttributeLabel('mail_delivery_method'), ['data-additional-search-terms' => '']) ?>

        <?= $form->field($model, 'mail_smtp_host')->textInput()
            ->label($model->getAttributeLabel('mail_smtp_host'), ['data-additional-search-terms' => '']) ?>

        <?= $form->field($model, 'mail_smtp_port')->textInput()
            ->label($model->getAttributeLabel('mail_smtp_port'), ['data-additional-search-terms' => '']) ?>

        <?= $form->field($model, 'mail_smtp_encryption')->dropDownList([
            '' => 'No encryption',
            'ssl' => 'SSL',
            'tls' => 'TLS'
        ])
            ->label($model->getAttributeLabel('mail_smtp_encryption'), ['data-additional-search-terms' => '']) ?>

        <?= $form->field($model, 'mail_smtp_username')->textInput()
            ->label($model->getAttributeLabel('mail_smtp_username'), ['data-additional-search-terms' => '']) ?>

        <?= $form->field($model, 'mail_smtp_password')->widget(PasswordInput::className())
            ->label($model->getAttributeLabel('mail_smtp_password'), ['data-additional-search-terms' => '']) ?>

        <?php Panel::end(); ?>

        <?php Panel::begin(['title' => 'Security']); ?>

        <?= $form->field($model, 'user_session_duration')->widget(IntervalInput::className())
            ->label($model->getAttributeLabel('user_session_duration'), ['data-additional-search-terms' => 'timeout']) ?>

        <?= $form->field($model, 'user_security_token_expiration')->widget(IntervalInput::className())
            ->label($model->getAttributeLabel('user_security_token_expiration'), ['data-additional-search-terms' => 'timeout']) ?>

        <?= $form->field($model, 'authclient_facebook_id')->textInput()
            ->label($model->getAttributeLabel('authclient_facebook_id'), ['data-additional-search-terms' => 'login']) ?>

        <?= $form->field($model, 'authclient_facebook_secret')->widget(PasswordInput::className())
            ->label($model->getAttributeLabel('authclient_facebook_secret'), ['data-additional-search-terms' => 'login']) ?>

        <?= $form->field($model, 'authclient_google_id')->textInput()
            ->label($model->getAttributeLabel('authclient_google_id'), ['data-additional-search-terms' => 'login']) ?>

        <?= $form->field($model, 'authclient_google_secret')->widget(PasswordInput::className())
            ->label($model->getAttributeLabel('authclient_google_secret'), ['data-additional-search-terms' => 'login']) ?>

        <?php Panel::end(); ?>

        <?php ActiveForm::end(); ?>

    </div>

<?= ImageModal::widget() ?>

<?php
$script = <<< JS
    /* On click of color box (change of color), change current color scheme as preview */
    $('.color-scheme-selector__color').click(function() {
        var body = $("body");
        /* Get color scheme from data attribute */
        var colorScheme = $(this).data('scheme-value');
        /* Update input with selected color scheme */
        $('#settingform-admin_color_scheme').val(colorScheme);
        /* Remove current classes from body element that begin with 'color-scheme__' */
        body.removeClass (function (index, className) {
            return (className.match (/(^|\s)color-scheme__\S+/g) || []).join(' ');
        });
        /* Add new class to body element for selected color scheme */
        body.addClass('color-scheme__' + colorScheme);
    })
JS;
$this->registerJs($script, View::POS_READY);

