<?php

use app\components\Functions;
use app\modules\backend\widgets\ImageModal;
use app\modules\backend\widgets\Panel;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\backend\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Images';
$this->params['breadcrumbs'][] = $this->title;

$this->params['action-buttons'] = [
    [
        'name' => 'main',
        'primary' => true,
        'items' => [
            [
                'icon' => 'fa-plus',
                'text' => 'New Image',
                'for' => 'image-modal',
                'options' => [
                    'class' => ['btn-images-upload', 'action-standard']
                ],
                'authorized' => \Yii::$app->user->can('imageCreate'),
            ],
        ]
    ],
    /* Action bar group for record selection */
    [
        'name' => 'index-selection',
        'items' => [
            [
                'icon' => 'fa-pencil',
                'text' => 'Edit',
                'url' => null,
                'options' => [
                    'data' => [
                        'href-template' => Url::to(['update', 'id' => '0']),
                        'selection-allowed' => '1',
                    ],
                ],
                'authorized' => true,
            ],
            [
                'icon' => 'fa-trash',
                'text' => 'Delete',
                'url' => null,
                'options' => [
                    'data' => [
                        'href-template' => Url::to(['delete', 'ids' => '0']),
                        'selection-allowed' => 'N',
                    ],
                ],
                'authorized' => true,
            ],
        ]
    ]
];
?>

    <div class="image-index content-wrapper">

        <?php Panel::begin(['title' => $this->title]); ?>

        <?php Pjax::begin(['id' => 'image-grid']); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'options' => ['class' => 'grid-view table-responsive table-avatar-first-column table-selectable table-selectable_avatar'],
            'emptyText' => 'No images found. Click "New Image" above to get started.',
            'columns' => [
                [
                    'format' => 'raw',
                    'value' => function ($data) {
                        /* @var $data app\modules\base\models\Image */
                        return Functions::getSelectableAvatar([
                            'action-bar-group' => 'index-selection',
                            'record-id' => $data->id,
                            'title' => $data->alternative_text,
                            'image' => $data->path,
                            'size' => '50px',
                        ]);
                    },
                ],
                [
                    'attribute' => 'alternative_text',
                    'format' => 'raw',
                    'value' => function ($data) {
                        /* @var $data app\modules\base\models\Image */
                        $text = Html::encode($data->alternative_text);
                        if ($data->imageFile()->variationsHaveWarning()) {
                            $text  .= ' <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>';
                        }
                        return Html::a($text, ['view', 'id' => $data->id], ['class' => 'primary'])
                        . Html::tag('div', $data->path, ['class' => 'secondary']);
                    },
                ],
                [
                    'attribute' => 'created_at',
                    'label' => 'Uploaded',
                    'value' => function ($data) {
                        return Yii::$app->formatter->asDate($data->created_at, 'full');
                    }
                ],
                //'imagePostsCount',
            ],
        ]); ?>

        <?php Pjax::end(); ?>

        <?php Panel::end(); ?>

    </div>

<?= ImageModal::widget([
    'selectable' => false,
    'headerText' => 'Upload',
    'acceptButtonText' => false,
    'cancelButtonText' => 'Close',
]) ?>

<?php
$script = <<< JS

// On modal hide, refresh image grid (lazysizes will automatically pick up the newly loaded image(s))
$('#image-modal').on('modalHidden', function() {
    $.pjax.reload({container:'#image-grid'});
});

JS;
$this->registerJs($script, $this::POS_READY);

