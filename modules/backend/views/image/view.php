<?php

use app\modules\backend\widgets\Panel;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\modules\base\models\Image */
/* @var $dataProvider yii\data\ArrayDataProvider */

$title = $model->alternative_text;

$this->title = 'View Image: ' . $title;
$this->params['breadcrumbs'][] = ['label' => 'Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = $title;

$this->params['action-buttons'] = [
    [
        'name' => 'main',
        'primary' => true,
        'items' => [
            [
                'icon' => 'fa-pencil',
                'text' => 'Edit',
                'url' => ['update', 'id' => $model->id],
                'options' => null,
                'authorized' => \Yii::$app->user->can('imageUpdate', ['model' => $model]),
            ],
            [
                'icon' => 'fa-trash',
                'text' => 'Delete',
                'url' => ['delete', 'ids' => $model->id],
                'options' => [
                    'data' => ['confirm' => 'Are you sure you want to delete this image?'],
                ],
                'authorized' => \Yii::$app->user->can('imageDelete', ['model' => $model]),
            ],
        ]
    ],
];
?>

<div class="image-view content-wrapper">

    <?php Panel::begin(['title' => 'Image Information']); ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'alternative_text',
            'title',
            'path',
            [
                'attribute' => 'width',
                'value' => $model->imageFile()->width,
            ],
            [
                'attribute' => 'height',
                'value' => $model->imageFile()->height,
            ],
            [
                'attribute' => 'fileSize',
                'value' => Yii::$app->formatter->asShortSize($model->imageFile()->fileSize, 0),
            ],
        ],
    ]) ?>

    <?php Panel::end(); ?>

    <?php
    /* If there are any warnings within the image variations, show the warning icon in the panel header. */
    $panelTitleImageVariations = 'Image Variations';
    if ($model->imageFile()->variationsHaveWarning()) {
        $panelTitleImageVariations .= ' <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>';
    }
    ?>
    <?php Panel::begin(['title' => $panelTitleImageVariations, 'collapsable' => true, 'collapsed' => true]); ?>

    <?php Pjax::begin(['id' => 'image-variation-subgrid', 'enablePushState' => false, 'linkSelector' => 'thead a, .pagination a']); ?>

    <?php
    /* If there are image variations warnings, display them here. */
    if ($model->imageFile()->variationsHaveWarning()) {
        echo Html::beginTag('div', ['class' => 'warning-summary']);
        if ($model->imageFile()->variationsMissing()) {
            echo 'Missing image variations:';
            echo '<ul><li>';
            echo implode("</li><li>", $model->imageFile()->variationsMissing());
            echo '</li></ul>';
        }
        if ($model->imageFile()->variationsLargerOrEqual()) {
            echo 'Image variations larger than or equal to the original: ';
            echo '<ul><li>';
            /* This really does get the column 'width' since that is what should display in the message. */
            echo implode("</li><li>", ArrayHelper::getColumn($model->imageFile()->variationsLargerOrEqual(), 'width'));
            echo '</li></ul>';
        }
        if ($model->imageFile()->variationsWiderOrEqual()) {
            echo 'Image variations wider than or equal to the original: ';
            echo '<ul><li>';
            echo implode("</li><li>", ArrayHelper::getColumn($model->imageFile()->variationsWiderOrEqual(), 'width'));
            echo '</li></ul>';
        }
        echo Html::endTag('div');
    }
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'options' => ['class' => 'grid-view table-responsive table-avatar-first-column table-selectable table-selectable_avatar'],
        'emptyText' => 'No variations found. To add image variations, edit the image and click "Add Variations" in the action bar.',
        'columns' => [
            'fileName',
            [
                'attribute' => 'width',
                'format' => 'raw',
                'value' => function ($data) use ($model) {
                    /* @var $data app\modules\base\models\ImageFile */
                    /* @var $model app\modules\base\models\Image */
                    $result = $data->width;
                    /* Display warning icon if variation width is in list of variationsWiderOrEqual() */
                    if (in_array($data->width, ArrayHelper::getColumn($model->imageFile()->variationsWiderOrEqual(), 'width'))) {
                        $result .= ' <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>';
                    }
                    return $result;
                }
            ],
            'height',
            [
                'attribute' => 'fileSize',
                'format' => 'raw',
                'value' => function ($data) use ($model) {
                    /* @var $data app\modules\base\models\ImageFile */
                    /* @var $model app\modules\base\models\Image */
                    $result = Yii::$app->formatter->asShortSize($data->fileSize, 0);
                    /*
                     * Display warning icon if this variation is in list of variationsLargerOrEqual(). Yes, this uses
                     * the width column because the width is essentially the key to variations. The method has already
                     * done work of determining if the variation should be included in this last based on file size.
                     */
                    if (in_array($data->width, ArrayHelper::getColumn($model->imageFile()->variationsLargerOrEqual(), 'width'))) {
                        $result .= ' <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>';
                    }
                    return $result;
                }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

    <?php Panel::end(); ?>

    <?php Panel::begin(['title' => 'Preview', 'collapsable' => true, 'collapsed' => true]); ?>

    <?php
    echo $model->imgTag([
        /*
         * (max-width: 543px) = 100% - 30px .content padding - 30px .panel__body padding
         * (max-width: 1170px) = 100% - 30px .content padding - 30px .panel__body padding - 240px .sidebar_main
         * 768px = 800px .content-wrapper max-width - 2px .panel border - 30px .panel__body padding
         */
        'sizes' => '(max-width: 543px) calc(100vw - 30px - 30px), (max-width: 1170px) calc(100vw - 30px - 30px - 240px), 768px',
        'class' => 'img-responsive',
    ]);
    ?>

    <?php Panel::end(); ?>

</div>
