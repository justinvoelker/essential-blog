<?php

use app\components\Functions;
use app\modules\backend\widgets\ImageModal;
use app\modules\backend\widgets\Panel;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\modules\base\models\Image */
/* @var $dataProvider yii\data\ArrayDataProvider */

$title = $model->alternative_text;
$truncatedTitle = StringHelper::truncate($title, 20);

$this->title = 'Update Image: ' . $title;
$this->params['breadcrumbs'][] = ['label' => 'Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $truncatedTitle, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';

$this->params['action-buttons'] = [
    [
        'name' => 'main',
        'primary' => true,
        'items' => [
            [
                'icon' => 'fa-floppy-o',
                'text' => 'Save',
                'url' => null,
                'form' => 'image-form',
                'options' => null,
                'authorized' => \Yii::$app->user->can('imageUpdate', ['model' => $model]),
            ],
            [
                'icon' => 'fa-retweet',
                'text' => 'Replace Original',
                'for' => 'image-modal',
                'options' => [
                    'data-actionUrl' => Url::to(['/admin/image/replace-original', 'id' => $model->id])
                ],
                'authorized' => \Yii::$app->user->can('imageUpdate', ['model' => $model]),
            ],
            [
                'icon' => 'fa-retweet',
                'text' => 'Recreate Variations',
                'url' => Url::to(['recreate-variations', 'id' => $model->id]),
                'options' => [
                    'data' => [
                        'href-action' => 'recreate-variations',
                    ]
                ],
                'authorized' => \Yii::$app->user->can('imageUpdate', ['model' => $model]),
            ],
            [
                'icon' => 'fa-plus',
                'text' => 'Upload Variations',
                'for' => 'image-modal',
                'options' => [
                    'data-actionUrl' => Url::to(['/admin/image/upload-variations', 'id' => $model->id])
                ],
                'authorized' => \Yii::$app->user->can('imageUpdate', ['model' => $model]),
            ],
        ]
    ],
    /* Action bar group for record selection */
    [
        'name' => 'variation-selection',
        'items' => [
            [
                'icon' => 'fa-trash',
                'text' => 'Delete Variation',
                'url' => null,
                'options' => [
                    'data' => [
                        'href-template' => Url::to(['delete-variation', 'id' => $model->id, 'variations' => '0']),
                        'href-action' => 'delete-variation',
                        'selection-allowed' => 'N',
                    ],
                ],
                'authorized' => \Yii::$app->user->can('imageUpdate', ['model' => $model]),
            ],
        ]
    ]
];
?>

<div class="image-update content-wrapper">

    <?php Panel::begin(['title' => 'Image Content']); ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

    <?php Panel::end(); ?>

    <?php Panel::begin(['title' => 'Image Variations', 'collapsable' => true, 'collapsed' => false]); ?>

    <?php Pjax::begin(['id' => 'image-variation-subgrid', 'enablePushState' => false, 'linkSelector' => 'thead a, .pagination a']); ?>

    <?php
    /* If there are image variations warnings, display them here. */
    if ($model->imageFile()->variationsHaveWarning()) {
        echo Html::beginTag('div', ['class' => 'warning-summary']);
        if ($model->imageFile()->variationsMissing()) {
            echo 'Missing image variations:';
            echo '<ul><li>';
            echo implode("</li><li>", $model->imageFile()->variationsMissing());
            echo '</li></ul>';
        }
        if ($model->imageFile()->variationsLargerOrEqual()) {
            echo 'Image variations larger than or equal to the original: ';
            echo '<ul><li>';
            /* This really does get the column 'width' since that is what should display in the message. */
            echo implode("</li><li>", ArrayHelper::getColumn($model->imageFile()->variationsLargerOrEqual(), 'width'));
            echo '</li></ul>';
        }
        if ($model->imageFile()->variationsWiderOrEqual()) {
            echo 'Image variations wider than or equal to the original: ';
            echo '<ul><li>';
            echo implode("</li><li>", ArrayHelper::getColumn($model->imageFile()->variationsWiderOrEqual(), 'width'));
            echo '</li></ul>';
        }
        echo Html::endTag('div');
    }
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => ['class' => 'grid-view grid-view_subgrid table-responsive table-avatar-first-column table-selectable table-selectable_avatar'],
        'emptyText' => 'No variations found. To add image variations, click "Add Variation" in the action bar.',
        'columns' => [
            [
                'format' => 'raw',
                'value' => function ($data) {
                    /* @var $data app\modules\base\models\ImageFile */
                    return Functions::getSelectableAvatar([
                        'action-bar-group' => 'variation-selection',
                        'record-id' => $data->width,
                        'size' => '20px',
                    ]);
                },
            ],
            'fileName',
            [
                'attribute' => 'width',
                'format' => 'raw',
                'value' => function ($data) use ($model) {
                    /* @var $data app\modules\base\models\ImageFile */
                    /* @var $model app\modules\base\models\Image */
                    $result = $data->width;
                    /* Display warning icon if variation width is in list of variationsWiderOrEqual() */
                    if (in_array($data->width, ArrayHelper::getColumn($model->imageFile()->variationsWiderOrEqual(), 'width'))) {
                        $result .= ' <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>';
                    }
                    return $result;
                }
            ],
            'height',
            [
                'attribute' => 'fileSize',
                'format' => 'raw',
                'value' => function ($data) use ($model) {
                    /* @var $data app\modules\base\models\ImageFile */
                    /* @var $model app\modules\base\models\Image */
                    $result = Yii::$app->formatter->asShortSize($data->fileSize, 0);
                    /*
                     * Display warning icon if this variation is in list of variationsLargerOrEqual(). Yes, this uses
                     * the width column because the width is essentially the key to variations. The method has already
                     * done work of determining if the variation should be included in this last based on file size.
                     */
                    if (in_array($data->width, ArrayHelper::getColumn($model->imageFile()->variationsLargerOrEqual(), 'width'))) {
                        $result .= ' <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>';
                    }
                    return $result;
                }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

    <?php Panel::end(); ?>

</div>

<?= ImageModal::widget([
    'selectable' => false,
    'actionUrl' => ['#'],
    'headerText' => 'Upload',
    'acceptButtonText' => false,
    'cancelButtonText' => 'Close',
]) ?>

<?php
/** @noinspection JSUnusedGlobalSymbols */
$script = <<< JS

// On modal hide, refresh image variation subgrid
$('#image-modal').on('modalHidden', function() {
    $.pjax.reload({container:'#image-variation-subgrid'});
});

/* On click of element with data-actionUrl attribute (upload variations, replace original) set image modal data-url */
$('[data-actionUrl]').click(function () {
    $('.image-modal #files').attr('data-url', $(this).attr('data-actionUrl'));
});

/*
 * Delete image variations and recreate image variations via ajax
 */

/* global flashAlert, setActionBarActiveGroup */

/* On click of element with data-action attribute set to delete-variation */
$('[data-href-action="delete-variation"], [data-href-action="recreate-variations"]').click(function () {

    /* Ajax call to publish action */
    $.ajax({
        url: $(this).attr('href'),
        type: 'POST',

        /* Ajax call was successful */
        success: function (data) {
            /**
             * @param data Data object returned from ajax submission
             * @param data.notify Notification message
             * @param data.notifyType Notification type (success, error, etc)
             */

            /* Display returned notification */
            flashAlert({type: data.notifyType, message: data.notify});

            /* Refresh the image variation subgrid */
            $.pjax.reload({container:'#image-variation-subgrid'});

            /* Reset the action bar to the primary group (only for delete-variations) */
            setActionBarActiveGroup('');
        },

        /* Ajax call failed (permissions issue, etc.) */
        error: function (jqXHR) {
            flashAlert({type: 'danger', message: jqXHR.responseText});
        }
    });
    return false;
});

JS;
$this->registerJs($script, $this::POS_READY);
