<?php

use app\modules\backend\widgets\Panel;

/* @var $this yii\web\View */
/* @var $model app\modules\base\models\Tag */

$this->title = 'New Tag';
$this->params['breadcrumbs'][] = ['label' => 'Tags', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['action-buttons'] = [
    [
        'name' => 'main',
        'primary' => true,
        'items' => [
            [
                'icon' => 'fa-floppy-o',
                'text' => 'Save',
                'url' => null,
                'form' => 'tag-form',
                'options' => null,
                'authorized' => \Yii::$app->user->can('tagCreate'),
            ],
        ]
    ],
];
?>

<div class="tag-create content-wrapper">

    <?php Panel::begin(['title' => 'Tag Content']); ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

    <?php Panel::end(); ?>

</div>
