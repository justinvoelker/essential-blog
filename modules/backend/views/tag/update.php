<?php

use app\modules\backend\widgets\Panel;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\base\models\Tag */

$title = $model->name;
$truncatedTitle = StringHelper::truncate($title, 20);

$this->title = 'Update Tag: ' . $title;
$this->params['breadcrumbs'][] = ['label' => 'Tags', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $truncatedTitle, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';

$this->params['action-buttons'] = [
    [
        'name' => 'main',
        'primary' => true,
        'items' => [
            [
                'icon' => 'fa-floppy-o',
                'text' => 'Save',
                'url' => null,
                'form' => 'tag-form',
                'options' => null,
                'authorized' => \Yii::$app->user->can('tagUpdate', ['model' => $model]),
            ],
        ]
    ],
];
?>

<div class="tag-update content-wrapper">

    <?php Panel::begin(['title' => 'Tag Content']); ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

    <?php Panel::end(); ?>

</div>
