<?php

use yii\widgets\ActiveForm;
use app\modules\backend\widgets\ImageModal;

/* @var $this yii\web\View */
/* @var $model app\modules\base\models\Tag */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tag-form">

    <?php $form = ActiveForm::begin([
        'id' => 'tag-form',
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'autofocus' => $model->isNewRecord]) ?>

    <?php if (!$model->isNewRecord) {
        echo $form->field($model, 'slug')->textInput(['maxlength' => true]);
    } ?>

    <?= $form->field($model, 'image', [
        'options' => ['class' => 'form-group form-group-image'],
        'template' => "{label}\n{hint}\n{error}\n{input}",
    ])->widget(\app\modules\backend\widgets\SelectImageInput::className(), [
        'sizes' => '(max-width: 543px) calc(100vw - 20px), 400px'
    ]) ?>

    <?= $form->field($model, 'description')
        ->textarea(['rows' => 3]) ?>

    <?php ActiveForm::end(); ?>

    <?= ImageModal::widget() ?>
</div>
