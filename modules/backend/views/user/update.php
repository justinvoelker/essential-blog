<?php

use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\base\models\User */

$title = (strlen(trim($model->firstNameLastName())) > 0) ? $model->firstNameLastName() : $model->email;

$truncatedTitle = StringHelper::truncate($title, 20);

$this->title = 'Update User: ' . $title;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $truncatedTitle, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';

/* Panels show/hide based on if a user is updating their own record */
$isUserUpdatingOwnRecord = $model->id == Yii::$app->user->id;

$this->params['action-buttons'] = [
    [
        'name' => 'main',
        'primary' => true,
        'items' => [
            [
                'icon' => 'fa-floppy-o',
                'text' => 'Save',
                'url' => null,
                'form' => 'user-form',
                'options' => null,
                'authorized' => \Yii::$app->user->can('userUpdate', ['model' => $model]),
            ],
        ]
    ],
];
?>

<div class="user-update content-wrapper">

    <?php /* Panel is included in _form so sections can be broken down */ ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

    <?php if ($isUserUpdatingOwnRecord): ?>

    <?= $this->render('_panel_two-factor', ['model' => $model]) ?>

    <?= $this->render('_panel_external-client-connections', ['model' => $model]) ?>

    <?php endif; ?>


</div>
