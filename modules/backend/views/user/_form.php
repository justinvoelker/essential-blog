<?php

use app\components\Functions;
use app\modules\backend\widgets\ImageModal;
use app\modules\backend\widgets\Panel;
use app\modules\backend\widgets\PasswordInput;
use app\modules\backend\widgets\SelectImageInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\base\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin([
    'id' => 'user-form',
]); ?>

<?php
/* Current user status is user by client-side validation to determine which fields are required */
echo Html::hiddenInput('_user-status', $model->status);
?>

<?php Panel::begin(['title' => 'User Content']); ?>

<?php
/* Standard getAvatar for initial page load value */
echo Functions::getAvatar([
    'text' => $model->avatarText(),
    'textAsIs' => true,
    'image' => $model->profile_photo,
    'size' => '150px',
]);
?>

<?php
/* Select image input (but the image is hidden via CSS) */
echo $form->field($model, "profile_photo", [
    'options' => ['class' => 'form-group form-group-image'],
    'template' => "{label}\n{hint}\n{error}\n{input}",
])
    ->widget(SelectImageInput::className(), [
        'sizes' => '150px'
    ])
    ->label(false)
?>

<?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'email')
    ->textInput(['maxlength' => true])
    ->hint('For communications such as password reset.') ?>

<?= $form->field($model, 'short_biography')
    ->textarea(['rows' => 6])
    ->hint('Depending on theme, this may or may not be used.') ?>

<?php Panel::end(); ?>

<?php Panel::begin(['title' => 'Login Credentials', 'collapsable' => true, 'collapsed' => true]); ?>

<?= $form->field($model, 'username')
    ->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'password')
    ->label('New Password')
    ->widget(PasswordInput::className())
    ->hint('Leave blank unless you intend to change your password.') ?>

<?php Panel::end(); ?>

<?php if ($model->id != Yii::$app->user->id): ?>
    <?php Panel::begin(['title' => 'Permissions', 'collapsable' => true, 'collapsed' => true]); ?>

    <?= $form->field($model, 'role')
        ->dropDownList($model->allRoles())
        ->hint('See documentation for permissions assigned to each role.') ?>

    <?php Panel::end(); ?>
<?php endif; ?>

<?php ActiveForm::end(); ?>

<?= ImageModal::widget() ?>

<?php
$script = <<< JS
$(function() {

    /* When the image modal is accepted, insert image as avatar. Not responive, but that's ok for this situation. */
    $('.image-modal').find(".modal-accept").click(function () {
        var imgPath = encodeURI($("#image-modal-selected-image-path").val());
        $('.avatar .avatar__img').remove();
        $('.avatar').prepend('<img class="avatar__img" src="'+imgPath+'" style="width: 150px; height: 150px;">');
    });

    /* When the profile photo is cleared, delete the avatar image */
    $('.field-user-profile_photo .clear_image').click(function() {
        $('.avatar .avatar__img').remove();
    });

});
JS;
$this->registerJs($script);
