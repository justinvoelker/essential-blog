<?php

use app\modules\backend\widgets\Panel;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\modules\base\models\User */
/* @var $postsCreatedBy yii\data\ActiveDataProvider */

$title = (strlen(trim($model->firstNameLastName())) > 0) ? $model->firstNameLastName() : $model->email;

$this->title = 'View User: ' . $title;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $title;

$this->params['action-buttons'] = [
    [
        'name' => 'main',
        'primary' => true,
        'items' => [
            [
                'icon' => 'fa-pencil',
                'text' => 'Edit',
                'url' => ['update', 'id' => $model->id],
                'options' => null,
                'authorized' => \Yii::$app->user->can('userUpdate', ['model' => $model]),
            ],
            [
                'icon' => 'fa-trash',
                'text' => 'Delete',
                'url' => ['delete', 'ids' => $model->id],
                'options' => [
                    'data' => ['confirm' => 'Are you sure you want to delete this user?']
                ],
                'authorized' => \Yii::$app->user->can('userDelete', ['model' => $model]),
            ],
//            [
//                'icon' => 'fa-check-circle-o',
//                'text' => 'Enable',
//                'url' => ['update-status', 'ids' => $model->id, 'status' => Injection::STATUS_ENABLED],
//                'authorized' => \Yii::$app->user->can('injectionUpdate', ['model' => $model]),
//            ],
//            [
//                'icon' => 'fa-times-circle-o',
//                'text' => 'Disable',
//                'url' => ['update-status', 'ids' => $model->id, 'status' => Injection::STATUS_DISABLED],
//                'authorized' => \Yii::$app->user->can('injectionUpdate', ['model' => $model]),
//            ],
        ]
    ],
];
?>

<div class="user-view content-wrapper">

    <?php Panel::begin(['title' => 'User Information']); ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'first_name',
            'last_name',
            'username',
            'email:email',
            'role',
            'profile_photo',
        ],
    ]) ?>

    <?php Panel::end(); ?>

    <?php Panel::begin(['title' => 'User Posts', 'collapsable' => true, 'collapsed' => true]); ?>

    <?php Pjax::begin(['id' => 'post-subgrid', 'enablePushState' => false, 'linkSelector' => 'thead a, .pagination a']); ?>

    <?= GridView::widget([
        'dataProvider' => $postsCreatedBy,
        'options' => ['class' => 'grid-view table-responsive'],
        'columns' => [
            [
                'attribute' => 'title_revised',
                'format' => 'raw',
                'value' => function ($data) {
                    /* @var $data app\modules\base\models\Post */
                    $linkText = Html::encode($data->title_revised);
                    if ($data->statusText() != 'Published') {
                        $linkText .= ' <strong>(' . $data->statusText() . ')</strong>';
                    }
                    return Html::a($linkText, ['post/view', 'id' => $data->id]);
                },
            ],
            [
                'attribute' => 'created_at',
                'label' => 'Created',
                'value' => function ($data) {
                    return Yii::$app->formatter->asDate($data->created_at, 'long');
                }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

    <?php Panel::end(); ?>

</div>
