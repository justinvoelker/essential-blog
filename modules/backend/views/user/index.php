<?php

use app\components\Functions;
use app\modules\base\models\User;
use app\modules\backend\widgets\Modal;
use app\modules\backend\widgets\Panel;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $inviteModel app\modules\base\models\User */
/* @var $searchModel app\modules\backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

/* Register the JavaScript necessary for publishing and unpublishing posts */
$this->registerJsFile('/backend/js/user/action-index.js', ['depends' => '\app\assets\AdminAsset']);

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;

$this->params['action-buttons'] = [
    [
        'name' => 'main',
        'primary' => true,
        'items' => [
            [
                'icon' => 'fa-plus',
                'text' => 'Invite User',
                'for' => 'user-invite-modal',
                'authorized' => \Yii::$app->user->can('userCreate'),
            ],
        ]
    ],
    /* Action bar group for record selection */
    [
        'name' => 'index-selection',
        'items' => [
            [
                'icon' => 'fa-pencil',
                'text' => 'Edit',
                'url' => null,
                'options' => [
                    'data' => [
                        'href-template' => Url::to(['update', 'id' => '0']),
                        'selection-allowed' => '1',
                    ],
                ],
                'authorized' => true,
            ],
            [
                'icon' => 'fa-trash',
                'text' => 'Delete',
                'url' => null,
                'options' => [
                    'data' => [
                        'href-template' => Url::to(['delete', 'ids' => '0']),
                        'selection-allowed' => 'N',
                    ],
                ],
                'authorized' => true,
            ],
//            [
//                'icon' => 'fa-check-circle-o',
//                'text' => 'Enable',
//                'url' => null,
//                'options' => [
//                    'data' => [
//                        'href-template' => Url::to(['update-status', 'ids' => '0', 'status' => Injection::STATUS_ENABLED]),
//                        'selection-allowed' => 'N',
//                    ],
//                ],
//                'authorized' => true,
//            ],
//            [
//                'icon' => 'fa-times-circle-o',
//                'text' => 'Disable',
//                'url' => null,
//                'options' => [
//                    'data' => [
//                        'href-template' => Url::to(['update-status', 'ids' => '0', 'status' => Injection::STATUS_DISABLED]),
//                        'selection-allowed' => 'N',
//                    ],
//                ],
//                'authorized' => true,
//            ],
        ]
    ]
];
?>

    <div class="user-index content-wrapper">

        <?php Panel::begin(['title' => $this->title]); ?>

        <?php Pjax::begin(['id' => 'user-grid']); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'options' => ['class' => 'grid-view table-responsive table-avatar-first-column table-selectable table-selectable_avatar'],
            'columns' => [
                [
                    'format' => 'raw',
                    'value' => function ($data) {
                        /* @var $data app\modules\base\models\User */
                        return Functions::getSelectableAvatar([
                            'action-bar-group' => 'index-selection',
                            'record-id' => $data->id,
                            'text' => $data->avatarText(),
                            'textAsIs' => true,
                            'image' => $data->profile_photo,
                            'size' => '50px',
                        ]);
                    },
                ],
                [
                    'attribute' => 'firstNameLastName',
                    'label' => 'Name',
                    'format' => 'raw',
                    'value' => function ($data) {
                        /* @var $data app\modules\base\models\User */
                        if ($data->status == User::STATUS_ENABLED) {
                            return Html::a(Html::encode($data->firstNameLastName()), ['view', 'id' => $data->id],
                                ['class' => 'primary'])
                            . Html::tag('div', $data->lastActive(), ['class' => 'secondary']);
                        } else {
                            /*
                             * Since the entire gridview is wrapped in a Pjax widget, adding data-pjax="0" to the
                             * resend link prevents it from triggering the action twice: once as a Pjax call and again
                             * as a typical link.
                             */
                            return Html::a(Html::encode($data->email), ['view', 'id' => $data->id],
                                    ['class' => 'primary'])
                                . Html::tag('div', Html::a('Resend invitation', ['resend-invite', 'id' => $data->id], ['data-pjax' => '0']), ['class' => 'secondary']);
                        }
                    },
                ],
                [
                    'attribute' => 'role',
                    'format' => 'raw',
                    'value' => function ($data) {
                        /* @var $data app\modules\base\models\User */
                        return ($data->role) ? $data->allRoles()[$data->role] : null;
                    },
                ],
            ],
        ]); ?>

        <?php Pjax::end(); ?>

        <?php Panel::end(); ?>

    </div>

<?php

// A couple buttons in the footer
$footer = '';
$footer .= '<label class="btn btn-default modal-close" for="user-invite-modal">Cancel</label>';
$footer .= '<button class="btn btn-scheme" type="submit" form="user-invite-form">Send Invite</button>';

/* Display image modal*/
Modal::begin([
    'id' => 'user-invite-modal',
    'title' => 'Invite User',
    'footer' => $footer,
]);
?>

<?php $form = ActiveForm::begin([
    'id' => 'user-invite-form',
    'enableAjaxValidation' => true,
    'validationUrl' => ['invite-ajax-validation'],
    'action' => ['invite'],
]); ?>

<?= $form->field($inviteModel, 'email')->textInput(['maxlength' => true]) ?>
<?= $form->field($inviteModel, 'role')->dropDownList($inviteModel->allRoles()) ?>

    <div class="error-summary error-summary-custom"></div>

<?php ActiveForm::end(); ?>

<?php
Modal::end();
