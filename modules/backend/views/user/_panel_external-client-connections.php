<?php

use app\modules\backend\widgets\Panel;
use yii\authclient\widgets\AuthChoice;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\modules\base\models\User */

/* Register the JavaScript necessary for connecting and disconnecting auth clients */
$this->registerJsFile('/backend/js/user/btn-auth-connect.js', ['depends' => '\app\assets\AdminAsset']);
$this->registerJsFile('/backend/js/user/btn-auth-disconnect.js', ['depends' => '\app\assets\AdminAsset']);
?>

<?php if (array_key_exists('authClientCollection',
        Yii::$app->components) && count(Yii::$app->components['authClientCollection']['clients']) > 0
): ?>

    <?php Panel::begin(['title' => 'External Client Connections', 'collapsable' => true, 'collapsed' => true]); ?>

    <?php Pjax::begin(['id' => 'user-auth-client-connections', 'linkSelector' => false, 'enablePushState' => false]); ?>

    <?php $authAuthChoice = AuthChoice::begin([
        'baseAuthUrl' => ['/admin/user/auth-connection-connect'],
        'options' => ['class' => 'auth-clients'],
    ]); ?>
    <table>
        <?php foreach ($authAuthChoice->getClients() as $client): ?>
            <tr>
                <td><?= $client->getTitle() ?></td>
                <?php if (in_array($client->getName(), $model->authConnectionClients())): ?>
                    <td>
                        <?= Html::tag('span', 'Disconnect', [
                            'class' => 'btn btn-default btn-block',
                            'data' => [
                                'action' => 'auth-client-disconnect',
                                'authclient' => $client->getName()
                            ]
                        ]) ?>
                    </td>
                    <td>
                        Connected <?= Yii::$app->formatter
                            ->asDate($model->authConnectionDates()[$client->getName()], 'long') ?>
                    </td>
                    <?php
                    /*
                     * When the standard widget is allowed to generate a link, autoRender is automatically set to
                     * false. Since that widget is not being used here to generate the link, this needs to be set to
                     * false manually to prevent the normal "Connect" link from being generated. Oddly, only an issue
                     * when one client present...
                     */
                    $authAuthChoice->autoRender = false;
                    ?>
                <?php else: ?>
                    <td>
                        <?= $authAuthChoice->clientLink($client, 'Connect', [
                            'class' => 'auth-link ' . $client->getName() . ' btn btn-scheme btn-block'
                        ]) ?></td>
                    <td>Not connected</td>
                <?php endif; ?>
            </tr>
        <?php endforeach; ?>
    </table>
    <?php AuthChoice::end(); ?>

    <?php Pjax::end(); ?>

    <?php Panel::end(); ?>

<?php endif; ?>
