<?php

use yii\widgets\ActiveForm;
use app\modules\base\models\Injection;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\modules\base\models\Injection */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="injection-form">

    <?php $form = ActiveForm::begin([
        'id' => 'injection-form',
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'autofocus' => $model->isNewRecord]) ?>

    <?= $form->field($model, 'xpath')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'action')->dropDownList(Injection::$actions) ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'parser')->dropDownList(Injection::$parsers) ?>

    <?= $form->field($model, 'order')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList([
        '0' => 'Disabled',
        '1' => 'Enabled'
    ]) ?>

    <?php ActiveForm::end(); ?>

</div>
