<?php

use app\modules\backend\widgets\Panel;
use app\modules\base\models\Injection;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\base\models\Injection */
/* @var $posts yii\data\ActiveDataProvider */

$title = $model->name;

$this->title = 'View Injection: ' . $title;
$this->params['breadcrumbs'][] = ['label' => 'Injections', 'url' => ['index']];
$this->params['breadcrumbs'][] = $title;

$this->params['action-buttons'] = [
    [
        'name' => 'main',
        'primary' => true,
        'items' => [
            [
                'icon' => 'fa-pencil',
                'text' => 'Edit',
                'url' => ['update', 'id' => $model->id],
                'options' => null,
                'authorized' => \Yii::$app->user->can('injectionUpdate', ['model' => $model]),
            ],
            [
                'icon' => 'fa-trash',
                'text' => 'Delete',
                'url' => ['delete', 'ids' => $model->id],
                'options' => [
                    'data' => ['confirm' => 'Are you sure you want to delete this injection?']
                ],
                'authorized' => \Yii::$app->user->can('injectionDelete', ['model' => $model]),
            ],
            [
                'icon' => 'fa-check-circle-o',
                'text' => 'Enable',
                'url' => ['update-status', 'ids' => $model->id, 'status' => Injection::STATUS_ENABLED],
                'authorized' => \Yii::$app->user->can('injectionUpdate', ['model' => $model]),
            ],
            [
                'icon' => 'fa-times-circle-o',
                'text' => 'Disable',
                'url' => ['update-status', 'ids' => $model->id, 'status' => Injection::STATUS_DISABLED],
                'authorized' => \Yii::$app->user->can('injectionUpdate', ['model' => $model]),
            ],
        ]
    ],
];
?>

<div class="injection-view content-wrapper">

    <?php Panel::begin(['title' => 'Injection Information']); ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'xpath',
            [
                'attribute' => 'action',
                'value' => $model->actionText(),
            ],
            [
                'attribute' => 'content',
                'format' => 'raw',
                'value' => '<pre class="pre-wrap">' . Html::encode($model->content) . '</pre>',
            ],
            [
                'attribute' => 'parser',
                'value' => $model->parserText(),
            ],
            'order',
            [
                'attribute' => 'status',
                'value' => $model->statusText(),
            ],
        ],
    ]) ?>

    <?php Panel::end(); ?>

</div>
