<?php

use app\modules\backend\widgets\Panel;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\base\models\Injection */

$title = $model->name;
$truncatedTitle = StringHelper::truncate($title, 20);

$this->title = 'Update Injection: ' . $title;
$this->params['breadcrumbs'][] = ['label' => 'Injections', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $truncatedTitle, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';

$this->params['action-buttons'] = [
    [
        'name' => 'main',
        'primary' => true,
        'items' => [
            [
                'icon' => 'fa-floppy-o',
                'text' => 'Save',
                'url' => null,
                'form' => 'injection-form',
                'options' => null,
                'authorized' => \Yii::$app->user->can('injectionUpdate', ['model' => $model]),
            ],
        ]
    ],
];
?>

<div class="injection-update content-wrapper">

    <?php Panel::begin(['title' => 'Injection Content']); ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

    <?php Panel::end(); ?>

</div>
