<?php

use app\components\Functions;
use app\modules\backend\widgets\Panel;
use app\modules\base\models\Injection;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\backend\models\InjectionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Injections';
$this->params['breadcrumbs'][] = $this->title;

$this->params['action-buttons'] = [
    [
        'name' => 'main',
        'primary' => true,
        'items' => [
            [
                'icon' => 'fa-plus',
                'text' => 'New Injection',
                'url' => ['create'],
                'form' => null,
                'options' => null,
                'authorized' => \Yii::$app->user->can('injectionCreate'),
            ],
        ]
    ],
    /* Action bar group for record selection */
    [
        'name' => 'index-selection',
        'items' => [
            [
                'icon' => 'fa-pencil',
                'text' => 'Edit',
                'url' => null,
                'options' => [
                    'data' => [
                        'href-template' => Url::to(['update', 'id' => '0']),
                        'selection-allowed' => '1',
                    ],
                ],
                'authorized' => true,
            ],
            [
                'icon' => 'fa-trash',
                'text' => 'Delete',
                'url' => null,
                'options' => [
                    'data' => [
                        'href-template' => Url::to(['delete', 'ids' => '0']),
                        'selection-allowed' => 'N',
                    ],
                ],
                'authorized' => true,
            ],
            [
                'icon' => 'fa-check-circle-o',
                'text' => 'Enable',
                'url' => null,
                'options' => [
                    'data' => [
                        'href-template' => Url::to([
                            'update-status',
                            'ids' => '0',
                            'status' => Injection::STATUS_ENABLED
                        ]),
                        'selection-allowed' => 'N',
                    ],
                ],
                'authorized' => true,
            ],
            [
                'icon' => 'fa-times-circle-o',
                'text' => 'Disable',
                'url' => null,
                'options' => [
                    'data' => [
                        'href-template' => Url::to([
                            'update-status',
                            'ids' => '0',
                            'status' => Injection::STATUS_DISABLED
                        ]),
                        'selection-allowed' => 'N',
                    ],
                ],
                'authorized' => true,
            ],
        ]
    ]
];
?>

<div class="injections-index content-wrapper">

    <?php Panel::begin(['title' => $this->title]); ?>

    <?php Pjax::begin(['id' => 'injection-grid']); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'options' => ['class' => 'grid-view table-responsive table-avatar-first-column table-selectable table-selectable_avatar'],
        'emptyText' => 'No injections found. Click "New Injection" above to get started.',
        'columns' => [
            [
                'format' => 'raw',
                'value' => function ($data) {
                    /* @var $data app\modules\base\models\Injection */
                    return Functions::getSelectableAvatar([
                        'action-bar-group' => 'index-selection',
                        'record-id' => $data->id,
                        'text' => $data->name,
                        'image' => null,
                        'size' => '50px',
                    ]);
                },
            ],
            [
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function ($data) {
                    /* @var $data app\modules\base\models\Injection */
                    $primary = Html::a(Html::encode($data->name), ['view', 'id' => $data->id], ['class' => 'primary']);
                    /* Display warning icon if there exists at last one warning */
                    if ($data->hasWarning()) {
                        $primary .= ' <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>';
                    }
                    if ($data->status == '0') {
                        $primary .= Html::tag('span', $data->statusText(), ['class' => 'primary_note']);
                    }
                    return $primary
                    . Html::tag('div', $data->xpath, ['class' => 'secondary']);
                },
            ],
            'order',
        ],
    ]); ?>

    <?php Pjax::end(); ?>

    <?php Panel::end(); ?>

</div>
