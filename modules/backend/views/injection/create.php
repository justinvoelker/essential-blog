<?php

use app\modules\backend\widgets\Panel;

/* @var $this yii\web\View */
/* @var $model app\modules\base\models\Injection */

$this->title = 'New Injection';
$this->params['breadcrumbs'][] = ['label' => 'Injections', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['action-buttons'] = [
    [
        'name' => 'main',
        'primary' => true,
        'items' => [
            [
                'icon' => 'fa-floppy-o',
                'text' => 'Save',
                'url' => null,
                'form' => 'injection-form',
                'options' => null,
                'authorized' => \Yii::$app->user->can('injectionCreate'),
            ],
        ]
    ],
];
?>

<div class="injection-create content-wrapper">

    <?php Panel::begin(['title' => 'Injection Content']); ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

    <?php Panel::end(); ?>

</div>
