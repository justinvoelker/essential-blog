<?php

use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<?php $this->beginContent('@app/modules/admin/views/layouts/main.php'); ?>

    <div class="help-content">
        <ul>
            <li><a href="<?= Url::toRoute(['help/post']) ?>">Posts</a></li>
            <li><a href="<?= Url::toRoute(['help/image']) ?>">Images</a></li>
            <li><a href="<?= Url::toRoute(['help/tag']) ?>">Tags</a></li>
            <li><a href="<?= Url::toRoute(['help/category']) ?>">Categories</a></li>
            <li><a href="<?= Url::toRoute(['help/user']) ?>">Users</a></li>
            <li><a href="<?= Url::toRoute(['help/setting']) ?>">Settings</a></li>
        </ul>

        <div class="cell">
            <?= $content; ?>
        </div>

    </div>
<?php $this->endContent();
