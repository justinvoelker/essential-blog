<?php

/* @var $this \yii\web\View */
/* @var $content string */
/* @var $authenticatedUser \app\modules\base\models\User */

use app\assets\AdminAsset;
use app\modules\backend\widgets\ActionBar;
use app\modules\backend\widgets\FlashAlert;
use app\modules\base\models\Help;
use app\modules\base\models\Setting;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use yii\widgets\Pjax;

AdminAsset::register($this);

$authenticatedUser = Yii::$app->user->identity;

/* Since settings pages are all one action but separate groups, identify the group for later setting active page. */
$settingGroup = (Yii::$app->controller->id == 'setting') ? Yii::$app->getRequest()->getQueryParam('group') : null;

$bodyClass = [];
$bodyClass[] = 'color-scheme__' . Setting::getSetting('admin_color_scheme');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title . ' - ') . Setting::getSetting('blog_title') ?> Admin</title>
    <?php $this->head() ?>
</head>
<body class="<?= implode(' ', $bodyClass) ?>">
<?php $this->beginBody() ?>

<div class="header">
    <?php
    /* This header__body is wrapped in a Pjax widget so that when pages are updated via Ajax, the changed page header
     * can be reflected to appear as it would if the page were to be refreshed. For example, when first saving a new
     * post the header changes from "New Post" to "Update Post" or when subsequent post saves are made that contain a
     * change to the post title, that changed title can be reflected in the header without requiring a page refresh.
     * The linkSelector parameter is set to false so that the links within the header will themselves not trigger Pjax.
     */
    ?>
    <?php Pjax::begin(['id' => 'header__body', 'linkSelector' => false]); ?>
    <div class="header__body">
        <div class="header__title">
            <?= $this->title ?>
        </div>
        <div class="header__breadcrumb">
            <?= Breadcrumbs::widget([
                'itemTemplate' => '<li class="header__breadcrumb-item">{link}<span class="header__breadcrumb-separator">&rsaquo;</span></li>',
                'activeItemTemplate' => '<li class="header__breadcrumb-item active">{link}</li>',
                'options' => ['class' => 'header__breadcrumb-list'],
                'homeLink' => false,
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []
            ]) ?>
        </div>
        <label class="header__sidebar-toggle" for="sidebar_main">
            <i class="fa fa-bars header__sidebar-toggle-icon" aria-hidden="true"></i>
        </label>
    </div>
    <?php Pjax::end(); ?>
</div>

<div class="main">
    <div class="sidebar sidebar_main">
        <input type="checkbox" class="sidebar__state" id="sidebar_main" />
        <div class="sidebar__content">
            <div class="sidebar__brand">
                <!--<div class="header__brand-logo">-->
                <!--<div class="logo">-->
                <!--<img src="https://upload.wikimedia.org/wikipedia/commons/5/53/Google_%22G%22_Logo.svg">-->
                <!--</div></div>-->
                <div class="sidebar__brand-name">
                    <?= Html::a(Setting::getSetting('blog_title'), Yii::$app->homeUrl, ['class' => 'sidebar__brand-link']) ?>
                    <br>
                    Admin
                </div>
            </div>
            <ul class="sidebar__list">
                <li class="sidebar__list-item <?= (Yii::$app->controller->id == 'post') ? 'active' : null ?>">
                    <a href="<?= Url::toRoute(['post/index']) ?>" class="sidebar__list-item-link">
                        <i class="fa fa-fw fa-book sidebar__list-item-link-icon" aria-hidden="true"></i>
                        Posts</a>
                </li>
                <li class="sidebar__list-item <?= (Yii::$app->controller->id == 'page') ? 'active' : null ?>">
                    <a href="<?= Url::toRoute(['page/index']) ?>" class="sidebar__list-item-link">
                        <i class="fa fa-fw fa-file-text sidebar__list-item-link-icon" aria-hidden="true"></i>
                        Pages</a>
                </li>
                <li class="sidebar__list-item <?= (Yii::$app->controller->id == 'image') ? 'active' : null ?>">
                    <a href="<?= Url::toRoute(['image/index']) ?>" class="sidebar__list-item-link">
                        <i class="fa fa-fw fa-picture-o sidebar__list-item-link-icon" aria-hidden="true"></i>
                        Images</a>
                </li>
                <li class="sidebar__list-item <?= (Yii::$app->controller->id == 'tag') ? 'active' : null ?>">
                    <a href="<?= Url::toRoute(['tag/index']) ?>" class="sidebar__list-item-link">
                        <i class="fa fa-fw fa-tags sidebar__list-item-link-icon" aria-hidden="true"></i>
                        Tags</a>
                </li>
                <li class="sidebar__list-item <?= (Yii::$app->controller->id == 'category') ? 'active' : null ?>">
                    <a href="<?= Url::toRoute(['category/index']) ?>" class="sidebar__list-item-link">
                        <i class="fa fa-fw fa-tag sidebar__list-item-link-icon" aria-hidden="true"></i>
                        Categories</a>
                </li>
                <li class="sidebar__list-item sidebar__list-item_expandable <?= (in_array(Yii::$app->controller->id, ['setting', 'injection', 'user'])) ? 'sidebar__list-item_expanded' : null ?>">
                    <span class="sidebar__list-item-link">
                        <i class="fa fa-fw fa-cogs sidebar__list-item-link-icon" aria-hidden="true"></i>
                        Settings
                        <i class="fa fa-chevron-right sidebar__list-item-expand-indicator" aria-hidden="true"></i>
                    </span>
                    <ul class="sidebar__list sidebar__list_child">
                        <li class="sidebar__list-item <?= ($settingGroup == 'blog') ? 'active' : null ?>">
                            <a href="<?= Url::toRoute(['setting/blog']) ?>" class="sidebar__list-item-link">Blog Settings</a>
                        </li>
                        <li class="sidebar__list-item <?= ($settingGroup == 'admin') ? 'active' : null ?>">
                            <a href="<?= Url::toRoute(['setting/admin']) ?>" class="sidebar__list-item-link">Admin Settings</a>
                        </li>
                        <li class="sidebar__list-item <?= (Yii::$app->controller->id == 'user') ? 'active' : null ?>">
                            <a href="<?= Url::toRoute(['user/index']) ?>" class="sidebar__list-item-link">Users</a>
                        </li>
                        <li class="sidebar__list-item <?= (Yii::$app->controller->id == 'injection') ? 'active' : null ?>">
                            <a href="<?= Url::toRoute(['injection/index']) ?>" class="sidebar__list-item-link">Injections</a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar__list-item sidebar__list-item_expandable <?= (Yii::$app->controller->action->id == 'profile') ? 'sidebar__list-item_expanded' : null ?>">
                    <span class="sidebar__list-item-link">
                        <i class="fa fa-fw fa-user sidebar__list-item-link-icon" aria-hidden="true"></i>
                        Account
                        <i class="fa fa-chevron-right sidebar__list-item-expand-indicator" aria-hidden="true"></i>
                    </span>
                    <ul class="sidebar__list sidebar__list_child">
                        <li class="sidebar__list-item">
                            <a href="<?= Url::toRoute(['user/view', 'id' => Yii::$app->user->id]) ?>" class="sidebar__list-item-link">Profile</a>
                        </li>
                        <li class="sidebar__list-item">
                            <a href="<?= Url::toRoute(['default/logout']) ?>" class="sidebar__list-item-link logout-link" data-method="post">Log out</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <label class="sidebar__backdrop" for="sidebar_main"></label>
    </div>

    <div class="content">
        <div class="action-bar action-bar_overflow-hidden-on-load">
            <?= ActionBar::widget() ?>
        </div>
        <?= $content ?>
    </div>
</div>

<?php Help::modal() ?>

<?= FlashAlert::widget() ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
