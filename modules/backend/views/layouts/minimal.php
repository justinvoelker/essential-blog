<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AdminAsset;
use app\modules\base\models\Setting;
use app\modules\backend\widgets\FlashAlert;
use yii\helpers\Html;

AdminAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="minimal">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title . ' - ') . Setting::getSetting('blog_title') ?></title>
    <?php $this->head() ?>
</head>
<body class="color-scheme__blue-700">
<?php $this->beginBody() ?>

<div class="main">
    <div class="content content_minimal">
        <?= $content ?>
    </div>
</div>

<?= FlashAlert::widget() ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
