<?php

use app\modules\backend\widgets\Panel;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\modules\base\models\Post (newestRestorePoint) */
/* @var $postRestorePoints yii\data\ActiveDataProvider */

$title = $model->title;

$this->title = 'View Post: ' . $title;
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $title;

/* Register the JavaScript necessary for publishing and unpublishing posts */
$this->registerJsFile('/backend/js/post/btn-publish.js', ['depends' => '\app\assets\AdminAsset']);
$this->registerJsFile('/backend/js/post/btn-unpublish.js', ['depends' => '\app\assets\AdminAsset']);

$this->params['action-buttons'] = [
    [
        'name' => 'main',
        'primary' => true,
        'items' => [
            [
                'icon' => 'fa-pencil',
                'text' => 'Edit',
                'url' => ['edit', 'id' => $model->primary_id],
                'options' => null,
                'authorized' => (\Yii::$app->user->can('postUpdate', ['model' => $model->primaryPost()])),
            ],
            [
                'icon' => 'fa-trash',
                'text' => 'Delete',
                'url' => ['delete', 'ids' => $model->primary_id],
                'options' => [
                    'data' => ['confirm' => 'Are you sure you want to delete this post?'],
                    'class' => ['post-delete']
                ],
                'authorized' => (\Yii::$app->user->can('postDelete', ['model' => $model->primaryPost()])),
            ],
            [
                'icon' => 'fa-share',
                'text' => $model->buttons()['publish']['text'],
                'url' => null,
                'options' => [
                    'data' => ['action' => 'publish', 'id' => $model->primary_id],
                    'class' => (!$model->buttons()['publish']['enabled']) ? 'disabled' : null
                ],
                'authorized' => (\Yii::$app->user->can('postPublish', ['model' => $model->primaryPost()])),
            ],
            [
                'icon' => 'fa-reply',
                'text' => $model->buttons()['unpublish']['text'],
                'url' => null,
                'options' => [
                    'data' => ['action' => 'unpublish', 'id' => $model->primary_id],
                    'class' => (!$model->buttons()['unpublish']['enabled']) ? 'disabled' : null
                ],
                'authorized' => (\Yii::$app->user->can('postPublish', ['model' => $model->primaryPost()])),
            ],
            [
                'icon' => 'fa-eye',
                'text' => 'Preview',
                'url' => Url::toRoute($model->route()) . '?preview=true',
                'options' => [
                    'class' => ['post-preview-link'],
                ],
                'authorized' => true,
            ],
        ]
    ],
];
?>

<div class="post-view content-wrapper">

    <?php Panel::begin(['title' => 'Post Information']); ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'title',
            'content:ntext',
            [
                'attribute' => 'category.name',
                'label' => 'Category',
            ],
            [
                'attribute' => 'tags',
                'value' => (!empty($model->tags)) ? implode(', ', ArrayHelper::getColumn($model->tags, 'name')) : null,
            ],
            'slug',
            'scheduled_for:datetime',
            'excerpt',
            'image',
            [
                'attribute' => 'status',
                'value' => $model->statusText(),
            ],
        ],
    ]) ?>

    <?php Panel::end(); ?>

    <?php
    Panel::begin([
        'title' => 'Post Restore Points',
        'collapsable' => true,
        'collapsed' => true,
    ]);
    ?>

    <?php Pjax::begin(['id' => 'post-subgrid', 'enablePushState' => false]); ?>

    <?= GridView::widget([
        'dataProvider' => $postRestorePoints,
        'options' => ['class' => 'grid-view table-responsive'],
        'tableOptions' => ['class' => 'table table-expanded'],
        'columns' => [
            'created_at:datetime',
            'updated_at:datetime',
            [
                'attribute' => 'is_locked',
                'label' => 'Locked',
                'format' => 'raw',
                'value' => function ($data) {
                    return ($data->is_locked) ? 'Yes' : 'No';
                },
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

    <?php Panel::end(); ?>

</div>
