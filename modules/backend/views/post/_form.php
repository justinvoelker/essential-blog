<?php

use app\modules\backend\widgets\CreateRecordModal;
use app\modules\backend\widgets\SelectOptionButtonsInput;
use app\modules\base\models\Category;
use app\modules\base\models\Post;
use app\modules\base\models\Setting;
use app\modules\backend\widgets\ImageModal;
use app\modules\backend\widgets\LinkModal;
use app\modules\backend\widgets\Modal;
use app\modules\backend\widgets\Panel;
use app\modules\base\models\Tag;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Markdown;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\modules\base\models\Post */
/* @var $form yii\widgets\ActiveForm */

/* Register the JavaScript necessary for publishing and unpublishing posts */
$this->registerJsFile('/backend/js/post/action-edit.js', ['depends' => '\app\assets\MarkdownAsset']);

?>

<?php $form = ActiveForm::begin([
    'id' => 'post-form',
    'enableClientValidation'=>false,
]); ?>

<?php Panel::begin(['title' => 'Post Content']); ?>

    <div class="container">
        <div class="row">
            <div class="col-xs-12">

                <?= $form->field($model, 'title')->textInput(['autofocus' => $model->isNewRecord]) ?>

                <?= $form->field($model, 'content')->textarea(['rows' => 10]) ?>

            </div>
        </div>
    </div>

<?php Panel::end(); ?>

<?php Panel::begin(['title' => 'Post Category and Tags', 'collapsable' => true, 'collapsed' => true]); ?>

    <?php Pjax::begin(['id' => 'create-category-refresh', 'enablePushState' => false, 'linkSelector' => false]); ?>

        <?php
        $categoryArray = ArrayHelper::map(\app\modules\base\models\Category::find()->orderBy('name ASC')->all(), 'id', 'name');
        $addCategoryButton = Html::label('<i class="fa fa-plus" aria-hidden="true"></i>', 'create-category-modal', ['class' => ['btn', 'btn-default']]);
        echo $form->field($model, 'category_id')->widget(SelectOptionButtonsInput::className(), [
            'items' => $categoryArray,
            'appendItem' => \Yii::$app->user->can('categoryCreate') ? $addCategoryButton : null,
            'options' => ['class' => 'form-control hidden', 'prompt' => 'No category']
        ]);
        ?>

    <?php Pjax::end(); ?>

    <?php Pjax::begin(['id' => 'create-tag-refresh', 'enablePushState' => false, 'linkSelector' => false]); ?>

        <?php
        $tagsArray = ArrayHelper::map(\app\modules\base\models\Tag::find()->orderBy('name ASC')->all(), 'id', 'name');
        $addTagButton = Html::label('<i class="fa fa-plus" aria-hidden="true"></i>', 'create-tag-modal', ['class' => ['btn', 'btn-default']]);
        echo $form->field($model, 'tags')->widget(SelectOptionButtonsInput::className(), [
            'items' => $tagsArray,
            'appendItem' => \Yii::$app->user->can('tagCreate') ? $addTagButton : null,
            'options' => ['class' => 'form-control hidden', 'multiple' => true]
        ]);
        ?>

    <?php Pjax::end(); ?>

<?php Panel::end(); ?>

<?php Panel::begin(['title' => 'Additional Post Content', 'collapsable' => true, 'collapsed' => true]); ?>

    <?= $form->field($model, 'primary_id', ['options' => ['class' => 'hidden']])->hiddenInput() ?>
    <?= $form->field($model, 'is_locked', ['options' => ['class' => 'hidden']])->hiddenInput() ?>

    <div class="container">
        <div class="row">
            <div class="col-sm-6">

                <?= $form->field($model, 'excerpt')->textarea(['rows' => 4]) ?>

                <?= $form->field($model, 'image', [
                    'options' => ['class' => 'form-group form-group-image'],
                    'template' => "{label}\n{hint}\n{error}\n{input}",
                ])->widget(\app\modules\backend\widgets\SelectImageInput::className(), [
                    'sizes' => '(max-width: 543px) calc(100vw - 20px), 400px'
                ]) ?>

            </div>
            <div class="col-sm-6">

                <?= $form->field($model, 'slug')->textInput()
                    ->hint('<span class="preview">'.Url::toRoute($model->route()).'</span>') ?>

                <?php
                // Additional class to hide the clear button if a value is present
                $scheduledForClearHidden = ($model->scheduled_for) ? '' : 'hidden';
                // This is to get it in the proper format for display and the input field
                $model->scheduled_for = ($model->scheduled_for) ? date('Y-m-d H:i', $model->scheduled_for) : null;
                // Input template to include an input group for the calendar icon plus an icon to clear the input
                $scheduledForInputTemplate = '<div class="input-group">';
                $scheduledForInputTemplate .= '{input}';
                $scheduledForInputTemplate .= '<div class="input-group-addon"><i class="fa fa-fw fa-calendar"></i></div>';
                $scheduledForInputTemplate .= '</div>';
                $scheduledForInputTemplate .= '<i class="fa fa-trash btn-clear-input '.$scheduledForClearHidden.'"></i>';
                ?>
                <?= $form->field($model, 'scheduled_for', [
                    'options' => ['class' => 'form-group pos-relative'],
                    /* When a calendar widget is re-added, there is some JS as well. Search js/post/action-edit.js for CALWIDGET */
                    /*
                     * Possible widget options:
                     * - http://xdsoft.net/jqplugins/datetimepicker/ though it needs styling and is missing the ability
                     *   to click the month/year and have those expand into more general timeframes for selection.
                     * - https://fengyuanchen.github.io/datepicker/ though it is date-only and either needs a time
                     *   component added, needs to be updated to include a time component, or just needs to ignore time.
                     * - https://www.syncfusion.com/products/javascript/ejdatetimepicker just looks nice. Not real.
                     */
                    /*'template' => "{label}\n" . $scheduledForInputTemplate . "\n{hint}\n{error}",*/
                    'inputOptions' => ['class' => 'form-control', 'placeholder' => 'YYYY-MM-DD HH:MM'],
                ])->textInput() ?>
            </div>
        </div>
    </div>

<?php Panel::end(); ?>

<?php ActiveForm::end(); ?>

<?= ImageModal::widget() ?>
<?= LinkModal::widget() ?>
<?= CreateRecordModal::widget([
    'modalId' => 'create-category-modal',
    'title' => 'New Category',
    'model' => new Category(),
    'formView' => '/category/_form-simple',
    'validationUrl' => ['category/ajax-validate'],
    'action' => ['category/ajax-create'],
    'pjaxContainer' => 'create-category-refresh',
]) ?>
<?= CreateRecordModal::widget([
    'modalId' => 'create-tag-modal',
    'title' => 'New Tag',
    'model' => new Tag(),
    'formView' => '/tag/_form-simple',
    'validationUrl' => ['tag/ajax-validate'],
    'action' => ['tag/ajax-create'],
    'pjaxContainer' => 'create-tag-refresh',
]) ?>

<?php
/* Begin modal output */
Modal::begin([
    'id' => 'markdown-help-modal',
    'title' => 'Markdown Help',
]);
/* Set path of help file and render the markdown */
$helpFile = \Yii::$app->modules['admin']->basePath . '/help/miscellanea/markdown-help.md';
echo Markdown::process(\Yii::$app->controller->renderFile($helpFile), 'gfm-comment');
/* End modal output */
Modal::end();
?>

<?php
$this->registerJs('var autosave_seconds="'.Setting::getSetting('autosave_seconds').'";', $this::POS_HEAD);
$this->registerJs('var post_is_locked_const="'.Post::IS_LOCKED.'";', $this::POS_HEAD);
