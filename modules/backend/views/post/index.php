<?php

use app\components\Functions;
use app\modules\backend\widgets\Panel;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\modules\base\models\Post */
/* @var $searchModel app\modules\backend\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Posts';
$this->params['breadcrumbs'][] = $this->title;

$this->params['action-buttons'] = [
    [
        'name' => 'main',
        'primary' => true,
        'items' => [
            [
                'icon' => 'fa-plus',
                'text' => 'New Post',
                'url' => ['edit'],
                'options' => null,
                'authorized' => (\Yii::$app->user->can('postCreate')),
            ],
        ]
    ],
    /* Action bar group for record selection */
    [
        'name' => 'index-selection',
        'items' => [
            [
                'icon' => 'fa-pencil',
                'text' => 'Edit',
                'url' => null,
                'options' => [
                    'data' => [
                        'href-template' => Url::to(['edit', 'id' => '0']),
                        'selection-allowed' => '1',
                    ],
                ],
                'authorized' => true,
            ],
            [
                'icon' => 'fa-trash',
                'text' => 'Delete',
                'url' => null,
                'options' => [
                    'data' => [
                        'href-template' => Url::to(['delete', 'ids' => '0']),
                        'selection-allowed' => 'N',
                    ],
                ],
                'authorized' => true,
            ],
        ]
    ]
];

?>

<div class="post-index content-wrapper">

    <?php Panel::begin(['title' => $this->title]); ?>

    <?php Pjax::begin(['id' => 'post-grid']); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'options' => ['class' => 'grid-view table-responsive table-avatar-first-column table-selectable table-selectable_avatar'],
        'emptyText' => 'No posts found. Click "New Post" above to get started.',
        'columns' => [
            [
                'format' => 'raw',
                'value' => function ($data) {
                    /* @var $data app\modules\base\models\Post */
                    return Functions::getSelectableAvatar([
                        'action-bar-group' => 'index-selection',
                        'record-id' => $data->id,
                        'text' => $data->title_revised,
                        'image' => $data->newestRestorePoint()->image,
                        'size' => '50px',
                    ]);
                },
            ],
            [
                'attribute' => 'title_revised',
                'label' => 'Title',
                'format' => 'raw',
                'value' => function ($data) {
                    /* @var $data app\modules\base\models\Post */
                    $linkText = Html::encode($data->title_revised);
                    if ($data->statusText() != 'Published') {
                        $linkText .= ' <strong>(' . $data->statusText() . ')</strong>';
                    }
                    $result = Html::a($linkText, ['view', 'id' => $data->id], ['class' => 'primary']);
                    $result .= Html::tag('div',
                        ($data->status == $data::STATUS_PUBLISHED) ? 'Published ' . Yii::$app->formatter->asDate($data->publish_date,
                                'full') : 'Not published', ['class' => 'secondary']);

                    return $result;
                },
            ],
            [
                'attribute' => 'created_at',
                'label' => 'Created',
                'value' => function ($data) {
                    /* @var $data app\modules\base\models\Post */
                    return Yii::$app->formatter->asDate($data->created_at, 'full');
                }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

    <?php Panel::end(); ?>

</div>
