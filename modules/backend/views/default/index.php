<?php

use app\modules\backend\widgets\Panel;

$this->title = 'Admin';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="post-index content-wrapper">

    <?php Panel::begin(['title' => $this->title]); ?>

    Body content

    <?php Panel::end(); ?>

</div>
