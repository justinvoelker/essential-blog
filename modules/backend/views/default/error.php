<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use app\modules\backend\widgets\Panel;
use yii\helpers\Html;

$this->title = $name;
$this->params['breadcrumbs'][] = 'Error';

if (Yii::$app->user->isGuest) {
    /** @noinspection PhpUndefinedFieldInspection */
    $this->context->layout = 'minimal';
}
?>
<div class="site-error content-wrapper content-wrapper_minimal">

    <?php Panel::begin(['title' => 'Error: ' . Html::encode($this->title)]); ?>

    <p><?= nl2br(Html::encode($message)) ?></p>

    <?php Panel::end(); ?>

</div>
