<?php

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \app\modules\base\models\RegisterForm */

use app\modules\backend\widgets\Panel;
use app\modules\backend\widgets\PasswordInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Register';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-register content-wrapper content-wrapper_minimal">

    <?php Panel::begin(['title' => $this->title]); ?>

    <div class="register-form">

        <p>Finish creating your account</p>

        <?php $form = ActiveForm::begin([
            'id' => 'register-form',
            'enableAjaxValidation' => true,
            'validationUrl' => ['register-ajax-validation', 'token' => $model->security_token],
            'action' => ['register', 'token' => $model->security_token],
        ]); ?>

        <?= $form->field($model, 'first_name')->textInput(['autofocus' => true]) ?>

        <?= $form->field($model, 'last_name')->textInput() ?>

        <?= $form->field($model, 'username')->textInput() ?>

        <?= $form->field($model, 'password')->widget(PasswordInput::className()) ?>

        <div class="form-group right-align">
            <?= Html::submitButton('Register', ['class' => 'btn btn-scheme']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

    <?php Panel::end(); ?>

</div>
