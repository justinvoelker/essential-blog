<?php

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \app\modules\base\models\PasswordResetRequestForm */

use app\modules\backend\widgets\Panel;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-password-reset-request content-wrapper content-wrapper_minimal">

    <?php Panel::begin(['title' => $this->title]); ?>

    <p>Please enter your email address. A link to reset your password will be sent there.</p>

    <div class="request-password-reset-form">

        <?php $form = ActiveForm::begin([
            'id' => 'request-password-reset-form',
            'enableClientValidation' => false,
        ]); ?>

        <?= $form->field($model, 'email')->textInput(['autofocus' => true])->label('Email address') ?>

        <div class="form-group right-align">
            <?= Html::a('Cancel', ['/admin/default/login'], ['class' => 'btn btn-default']) ?>
            <?= Html::submitButton('Send', ['class' => 'btn btn-scheme']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

    <?php Panel::end(); ?>
</div>
