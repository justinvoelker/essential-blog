<?php

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \app\modules\base\models\LoginForm */

use app\modules\backend\widgets\Panel;
use app\modules\backend\widgets\PasswordInput;
use yii\authclient\widgets\AuthChoice;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* Register the JavaScript necessary for publishing and unpublishing posts */
$this->registerJsFile('/backend/js/default/action-login.js', ['depends' => '\app\assets\AdminAsset']);

$this->title = 'Login';
?>
<div class="site-login content-wrapper content-wrapper_minimal">

    <?php Panel::begin(['title' => $this->title]); ?>

    <div class="login-form">

        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'enableClientValidation' => false,
        ]); ?>

        <div class="wizard-page-container">

            <div class="wizard-page page-active">

                <?= $form->field($model, 'username')
                    ->textInput(['autofocus' => true])->label('Username or email address') ?>

                <?= $form->field($model, 'password')
                    ->widget(PasswordInput::className(),
                        ['options' => ['class' => 'form-control']]) ?>

            </div>

            <div class="wizard-page page-two-factor">

                <?= $form->field($model, 'two_factor_code')
                    ->widget(PasswordInput::className(), ['options' => ['class' => 'form-control']])
                    ->label('Two-factor authentication code') ?>

            </div>
        </div>

        <?= Html::submitButton('Login', ['class' => 'btn btn-scheme btn-block btn-login', 'name' => 'login-button']) ?>

        <div class="forgot-password">
            <?= Html::a('Forgot password?', ['default/password-reset-request']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

    <?php Panel::end(); ?>

    <?php
    /* Only going to display second block if auth clients are set */
    if (array_key_exists('authClientCollection', Yii::$app->components)
        && count(Yii::$app->components['authClientCollection']['clients']) > 0
    ) {

        echo '<div class="auth-clients-wrapper">';

        echo '<p>Or, log in with one of the following services</p>';

        /* Begin AuthChoice widget */
        $authAuthChoice = AuthChoice::begin([
            'baseAuthUrl' => ['/admin/default/auth-connection-login'],
            'options' => ['class' => 'auth-clients'],
        ]);

        /* Loop through each configured AuthChoice Client */
        foreach ($authAuthChoice->getClients() as $client) {
            echo $authAuthChoice->clientLink($client);
        }

        /* End AuthChoice widget */
        AuthChoice::end();

        echo '</div>';
    }
    ?>

</div>
