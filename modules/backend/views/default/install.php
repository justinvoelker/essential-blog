<?php

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \app\modules\base\models\InstallForm */

use app\modules\backend\widgets\Panel;
use app\modules\base\models\Setting;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Install';

/* Active page, by default, is the first page, introducing the installation wizard */
$activePage = 0;
/* If the model has errors, the active page will be the first page with errors */
if ($model->hasErrors()) {
    $errors = $model->getFirstErrors();
    /* Page/step 1 */
    if (array_key_exists('blogTitle', $errors)
        || array_key_exists('blogTagline', $errors)
    ) {
        $activePage = 1;
    } elseif (array_key_exists('firstName', $errors)
        || array_key_exists('lastName', $errors)
        || array_key_exists('emailAddress', $errors)
    ) {
        $activePage = 2;
    } elseif (array_key_exists('username', $errors)
        || array_key_exists('password', $errors)
    ) {
        $activePage = 3;
    } elseif (array_key_exists('supportEmailAddress', $errors)
    ) {
        $activePage = 4;
    }
}
?>

    <div class="site-install content-wrapper content-wrapper_minimal">

        <?php Panel::begin(['title' => $this->title]); ?>

        <div class="install-form">

            <?php $form = ActiveForm::begin([
                'id' => 'install-form',
                'enableClientValidation' => false,
            ]); ?>

            <div class="wizard-page-container">

                <div class="wizard-page page-first <?= ($activePage == 0) ? 'page-active' : null ?>">

                    <h2>Installation Wizard</h2>

                    <p>The following pages will walk you through entering blog and user information to complete the
                        installation. All fields are required and all of the data entered here can be changed by
                        navigating to the <em>Settings</em> page once logged in.</p>

                    <p>Click <strong>Next</strong> below to get started.</p>

                </div>

                <div class="wizard-page <?= ($activePage == 1) ? 'page-active' : null ?>">

                    <h2>Step 1 of 4 - Blog basics</h2>

                    <p>To get started, give your blog a title and catchy tagline.</p>

                    <?= $form->field($model, 'blogTitle')
                        ->textInput() ?>

                    <?= $form->field($model, 'blogTagline')
                        ->textarea(['rows' => 2]) ?>

                </div>

                <div class="wizard-page <?= ($activePage == 2) ? 'page-active' : null ?>">

                    <h2>Step 2 of 4 - User name and email</h2>

                    <p>Next, some basic information about yourself</p>

                    <div class="clearfix">
                        <div class="half-width half-width-left">
                            <?= $form->field($model, 'firstName')
                                ->textInput() ?>
                        </div>
                        <div class="half-width half-width-right">
                            <?= $form->field($model, 'lastName')
                                ->textInput() ?>
                        </div>
                    </div>

                    <?= $form->field($model, 'emailAddress')
                        ->textInput() ?>

                </div>

                <div class="wizard-page <?= ($activePage == 3) ? 'page-active' : null ?>">

                    <h2>Step 3 of 4 - User credentials</h2>

                    <p>Login credentials so you can log in.</p>

                    <?= $form->field($model, 'username')
                        ->textInput() ?>

                    <?= $form->field($model, 'password')
                        ->passwordInput() ?>

                </div>

                <div class="wizard-page page-last <?= ($activePage == 4) ? 'page-active' : null ?>">

                    <h2>Step 4 of 4 - Blog setup</h2>

                    <p>Finally, just a couple required administrative settings.</p>

                    <?= $form->field($model, 'supportEmailAddress')
                        ->textInput() ?>

                    <?= $form->field($model, 'timeZone')
                        ->dropDownList(Setting::getTimeZones()) ?>

                </div>
            </div>

            <div class="wizard-controls clearfix">
                <?= Html::button('Previous', ['class' => 'btn btn-default pull-left page-prev hidden']) ?>

                <?= Html::button('Next', ['class' => 'btn btn-default pull-right page-next']) ?>
                <?= Html::submitButton('Install', ['class' => 'btn btn-scheme pull-right hidden btn-submit']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div> <!-- .install-form -->

        <?php Panel::end(); ?>

    </div> <!-- .site-install -->

<?php
$script = <<< JS

    /* Since the active page may not be the first page, perform updateButtons on page load */
    updateButtons();

    /* On click of next button, show next page */
    $("button.page-next").click(function() {
        $(".wizard-page.page-active").removeClass("page-active").next().addClass("page-active");
        updateButtons();
    });
    /* On click of previous button, show previous page */
    $("button.page-prev").click(function() {
        $(".wizard-page.page-active").removeClass("page-active").prev().addClass("page-active");
        updateButtons();
    });

    function updateButtons() {
        /* Send focus to the first visible field */
        $('#install-form :input:visible:enabled:first').focus();
        /* Hide previous button when on first page */
        $("button.page-prev").toggleClass("hidden", $(".wizard-page.page-active").hasClass("page-first"));
        /* Hide next button when on last page */
        $("button.page-next").toggleClass("hidden", $(".wizard-page.page-active").hasClass("page-last"));
        /* Show install button when on last page */
        $("button.btn-submit").toggleClass("hidden", !$(".wizard-page.page-active").hasClass("page-last"));
    }

    /* Continue from page to page on press of enter key */
    $(document).on('keypress',  function(e) {
        /* If enter key was pressed and target was anything other than "previous" button ... */
        if(e.key === "Enter" && !$(e.target).hasClass('page-prev')) {
            /* ... as long as this is not the last page, click the next page button and do not submit the form */
            if (!$(".wizard-page.page-active").hasClass("page-last")) {
                /* Click the "next" button */
                $("button.page-next").click();
                /* Prevent the form from submitting */
                e.preventDefault();
            }
        }
    });

JS;
$this->registerJs($script, $this::POS_READY);
