<?php

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \app\modules\base\models\PasswordResetForm */

use app\modules\backend\widgets\Panel;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Password reset';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-password-reset content-wrapper content-wrapper_minimal">

    <?php Panel::begin(['title' => $this->title]); ?>

    <p>Please enter your new password.</p>

    <div class="reset-password-form">

        <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

        <?= $form->field($model, 'password')->passwordInput(['autofocus' => true])->label('New password') ?>

        <div class="form-group right-align">
            <?= Html::submitButton('Save', ['class' => 'btn btn-scheme']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

    <?php Panel::end(); ?>

</div>
