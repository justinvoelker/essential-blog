<?php

use app\modules\backend\widgets\Panel;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\base\models\Category */

$title = $model->name;
$truncatedTitle = StringHelper::truncate($title, 20);

$this->title = 'Update Category: ' . $title;
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $truncatedTitle, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';

$this->params['action-buttons'] = [
    [
        'name' => 'main',
        'primary' => true,
        'items' => [
            [
                'icon' => 'fa-floppy-o',
                'text' => 'Save',
                'url' => null,
                'form' => 'category-form',
                'options' => null,
                'authorized' => \Yii::$app->user->can('categoryUpdate', ['model' => $model]),
            ],
        ]
    ],
];
?>

<div class="category-update content-wrapper">

    <?php Panel::begin(['title' => 'Category Content']); ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

    <?php Panel::end(); ?>

</div>
