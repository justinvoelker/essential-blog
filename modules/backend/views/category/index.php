<?php

use app\components\Functions;
use app\modules\backend\widgets\Panel;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\backend\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categories';
$this->params['breadcrumbs'][] = $this->title;

$this->params['action-buttons'] = [
    [
        'name' => 'main',
        'primary' => true,
        'items' => [
            [
                'icon' => 'fa-plus',
                'text' => 'New Category',
                'url' => ['create'],
                'form' => null,
                'options' => null,
                'authorized' => \Yii::$app->user->can('categoryCreate'),
            ],
        ]
    ],
    /* Action bar group for record selection */
    [
        'name' => 'index-selection',
        'items' => [
            [
                'icon' => 'fa-pencil',
                'text' => 'Edit',
                'url' => null,
                'options' => [
                    'data' => [
                        'href-template' => Url::to(['update', 'id' => '0']),
                        'selection-allowed' => '1',
                    ],
                ],
                'authorized' => true,
            ],
            [
                'icon' => 'fa-trash',
                'text' => 'Delete',
                'url' => null,
                'options' => [
                    'data' => [
                        'href-template' => Url::to(['delete', 'ids' => '0']),
                        'selection-allowed' => 'N',
                    ],
                ],
                'authorized' => true,
            ],
        ]
    ]
];
?>

<div class="category-index content-wrapper">

    <?php Panel::begin(['title' => $this->title]); ?>

    <?php Pjax::begin(['id' => 'category-grid']); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'options' => ['class' => 'grid-view table-responsive table-avatar-first-column table-selectable table-selectable_avatar'],
        'emptyText' => 'No categories found. Click "New Category" above to get started.',
        'columns' => [
            [
                'format' => 'raw',
                'value' => function ($data) {
                    /* @var $data app\modules\base\models\Category */
                    return Functions::getSelectableAvatar([
                        'action-bar-group' => 'index-selection',
                        'record-id' => $data->id,
                        'text' => $data->name,
                        'image' => $data->image,
                        'size' => '50px',
                    ]);
                },
            ],
            [
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function ($data) {
                    /* @var $data app\modules\base\models\Category */
                    $result = Html::a(Html::encode($data->name), ['view', 'id' => $data->id], ['class' => 'primary']);
                    if ($data->description) {
                        $result .= Html::tag('div', $data->description, ['class' => 'secondary']);
                    } else {
                        $result .= Html::tag('div', 'No description', ['class' => 'secondary secondary-accent']);
                    }
                    return $result;
                },
            ],
            'slug',
            'primary_posts_count',
            'primary_pages_count',
        ],
    ]); ?>

    <?php Pjax::end(); ?>

    <?php Panel::end(); ?>

</div>
