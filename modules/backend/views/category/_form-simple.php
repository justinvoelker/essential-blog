<?php

use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\base\models\Category */
/* @var $formId string */
/* @var $enableAjaxValidation boolean */
/* @var $validationUrl array|string */
/* @var $action array|string */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form-simple">

    <?php $form = ActiveForm::begin([
        'id' => $formId,
        'enableAjaxValidation' => $enableAjaxValidation,
        'validationUrl' => $validationUrl,
        'action' => $action,
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= '<div class="error-summary error-summary-custom"></div>' ?>

    <?php ActiveForm::end(); ?>

</div>
