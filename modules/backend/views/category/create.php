<?php

use app\modules\backend\widgets\Panel;

/* @var $this yii\web\View */
/* @var $model app\modules\base\models\Category */

$this->title = 'New Category';
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['action-buttons'] = [
    [
        'name' => 'main',
        'primary' => true,
        'items' => [
            [
                'icon' => 'fa-floppy-o',
                'text' => 'Save',
                'url' => null,
                'form' => 'category-form',
                'options' => null,
                'authorized' => \Yii::$app->user->can('categoryCreate'),
            ],
        ]
    ],
];
?>

<div class="category-create content-wrapper">

    <?php Panel::begin(['title' => 'Category Content']); ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

    <?php Panel::end(); ?>

</div>
