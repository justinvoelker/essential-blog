<?php

use app\modules\backend\widgets\Panel;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\modules\base\models\Category */
/* @var $posts yii\data\ActiveDataProvider */
/* @var $pages yii\data\ActiveDataProvider */

$title = $model->name;

$this->title = 'View Category: ' . $title;
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $title;

$this->params['action-buttons'] = [
    [
        'name' => 'main',
        'primary' => true,
        'items' => [
            [
                'icon' => 'fa-pencil',
                'text' => 'Edit',
                'url' => ['update', 'id' => $model->id],
                'options' => null,
                'authorized' => \Yii::$app->user->can('categoryUpdate', ['model' => $model]),
            ],
            [
                'icon' => 'fa-trash',
                'text' => 'Delete',
                'url' => ['delete', 'ids' => $model->id],
                'options' => [
                    'data' => ['confirm' => 'Are you sure you want to delete this category?']
                ],
                'authorized' => \Yii::$app->user->can('categoryDelete', ['model' => $model]),
            ],
        ]
    ],
];
?>

<div class="category-view content-wrapper">

    <?php Panel::begin(['title' => 'Category Information']); ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'slug',
            'image',
            'description',
            'primary_posts_count',
            'primary_pages_count',
        ],
    ]) ?>

    <?php Panel::end(); ?>

    <?php Panel::begin(['title' => 'Category Posts', 'collapsable' => true, 'collapsed' => true]); ?>

    <?php Pjax::begin(['id' => 'post-subgrid', 'enablePushState' => false, 'linkSelector' => 'thead a, .pagination a']); ?>

    <?= GridView::widget([
        'dataProvider' => $posts,
        'options' => ['class' => 'grid-view table-responsive'],
        'tableOptions' => ['class' => 'table table-expanded'],
        'columns' => [
            [
                'attribute' => 'title_revised',
                'label' => 'Title',
                'format' => 'raw',
                'value' => function ($data) {
                    /* @var $data app\modules\base\models\Post */
                    return Html::a(Html::encode($data->title_revised), ['post/view', 'id' => $data->id]);
                },
            ],
            [
                'attribute' => 'status',
                'value' => function ($data) {
                    /* @var $data app\modules\base\models\Post */
                    return $data->statusText();
                },
                'contentOptions' => function ($model) {
                    /* @var $model app\modules\base\models\Post */
                    return ['class' => 'post-meta ' . strtolower($model->statusText())];
                }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

    <?php Panel::end(); ?>

    <?php Panel::begin(['title' => 'Category Pages', 'collapsable' => true, 'collapsed' => true]); ?>

    <?php Pjax::begin(['id' => 'page-subgrid', 'enablePushState' => false, 'linkSelector' => 'thead a, .pagination a']); ?>

    <?= GridView::widget([
        'dataProvider' => $pages,
        'options' => ['class' => 'grid-view table-responsive'],
        'tableOptions' => ['class' => 'table table-expanded'],
        'columns' => [
            [
                'attribute' => 'title_revised',
                'label' => 'Title',
                'format' => 'raw',
                'value' => function ($data) {
                    /* @var $data app\modules\base\models\Page */
                    return Html::a(Html::encode($data->title_revised), ['page/view', 'id' => $data->id]);
                },
            ],
            [
                'attribute' => 'status',
                'value' => function ($data) {
                    /* @var $data app\modules\base\models\Page */
                    return $data->statusText();
                },
                'contentOptions' => function ($model) {
                    /* @var $model app\modules\base\models\Page */
                    return ['class' => 'page-meta ' . strtolower($model->statusText())];
                }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

    <?php Panel::end(); ?>

</div>
