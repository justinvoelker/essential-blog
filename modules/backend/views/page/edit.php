<?php

use app\modules\base\models\Setting;
use yii\helpers\StringHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\base\models\Page */

$title = $model->title;
/* Truncated title for display in the breadcrumb */
$truncatedTitle = StringHelper::truncate($title, 20);

/* Different title depending on whether or not this is a new page or an updated page. */
$this->title = $model->isNewRecord ? 'New Page' : 'Update Page: ' . $title;
/* Breadcrumb always begines with Page index */
$this->params['breadcrumbs'][] = ['label' => 'Pages', 'url' => ['index']];
/* If updating a page display link back to page view */
if (!$model->isNewRecord) {
    $this->params['breadcrumbs'][] = ['label' => $truncatedTitle, 'url' => ['view', 'id' => $model->primary_id]];
}
/* End breadcrumb with new or update */
$this->params['breadcrumbs'][] = $model->isNewRecord ? 'New Page' : 'Update';

/* Register the JavaScript necessary for publishing and unpublishing pages */
$this->registerJsFile('/backend/js/page/btn-publish.js', ['depends' => '\app\assets\AdminAsset']);
$this->registerJsFile('/backend/js/page/btn-unpublish.js', ['depends' => '\app\assets\AdminAsset']);

/* Whether or not post restore points are enabled. Toggles display of Save Restore Point button. */
$restore_points_enabled = filter_var(Setting::getSetting('restore_points_enabled'), FILTER_VALIDATE_BOOLEAN);

$this->params['action-buttons'] = [
    [
        'name' => 'main',
        'primary' => true,
        'items' => [
            [
                'icon' => 'fa-floppy-o',
                'text' => $model->buttons()['save']['text'],
                'url' => null,
                'options' => [
                    'data' => ['submit-type' => 'save'],
                    'class' => (!$model->buttons()['save']['enabled']) ? 'disabled' : null
                ],
                'authorized' => (($model->isNewRecord && \Yii::$app->user->can('pageCreate'))
                    || (!$model->isNewRecord && \Yii::$app->user->can('pageUpdate', ['model' => $model->primaryPage()]))),
            ],
            [
                'icon' => 'fa-floppy-o',
                'text' => $model->buttons()['saverestorepoint']['text'],
                'url' => null,
                'options' => [
                    'data' => ['submit-type' => 'saverestorepoint'],
                    'class' => (!$model->buttons()['saverestorepoint']['enabled']) ? 'disabled' : null
                ],
                'authorized' => ((($model->isNewRecord && \Yii::$app->user->can('pageCreate'))
                        || (!$model->isNewRecord && \Yii::$app->user->can('pageUpdate', ['model' => $model->primaryPage()])))
                    && $restore_points_enabled),
            ],
            [
                'icon' => 'fa-share',
                'text' => $model->buttons()['publish']['text'],
                'url' => null,
                'options' => [
                    'data' => ['action' => 'publish', 'id' => $model->primary_id],
                    'class' => (!$model->buttons()['publish']['enabled']) ? 'disabled' : null
                ],
                'authorized' => (\Yii::$app->user->can('pagePublish', ['model' => $model->primaryPage()])),
            ],
            [
                'icon' => 'fa-reply',
                'text' => $model->buttons()['unpublish']['text'],
                'url' => null,
                'options' => [
                    'data' => ['action' => 'unpublish', 'id' => $model->primary_id],
                    'class' => (!$model->buttons()['unpublish']['enabled']) ? 'disabled' : null
                ],
                'authorized' => (\Yii::$app->user->can('pagePublish', ['model' => $model->primaryPage()])),
            ],
            [
                'icon' => 'fa-eye',
                'text' => 'Preview',
                'url' => ($model->isNewRecord) ? null : Url::toRoute($model->route()) . '?preview=true',
                'options' => [
                    'class' => ['page-preview-link', ($model->isNewRecord) ? 'disabled' : null],
                ],
                'authorized' => true,
            ],
        ]
    ],
];
?>

    <div class="page-edit content-wrapper">

        <?php /* Panel is included in _form so sections can be broken down */ ?>

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
