## Overview

Post view shows details of the post which can then be edited or deleted via buttons in the action bar. In addition to the post details, post restore points are also shown.

## Action Bar

* **Edit** Make changes to the post such as updating content or publishing/unpublishing.
* **Delete** Permanently delete the post. If the post should be hidden from visitors but remain in the system, click *unpublish* which will set the post status back to *Draft*.
* **Publish** Immediately publish the saved content. If a *schedule date* has been set, the button will read **Schedule** and the post will be published at that date and time.
* **Unpublish** Hide the post from visitors but do not delete it. If a *schedule date* has been set, the button will read **Unschedule**.
* **Preview** Navigate to a page showing a preview of the post as it will appear to visitors (opens in same page).

## Notes

* Once deleted, posts cannot be recovered.
