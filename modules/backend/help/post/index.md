## Overview

Posts are the focal point of every blog. On this page, posts of all statuses from all users are shown, sorted by descending creation date (most recently created first). Click a post title to view that individual post.

## Action Bar

* **New Post** Create a new post

## Notes

* A post *publish date* is only shown once the post is visible to blog visitors. Scheduled posts, for instance, will not show a *publish date* until the schedule date and time have passed.
