## Overview

When creating or editing a post, no fields are required, though title and content should be provided before a post is published. The most commonly used fields are displayed first on the form with additional fields available by clicking *additional fields*. Once saved, posts can be published immediately or at a future time by setting a scheduled date and clicking *schedule* in the action bar or by simply returning to the post edit page at a future time and clicking *publish*.

## Action Bar

* **Save** Save the post and continue editing.
* **Save Restore Point** Save the post as a new restore point and continue editing.
* **Publish** Immediately publish the saved content. If a *schedule date* has been set, the button will read **Schedule** and the post will be published at that date and time.
* **Unpublish** Hide the post from visitors but do not delete it. If a *schedule date* has been set, the button will read **Unschedule**.
* **Preview** Navigate to a page showing a preview of the post as it will appear to visitors (opens in same page).

## Fields

* **Title** Post title that will appear to visitors.
* **Content** The body of content that comprises the actual post.
* **Tags** Select from existing tags or create new tags to keywords within the post.
* **Image** If selected, a post image may accompany the post title and content (theme dependent).
* **Category** Identifies the main topic of the post. Categories cannot be created on-the-fly and must already exist.
* **Excerpt** A post excerpt is used to give visitors a brief preview of post content. If an excerpt is not provided a portion of post content will be used.
* **Slug** The URL for this post. Populated automatically, the slug can be manually edited but should **not** be changed once a post is published. Also shown is the full URL of the post according to the blog URL structure.
* **Scheduled For** To schedule a post to automatically publish at a future date and time, set the appropriate value here and click *schedule* in the action bar.

## Notes

* By default, posts are automatically saved after 10 seconds of keyboard inactivity on this page.
* Posts cannot be published until they are saved, either automatically or manually.
* Even after a post is published, it can continue to be edited and the edited content will not be available to visitors until it is republished via the **republish** button in the action bar.
* Once a post is published, the slug and publish date should **not** be changed. Though the blog will contain updated links, external links from other websites or visitors' bookmarked favorites will reference the old, invalid link.
