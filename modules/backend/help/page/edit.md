## Overview

When creating or editing a page, no fields are required, though title and content should be provided before a page is published. The most commonly used fields are displayed first on the form with additional fields available by clicking *additional fields*. Once saved, pages can be published immediately or at a future time by setting a scheduled date and clicking *schedule* in the action bar or by simply returning to the page edit page at a future time and clicking *publish*.

## Action Bar

* **Save** Save the page and continue editing.
* **Save Restore Point** Save the page as a new restore point and continue editing.
* **Publish** Immediately publish the saved content. If a *schedule date* has been set, the button will read **Schedule** and the page will be published at that date and time.
* **Unpublish** Hide the page from visitors but do not delete it. If a *schedule date* has been set, the button will read **Unschedule**.
* **Preview** Navigate to a page showing a preview of the page as it will appear to visitors (opens in same page).

## Fields

* **Title** Page title that will appear to visitors.
* **Content** The body of content that comprises the actual page.
* **Tags** Select from existing tags or create new tags to keywords within the page.
* **Image** If selected, a page image may accompany the page title and content (theme dependent).
* **Category** Identifies the main topic of the page. Categories cannot be created on-the-fly and must already exist.
* **Excerpt** A page excerpt is used to give visitors a brief preview of page content. If an excerpt is not provided a portion of page content will be used.
* **Slug** The URL for this page. Populated automatically, the slug can be manually edited but should **not** be changed once a page is published. Also shown is the full URL of the post according to the blog URL structure.
* **Scheduled For** To schedule a page to automatically publish at a future date and time, set the appropriate value here and click *schedule* in the action bar.

## Notes

* By default, pages are automatically saved after 10 seconds of keyboard inactivity on this page.
* Pages cannot be published until they are saved, either automatically or manually.
* Even after a page is published, it can continue to be edited and the edited content will not be available to visitors until it is republished via the **republish** button in the action bar.
* Once a page is published, the slug and publish date should **not** be changed. Though the blog will contain updated links, external links from other websites or visitors' bookmarked favorites will reference the old, invalid link.
