## Overview

Page view shows details of the page which can then be edited or deleted via buttons in the action bar. In addition to the page details, page restore points are also shown.

## Action Bar

* **Edit** Make changes to the page such as updating content or publishing/unpublishing.
* **Delete** Permanently delete the page. If the post should be hidden from visitors but remain in the system, click *unpublish* which will set the page status back to *Draft*.
* **Publish** Immediately publish the saved content. If a *schedule date* has been set, the button will read **Schedule** and the page will be published at that date and time.
* **Unpublish** Hide the page from visitors but do not delete it. If a *schedule date* has been set, the button will read **Unschedule**.
* **Preview** Navigate to a page showing a preview of the page as it will appear to visitors (opens in same page).

## Notes

* Once deleted, pages cannot be recovered.
