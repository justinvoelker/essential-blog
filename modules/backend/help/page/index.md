## Overview

Pages represent static content of a blog (such as an *About Me* page). On this page, pages of all statuses from all users are shown, sorted by descending creation date (most recently created first). Click a page title to view that individual page.

## Action Bar

* **New Page** Create a new page

## Notes

* A page *publish date* is only shown once the page is visible to blog visitors. Scheduled pages, for instance, will not show a *publish date* until the schedule date and time have passed.
