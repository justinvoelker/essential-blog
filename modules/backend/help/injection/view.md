## Overview

Injection view shows details of the injection which can then be edited or deleted via buttons in the action bar.

## Action Bar

* **Edit** Make changes to the injection such as updating name, xpath, action, content, parser, order, or status.
* **Delete** Permanently delete the injection.

## Notes

* Once deleted, injections cannot be recovered.
