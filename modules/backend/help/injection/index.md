## Overview

Injections are used to alter content before pages are rendered. This page displays all injections created from the *New injection* page. Injections are sorted by ascending order. Click an injection name to view that individual injection.

Injections can be as simple as replacing a single page header or as complex as reformatting the page to include a sidebar with additional content.
 
Common injections include adding custom styles or favicons to the `<head>` tag. 

## Action Bar

* **New Injection** Create a new injection

## Notes

* Only *enabled* injections will be used when altering content. To keep an injection but temporarily disable it, edit the record and set *Status* to *disabled*.
