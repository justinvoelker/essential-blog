## Overview

When creating an injection, only name and xpath are required. *Action*, *parser*, and *status* are pre-populated but can be changed. *Content* is not required but most injections will be ineffective without content (exceptions are when a replacement is used to remove an element or its content). *Order* is only necessary if injections depend on other injections and should be applied in a given order.

## Action Bar

* **Save** Save the injection.

## Fields

* **Name** Injection name that identifies what this injection is used for.
* **Xpath** The Xpath that identifies the element to be acted upon.
* **Action** Action to use when applying content to the element.
* **Content** The content to be injected using the given action.
* **Parser** How to parse the injected content.
* **Order** Option order in which the injections should be applied.
* **Status** Whether or not the injection is current active and should be applied to the content.

## Notes

* Change the status of injection to *disabled* to keep the record but prevent it from being applied to the content.
* Order is only required if injections have dependencies on other injections.
