## Overview

Control the administrative interface of your blog.

## Action Bar

* **Save** Save the settings.

## Fields

* *See individual field help for details*

## Notes

* Settings with a lock icon next to the name will be encrypted before being saved.
* All changes are saved at the same time (editing multiple settings does not require multiple saves).
* Changes take effect the next time any page is loaded (either on the blog or administrative console).

## Individual Setting Descriptions

### Interface

#### Color Scheme

Color scheme to apply to various elements of the administrative interface (does not affect what visitors see).

Default: *Blue 700*

#### Items Per Page

When using the administrative interface, this is how many posts/images/categories/etc will be displayed per page. Once this value is exceeded, multiple pages will be available.

Default: *10*

### Content

#### Autosave Seconds

To help prevent loss of work-in-progress, posts and pages can be automatically saved after a number of seconds of inactivity. To enable the automatic saving of posts and pages, enter the number of seconds after which you would like your work to be automatically saved. To completely disable automatic saving, set this value to *0* (posts and pages will only be saved when manually clicking the *Save* button).

Default: *10*

#### Enable Restore Points

If desired, posts and pages allow multiple restore points to be saved for future reference. If enabled, a restore point will be saved whenever the *Save Restore Point* button is clicked. If a post or page is published then edited, a new restore point will automatically be created to preserve the existing content.

Default: *Yes, enable restore points*

#### Maximum Upload Size (MB)

Uploaded images should be limited to a given file size (measured in MB). Uploading large images may result in the web server timing out so it is recommended to set the *Maximum Upload Size* to a reasonable value.

**NOTE: In addition to this setting, the web server may otherwise limit upload file size.**

Default: *100*

#### Responsive Image Widths

To help reduce page load times, every uploaded image is saved in multiple sizes. When a visitor visits your blog, only the smallest image that fits their device is loaded. By loading smaller images, visitors will be shown content much quicker than if they had to wait for the full size image to display.

The provided comma-separated list indicates the *breakpoints* to be be used to generate the responsive images. For example, using the default value below, an uploaded image will be saved in its original size, a copy resized to 1140px wide, another copy resized to 940px wide, etc. down to the smallest of 120px wide. Though storing multiple copies of the same image requires additional storage space, the reduction in page load time and bandwidth can be significant.

Default: *1140,940,720,480,240,120*

### Outgoing Email

#### Support Email Address

The email address that outgoing administrative emails will appear to come from. This email address can be different from the SMTP authentication username.

Default: *[specified during install]*

#### Delivery method

Emails can be simply saved to a file on the server or sent via an external SMTP server.

Default: *Do not send emails--only save emails locally in runtime/mail directory*

#### Host

SMTP server host. Only used if delivery method is "Send emails via external SMTP server."

Default: *[none]*

#### Port

SMTP port. Only used if delivery method is "Send emails via external SMTP server."

Default: *[none]*

#### Encryption

SMTP encryption type. Only used if delivery method is "Send emails via external SMTP server."

Default: *[none]*

#### Username

SMTP authentication username. Only used if delivery method is "Send emails via external SMTP server."

Default: *[none]*

#### Password

SMTP authentication password. Only used if delivery method is "Send emails via external SMTP server."

Default: *[none]*

### Security

#### Session Duration

The length of time to keep a user session open. Setting this to *0 Seconds* will force users to login for each new session. Note that this is not an "inactivity" timer. As long as a browser session is open, the user will remain logged in. This session duration setting determines how long after a user closes the browser they will remain logged in.

Default: *0 Seconds*

#### User Security Token Expiration

The length of time for which certain security tasks (such as password reset requests) remain valid.

Default: *10 Minutes*

#### Facebook Client ID

The Client ID provided by Facebook which will allow users to login with their Facebook account.

**NOTE: An application must be registered within the Facebook Developer Console**

Default: *[none]*

#### Facebook Client Secret

The secret key that accompanies the Facebook Client ID.

Default: *[none]*

#### Google Client ID

The Client ID provided by Google which will allow users to login with their Google account.

**NOTE: An OAuth client must first be created within the Google Developer Console.**

Default: *[none]*

#### Google Client Secret

The secret key that accompanies the Google Client ID.

Default: *[none]*
