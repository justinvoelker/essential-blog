## Overview

Control how your blog is presented to visitors.

## Action Bar

* **Save** Save the settings.

## Fields

* *See individual field help for details*

## Notes

* Settings with a lock icon next to the name will be encrypted before being saved.
* All changes are saved at the same time (editing multiple settings does not require multiple saves).
* Changes take effect the next time any page is loaded (either on the blog or administrative console).

## Individual Setting Descriptions

### General

#### Blog Title

The primary naming element of every blog: the blog title. This title will likely appear predominantly across every page of your blog, in search engine results (if indexed), and page title that appears in a visitors browser.

Default: *My New Blog*

#### Blog Tagline

A tagline is a short, simple statement about your blog. Based on your selected theme, this tagline may appear predominantly on each page or may only be used as the description that accompanies your blogs home page in search engine results.

Default: *Blogging: Never before have so many people with so little to say said so much to so few.*

#### Allow search engine indexing

In order for your blog to appear in search engine results, it must be indexed. If you would like your blog to be kept out of search engine results, change this setting to *No, attempt to prevent indexing*. The word *attempt* is used here on purpose as this setting only works for well-behaved robots by adding the `<meta name="robots" content="noindex">` tag to each rendered page.

Default: *Yes, allow indexing*

### Appearance

#### Theme

Themes control what visitors see when viewing your blog. Themes can vary greatly from each other and offer many different ways of viewing the same content. Aside from the default themes, additional themes can be used by simply copying the theme directory into the *web/themes* directory of the site.

If desired, themes can even be copied and modified to fit your specific needs. However, avoid modifying the original copy of a theme as your changes may be overwritten by future upgrades.

Default: *Default*

#### Items Per Page

This is how many pages/posts/tags/categories will be displayed per page to visitors. Once this value is exceeded, multiple pages will be available.

Default: *10*

#### Time Zone

All blog dates and times are stored in UTC time to remain time zone agnostic. By setting a specific time zone, dates and times can be properly converted from local time to UTC time when editing post and other blog records. Additionally, the dates and times displayed to visitors will be converted back from UTC to the time zone specified.

Default: *UTC*

### Links

**NOTE: Avoid changing these values for an established blog as doing so may break links to your blog and inside your blog.**

#### Post URL Structure

Post links are comprised of various post attributes separated by forward flashes. Using the default structure of */%year%/%month%/%slug%*, a post titled ""New Post"", published on January 1st, 2000 would have a URL of `example.com/2000/01/new-post`. The following list contains the possible values that can be used as components of the URL structure (the percent signs indicate that the value should be replaced with data from the post).

* *%year%* - publish year
* *%month%* - publish month
* *%day%* - publish day
* *%slug%* - post slug (built from title)
* *%id%* - post id (database key)

Default: */%year%/%month%/%slug%*

#### Page URL Structure

Page links are comprised of various page attributes separated by forward flashes. Using the default structure of */%slug%*, a page titled ""New Page"" would have a URL of `example.com/new-page`. The following list contains the possible values that can be used as components of the URL structure (the percent signs indicate that the value should be replaced with data from the page).

* *%year%* - publish year
* *%month%* - publish month
* *%day%* - publish day
* *%slug%* - page slug (built from page title)
* *%id%* - page id (database key)

Default: */%slug%*

#### Tag URL Structure

Tag links are comprised of various tag attributes separated by forward flashes. Using the default structure of */tag/%slug%*, a tag named ""New Tag"" would have a URL of `example.com/tag/new-tag`. The following list contains the possible values that can be used as components of the URL structure (the percent signs indicate that the value should be replaced with data from the tag).

* *%slug%* - tag slug (built from tag name)
* *%id%* - tag id (database key)

Default: */tag/%slug%*

#### Category URL Structure

Category links are comprised of various category attributes separated by forward flashes. Using the default structure of */category/%slug%*, a category named ""New Category"" would have a URL of `example.com/category/new-category`. The following list contains the possible values that can be used as components of the URL structure (the percent signs indicate that the value should be replaced with data from the category).

* *%slug%* - category slug (built from category name)
* *%id%* - category id (database key)

Default: */category/%slug%*
