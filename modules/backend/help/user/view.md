## Overview

User view shows details of the user which can then be edited or deleted via buttons in the action bar. In addition to the user details, posts created by that user are also shown.

## Action Bar

* **Edit** Make changes to the user such as updating name, email, or password.
* **Delete** Permanently delete the user.

## Notes

* Users cannot be deleted. This is to maintain the integrity of the blog and the content that user may have created.
* Users may edit their own user record by clicking *edit* in the action bar while viewing their user record or by navigating to their *My Profile* page.
