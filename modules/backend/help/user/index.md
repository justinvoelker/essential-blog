## Overview

Users are the contributors, authors, editors, or administrators of the blog. On this page, all users, either active or invited, are shown, sorted by ascending name. Click a user name to view that individual user. Or, for invited users, click *Resend invitation* to resend the invitation email.

## Action Bar

* **Invite User** Invite a new user to participate in the blog. The modal window that appears will require an email address and initial role to be assigned to the user (can be changed later if necessary).

## Notes

* Once a user is invited, their email and role cannot be changed until the invite is accepted.
* Once an invite is accepted, that user cannot be deleted. This is to maintain the integrity of the blog and the content that user may have created.
