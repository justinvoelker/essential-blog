## Overview

When updating a user, basic demographic information can be entered as well as a short biography and login credentials. Leave password field blank unless intending to change the password. Connections can also be edited which may be used to connect the local user account with other services (if optionally configured).

## Action Bar

* **Save** Save profile.

## Fields

* **Profile Image** An image that accompanies the user throughout the administrative interface. May or may not also be displayed by the selected blog theme.
* **First Name** User first name
* **Last Name** User last name
* **Email** Email address used for password reset and other communications
* **Short Biography** A short user biography for possible inclusion in posts. May or may not be displayed by the selected blog theme.
* **Username** Username for administrative authentication
* **New Password** Only to be populated if intending to change user password
* **Connections** Connect local account to additional services for simpler logins.

## Notes

* Users cannot change their own roles.
* Users cannot set connections for other users.
