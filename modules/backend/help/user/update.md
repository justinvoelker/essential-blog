## Overview

When updating a user, basic demographic information can be entered as well as a short biography and login credentials. Leave password field blank unless intending to change the password. User security role can also be selected.


## Action Bar

* **Save** Save the user.

## Fields

* **Profile Image** An image that accompanies the user throughout the administrative interface. May or may not also be displayed by the selected blog theme.
* **First Name** User first name
* **Last Name** User last name
* **Email** Email address used for password reset and other communications
* **Short Biography** A short user biography for possible inclusion in posts. May or may not be displayed by the selected blog theme.
* **Username** Username for administrative authentication
* **New Password** Only to be populated if intending to change user password
* **Role** Security role that defines authorized actions for the user.

## Notes

* Users cannot change their own roles.
* Users cannot set connections for other users.
* Once an invite is accepted, that user cannot be deleted. This is to maintain the integrity of the blog and the content that user may have created.
