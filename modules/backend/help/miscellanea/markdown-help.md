Type | ... to get
--- | ---
\*Italic\* | *Italic*
\*\*Bold\*\* | **Bold**
&#126;&#126;Strikethrough&#126;&#126; | ~~Strikethrough~~
\# Heading 1 | <span class="h1-example">Heading 1</span>
\## Heading 2 | <span class="h2-example">Heading 2</span>
\### Heading 3 | <span class="h3-example">Heading 3</span>
\[Link\]\(http\://google.com\) | [Link](http://google.com)
\!\[Image\]\(http\://google.com/logo.png\) | ![Link](https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png)
\> Blockquote<br>> Single or multiline | <blockquote>Blockquote<br>Single or multiline</blockquote>
\* First<br>\* Second<br>\* Third | <ul><li>First</li><li>Second</li><li>Third</li><ul> 
1. First<br>2. Second<br>3. Third | <ol><li>First</li><li>Second</li><li>Third</li><ol> 
\`Inline code\` with backticks | `Inline code` with backticks
\`\`\`<br># code block<br>begins and ends with 3 backticks<br>\`\`\` | <pre># code block<br>begins and ends with 3 backticks</pre>
