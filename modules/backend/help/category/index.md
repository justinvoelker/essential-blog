## Overview

Categories are used to separate posts based on topic. This page displays all categories created from the *New category* page. Categories are sorted by ascending name. Click a category name to view that individual category.

## Action Bar

* **New Category ** Create a new category

## Notes

* Category *posts count* only includes *primary* posts (not restore points). There may be posts in a draft status that use a given category that are not included in the total count.
