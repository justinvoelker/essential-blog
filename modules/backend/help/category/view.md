## Overview

Category view shows details of the category which can then be edited or deleted via buttons in the action bar. In addition to the category details, *primary* posts using that category are also shown.

## Action Bar

* **Edit** Make changes to the category such as updating name, image, or description.
* **Delete** Permanently delete the category.

## Notes

* Once deleted, categories cannot be recovered.
* In-use categories cannot be deleted. Categories must be removed from any posts using them to be deleted.
