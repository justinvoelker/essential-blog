## Overview

When creating a category, only name is required. An optional image and description can be populated or edited at a later time.

## Action Bar

* **Save** Save the category.

## Fields

* **Name** Category name as it will appear to visitors.
* **Image** An image that accompanies the category. May or may not be displayed by the selected blog theme.
* **Description** A short description of what this category represents. May or may not be displayed by the selected blog theme.

## Notes

* Once a category is created, the slug (not editable) will not be changed even if the name is updated.
* Any changes made to categories are immediately visible to visitors.
