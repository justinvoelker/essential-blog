## Overview

Images are used throughout the blog as logos, author photos, post header photos, inline post photos, and more. This page displays all images uploaded from anywhere in the administrative interface. Images can be uploaded directly on this page or from anywhere that images can be selected such as when editing posts, user profiles, or changing settings. Images are sorted by descending creation date (most recently uploaded first). Click an image to view it or edit its details.

In order to facilitate a faster web experience for visitors, responsive images are used. Rather than displaying the uploaded image to visitors, many smaller variations of the image may be displayed on small screens. For uploaded JPG images, the creation of these smaller variations is handled automatically. However, for other image types, smaller variations should be created manually then uploaded by editing the image and clicking *Add Variation*.

## Action Bar

* **New Image** Upload a new image

## Notes

* None
* Image variations are only automatically create for JPG images. Variations of other file types need to be created and uploaded manually.
