## Overview

Image view shows details of the image which can then be edited or deleted via buttons in the action bar. In addition to the image details, additional sections can be expanded to show image variations and an image preview.

## Action Bar

* **Edit** Make changes to the image such as updating the title.
* **Delete** Permanently delete the image.

## Notes

* Once deleted, images cannot be recovered.
* Images may be used within post content, as images for tags or categories, or as user profile photos. Use caution when deleting images to ensure they are not used throughout the blog.
