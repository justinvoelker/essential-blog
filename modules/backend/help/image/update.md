## Overview

When updating an image, only alternative_text is required. Though the actual uploaded image cannot be changed, image variations can be added and deleted. Click *Add Variation* in the action bar to display the upload window. To delete an image variation, expand the *Image Variations* section, select the image variation(s) to be deleted and click *Delete Variation* in the action bar.

## Action Bar

* **Save** Save the image.

## Fields

* **Alternative Text** Used as the *alt* attribute of the rendered *img* tag.
* **Title** Used as the *title* attribute of the rendered *img* tag.

## Notes

* Any changes made to images are immediately visible to visitors (except alt attribute of images embedded in posts and pages as they are explicitly set in the post/page).
* Adding and deleting image variations is instantaneous and is not affected by saving the record.
