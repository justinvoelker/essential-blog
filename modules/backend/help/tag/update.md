## Overview

When updating a tag, only name is required. An optional image and description can be populated or edited at a later time.

## Action Bar

* **Save** Save the tag.

## Fields

* **Name** Tag name as it will appear to visitors.
* **Image** An image that accompanies the tag. May or may not be displayed by the selected blog theme.
* **Description** A short description of what this tag represents. May or may not be displayed by the selected blog theme.

## Notes

* Once a tag is created, the slug (not editable) will not be changed even if the name is updated.
* Any changes made to tags are immediately visible to visitors.
