## Overview

Tag view shows details of the tag which can then be edited or deleted via buttons in the action bar. In addition to the tag details, *primary* posts using that tag are also shown.

## Action Bar

* **Edit** Make changes to the tag such as updating name, image, or description.
* **Delete** Permanently delete the tag.

## Notes

* Once deleted, tags cannot be recovered.
* In-use tags cannot be deleted. Tags must be removed from any posts using them to be deleted.
