## Overview

Tags are used as keywords to identify various topics in a given post. This page displays all tags created anywhere in the administrative interface. Tags can be created from the *New tag* page or on-the-fly while editing posts. Tags are sorted by ascending name. Click a tag name to view that individual tag.

## Action Bar

* **New Tag** Create a new tag

## Notes

* Tag *posts count* only includes *primary* posts (not restore points). There may be posts in a draft status that use a given tag that are not included in the total count.
