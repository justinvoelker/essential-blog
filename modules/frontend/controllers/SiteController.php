<?php

namespace app\modules\frontend\controllers;

use app\modules\base\models\Archive;
use app\modules\base\models\Category;
use app\modules\base\models\Injection;
use app\modules\base\models\Page;
use app\modules\base\models\Post;
use app\modules\base\models\Setting;
use app\modules\base\models\Tag;
use app\modules\frontend\helpers\ControllerHelper;
use app\modules\frontend\models\PageSearch;
use app\modules\frontend\models\PostSearch;
use yii;
use yii\caching\TagDependency;
use yii\data\Pagination;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

/**
 * SiteController implements the generic site actions (those not related to posts)
 */
class SiteController extends Controller
{
    /**
     * @var string The name of the setting that indicates items per page (differs between backend and frontend)
     */
    public $pageSizeSetting = 'frontend_items_per_page';

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
    * Lists all Post models.
    * @return mixed
    */
    public function actionIndex()
    {
        /* Standard search model with ActiveDataProvider */
        $searchModel = new PostSearch();
        $postDataProvider = $searchModel->search(null);

        /* Query and Pagination for posts without using ActiveDataProvider */
        $qp = $this->getQueryAndPagination($searchModel->query);

        return $this->render('index', [
            'postDataProvider' => $postDataProvider,
            'records' => $qp['records'],
            'pages' => $qp['pages'],
        ]);
    }

    /**
     * Lists all Post models for a year, month, and day (or subset of those)
     * @param null $year
     * @param null $month
     * @param null $day
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionArchive($year = null, $month = null, $day = null)
    {
        /* Standard search model with ActiveDataProvider */
        $searchModel = new PostSearch(['publish_year' => $year, 'publish_month' => $month, 'publish_day' => $day]);
        $postDataProvider = $searchModel->search(null);

        /* Query and Pagination for posts without using ActiveDataProvider */
        $qp = $this->getQueryAndPagination($searchModel->query);

        /* Archive model for the specific archive requested */
        $archiveModel = new Archive(['year' => $year, 'month' => $month, 'day' => $day]);

        return $this->render('archive', [
            'postDataProvider' => $postDataProvider,
            'records' => $qp['records'],
            'pages' => $qp['pages'],
            'archive' => $archiveModel,
        ]);
    }

    /**
     * Displays a single Post model.
     * @param null $year
     * @param null $month
     * @param null $day
     * @param null $slug
     * @param null $id
     * @param null $preview
     * @param null $restorepoint
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionPost($year = null, $month = null, $day = null, $slug = null, $id = null, $preview = null, $restorepoint = null)
    {
        // Find model
        $postModel = $this->findModelPost($year, $month, $day, $slug, $id, $preview, $restorepoint);

        // Set some app-level params to be accessed, for example, in injections
        \Yii::$app->params['modelAttributes'] = $postModel->attributes;

        return $this->render('post', [
            'post' => $postModel,
        ]);
    }

    /**
     * Displays a single Page model.
     * @param null $year
     * @param null $month
     * @param null $day
     * @param null $slug
     * @param null $id
     * @param null $preview
     * @param null $restorepoint
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionPage($year = null, $month = null, $day = null, $slug = null, $id = null, $preview = null, $restorepoint = null)
    {
        // Find model
        $pageModel = $this->findModelPage($year, $month, $day, $slug, $id, $preview, $restorepoint);

        // Set some app-level params to be accessed, for example, in injections
        \Yii::$app->params['modelAttributes'] = $pageModel->attributes;

        return $this->render('page', [
            'page' => $pageModel,
        ]);
    }

    /**
     * Lists all Post models for a given tag.
     * @param null $slug
     * @param null $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionTag($slug = null, $id = null)
    {
        /* Standard search model with ActiveDataProvider */
        $searchModel = new PostSearch(['tag_id' => $id, 'tag_slug' => $slug]);
        $postDataProvider = $searchModel->search(Yii::$app->request->queryParams);

        /* Query and Pagination for posts without using ActiveDataProvider */
        $qp = $this->getQueryAndPagination($searchModel->query);

        /* Tag model for the specific tag requested */
        $tagModel = $this->findModelTag($slug, $id);

        // Set some app-level params to be accessed, for example, in injections
        \Yii::$app->params['modelAttributes'] = $tagModel->attributes;

        return $this->render('tag', [
            'postDataProvider' => $postDataProvider,
            'records' => $qp['records'],
            'pages' => $qp['pages'],
            'tag' => $tagModel,
        ]);
    }

    /**
     * Lists all Post models for a given category.
     * @param null $slug
     * @param null $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionCategory($slug = null, $id = null)
    {
        /* Standard search model with ActiveDataProvider */
        $searchModel = new PostSearch(['category_id' => $id, 'category_slug' => $slug]);
        $postDataProvider = $searchModel->search(Yii::$app->request->queryParams);

        /* Query and Pagination for posts without using ActiveDataProvider */
        $qp = $this->getQueryAndPagination($searchModel->query);

        /* Category model for the specific tag requested */
        $categoryModel = $this->findModelCategory($slug, $id);

        // Set some app-level params to be accessed, for example, in injections
        \Yii::$app->params['modelAttributes'] = $categoryModel->attributes;

        return $this->render('category', [
            'postDataProvider' => $postDataProvider,
            'records' => $qp['records'],
            'pages' => $qp['pages'],
            'category' => $categoryModel,
        ]);
    }

    /**
     * Finds the Post model based on its publish year, publish month, publish day, slug and/or id.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param null $year
     * @param null $month
     * @param null $day
     * @param null $slug
     * @param null $id
     * @param null $preview
     * @param null $restorepoint
     * @return Post|array|yii\db\ActiveRecord the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     * @throws HttpException
     */
    protected function findModelPost($year = null, $month = null, $day = null, $slug = null, $id = null, $preview = null, $restorepoint = null)
    {
        $query = Post::find();

        /**
         * Begin with criteria to find the exact post requested based on url structure.
         */
        $query->andFilterWhere([PostSearch::queryUrlPiece('%year%') => $year])
            ->andFilterWhere([PostSearch::queryUrlPiece('%month%') => $month])
            ->andFilterWhere([PostSearch::queryUrlPiece('%day%') => $day])
            ->andFilterWhere(['slug' => $slug])
            ->andFilterWhere(['id' => $id]);


        /* Logic changes if preview is true */
        if ($preview == 'true') {
            /* Throw an exception if a guest is attempting to view a preview */
            if (Yii::$app->user->isGuest) {
                throw new HttpException(403, 'Must be logged in to view previews.');
            }

            /* If a restore point is set ... */
            if ($restorepoint) {
                /* ... find that restore point */
                $query->andFilterWhere(['id' => $restorepoint]);
            } else {
                /* ... otherwise sort by created_at so when one post is returned it is the newest */
                $query->orderBy(['created_at' => SORT_DESC]);
            }

        } else {

            /**
             * The following conditions will ALWAYS apply:
             * 1) Post is primary
             * 2) status is published
             * 3) scheduled_for is either in the past or not set
             *
             * Same criteria used in frontend\PostSearch search() to ensure individual posts
             * are not show when they should not be.
             */
            $query->andWhere(['primary_id' => null]);
            $query->andWhere(['status' => Post::STATUS_PUBLISHED]);
            $query->andWhere(['or', ['<=', 'scheduled_for', time()], ['scheduled_for' => null]]);

        }

        /* Based on the filtering set above, only return one result */
        $model = $query->one();

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested post does not exist.');
        }
    }

    /**
     * Finds the Page model based on its publish year, publish month, publish day, slug and/or id.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param null $year
     * @param null $month
     * @param null $day
     * @param null $slug
     * @param null $id
     * @param null $preview
     * @param null $restorepoint
     * @return Page|array|yii\db\ActiveRecord the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     * @throws HttpException
     */
    protected function findModelPage($year = null, $month = null, $day = null, $slug = null, $id = null, $preview = null, $restorepoint = null)
    {
        $query = Page::find();

        /**
         * Begin with criteria to find the exact page requested based on url structure.
         */
        $query->andFilterWhere([PageSearch::queryUrlPiece('%year%') => $year])
            ->andFilterWhere([PageSearch::queryUrlPiece('%month%') => $month])
            ->andFilterWhere([PageSearch::queryUrlPiece('%day%') => $day])
            ->andFilterWhere(['slug' => $slug])
            ->andFilterWhere(['id' => $id]);


        /* Logic changes if preview is true */
        if ($preview == 'true') {
            /* Throw an exception if a guest is attempting to view a preview */
            if (Yii::$app->user->isGuest) {
                throw new HttpException(403, 'Must be logged in to view previews.');
            }

            /* If a restore point is set ... */
            if ($restorepoint) {
                /* ... find that restore point */
                $query->andFilterWhere(['id' => $restorepoint]);
            } else {
                /* ... otherwise sort by created_at so when one post is returned it is the newest */
                $query->orderBy(['created_at' => SORT_DESC]);
            }

        } else {

            /**
             * The following conditions will ALWAYS apply:
             * 1) Page is primary
             * 2) status is published
             * 3) scheduled_for is either in the past or not set
             *
             * Same criteria used in frontend\PageSearch search() to ensure individual pages
             * are not show when they should not be.
             */
            $query->andWhere(['primary_id' => null]);
            $query->andWhere(['status' => Page::STATUS_PUBLISHED]);
            $query->andWhere(['or', ['<=', 'scheduled_for', time()], ['scheduled_for' => null]]);

        }

        /* Based on the filtering set above, only return one result */
        $model = $query->one();

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelTag($slug = null, $id = null)
    {
        $query = Tag::find();

        $query
            ->andFilterWhere(['slug' => $slug])
            ->andFilterWhere(['id' => $id]);

        $model = $query->one();

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested tag does not exist.');
        }
    }

    protected function findModelCategory($slug = null, $id = null)
    {
        $query = Category::find();

        $query
            ->andFilterWhere(['slug' => $slug])
            ->andFilterWhere(['id' => $id]);

        $model = $query->one();

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested category does not exist.');
        }
    }

    /**
     * Starting with yii\db\ActiveQuery, extract and return an array of Posts and a Pagination instance.
     * This allows to use of posts and pagination without requiring the use of ActiveDataProvider.
     *
     * @param yii\db\ActiveQuery $searchQuery
     * @return array the loaded model
     */
    protected function getQueryAndPagination($searchQuery) {
        /* Query and Pagination for posts without using ActiveDataProvider */
        $countQuery = clone $searchQuery;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => Setting::getSetting($this->pageSizeSetting),
        ]);
        $records = $searchQuery->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return ['records' => $records, 'pages' => $pages];
    }

    /**
     * @inheritdoc
     */
    public function render($view, $params = [])
    {
        // Add robots noindex meta tag if allow_indexing setting is false
        if (!filter_var(Setting::getSetting('allow_indexing'), FILTER_VALIDATE_BOOLEAN)) {
            $this->view->registerMetaTag(['name' => 'robots', 'content' => 'noindex']);
        }
        $this->view->registerLinkTag(['rel' => 'canonical', 'href' => Url::canonical()]);

        /* Get rendered output ready for injections */
        $render = parent::render($view, $params);

        /* Retrieve injections (from cache or database) and sort according to order */
        /* Not sorting in retrieve to keep that query basic to ensure cached version is used */
        /** @noinspection PhpUnusedParameterInspection */
        $injections = Injection::getDb()->cache(function ($db) {
            return Injection::find()->asArray()->all();
        }, 0, new TagDependency(['tags' => ['injections']]));
        usort($injections, function($a, $b) {
            return $a['order'] - $b['order'];
        });

        /* Loop through each injection, updating rendered output each time */
        foreach ($injections as $injection) {
            /* Only continue with enabled injections */
            if($injection['status'] == Injection::STATUS_ENABLED) {
                $render = ControllerHelper::inject([
                    'render' => $render,
                    'xpath' => $injection['xpath'],
                    'action' => $injection['action'],
                    'content' => $injection['content'],
                    'parser' => $injection['parser']
                ]);
            }
        }

        return $render;
    }
}
