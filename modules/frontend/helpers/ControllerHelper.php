<?php

namespace app\modules\frontend\helpers;

use app\modules\base\models\Injection;
use Wa72\HtmlPageDom\HtmlPageCrawler;
use yii\helpers\Markdown;

class ControllerHelper
{
    /**
     * @inheritdoc
     */
    public static function inject($options = [])
    {
        /* Pull options from parameters */
        $render = isset($options['render']) ? $options['render'] : null;
        $xpath = isset($options['xpath']) ? $options['xpath'] : null;
        $action = isset($options['action']) ? $options['action'] : null;
        $content = isset($options['content']) ? $options['content'] : null;
        $parser = isset($options['parser']) ? $options['parser'] : null;

        /* Parse content according to parser */
        switch ($parser) {
            case Injection::PARSER_MARKDOWN:
                $content = Markdown::process($content, 'gfm');
                break;
            case Injection::PARSER_PHP;
                /* Do not allow entire frontend site to fail with error if parsing fails */
                try {
                    $content = eval($content);
                } catch (\Throwable $t) {
                    /* Clear content so as to not output unparsed PHP */
                    $content = '';
                    \Yii::warning('ParseError in injection: ' . $t->getMessage());
                }
                break;
        }

        /* Create object from original HTML source */
        $page = HtmlPageCrawler::create($render);

        /* Based on action, perform injection */
        switch ($action) {
            case Injection::ACTION_INSERT_BEFORE_ELEMENT:
                /* <h2>Title</h2> becomes <span>new</span><h2>Title</h2> */
                $page->filterXPath($xpath)->before($content);
                break;
            case Injection::ACTION_INSERT_AFTER_ELEMENT:
                /* <h2>Title</h2> becomes <h2>Title</h2><span>new</span> */
                $page->filterXPath($xpath)->after($content);
                break;
            case Injection::ACTION_PREPEND_TO_CONTENT:
                /* <h2>Title</h2> becomes <h2><span>new</span>Title</h2> */
                $page->filterXPath($xpath)->prepend($content);
                break;
            case Injection::ACTION_APPEND_TO_CONTENT:
                /* <h2>Title</h2> becomes <h2>Title<span>new</span></h2> */
                $page->filterXPath($xpath)->append($content);
                break;
            case Injection::ACTION_REPLACE_CONTENT:
                /* <h2>Title</h2> becomes <h2><span>new</span></h2> */
                $page->filterXPath($xpath)->html($content);
                break;
            case Injection::ACTION_REMOVE_CONTENT:
                /* <h2>Title</h2> becomes <h2></h2> */
                $page->filterXPath($xpath)->makeEmpty();
                break;
            case Injection::ACTION_REPLACE_ELEMENT:
                /* <h2>Title</h2> becomes <span>new</span> */
                $page->filterXPath($xpath)->replaceWith($content);
                break;
            case Injection::ACTION_REMOVE_ELEMENT:
                /* <h2>Title</h2> becomes [nothing] */
                $page->filterXPath($xpath)->remove();
                break;
        }

        /* Return HTML with injected content */

        return $page->saveHTML();
    }
}
