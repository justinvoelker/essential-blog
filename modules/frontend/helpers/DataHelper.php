<?php

namespace app\modules\frontend\helpers;

use app\components\Functions;
use yii\helpers\Url;

class DataHelper
{
    /*
     * Constants
     */
    const TITLE_SEPARATOR = ' - ';

    public static function headTitle($options = [])
    {
        $pageTitle = isset($options['pageTitle']) ? $options['pageTitle'] : null;
        $separator = isset($options['separator']) ? $options['separator'] : self::TITLE_SEPARATOR;
        $blogTitle = isset($options['blogTitle']) ? $options['blogTitle'] : Functions::getSetting('blog_title');

        if (isset($pageTitle)) {
            return $pageTitle . $separator . $blogTitle;
        } else {
            return $blogTitle;
        }
    }

    public static function urlPath()
    {
        return Url::current();
    }

    public static function pageAction()
    {
        return \Yii::$app->controller->action->id;
    }
}
