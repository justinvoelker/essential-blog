<?php

namespace app\modules\frontend;

use app\modules\base\models\BlogTheme;
use yii;
use yii\base\Module as YiiBaseModule;
use yii\base\Theme;

/**
 * Frontend module definition class
 *
 * The Frontend module contains all of the class definitions for frontend interaction.
 *
 * Models: All model properties and methods that are used for frontend (and possibly administrative functionality) are
 * included in the Frontend module's models. If a property or method is only necessary for the Backend, it should not
 * be included in the Frontend module. All properties and methods included in the Frontend module are accessible within
 * the Backend module as the Backend models extend the Frontend models.
 *
 * Views: All views for frontend functionality.
 *
 * Controllers: All controllers for frontend functionality.
 */
class Module extends YiiBaseModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\frontend\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        /* Get the blog theme */
        $theme = BlogTheme::getBlogTheme();

        /* Set the blog theme, if one is selected. Default theme will be set as NULL in database. */
        if ($theme != null) {
            Yii::$app->view->theme = new Theme([
                'basePath' => '@app/web/themes/' . $theme,
                'baseUrl' => '@web/themes/' . $theme,
                'pathMap' => [
                    '@app/modules/frontend/views' => '@app/web/themes/' . $theme . '/views/',
                ],
            ]);
        }

    }
}
