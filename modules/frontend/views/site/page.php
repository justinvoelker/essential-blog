<?php

use app\components\Functions;
use app\modules\base\models\Image;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $page app\modules\base\models\Page */

$this->title = Html::encode($page->title);
$this->params['description'] = strip_tags($page->parsedExcerpt());
?>

<article class="page site-main__item">

    <header class="page__page-header">

        <?php if ($page->image): ?>
            <div class="page__page-image">
                <?php
                $pageImage = Image::findModelByPath($page->image);
                echo $pageImage->imgTag([
                    /* <=818 = 100% - 25px left padding - 25px right padding */
                    /* 768px */
                    'sizes' => '(max-width: 818px) calc(100vw - 50px), 768px',
                    'class' => 'page__page-img',
                ]);
                ?>
            </div>
        <?php endif; ?>

        <h1 class="page__title">
            <?= $page->title ?>
        </h1>

        <div class="page-meta page__meta">
            <?= Yii::$app->formatter->asDate($page->publish_date, 'long') ?>
        </div>

    </header>

    <div class="page-content page__content">
        <?= $page->parsedContent([
            /* <=818 = 100% - 25px left padding - 25px right padding */
            /* 768px */
            'imgSizes' => '(max-width: 818px) calc(100vw - 50px), 768px',
            'imgClass' => 'page-content__img',
            'hrefClass' => 'page-content__href',
        ]) ?>
    </div>

    <footer class="page__footer">
        <div class="page-footer">
            <div class="page-footer__category">
                <?php
                if (!empty($page->category_id)) {
                    echo 'Category: ' . Html::a($page->category->name, $page->category->route(),
                            ['class' => 'page-footer__link']);
                }
                ?>
            </div>
            <div class="page-footer__tags">
                <?php
                if (!empty($page->tags)) {
                    echo 'Tags: ';
                    foreach ($page->tags as $tag) {
                        echo yii\helpers\Html::a($tag->name, $tag->route(), ['class' => 'page-footer__link']) . " ";
                    }
                }
                ?>
            </div>

            <div class="page-footer__author-info clearfix">
                <div class="author-info">
                    <div class="author-info__photo">
                        <?= Functions::getAvatar([
                            'text' => $page->createdBy->firstInitialLastInitial(),
                            'textAsIs' => true,
                            'image' => $page->createdBy->profile_photo,
                            'size' => '100px',
                        ]); ?>
                    </div>
                    <div class="author-info__body">
                        <div class="author-info__name"><?= $page->createdBy->firstNameLastName() ?></div>
                        <div class="author-info__bio"><?= $page->createdBy->short_biography ?></div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</article>
