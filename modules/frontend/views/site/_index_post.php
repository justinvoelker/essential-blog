<?php

use app\modules\base\models\Image;

/* @var $this yii\web\View */
/* @var $model app\modules\base\models\Post */
?>

<article class="post post_preview site-main__item">

    <header class="post__post-header">

        <?php if ($model->image): ?>
            <!--
            <div class="post__post-image">
                <?php
                $postImage = Image::findModelByPath($model->image);
                echo $postImage->imgTag([
                    /* <=818 = 100% - 25px left padding - 25px right padding */
                    /* 768px */
                    'sizes' => '(max-width: 818px) calc(100vw - 50px), 768px',
                    'class' => 'post__post-img',
                ]);
                ?>
            </div>
            -->
        <?php endif; ?>

        <h1 class="post__title">
            <?= \yii\helpers\Html::a($model->title, $model->route(), ['class' => 'post__title-link']) ?>
        </h1>

        <div class="post-meta post__meta">
            <?= Yii::$app->formatter->asDate($model->publish_date, 'long') ?>
        </div>

    </header>

    <div class="post-content post__content">
        <?= $model->parsedExcerpt([
            /* <=818 = 100% - 25px left padding - 25px right padding */
            /* 768px */
            'imgSizes' => '(max-width: 818px) calc(100vw - 50px), 768px',
            'imgClass' => 'post-content__img',
            'hrefClass' => 'post-content__href',
        ]) ?>
        <?= \yii\helpers\Html::a('Continue reading...', $model->route(), ['class' => 'XXXpost__read-more']) ?>
    </div>

</article>
