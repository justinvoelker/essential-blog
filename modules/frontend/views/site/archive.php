<?php

use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $postDataProvider yii\data\ActiveDataProvider */
/* @var $posts app\modules\base\models\Post[] */
/* @var $pages \yii\data\Pagination */
/* @var $archive \app\modules\base\models\Archive */

$this->title = 'Post archive for ' . $archive->title;
$this->params['description'] = $this->title;

?>

<div class="subset-header site-main__subset-header">
    <div class="subset-header__title">
        Post archive for <span class="subset-header__title_accent"><?= $archive->title ?></span>
    </div>
</div>

<?= ListView::widget([
    'dataProvider' => $postDataProvider,
    'layout' => "{items}\n{pager}",
    'pager' => [
        'prevPageLabel' => '&lsaquo;',
        'nextPageLabel' => '&rsaquo;',
    ],
    'itemView' => '_index_post',
]) ?>
