<?php

use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $postDataProvider yii\data\ActiveDataProvider */
/* @var $posts app\modules\base\models\Post[] */
/* @var $pages \yii\data\Pagination */
/* @var $tag app\modules\base\models\Tag */

$this->title = 'Posts tagged with ' . $tag->name;
$this->params['description'] = $this->title;

?>

<div class="subset-header site-main__subset-header">
    <div class="subset-header__title">
        Posts tagged with <span class="subset-header__title_accent"><?= $tag->name ?></span>
    </div>
</div>

<?= ListView::widget([
    'dataProvider' => $postDataProvider,
    'layout' => "{items}\n{pager}",
    'pager' => [
        'prevPageLabel' => '&lsaquo;',
        'nextPageLabel' => '&rsaquo;',
    ],
    'itemView' => '_index_post',
]) ?>
