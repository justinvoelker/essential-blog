<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
$this->params['description'] = $this->title;

?>

<div class="subset-header site-main__subset-header">
    <div class="subset-header__title">
        <?= Html::encode($this->title) ?>
    </div>
</div>

<article class="page">

    <div class="page-content">
        <p>
            <?= nl2br(Html::encode($message)) ?>
        </p>
        <p>
            Please contact us if you think this is a server error. Thank you.
        </p>
    </div>

</article>
