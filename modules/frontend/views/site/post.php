<?php

use app\components\Functions;
use app\modules\base\models\Image;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $post app\modules\base\models\Post */

$this->title = Html::encode($post->title);
$this->params['description'] = strip_tags($post->parsedExcerpt());
?>

<article class="post site-main__item">

    <header class="post__post-header">

        <?php if ($post->image): ?>
            <div class="post__post-image">
                <?php
                $postImage = Image::findModelByPath($post->image);
                echo $postImage->imgTag([
                    /* <=818 = 100% - 25px left padding - 25px right padding */
                    /* 768px */
                    'sizes' => '(max-width: 818px) calc(100vw - 50px), 768px',
                    'class' => 'post__post-img',
                ]);
                ?>
            </div>
        <?php endif; ?>

        <h1 class="post__title">
            <?= $post->title ?>
        </h1>

        <div class="post-meta post__meta">
            <?= Yii::$app->formatter->asDate($post->publish_date, 'long') ?>
        </div>

    </header>

    <div class="post-content post__content">
        <?= $post->parsedContent([
            /* <=818 = 100% - 25px left padding - 25px right padding */
            /* 768px */
            'imgSizes' => '(max-width: 818px) calc(100vw - 50px), 768px',
            'imgClass' => 'post-content__img',
            'hrefClass' => 'post-content__href',
        ]) ?>
    </div>

    <footer class="post__footer">
        <div class="post-footer">
            <div class="post-footer__category">
                <?php
                if (!empty($post->category_id)) {
                    echo 'Category: ' . Html::a($post->category->name, $post->category->route(),
                            ['class' => 'post-footer__link']);
                }
                ?>
            </div>
            <div class="post-footer__tags">
                <?php
                if (!empty($post->tags)) {
                    echo 'Tags: ';
                    foreach ($post->tags as $tag) {
                        echo yii\helpers\Html::a($tag->name, $tag->route(), ['class' => 'post-footer__link']) . " ";
                    }
                }
                ?>
            </div>

            <div class="post-footer__author-info clearfix">
                <div class="author-info">
                    <div class="author-info__photo">
                        <?= Functions::getAvatar([
                            'text' => $post->createdBy->firstInitialLastInitial(),
                            'textAsIs' => true,
                            'image' => $post->createdBy->profile_photo,
                            'size' => '100px',
                        ]); ?>
                    </div>
                    <div class="author-info__body">
                        <div class="author-info__name"><?= $post->createdBy->firstNameLastName() ?></div>
                        <div class="author-info__bio"><?= $post->createdBy->short_biography ?></div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</article>
