<?php

use app\components\Functions;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $postDataProvider yii\data\ActiveDataProvider */
/* @var $posts app\modules\base\models\Post[] */
/* @var $pages \yii\data\Pagination */

$this->title = null;
$this->params['description'] = Functions::getSetting('blog_tagline');

?>

<?= ListView::widget([
    'dataProvider' => $postDataProvider,
    'layout' => "{items}\n{pager}",
    'pager' => [
        'prevPageLabel' => '&lsaquo;',
        'nextPageLabel' => '&rsaquo;',
    ],
    'itemView' => '_index_post',
]) ?>
