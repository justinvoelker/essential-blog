<?php
/* @var $this yii\web\View */
/* @var $content string */

use app\components\Functions;
use app\modules\frontend\helpers\DataHelper;
?>

<?php $this->beginPage() ?>
<!doctype html>
<html lang="<?= Yii::$app->language ?>" data-url-path="<?= DataHelper::urlPath() ?>" data-page-action="<?= DataHelper::pageAction() ?>">
<head>
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="<?= $this->params['description'] ?>">
    <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">

    <title><?= DataHelper::headTitle(['pageTitle' => $this->title]) ?></title>

    <!--suppress HtmlUnknownTarget -->
    <link rel="stylesheet" href="/frontend/css/styles.min.css">

    <?php $this->head() ?>
</head>
<body class="body theme-default">
<?php $this->beginBody() ?>
<div class="container">

    <header class="container__header">
        <div class="site-header">
            <div class="site-header__title">
                <a class="site-header__title-link" href="<?= Yii::$app->homeUrl ?>">
                    <h1 class="site-header__title-text"><?= Functions::getSetting('blog_title') ?></h1>
                </a>
            </div>
            <div class="site-header__tagline">
                <?= Functions::getSetting('blog_tagline') ?>
            </div>
        </div>
    </header>

    <div class="container__nav">
        <?php
        /*
         * To include site navigation, create an injection with the following content:
         * Xpath:       //*[contains(@class,'container__nav')]
         * Action:      Replace content
         * Content:     * [Home](/)
         *              * [About](/about)
         *              * etc...
         * Parser:      Markdown
         */
        ?>
    </div>

    <div class="container__main clearfix">
        <div class="site-main">
            <?= $content ?>
        </div>
    </div>

    <footer class="container__footer">
        <div class="site-footer">
            <div class="site-footer__content clearfix">

                <div class="links-widget site-footer__links-widget">
                    <h3 class="links-widget__title">Categories</h3>
                    <ul class="links-widget__list">
                        <?php $categoriesInUse = Functions::getCategoryList() ?>
                        <?php foreach ($categoriesInUse as $category): ?>
                            <li class="links-widget__item">
                                <?= \yii\helpers\Html::a($category->name, $category->route(), ['class' => 'links-widget__link']) ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>

                <div class="links-widget site-footer__links-widget">
                    <h3 class="links-widget__title">Tags</h3>
                    <ul class="links-widget__list">
                        <?php $tags = Functions::getTagList(); ?>
                        <?php foreach ($tags as $tag): ?>
                            <li class="links-widget__item">
                                <?= \yii\helpers\Html::a($tag->name, $tag->route(), ['class' => 'links-widget__link']) ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>

                <div class="links-widget site-footer__links-widget">
                    <h3 class="links-widget__title">Archives</h3>
                    <ul class="links-widget__list">
                        <?php $archives = Functions::getArchiveList(['count' => 5]) ?>
                        <?php foreach ($archives as $archive): ?>
                            <li class="links-widget__item">
                                <?= \yii\helpers\Html::a($archive->title, $archive->url, ['class' => 'links-widget__link']) ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>

                <div class="links-widget site-footer__links-widget">
                    <h3 class="links-widget__title">Recent Posts</h3>
                    <ul class="links-widget__list">
                        <?php $recentPosts = Functions::getPostList(['count' => 5]) ?>
                        <?php foreach ($recentPosts as $recentPost): ?>
                            <li class="links-widget__item">
                                <?= \yii\helpers\Html::a($recentPost->title, $recentPost->route(), ['class' => 'links-widget__link']) ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>

            </div>
        </div>
    </footer>
</div>

<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>
