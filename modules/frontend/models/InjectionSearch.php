<?php

namespace app\modules\frontend\models;

use app\modules\base\models\Injection;
use app\modules\base\models\Setting;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * InjectionSearch represents the model behind the search form about `app\modules\base\models\Injection`.
 */
class InjectionSearch extends Injection
{
    /**
     * @var string The name of the setting that indicates items per page (differs between backend and frontend)
     */
    public $pageSizeSetting = 'frontend_items_per_page';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'updated_by', 'action', 'parser', 'order', 'status'], 'integer'],
            [['name', 'xpath', 'content'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Injection::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => Setting::getSetting($this->pageSizeSetting),
            ],
            'sort' => [
                'defaultOrder' => ['order' => SORT_ASC]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
            'action' => $this->action,
            'parser' => $this->parser,
            'order' => $this->order,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'xpath', $this->xpath])
            ->andFilterWhere(['like', 'content', $this->content]);

        return $dataProvider;
    }
}
