<?php

namespace app\modules\frontend\models;

use app\modules\base\models\Category;
use app\modules\base\models\Setting;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CategorySearch represents the model behind the search form about `app\modules\base\models\Category`.
 */
class CategorySearch extends Category
{
    /**
     * @var string The name of the setting that indicates items per page (differs between backend and frontend)
     */
    public $pageSizeSetting = 'frontend_items_per_page';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'primary_posts_count'], 'integer'],
            [['name', 'slug'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        /**
         * For being able to sort/filter calculated field:
         * http://www.yiiframework.com/wiki/679/filter-sort-by-summary-data-in-gridview-yii-2-0/
         * https://github.com/yiisoft/yii2/issues/2179#issuecomment-191476137
         */
        $query = Category::find()
            ->select([
                '{{%category}}.*', // select all category fields
                'COUNT(CASE WHEN {{%post}}.primary_id IS NULL THEN 1 ELSE NULL END) AS primary_posts_count', // calculate primary posts count
                'COUNT(CASE WHEN {{%page}}.primary_id IS NULL THEN 1 ELSE NULL END) AS primary_pages_count' // calculate primary pages count
            ])
            ->joinWith('posts')// ensure table junction
            ->joinWith('pages')// ensure table junction
            ->groupBy('{{%category}}.id'); // group the result to ensure aggregation function works

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => Setting::getSetting($this->pageSizeSetting),
            ],
            'sort'=> [
                'defaultOrder' => ['name'=>SORT_ASC]
            ],
        ]);

        $dataProvider->sort->attributes['primary_posts_count'] = [
            'asc' => ['primary_posts_count' => SORT_ASC],
            'desc' => ['primary_posts_count' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['primary_pages_count'] = [
            'asc' => ['primary_pages_count' => SORT_ASC],
            'desc' => ['primary_pages_count' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', '{{%category}}.slug', $this->slug]);

        return $dataProvider;
    }
}
