<?php

namespace app\modules\frontend\models;

use app\modules\base\models\Post;
use app\modules\base\models\Setting;
use yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PostSearch represents the model behind the search form about `app\modules\base\models\Post`.
 */
class PostSearch extends Post
{
    /**
     * @var string The name of the setting that indicates items per page (differs between backend and frontend)
     */
    public $pageSizeSetting = 'frontend_items_per_page';

    /**
     * @var string Filter posts by publish year
     */
    public $publish_year;

    /**
     * @var string Filter posts by publish month
     */
    public $publish_month;

    /**
     * @var string Filter posts by publish day
     */
    public $publish_day;

    /*
     * @var string Filter posts by tag
     */
    public $tag_id;
    public $tag_slug;

    /*
     * @var string Filter posts by category
     */
    public $category_id;
    public $category_slug;
    /*
     * @var string Used in app\modules\base\models\Post validateSlug()
     */
    public $url_path;

    /**
     * @var yii\db\ActiveQuery  non-ActiveDataProvider search query
     */
    public $query;

    /**
     * @inheritdoc
     */
    // TODO Are these rules needed? They don't seem to affect anything...
//    public function rules()
//    {
//        return [
//            [['id', 'published_at', 'revised_at'], 'integer'],
//            [['title', 'slug', 'content'], 'safe'],
//        ];
//    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        /**
         * Post publish date is either the scheduled date (if set) or the published date (if set). If neither are set,
         * then current timestamp. Current time stamp is used as the "else" since that is the date/time that will be
         * used in generating slugs for newly created posts. Besides, that date won't matter on the frontend anyway
         * since only published posts are shown.
         */
        $queryPublishDate = 'IF({{%post}}.scheduled_for IS NOT NULL, {{%post}}.scheduled_for, IF({{%post}}.published_at IS NOT NULL, {{%post}}.published_at, UNIX_TIMESTAMP()))';

        $query = Post::find()
            ->select([
                /* All post fields */
                '{{%post}}.*',
                /* Calculated fields for publish_date and publish date parts */
                $queryPublishDate . ' AS publish_date',
                self::queryUrlPiece('%year%') . ' AS publish_year',
                self::queryUrlPiece('%month%') . ' AS publish_month',
                self::queryUrlPiece('%day%') . ' AS publish_day',
                /* Calculated field for configured url structure (for validating slug uniqueness) */
                self::queryUrlPath() . ' AS url_path'
            ])
            ->joinWith('tags')
            ->joinWith('category')
            /* OrderBy is specified here and not in the dataProvider since this is where the calculated column can be used. */
            ->orderBy('publish_date DESC')
            /* Prevent posts with multiple tags from appearing more than once */
            ->groupBy('{{%post}}.id');

        /**
         * The following conditions will ALWAYS apply:
         * 1) Post is the parent
         * 2) status is published
         * 3) scheduled_for is either in the past or not set
         *
         * Same criteria used in frontend\SiteController findModel() to ensure individual posts
         * are not show when they should not be.
         */
        $query->andWhere(['primary_id' => null]);
        $query->andWhere(['status' => Post::STATUS_PUBLISHED]);
        $query->andWhere(['or', ['<=', 'scheduled_for', time()], ['scheduled_for' => null]]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => Setting::getSetting($this->pageSizeSetting),
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            self::queryUrlPiece('%year%') => $this->publish_year,
            self::queryUrlPiece('%month%') => $this->publish_month,
            self::queryUrlPiece('%day%') => $this->publish_day,
            self::queryUrlPath() => $this->url_path,
            '{{%tag}}.id' => $this->tag_id,
            '{{%tag}}.slug' => $this->tag_slug,
            '{{%category}}.id' => $this->category_id,
            '{{%category}}.slug' => $this->category_slug,
        ]);

        $this->query = $query;

        return $dataProvider;
    }

    public static function queryUrlPath()
    {
        // URL Structure
        $urlStructure = Setting::getSetting('url_structure_post');
        // Split into pieces, removing the first slash to avoid a blank first item
        $urlPieces = explode('/', substr($urlStructure, 1));

        // Append each piece of the url pattern to the url being built
        $url = [];
        foreach ($urlPieces as $urlPiece) {
            $url[] = self::queryUrlPiece($urlPiece);
        }

        return 'CONCAT("/", ' . implode(', "/", ', $url) . ')';
    }

    public static function queryUrlPiece($piece)
    {
        $date = 'IF({{%post}}.scheduled_for IS NOT NULL, {{%post}}.scheduled_for, IF({{%post}}.published_at IS NOT NULL, {{%post}}.published_at, UNIX_TIMESTAMP()))';
        if ($piece == '%year%') {
            return 'DATE_FORMAT(FROM_UNIXTIME(' . $date . '), \'%Y\')';
        } elseif ($piece == '%month%') {
            return 'DATE_FORMAT(FROM_UNIXTIME(' . $date . '), \'%m\')';
        } elseif ($piece == '%day%') {
            return 'DATE_FORMAT(FROM_UNIXTIME(' . $date . '), \'%d\')';
        } elseif ($piece == '%slug%') {
            return Post::tableName() . '.`slug`';
        } elseif ($piece == '%id%') {
            return Post::tableName() . '.`id`';
        } else {
            return '"' . $piece  .'"';
        }
    }
}
