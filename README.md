# Essential Blog (Archived)

In late 2015, I started writing my own blogging application. Though I
cannot remember all of the reasoning behind this decision, there were
two factors that played an important role.

First, I'm not a huge fan of the way WordPress tries to be everything
to everyone. I just want a blog. Second, when I found Ghost, I thought
I was set. However, it was pretty new at the time and missing a couple
features I wanted.

So, I decided to head down the "roll your own" path. What I thought
would take maybe a year actually took over three. In the end, it was
over 1,300 commits but I was happy with the results. 

The finished product was Essential Blog. It combined the barebones,
blogging-only approach of Ghost but tweaked some of the features I did
not like and added a few I thought were missing.

Now that it's done, I have a notebook full of improvements. Given the
time (and desire) I would likely just start from scratch. That said,
I'm going to leave Essential Blog as it is. Not only does Ghost now
include most of the features I was originally missing, as it turns out,
I just don't like blogging. It would have been nice to have that
revelation three years ago...

But, all is not lost. I learned a ton during the process. Not only with
regard to the actual process of developing an entire application from
scratch, but also how to use that application. When I started the
project I had never even heard of Docker. By the end, I was using
GitLab's CI pipeline to build Essential Blog into images hosted in a
private repository.

For now, I'm squashing the entire git history into a single commit and
archiving the respository. Who knows how many passwords or other bits
of sensitive data exist in the 1,300 commits I made over three years
when I just plain didn't know any better.

What follows is the original readme

# Essential Blog

**Essential Blog** is a simple blogging solution with all of the
features necessary to publish content without any of the unnecessary
bloat.

What features can be found in **Essential Blog**?

* Markdown editor
* Scheduled posts
* Image management
* Responsive images
* Customizable URLs
* Themes
* Multiple post/page restore points
* External service authentication
* much more...

With all of the great blog solutions that already exist, why choose
**Essential Blog**? Rather than having the largest set of features,
**Essential Blog** focuses on the following three tenants:

* **Quality** The quality of the software is more important than
  how many features it contains--no exceptions.
* **Focus** Don't be everything to everyone. Just be a blog.
* **Simplicity** Everything should be easy to use and self-explanatory.

No automatic updates. No endless libraries of plugins or themes. Just
the features necessary to publish blog content without getting the way.

## Essential Blog - Docker

Using **Essential Blog** is easiest with Docker. Use the following
`docker-compose.yml` example as a basis for your own configuration.

```yaml
version: '3'
services:
  db:
    image: mariadb
    volumes:
      - ./data/db/mysql:/var/lib/mysql
    environment:
      - MYSQL_ROOT_PASSWORD=root-password-here
      - MYSQL_DATABASE=essential-blog
      - MYSQL_USER=essential-blog
      - MYSQL_PASSWORD=user-password-here
  app:
    image: registry.gitlab.com/justinvoelker/essential-blog
    depends_on:
      - db
    ports:
      - "80:80"
    environment:
      - MYSQL_HOST=db:3306
      - MYSQL_DATABASE=essential-blog
      - MYSQL_USER=essential-blog
      - MYSQL_PASSWORD=user-password-here
      - COOKIE_VALIDATION_KEY=generate-a-long-key-here
      - ENCRYPTION_KEY=generate-another-long-key-here
      - NGINX_SET_REAL_IP_FROM=172.0.0.0/8
    volumes:
      - ./data/app/uploads:/var/www/html/web/uploads
      - ./data/app/favicons/favicon.ico:/var/www/html/web/favicon.ico
```

## Essential Blog - Manual Installation

Though installing **Essential Blog** requires a little technical knowledge,
the steps below should help anyone with web experience get started.

### Prerequisites

Before starting the install procedure, ensure the following
prerequisites are met.

* A web server is running (apache, nginx, etc.) and configured to serve
  PHP pages.
* A web accessible directory exists and is available by navigating to
  the desired domain name.
* Imagick must be installed.
* A database has been created and configured.
* Git must be installed.
* Composer must be installed with the fxp composer asset plugin:
  `composer global require "fxp/composer-asset-plugin:^1.2.0"`.

Please note that all instructions use *example.com* as a sample domain.
Replace *example.com* with an actual domain when performing all of the
following steps.

### Quick Setup

To quickly jump into using **Essential Blog**, the commands below should be
enough to get started. Be sure to read the comments contained within
the *config-sample.php* file and/or the full setup instructions below
for additional details. The `git clone` command below assumes that
*example.com* is the directory from which the website is accessible.
Once installed, the nginx config for the site should be edited to serve
the content of the `web` directory (rather than the root directory).
If using Apache, the delivered .htaccess files will perform the
necessary redirection to the `web` directory automatically.

```
cd /var/www/example.com
git clone git@gitlab.com:justinvoelker/essential-blog.git .
chmod -R g-w *
chmod g+w runtime web/assets web/uploads
cp config/config-sample.php config/config.php && vi config/config.php
composer install --no-dev
./yii migrate
```

Once the above steps are complete, navigate a web browser to
*example.com/admin/install* to complete the initial installation.

Once all installation steps are complete, review *ALL* settings within
the administrative console to ensure they are set appropriately. To
review the settings once logged in, simply navigate to
*example.com/admin* and click on *Settings*.

### Full Setup

Once the prerequisites are met, the first step in installing **Essential
Blog** is to download it. Navigate to the directory where **Essential
Blog** is to be installed, clone the project, and set permissions using
the following commands.

```
cd /var/www/example.com
git clone git@gitlab.com:justinvoelker/essential-blog.git .
chmod -R g-w *
chmod g+w runtime web/assets web/uploads
```

Now that **Essential Blog** is downloaded and permissions are set, the
*config/config.php* configuration file needs to be created and updated.
A sample of this file is provided to be used as the basis for the
actual configuration file. To create a copy of this file and then edit
it, execute the following commands from the **Essential Blog** root
directory:

```
cp config/config-sample.php config/config.php
vi config/config.php
```

For information on how each section of the file is used and the changes
necessary to each, read the comments within the file and/or the detailed
sections below.

Once configured, the installation is ready to download additional
dependencies and perform database migrations (inserting and updating the
necessary tables and records). These steps may take a few seconds so be
patient.

```
composer install --no-dev
./yii migrate
```

The final step to the installation is to navigate to
*example.com/admin/install* and specify some initial blog settings and create
the first user account. Once completed, navigate to *example.com/admin*
and log in to get started.

#### Database Configuration

Before configuring **Essential Blog** to connect to a database, one must be
created manually. It is recommended to use MySQL and to create a
database with the utf8mb4 charset (rather than simply utf8). Though a
more detailed explanation can be found
[here](https://dev.mysql.com/doc/refman/5.5/en/charset-unicode-utf8mb4.html),
the basic reason for using utf8mb4 instead of utf8 is that utf8mb4
supports 4 byte characters (such as the unicodes for emoticons) whereas
utf8 does not.

The database user should have the following privileges:

* Table: alter, create, delete, index, insert, references, select, update
* Column: select, insert, update

Once the database is created, the **Essential Blog** configuration file can
be updated. In the `config/config.php` file, update the content of the
`db` block and set the dbname, username, and password as appropriate.
All other `db` parameters can (and likely should) be left alone.

#### Mailer Configuration

By default, **Essential Blog** is configured not to send actual emails but
rather to save them as files in the `runtime/mail` directory. Though
this is acceptable for local blogs or testing, when used in production,
emails should actually be sent to the recipient. To send emails, the
`mailer` block of the `config/config.php` file should be configured to
use a mail transport. First, the `useFileTransport` should be set to
false. Second, the host, username, password, port, and ssl parameters
need to be set according to desired email provider. Two examples are
provided in the sample file for standard shared hosting and Gmail.

#### Auth Client Connection Configuration

By default, **Essential Blog** authentication is performed via username and
password. However, authentication via Google or Facebook is also
supported. To enable authentication via these services, the appropriate
applications must first be created with the respective service provider.

* Google OAuth - [https://console.developers.google.com/project](https://console.developers.google.com/project)
* Facebook OAuth - [https://developers.facebook.com/apps](https://developers.facebook.com/apps)

For Google OAuth, Google+ API must be enabled and authorized redirect
URLs must to be added (replacing example.com with appropriate domain):
* http://example.com/admin/auth-connection-login/google
* http://example.com/admin/user/auth-connection-connect/google

Once the service provider application is setup, the **Essential Blog**
configuration file can be updated. In the
`config/config.php` file, update the `authClientCollection` block by
uncommenting the appropriate service and set the `clientId` and
`clientSecret` to the values provided by the service provider.

### Upgrading

Upgrading **Essential Blog** is similar to installing but requires fewer
steps. See the `UPGRADE.md` file for specific instructions on how to
upgrade to newer versions of **Essential Blog**. In general, the steps to
upgrade will include some variation of the following:

```
cd /var/www/example.com
git pull
composer install --no-dev
./yii migrate
```

These steps will pull the updated software, install any updated or new
dependencies, and perform any database migrations. Specific upgrade
instructions may exist which means `UPGRADE.md` should be followed for
upgrading **Essential Blog**

## Theme

To get publishing quickly, a default theme is provided. If desired, this
theme may be copied and modified as needed (do not modify the delivered
theme as changes will be overwritten during upgrades). Except for the
default, delivered theme, themes are placed in the `web/themes`
directory.

## Local testing

Before installing **Essential Blog** into a production environment, local
testing may be desired. In addition to the setup steps above, edit the
`web/index.php` file and, as directed, uncomment the two lines near the
top that define YII_DEBUG and YII_ENV. Uncommenting these two lines will
place **Essential Blog** into debug and development mode which allows for
more detailed error reporting. This detailed error reporting can reveal
important information about web server configuration which is why these
two lines should never be uncommented in a production environment.

## Requirements

* exif - for image processing (e.g. exif_imagetype())

## Roles and Permissions

|                 | Contributor | Author | Editor | Admin |
| --------------- | ----------- | ------ | ------ | ----- |
| postCreate      | Own         | Own    | All    | All   |
| postUpdate      | Own         | Own    | All    | All   |
| postDelete      | Own         | Own    | All    | All   |
| postPublish     |             | Own    | All    | All   |
| pageCreate      | Own         | Own    | All    | All   |
| pageUpdate      | Own         | Own    | All    | All   |
| pageDelete      | Own         | Own    | All    | All   |
| pagePublish     |             | Own    | All    | All   |
| tagCreate       | Own         | Own    | All    | All   |
| tagUpdate       | Own         | Own    | All    | All   |
| tagDelete       | Own         | Own    | All    | All   |
| categoryCreate  |             |        | All    | All   |
| categoryUpdate  |             |        | All    | All   |
| categoryDelete  |             |        | All    | All   |
| imageCreate     | Own         | Own    | All    | All   |
| imageUpdate     | Own         | Own    | All    | All   |
| imageDelete     | Own         | Own    | All    | All   |
| injectionCreate |             |        | All    | All   |
| injectionUpdate |             |        | All    | All   |
| injectionDelete |             |        | All    | All   |
| userCreate      |             |        |        | All   |
| userUpdate      | Own         | Own    | Own    | All   |
| userDelete      | Own         | Own    | Own    | All   |
| settingUpdate   |             |        |        | All   |

## Development

The steps below will setup a container for development purposes. The
basic steps below will 1) use the *latest-dev* image and environment
variables to prepare the container for development, 2) pull the blog
repository to the host machine so it can be edited, 3) mount the cloned
repository into the container, and 4) start the container and use
composer to pull vendor dependencies.

1. Create a docker-compose.yml just as in production, but, rather than
   the *latest* tag, use the *latest-dev* tag and add YII_DEBUG=true
   and YII_ENV=dev to the environment variables.
2. From the same directory that contains the docker-compose.yml file,
   issue the following command to pull the blog repository into a new
   *html* directory `git clone git@gitlab.com:justinvoelker/essential-blog.git html`.
3. Update the docker-compose.yml file to add a volume that will mount
   the new *html* directory over the existing html directory:
   `./html:/var/www/html`
4. Start the container with `docker-compose up -d`. Once the container
   is running, run the following command to install dependencies:
   `docker exec -it [container-name] composer install`
5. Last, recreate the container `docker-compose up -d --force-recreate`
