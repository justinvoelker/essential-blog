<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class JqueryFileUploadAsset extends AssetBundle
{
    public $sourcePath = '@vendor/blueimp/jquery-file-upload';
    public $css = [
        //'css/style.css', // This only has one style, body: padding-top 60px;
    ];
    public $js = [
        'js/vendor/jquery.ui.widget.js',
        'js/jquery.fileupload.js',
        'js/jquery.iframe-transport.js',
    ];
    public $depends = [
        'app\assets\AdminAsset',
    ];
}
