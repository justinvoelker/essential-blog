<?php

namespace app\assets;

use yii\web\AssetBundle;

class FlexMenuAsset extends AssetBundle
{
    public $sourcePath = '@bower/flexmenu';
    public $css = [
    ];
    public $js = [
        'flexmenu.min.js',
    ];
    public $depends = [
    ];
}
