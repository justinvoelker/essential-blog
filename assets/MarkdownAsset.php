<?php

namespace app\assets;

use yii\web\AssetBundle;

class MarkdownAsset extends AssetBundle
{
    public $sourcePath = '@vendor/sparksuite/simplemde-markdown-editor/dist';
    public $css = [
        'simplemde.min.css',
    ];
    public $js = [
        'simplemde.min.js',
    ];
    public $depends = [
        'app\assets\AdminAsset',
    ];
}
