<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Replaces blazy.
 *  - blazy has a "container" option that only load elements visible in a given container
 *  - lazysizes does not have a container option and if the image is in the viewport (visible or not) it loads
 *
 * However, blazy is loading images from srcset based on entire viewport width for some reason and not respecting
 * "sizes." Even though lazysizes is loading a larger quantity of images, it is loading the correct sizes which is a
 * huge overall page weight reduction vs loading fewer, but larger, images.
 *
 * Lazy loaded images:
 *  - admin/views/images/index.php
 *  - admin/widgets/ImageModal.php
 */
class LazysizesAsset extends AssetBundle
{
    public $sourcePath = '@bower/lazysizes';
    public $css = [
    ];
    public $js = [
        'lazysizes.min.js',
    ];
    public $depends = [
        'app\assets\AdminAsset',
    ];
}
