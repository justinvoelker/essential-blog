<?php

namespace app\assets;

use yii\web\AssetBundle;

class GrowlAsset extends AssetBundle
{
    public $sourcePath = '@bower/growl';
    public $css = [
        /* Included in admin style sheet as vendor-growl.scss */
        //'stylesheets/jquery.growl.css',
    ];
    public $js = [
        'javascripts/jquery.growl.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
