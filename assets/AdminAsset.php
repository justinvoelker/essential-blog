<?php

namespace app\assets;

use yii\web\AssetBundle;

class AdminAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'backend/css/admin.min.css',
    ];
    public $js = [
        'backend/js/admin.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'app\assets\FlexMenuAsset',
    ];
}
