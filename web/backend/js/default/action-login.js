/* global flashAlert */

/* Declare elements used multiple times */
var $btnLogin = $('.btn-login');
var $wizardPageTwoFactor = $(".wizard-page.page-two-factor");
var $formFieldTwoFactor = $("#loginform-two_factor_code");

/* Catch form submit and perform ajax submit instead */
$('#login-form').on('beforeSubmit', function() {
    loginButtonDisable();
    $.ajax({
        url: $(this).attr('action'),
        type: 'POST',
        data: new FormData(this),
        dataType: 'json',
        processData: false,
        contentType: false,
        success: function (data) {
            /**
             * @param data Data object returned from ajax submission
             * @param data.notify Notification message
             * @param data.notifyType Notification type (success, error, etc)
             * @param data.twoFactorCodeError If two-factor code has an error
             */
            /* If not a success, reenable login button and show message (such as incorrect password). */
            if (data.notifyType !== 'success') {
                /* Regardless of cause, any non-successful return means re-enabling the login button */
                loginButtonEnable();
                /*
                 * Just because an error was returned does not mean it will be displayed. If the error is that the
                 * two-factor code is required and it has not even been shown to the user, do not display the error,
                 * just show the field. Subsequent errors for the two-factor code field will be shown, or any other
                 * non-two-factor code errors (such as username and password)
                 */
                if (data.twoFactorCodeError && !$wizardPageTwoFactor.hasClass('page-active')) {
                    /* If two-factor code has error and the two-factor code page is not shown, show it */
                    $(".wizard-page.page-active").removeClass("page-active");
                    $wizardPageTwoFactor.addClass("page-active");
                    $formFieldTwoFactor.focus();
                } else {
                    /* In every other scenario, show the alert */
                    flashAlert({type: data.notifyType, message: data.notify});
                    /* If the error is in the two-factor code, clear it */
                    if (data.twoFactorCodeError) {
                        $formFieldTwoFactor.val('');
                    }
                }
            }
        },

        /* Ajax call failed (permissions issue, etc.) */
        error: function (jqXHR) {
            /*
             * Only if a true error occurred should this logic be applied. Because the controller issues a redirect
             * on successfuly login, that actually results in a bad Ajax response and the error is caught here. In that
             * case, there is no error message.
             */
            if (jqXHR.responseText.length > 0) {
                loginButtonEnable();
                flashAlert({type: 'danger', message: jqXHR.responseText});
            }
        }

    });
    /* Prevent default form submission action */
    return false;
});

/*
 * Disable the login button by adding the disabled and spinner classes and setting the text to transparent.
 */
function loginButtonDisable() {
    $btnLogin.addClass('disabled spinner').css('color', 'transparent');
}
/*
 * Enable the login button by removing the disabled and spinner classes and removing the text transparency.
 */
function loginButtonEnable() {
    $btnLogin.removeClass('disabled spinner').css('color', '');
}
