/* global flashAlert */

/*
 * On click of element with data-action attribute set to two-factor-enable, trigger ajax.
 * Using "on" function and click of selector so buttons can still be clicked after pjax container reloads.
 */
$(document).on('click', '[data-action="two-factor-enable"]', function () {
    /* Disable the button so it cannot be clicked again */
    $(this).addClass('disabled');
    /* Submit Ajax request */
    $.ajax({
        url: '/admin/user/' + $(this).attr('data-action'),
        type: 'POST',
        data: { two_factor_secret: $('[name=two_factor_secret]').val(), two_factor_code: $('[name=two_factor_code]').val() },
        dataType: 'json',
        success: function (data) {
            /**
             * @param data Data object returned from ajax submission
             * @param data.twoFactorBackupCodes Two-factor authentication backup codes
             * @param data.notify Notification message
             * @param data.notifyType Notification type (success, error, etc)
             */
            /* Display flash message */
            flashAlert({type: data.notifyType, message: data.notify});

            /* If success, show backup codes */
            if (data.notifyType === 'success') {
                /* Display backup codes */
                var backupCodes = JSON.parse(data.twoFactorBackupCodes);
                $('.two-factor-setup__backup-codes').html(backupCodes.join('<br>'));
                /* Hide initial block of setup instructions */
                $('.two-factor-setup__initial-block').addClass('hidden');
                /* Show final section of setup instuctions with backup codes */
                $('.two-factor-setup__backup-codes-block').removeClass('hidden');

            }
        },

        /* Ajax call failed (permissions issue, etc.) */
        error: function (jqXHR) {
            flashAlert({type: 'danger', message: jqXHR.responseText});
        },

        /*
         * Regardless of ajax result, re-enable verify button
         */
        complete: function () {
            $('[data-action="two-factor-enable"]').removeClass('disabled');
        }

    });
});
