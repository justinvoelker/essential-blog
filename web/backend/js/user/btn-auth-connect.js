/**
 * Disconnect an auth client via Ajax call.
 */

/**
 * Functions declared elsewhere:
 * flashAlert, set in widgets/FlashAlert
 */
/* global flashAlert */

/*
 * On click of element with data-action attribute set to auth-client-disconnect, trigger ajax.
 * Using "on" function and click of selector so buttons can still be clicked after pjax container reloads.
 */
$(document).on('click', '.auth-link', function () {

    /* Disable the button to prevent multiple submissions. Pjax container will be reloaded after ajax call. */
    $(this).addClass('disabled');

    /* Keep track of loop count to prevent looping forever */
    var completedLoops = 0;

    /* Loop through ajax call until a notofication is returned. Once returned, display that notification. */
    var ajaxloop = setInterval(function(){

        /* Quick note that indicates the ajax call is again being executed */
        console.log('Call auth-connection-refresh');

        /* Ajax call to refresh action */
        return $.ajax({
            url: '/admin/user/auth-connection-refresh',
            type: 'POST',

            /* Ajax call was successful */
            success: function (data) {
                /**
                 * @param data Data object returned from ajax submission
                 * @param data.notify Notification message
                 * @param data.notifyType Notification type (success, error, etc)
                 * @param data.notifyFound Whether or not notification was found
                 */

                /* If a notification was found, the connection process has completed. */
                if (data.notifyFound) {
                    /* Stop the loop */
                    clearInterval(ajaxloop);
                    /* Display the returned notification */
                    flashAlert({type: data.notifyType, message: data.notify});
                    /* Refresh the pjax container to either re-enable disabled buttons or update auth client status */
                    $.pjax.reload({container:'#user-auth-client-connections'});
                }
            },

            /* Ajax call failed (permissions issue, etc.) */
            error: function (jqXHR) {
                /* Stop the loop */
                clearInterval(ajaxloop);
                /* Display the failure error */
                flashAlert({type: 'danger', message: jqXHR.responseText});
                /* Refresh the pjax container to re-enable any disabled buttons */
                $.pjax.reload({container:'#user-auth-client-connections'});
            },

            complete: function () {
                completedLoops++;
                if (completedLoops > 60) {
                    /* Looping for too long, stop the loop */
                    console.log('End loop due to high loop counter.');
                    /* Stop the loop */
                    clearInterval(ajaxloop);
                }
            }
        });
    }, 1000);
});
