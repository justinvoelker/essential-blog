/**
 * Disconnect an auth client via Ajax call.
 */

/**
 * Functions declared elsewhere:
 * flashAlert, set in widgets/FlashAlert
 */
/* global flashAlert */

/*
 * On click of element with data-action attribute set to auth-client-disconnect, trigger ajax.
 * Using "on" function and click of selector so buttons can still be clicked after pjax container reloads.
 */
$(document).on('click', '[data-action="auth-client-disconnect"]', function () {

    /* Disable the button to prevent multiple submissions. Pjax container will be reloaded after ajax call. */
    $('[data-action="auth-client-disconnect"]').addClass('disabled');

    /* Ajax call to disconnect action */
    return $.ajax({
        url: '/admin/user/auth-connection-disconnect?authclient=' + $(this).data('authclient'),
        type: 'POST',

        /* Ajax call was successful */
        success: function (data) {
            /**
             * @param data Data object returned from ajax submission
             * @param data.notify Notification message
             * @param data.notifyType Notification type (success, error, etc)
             */

            /* Display returned notification */
            flashAlert({type: data.notifyType, message: data.notify});
        },

        /* Ajax call failed (permissions issue, etc.) */
        error: function (jqXHR) {
            flashAlert({type: 'danger', message: jqXHR.responseText});
        },

        /* Regardless of ajax result, reload pjax container. Either ajax was a success and the button may now need to be
         * changed or it was disabled and simply needs to be re-enabled.
         *
         */
        complete: function () {
            $.pjax.reload({container:'#user-auth-client-connections'});
        }
    });
});
