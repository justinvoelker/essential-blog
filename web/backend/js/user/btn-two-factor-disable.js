/* global flashAlert */

/*
 * On click of element with data-action attribute set to two-factor-disable, trigger ajax.
 * Using "on" function and click of selector so buttons can still be clicked after pjax container reloads.
 */
$(document).on('click', '[data-action="two-factor-disable"]', function () {
    /* Disable the button so it cannot be clicked again */
    $(this).addClass('disabled');
    /* Submit Ajax request */
    $.ajax({
        url: '/admin/user/' + $(this).attr('data-action'),
        type: 'POST',
        success: function (data) {
            /**
             * @param data Data object returned from ajax submission
             * @param data.notify Notification message
             * @param data.notifyType Notification type (success, error, etc)
             */
            /* Display flash message */
            flashAlert({type: data.notifyType, message: data.notify});
        },

        /* Ajax call failed (permissions issue, etc.) */
        error: function (jqXHR) {
            flashAlert({type: 'danger', message: jqXHR.responseText});
        },

        /*
         * Regardless of ajax result, reload pjax container. Either ajax was a success and the button may now need to be
         * changed or it was disabled and simply needs to be re-enabled.
         */
        complete: function () {
            $.pjax.reload({container:'#user-two-factor-authentication'});
        }

    });
});
