/* global flashAlert */

/* Actions on modal shown and hidden */
$('#user-invite-modal').on('modalShown', function() {
    /* When the modal is opened, send focus to the email field */
    $('#user-email').focus();
}).on('modalHidden', function() {
    /* When the modal is closed, reset the form (clear email and validation errors */
    $('#user-invite-form').trigger('reset');
});

// Catch form submit and perform ajax submit instead
$('#user-invite-form').on('beforeSubmit', function() {
    $.ajax({
        url: $(this).attr('action'),
        type: 'POST',
        data: new FormData(this),
        dataType: 'json',
        processData: false,
        contentType: false,
        success: function (data) {
            /**
             * @param data Data object returned from ajax submission
             * @param data.successUser Boolean, whether or not user was successfully saved
             * @param data.successEmail Boolean, whether or not email was successfully sent
             * @param data.message String, message to be displayed
             */
            if (data.successUser) {
                // Reload the user list and hide the modal window
                $.pjax.reload({container:'#user-grid'});
                // Close the modal
                $('#user-invite-modal').trigger('modalHide');
                if (data.successEmail) {
                    flashAlert({type: "success", message: data.message});
                } else {
                    flashAlert({type: "warning", message: data.message});
                }
            } else {
                $('.error-summary-custom').empty().append("<p>" + data.message  + "</p>").show();
            }
        }
    });
    // Prevent default form submission action
    return false;
});
