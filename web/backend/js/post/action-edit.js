/**
 * SECTION 1: GLOBAL
 *
 * Anything needed to make the following sections function
 */

/**
 * The following variables are set globally, outside this file. Included here for editor error suppression. This
 * comment only contains details about where the global variables are set. The following individual comments are
 * the actual declarations of those variables (they cannot contain additional text which is why this is here).
 * autosave_seconds Set in post/_form.php
 * post_is_locked_const Set in post/_form.php
 */
/* global autosave_seconds, post_is_locked_const */
var autosave_microseconds = autosave_seconds * 1000;
var postIsLockedConst = post_is_locked_const;

/**
 * Functions declared elsewhere:
 * updateActionBarButtonText, set in widgets/ActionBar
 * updateActionBarButtonState, set in widgets/ActionBar
 */
/* global updateActionBarButtonText, updateActionBarButtonState */

/**
 * Additional variables to be used throughout the JavaScript
 */
/* Timeout used to trigger saving after given amount of time has passed. */
var timeoutId;
/* SimpleMDE instance. Set in page ready. */
var simplemde;
/* CodeMirror instance. Set after SimpleMDE is initialized to reference CodeMirror well after SimpleMDE initialization */
var cm;
/* Type of save that trigger the form submit (autosave, manual save, ctrl+s save, etc. */
var submitType;

/**
 * jQuery selectors for commonly used items
 */
/* Standard input fields */
var $postTitle = $('#post-title');
var $postContent = $('#post-content'); /* May not be used. Replaced with SimpleMDE */
//var $postTags = $('#post-tags');
//var $postCategoryId = $('#post-category_id');
var $postImage = $('#post-image');
var $postExcerpt = $('#post-excerpt');
var $postSlug = $('#post-slug');
var $postScheduledFor = $('#post-scheduled_for');
var $postIsLocked = $('#post-is_locked');
/* Customized items used in addition to standard input fields */
var $fieldPostSlug = $(".field-post-slug");
var $imageModal = $(".image-modal");
var $imageModalTriggerField = $("#image-modal-trigger-field");
var $linkModal = $(".link-modal");
var $postImageButtonClear = $("#post-image-clear");
//var $postScheduledForInputGroup = $('.field-post-scheduled_for .input-group');
var $postScheduledForButtonClear = $('.field-post-scheduled_for .btn-clear-input');
var $btnSave = $('[data-submit-type="save"]');
var $btnSaveRestorePoint = $('[data-submit-type="saverestorepoint"]');
var $btnPublishLocator = '[data-action="publish"]'; /* Used to update action bar button text/state */
var $btnPublish = $('[data-action="publish"]'); /* Used to set data-id after save */
var $btnUnpublishLocator = '[data-action="unpublish"]'; /* Used to update action bar button text/state */
var $btnUnpublish = $('[data-action="unpublish"]'); /* Used to set data-id after save */


/**
 * SECTION 2: FIELDS
 *
 * Logic triggered via actions to individual form fields.
 *
 * For title, excerpt, and slug, keydown is used (rather than the desirable keyup) because when focus is inside the
 * field, ctrl+s would not work. Although ctrl+s sets submitType to 'save' (manual save) the subsequent release of
 * ctrl+s keys triggered the keyup which changed submitType back to 'auto'. And if, for instance, autosave was disabled,
 * the form would now be submitted with 'auto' even though it was a manual ctrl-s save, and the autosave disabled check
 * would prevent the form from saving because autosave is disabled and now the submitType is auto. Keydown is fine
 * unless someone is holding down a key. In that case the autosave may save a post that is being written to from a
 * long-held keydown.
 */

/**
 * Title
 * Autosave timer can be triggered by keydown, but slug generation needs to happen on key up, after all title is known.
 */
$postTitle.keydown(function (event) {
    if (!event.ctrlKey && !event.metaKey && !event.shiftKey) {
        resetAutosaveTimer('keydown on #post-title');
    }
});
$postTitle.keyup(function () {
    generateOrValidateSlug('title');
});

/**
 * Content
 */
$postContent.change(function () {
    // This change event is actually triggered via the cm.change event
    resetAutosaveTimer('change of #post-content');
});

/**
 * Tags
 * Since tags are within a Pjax container that will be refreshed after new records are created, need to bind the
 * 'on' event to the Pjax container and set the actual field as the selector. This allows the select input to still
 * listen for change events after the Pjax container is refreshed.
 */
$('#create-tag-refresh').on('change', '#post-tags', function () {
    resetAutosaveTimer('change of #post-tags');
});

/**
 * Category
 * Since category is within a Pjax container that will be refreshed after new records are created, need to bind the
 * 'on' event to the Pjax container and set the actual field as the selector. This allows the select input to still
 * listen for change events after the Pjax container is refreshed.
 */
$('#create-category-refresh').on('change', '#post-category_id', function () {
    resetAutosaveTimer('change of #post-category_id');
});

/**
 * Image
 */
$postImage.change(function () {
    /* Change event triggered by click of .image-modal .modal-accept and click of #post-image-clear */
    resetAutosaveTimer('change of #post-image');
});
$postImageButtonClear.click(function () {
    /* Trigger change event of the post-image field */
    $postImage.change();
});

/**
 * Excerpt
 */
$postExcerpt.keydown(function (event) {
    if (!event.ctrlKey && !event.metaKey && !event.shiftKey) {
        resetAutosaveTimer('keydown on #post-excerpt');
    }
});

/**
 * Slug
 */
$postSlug.keydown(function (event) {
    if (!event.ctrlKey && !event.metaKey && !event.shiftKey) {
        resetAutosaveTimer('keydown on #post-slug');
    }
});
$postSlug.keyup(function() {
    generateOrValidateSlug('slug');
});

/**
 * Scheduled At
 */
$postScheduledFor.change(function () {
    /* Change event is triggered by manual edit, closing datetimepicker widget, or clicking the clear button. */
    resetAutosaveTimer('change of #post-scheduled_for');
    generateOrValidateSlug('scheduled_for');
});



/**
 * SECTION 3.1: EVENTS - FIELD RELATED
 *
 * Events for various form fields
 */

/**
 * Image Modal (from SimpleMDE editor or post image)
 */
/* When the image modal is closed via accept */
$imageModal.find(".modal-accept").click(function () {
    /* Different action depending on what triggered the modal to open */
    if ($imageModalTriggerField.val() === "post-content") {
        /* Triggered from post-content - insert the selected image into the post-content */
        consoleLog('Image modal (triggered from post-content) accepted. Inserting image.');
        var doc = cm.getDoc();
        var cursor = doc.getCursor();
        var imgAlternativeText = $("#image-modal-selected-image-alternative_text").val();
        /* Encoded to protect against spaces (encodeURL does not encode '/' like encodeURIComponent does) */
        var imgPath = encodeURI($("#image-modal-selected-image-path").val());
        doc.replaceRange("![" + imgAlternativeText + "](" + imgPath + ")", cursor);
    } else if ($imageModalTriggerField.val() === "post-image") {
        /* Trigger change event of the post-image field */
        $postImage.change();
    }
});
/* When the image modal is closed via cancel, close the modal */
$('#image-modal').on('modalHidden', function() {
    /* Different action depending on what triggered the modal to open */
    if ($("#image-modal-trigger-field").val() === "post-content") {
        /* No need to resetAutosaveTimer() because it will be triggered from the SimpleMDE cm.change event */
        consoleLog('Image modal (triggered from post-content) closed. Sending focus to editor.');
        /* Triggered from post-content - send focus back there */
        cm.focus();
    }
});

/**
 * Image Modal (from SimpleMDE editor only - trigger field checks are just for consistency)
 */
/* When the link modal is closed via accept, insert the link */
$linkModal.find(".modal-accept").click(function () {
    /* Different action depending on what triggered the modal to open */
    if ($("#link-modal-trigger-field").val() === "post-content") {
        /* Triggered from post-content - insert the selected link into the post-content */
        consoleLog('Link modal (triggered from post-content) accepted. Inserting link.');
        var doc = cm.getDoc();
        var cursor = doc.getCursor();
        var linkTitle = $("#link-modal-link-title").val();
        /* Encoded to protect against spaces (encodeURL does not encode '/' like encodeURIComponent does) */
        var linkUrl = encodeURI($("#link-modal-link-url").val());
        doc.replaceRange("[" + linkTitle + "](" + linkUrl + ")", cursor);
    }
});
/* When the image modal is closed via cancel, close the modal */
$('#link-modal').on('modalHidden', function() {
    /* Different action depending on what triggered the modal to open */
    if ($("#link-modal-trigger-field").val() === "post-content") {
        /* No need to resetAutosaveTimer() because it will be triggered from the SimpleMDE cm.change event */
        consoleLog('Link modal (triggered from post-content) closed. Sending focus to editor.');
        /* Triggered from post-content - send focus back there */
        cm.focus();
    }
});

/**
 * Scheduled At handling clear button and clicking outside datetime widget
 */
/* On click of scheduled_for clear button ... */
$postScheduledForButtonClear.click(function() {
    /* ... clear the DateTimePicker ... */
    /* CALWIDGET Uncomment following line (and probably update it) when a calendar widget is added */
    // $postScheduledForInputGroup.data("DateTimePicker").clear();
    /* ... and trigger the scheduled_for change event */
    $postScheduledFor.change();
});
/* Clicked outside the datetimepicker, hide it */
$(document).on('mousedown', function(e) {
    /* Hide picker when clicking outside (https://github.com/Eonasdan/bootstrap-datetimepicker/pull/1615) */
    /* CALWIDGET Uncomment following block (and probably update it) when a calendar widget is added */
    // if ($(e.target).closest('.datetimepicker').length === 0) {
    //     $postScheduledForInputGroup.data("DateTimePicker").hide();
    // }
});



/**
 * SECTION 3.2: EVENTS - GENERAL
 *
 * Events not related to form fields
 */

/*
 * Capture the standard form submit and instead call savePost which will submit the form data via ajax
 */
$('form#post-form').on('beforeSubmit', function () {
    consoleLog('Form beforeSubmit captured.');
    /* Save the post */
    savePost();
    /* Prevent default form submission */
    return false;
});

/*
 * Handling the Save and Save Restore Point buttons
 */
/* When a link/button with submit-type data attribute is clicked, copy the submitType to the form field and submit the form. */
$('[data-submit-type]').click(function () {
    submitType = $(this).data('submit-type');
    consoleLog('Action bar button clicked: ' + submitType);
    /* Submit the form (will be caught by beforeSubmit) */
    $('form#post-form').submit();
});

/*
 * Capture ctrl+s as save
 */
$(window).bind('keydown', function(event) {
    if (event.ctrlKey || event.metaKey) {
        switch (String.fromCharCode(event.which).toLowerCase()) {
            case 's':
                event.preventDefault();
                consoleLog('Keydown combination: ctrl+s');
                if (!$('[data-submit-type=save]').hasClass('disabled')) {
                    submitType = 'save';
                    $('form#post-form').submit();
                } else {
                    consoleLog('Save button is disabled');
                }
                break;
        }
    }
});



/**
 * SECTION 4.1: FUNCTIONS - GENERAL
 *
 * General functions triggered by actions performed on individual fields
 */

function consoleLog(msg) {
    console.log(msg);
}

/**
 * Saving restore points can be enabled or disabled as needed. For example, after a save restore point, disable save restore point to
 * protect against a second, duplicate restore point. After any field changes, re-enable save restore point.
 * Values will be set as follows:
 *  - On page load: TRUE/FALSE. If most recent child is locked FALSE. Else TRUE.
 *  - On any field edit: TRUE. All edits enable all types of save (from resetAutosaveTimer()).
 *  - On form submit - auto: TRUE. Automatic save, restore point can still be saved.
 *  - On form submit - save: TRUE. Manual save, restore point can still be saved.
 *  - On form submit - saverestorepoint: FALSE. Manual restore point save prohibits another restore point save.
 *  - On form submit - ajax error: TRUE. A failed save allows another attempt at saving.
 * @param state
 */
function btnSaveRestorePointEnableDisable(state) {
    $btnSaveRestorePoint.toggleClass('disabled', !state);
}

/**
 * Manual and autosaving can be permitted/prohibited as needed. For example, after a manual save,
 * disable autosave so an autosave timer does not trigger another save.
 * Values will be set as follows:
 *  - On page load: FALSE. Page was just opened so nothing has changed to be saved.
 *  - On any field edit: TRUE. All edits enable all types of save (from resetAutosaveTimer()).
 *  - On form submit - any type: FALSE. Any save prohibits another save.
 *  - On form submit - ajax error: TRUE. A failed save allows another attempt at saving.
 * @param state
 */
function btnSaveEnableDisable(state) {
    $btnSave.toggleClass('disabled', !state);
}

/**
 * Reset the autosave timeout timer, triggering an autosave after the time expired
 * @param trigger Message about what triggered the call to this function
 */
function resetAutosaveTimer(trigger) {
    consoleLog('Autosave timer reset. Triggered via ' + trigger);
    /* Ensure save is enabled */
    btnSaveEnableDisable(true, trigger);
    /* Ensure saverestorepoint is enabled */
    btnSaveRestorePointEnableDisable(true, trigger);
    /* Disable publish and unpublish (will potentially be be enabled after save) */
    updateActionBarButtonState($btnPublishLocator, false);
    updateActionBarButtonState($btnUnpublishLocator, false);
    /* Clear current timeout */
    window.clearTimeout(timeoutId);
    /* Set a new timeout */
    timeoutId = window.setTimeout(function () {
        consoleLog('Autosave timer expired. Post save triggered.');
        submitType = 'auto';
        savePost();
    }, autosave_microseconds);
}

/**
 * Update the actual slug field and the preview containing the full path (including slug)
 */
function updateSlugValue(slug) {
    /* Update the actual slug input with the new slug (may not have changed depending on controller logic) */
    $('#post-slug').val(slug);
}
function updateSlugPreview(preview) {
    /* Update the url preview */
    $('.field-post-slug .hint-block .preview').html(preview);
}



/**
 * SECTION 4.2: FUNCTIONS - SPECIFIC
 *
 * Specific functions used by only a limited set of calling
 */

/**
 * Generate a unique post slug for the given title and scheduled_for (if present). Or, if triggered from the slug field,
 * validate the specified slug. The post ID is needed so that if this is a post being updated, the
 * slug generated is allowed to be slug that it is already assigned. Otherwise, generating a the post slug would
 * conflict with itself. A new slug is only returned if post is in draft status.
 * @param trigger
 */
function generateOrValidateSlug(trigger) {
    consoleLog('generateOrValidateSlug. Triggered via ' + trigger);
    return $.ajax({
        url: '/admin/post/generate-or-validate-slug?primary_id=' + $('#post-primary_id').val() + '&trigger=' + trigger,
        type: 'POST',
        data: $('form#post-form').serialize(),
        /**
         * @param data Data object returned from ajax submission
         * @param data.notify Notification message
         * @param data.notifyType Notification type (success, error, etc)
         * @param data.slug Post slug (without other URL parameters)
         * @param data.url Entire post URL path (including slug)
         */
        success: function (data) {
            /* If triggered via anything other than the slug field, update the value */
            if (trigger !== 'slug') {
                updateSlugValue(data.slug);
            }
            /* Regardless of the trigger, the preview should be updated */
            updateSlugPreview(data.url);
            /* Only display non-success return notifications */
            if (data.notifyType !== 'success') {
                //noinspection JSUnresolvedFunction
                flashAlert({type: data.notifyType, message: data.notify});
            } else {
                /* As a success, no alert is show, but, the result can at least be logged in the console */
                consoleLog(data.notify);
            }
        }
    });
}

/**
 * Save the post to the database (if there are no form errors). After form is saved via ajax, a lot happens:
 * 1) The form action is returned/update. Next save will post to the proper update action.
 * 2) The url is updated via push state. A refreshed page will now appear to be on the update page.
 * 3) The post id is updated. Ensures subsequent submissions are aware a primary exists and so slug won't conflict with its own primary post.
 * $) Save notifications are returned/displayed. Indicates success or error during save.
 * @returns {boolean}
 */
function savePost() {
    consoleLog('Inside savePost() with submitType of ' + submitType);

    /* Only continue if NOT an autosave or IS an autosave and the auto save seconds are not 0 */
    if (submitType !== 'auto' || (submitType === 'auto' && autosave_seconds !== '0')) {

        /* If auto save and the record has not yet been (manually) saved, return message and exit */
        if ((submitType === 'auto') && $('#post-primary_id').val().length === 0) {
            consoleLog('submitType is "'+submitType+'" but post has not yet been manually saved. Exiting savePost().');
            //noinspection JSUnresolvedFunction
            flashAlert({type: 'warning', message: 'Autosave temporarily disable. Manually save the post to enable autosaving.'});
            return false;
        }

        /* Do nothing if this is a manual or auto save and save is disabled */
        if ((submitType === 'save' || submitType === 'auto') && $btnSave.hasClass('disabled')) {
            consoleLog('submitType is "'+submitType+'" but save is disabled. Exiting savePost().');
            return false;
        }

        /* Do nothing if this is a save restore point and save restore point is disabled */
        if (submitType === 'saverestorepoint' && $btnSaveRestorePoint.hasClass('disabled')) {
            consoleLog('submitType is "'+submitType+'" but save restore point is disabled. Exiting savePost().');
            return false;
        }

        /* Do nothing and return false if the form still has validation errors */
        if ($("form#post-form").find(".has-error").length) {
            consoleLog('Form contains errors. Exiting savePost().');
            return false;
        }

        /* If submitType is saverestorepoint, set "is_locked" field to lock this restore point when it is saved. */
        if (submitType === 'saverestorepoint') {
            consoleLog('SubmitType is saverestorepoint. Setting is_locked to "true".');
            $postIsLocked.val(postIsLockedConst);
        }

        // Submit the ajax request to save the post
        var form = $('form#post-form');
        $.ajax({
            url: form.attr('action'),
            type: 'POST',
            data: form.serialize(),
            success: function (data) {
                /**
                 * @param data Data object returned from ajax submission
                 * @param data.notify Notification message
                 * @param data.notifyType Notification type (success, error, etc)
                 * @param data.buttons Button text and enabled/disabled state for all of the action bar buttons
                 * @param data.primary_id ID of primary post
                 * @param data.action Form action to be set
                 * @param data.slug Post slug
                 * @param data.url Entire post URL path (which includes additional URL parameters)
                 * @param data.urlPreview Text URL of the post preview link
                 */

                /* Display returned notification */
                //noinspection JSUnresolvedFunction
                flashAlert({type: data.notifyType, message: data.notify});

                /* Update text and enabled/disabled state of action bar buttons */
                var buttons = data.buttons;
                for (var button in buttons) {
                    if (buttons.hasOwnProperty(button)) {
                        updateActionBarButtonText(buttons[button].locator, buttons[button].text);
                        updateActionBarButtonState(buttons[button].locator, buttons[button].enabled);
                    }
                }
                /* Update the data-id for publish and ubpublish buttons */
                $btnPublish.attr('data-id', data.primary_id);
                $btnUnpublish.attr('data-id', data.primary_id);

                /*
                 * Update the primary page ID attribute so it can be used during future form submissions.
                 * First ensure primary_id was returned, since, if there was a validaton error it will not be returned
                 * and without this check it would set the primary_id to empty which would cause the next save to not
                 * include a primary_id which is very bad.
                 */
                if (data.primary_id) {
                    $('#post-primary_id').val(data.primary_id);
                }

                /**
                 * The returned form action will change when a new post becomes an update post. Note, if there was a
                 * validation error, the child will not be saved and the action returned will be the same as a new post.
                 * This logic should only ever be called once (when changing from new to update page)
                 */
                if (form.attr('action') !== data.action) {

                    /* Update the form action with the new action (only ever changes from create to update) */
                    form.attr('action', data.action);

                    /**
                     * Update the current URL via pushState. This is used when a new post is saved for the first time
                     * to update the url to be that of the update page that includes the post id
                     */
                    var obj = {ID: data.primary_id, Title: 'Update', Url: data.action};
                    history.pushState(obj, obj.Title, obj.Url);

                    /* Update the preview post link and remove disabled class */
                    $('a.post-preview-link').prop('href', data.urlPreview).removeClass('disabled');
                }

                /* Update the slug and url. Any time the post is saved, this has the potential to be different. */
                updateSlugValue(data.slug);
                updateSlugPreview(data.url);

                /* Update header to reflect that this is no longer a new post but is now and update */
                $.pjax.reload({container:'#header__body'});
            },
            error: function (jqXHR) {
                //noinspection JSUnresolvedFunction
                flashAlert({type: 'danger', message: jqXHR.responseText});
                /* Log to console */
                consoleLog('savePost() ajax resulted in an error.');
                /* Since something must have gone wrong, ensure all saving is enabled */
                btnSaveEnableDisable(true, 'Ajax error in savePost()');
                btnSaveRestorePointEnableDisable(true, 'Ajax error in savePost()');
            },
            complete: function () {
                /*
                 * Regardless of success of failure, always set "is_locked" back to empty so that subsequent form
                 * submits are not locked unless a restore point is being saved.
                 */
                $postIsLocked.val('');
            }
        });
    } else {
        consoleLog('Autosave is disabled.');
    }
}



/**
 * SECTION 5: PAGE READY
 *
 * Everything that needs to happen when the page is ready
 */
$(function () {
    /**
     * SimpleMDE - post-content
     */
    simplemde = new SimpleMDE({
        element: document.getElementById("post-content"),
        autoDownloadFontAwesome: false,
        forceSync: true,
        indentWithTabs: false,
        status: false,
        toolbar: [
            'bold', 'italic', 'heading', '|', 'quote', 'unordered-list', 'ordered-list', '|',
            {
                name: "link",
                action: function drawLinkCustom(){
                    consoleLog('Link modal opened from post-content.');
                    $("#link-modal-trigger-field").val('post-content');
                    $('#link-modal').trigger('modalShow'); // Display modal for link insertion
                },
                className: "fa fa-link",
                title: "Insert Link"
            },
            {
                name: "image",
                action: function drawImageCustom(){
                    consoleLog('Image modal opened from post-content.');
                    $("#image-modal-trigger-field").val('post-content');
                    $('#image-modal').trigger('modalShow'); // Display modal for image selection
                },
                className: "fa fa-picture-o",
                title: "Insert Image"
            },
            '|', 'preview', 'side-by-side', 'fullscreen',
            {
                name: "markdown-help",
                action: function showMarkdownGuide(){
                    consoleLog('Markdown Help modal opened from post-content.');
                    $('#markdown-help-modal').trigger('modalShow'); // Display modal for markdown guide
                },
                className: "fa fa-question-circle",
                title: "Markdown Help"
            }
        ]

    });
    // CodeMirror instance for later use in setting content or focus
    cm = simplemde.codemirror;
    // On change of SimpleMDE, trigger change event of post-content
    cm.on("change", function () {
        $postContent.change();
    });

    /**
     * DateTimePicker - post-scheduled_for
     */
    /* CALWIDGET Uncomment following line (and probably update it) when a calendar widget is added */
    /*$postScheduledForInputGroup.datetimepicker({
        format: 'YYYY-MM-DD HH:mm',
        focusOnShow: false,
        showClose: true, // Required for this: https://github.com/Eonasdan/bootstrap-datetimepicker/pull/1615
        icons: {
            time: 'btn-toggle-time',
            date: 'btn-toggle-date',
            up: 'fa fa-chevron-up',
            down: 'fa fa-chevron-down',
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            close: 'btn-calendar-close'
        },
        useCurrent: false // Used in conjunction with the dp.show below
    }).on('dp.show', function() {
        // Since a date shows up instantly upon opening, show the clear icon
        $postScheduledForButtonClear.removeClass('hidden');
        // Fix for time always starting at 12:00 AM ( https://github.com/Eonasdan/bootstrap-datetimepicker/issues/1311)
        if($(this).data("DateTimePicker").date() === null) {
            // This likely needs moment javascript library
            $(this).data("DateTimePicker").date(moment());
        }
    }).on('dp.change', function(e) {
        // Re-evaluate the visibility of the clear icon
        $postScheduledForButtonClear.toggleClass('hidden', e.date == false);
    }).on('dp.hide', function() {
        // Since dp.change triggers on every date selection within the widget and we only want the final date selected,
        // the scheduled_for change event is only fired when the widget is closed.
        $postScheduledFor.change();
    });*/
});
