/**
 * Publish a single post via Ajax call.
 */

/**
 * Functions declared elsewhere:
 * updateActionBarButtonText, set in widgets/ActionBar
 * updateActionBarButtonState, set in widgets/ActionBar
 * updateActionBarButtonState, set in widgets/FlashAlert
 */
/* global updateActionBarButtonText, updateActionBarButtonState, flashAlert */

/* Define publish button */
var $btnPublish = $('[data-action="publish"]');

/* On click of element with data-action attribute set to publish, publish the post */
$btnPublish.click(function () {

    /* Disable the button to prevent multiple submissions. Will be re-enabled later if publish failed. */
    $btnPublish.addClass('disabled');

    /* Ajax call to publish action */
    return $.ajax({
        url: '/admin/post/publish?id=' + $(this).data('id'),
        type: 'POST',

        /* Ajax call was successful */
        success: function (data) {
            /**
             * @param data Data object returned from ajax submission
             * @param data.notify Notification message
             * @param data.notifyType Notification type (success, error, etc)
             * @param data.buttons Button text and enabled/disabled state for all of the action bar buttons
             */

            /* Display returned notification */
            flashAlert({type: data.notifyType, message: data.notify});

            /* Update text and enabled/disabled state of action bar buttons */
            var buttons = data.buttons;
            for (var button in buttons) {
                if (buttons.hasOwnProperty(button)) {
                    updateActionBarButtonText(buttons[button].locator, buttons[button].text);
                    updateActionBarButtonState(buttons[button].locator, buttons[button].enabled);
                }
            }
        },

        /* Ajax call failed (permissions issue, etc.) */
        error: function (jqXHR) {
            flashAlert({type: 'danger', message: jqXHR.responseText});
            /* Re-enable publish button since something failed */
            $btnPublish.removeClass('disabled');
        }

    });

});
