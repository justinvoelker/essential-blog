/*
 * Functions and actions used throughout the admin interface.
 */

/* This same transition duration is set in scss/variables.scss */
transitionduration = 250;

/*
 * Global functions that exist in other files and are referenced below. This prevents IDE errors.
 */
/* global setActionBarActiveGroup */


$(document).ready(function () {

    /*
     * This class exists on page render but is removed via admin.js on page ready. Hiding overflow prevents buttons from
     * wrapping and appearing outside/below the action bar before Flexmenu kicks in. The class is removed because with
     * overflow hidden, the Flexmenu menu will not be shown (as it drops down outside of the action bar).
     */
    $('.action-bar.action-bar_overflow-hidden-on-load').removeClass('action-bar_overflow-hidden-on-load');

    /*
     * Sidebar
     */

    /* Toggle visibility of sidebar child menus */
    $('.sidebar__list-item_expandable').click(function () {
        /* SlideToggle the child list */
        $(this).find("ul.sidebar__list_child").slideToggle(250);
        /* Add expanded class to the clicked item */
        $(this).toggleClass('sidebar__list-item_expanded');
        /* SlideUp and child list that is not the child of the clicked item */
        $('.sidebar .sidebar__list-item_expandable ul.sidebar__list_child').not($(this).find("ul.sidebar__list_child")).slideUp(250);
        /* Remove expanded class from every item that is not this item */
        $('.sidebar .sidebar__list-item_expandable').not(this).removeClass('sidebar__list-item_expanded');
    });

    /* When a child list is clicked, stop propagation from clicking (and thus collapsing) the parent. Purely aesthetic. */
    $(".sidebar__list-item_expandable .sidebar__list_child").click(function (e) {
        var target = $( e.target );
        /* Cannot stop propagation on the logout link because then the post request isn't sent, it's just a get request. */
        if ( !target.hasClass("logout-link") ) {
            e.stopPropagation();
        }
    });

    /*
     * CSS already controls showing of expanded blocks when the page loads (children NOT under expanded parent get
     * "display none." Without that CSS, we would use .show() here. But, doing that means all children are hidden on
     * page load, then the expanded child is slid into view. That's gross when moving between pages within the same
     * child menu. Instead, CSS ensures all expanded children are shown on page load. Then, adding the "display block"
     * here ensures that if a different parent menu is selected, the already-open menu can slide closed, just as if it
     * were originally shown with a show() call on page load. Without adding this "display block" here, clicking a
     * different parent menu simply hid the open menu and slid the new child into view which is not pretty.
     */
    $('.sidebar__list-item_expanded').find("ul.sidebar__list_child").css('display', 'block');

    /*
     * Action bar, Flexmenu
     */

    /* Using Flexmenu, hide overflow items in action bar under a "more" button */
    /* If the list is empty (for example, no actions were authorized) don't want "More" to appear for nothing. */
    $('.action-bar ul').each(function(index, element) {
        if ($.fn.flexMenu && $(this).find('li').length > 0) {
            $(this).flexMenu({
                threshold: 1,
                cutoff: 0,
                linkText: '<i class="fa fa-fw fa-ellipsis-h" aria-hidden="true"></i><span>More</span>'
            });
        }
    });

    /* Using Flexmenu, hide the overflow "more" menu when clicked outside menu or item is clicked inside menu */
    /* http://stackoverflow.com/a/3028037/3344794 */
    $(document).click(function(event) {
        /* If click was outside the menu and wasn't the menu button itself OR was a link inside the menu, close menu */
        if ((!$(event.target).closest('.flexMenu-viewMore').length && !$(event.target).is('.flexMenu-viewMore')) || $(event.target).closest('.flexMenu-popup a').length) {
            $('.flexMenu-viewMore').removeClass('active');
            $('.flexMenu-popup').hide();
        }
    });

    /*
     * Action bar, selection buttons
     */
    /* By default, ensure everything is unchecked (when using browser back button, records may still be selected) */
    $('.table-selectable input[type=checkbox]').prop('checked', false);
    /* When a table allows selectable avatars and an avatar is clicked ... */
    $(document).on('click', '.table-selectable_avatar .avatar', function () {
        /* ... trigger a click on the checkbox found within the row */
        $(this).closest('tr').find('input[type=checkbox]').trigger('click');
    });
    /* When any selectable table checkbox changes ... */
    $(document).on('change', '.table-selectable input[type=checkbox]', function () {
        /* This table is read each time an input is checked since after an image upload, the pjax will refresh and the table is technically a new DOM element. */
        var $tableSelectable = $('.table-selectable');
        /* ... toggle the selected class on the table row based on the status of this checkbox */
        $(this).closest('tr').toggleClass('selected', $(this).is(":checked"));
        /* ... count the number of checked checkboxes within the table */
        var selectedRecordCount = $tableSelectable.find('input[type=checkbox]:checked').length;
        /* ... and activate the appropriate action bar group if more than one record is selected */
        setActionBarActiveGroup(((selectedRecordCount > 0) ? $(this).data("action-bar-group") : ''));
        /* Comma separated list of selected records to be set in href attributes */
        var selectedRecordIds = $tableSelectable.find('input[type=checkbox]:checked').map(function(i, e) {
            return $(e).val();
        }).get().join();
        /*
         * Update action bar selection buttons. Buttons with data-selection-allowed=1 allow single selection whereas
         * buttons with data-selection-allowed=N allow multiple selection. For single selection buttons, button is
         * disabled if count is anything other than 1 and the href will only be set if the count is 1. For multiple
         * selection buttons, button is disabled only if no records are selected and the href will be set for
         * anything other than no selection.
         */
        $.each($('.action-bar [data-selection-allowed=1][data-href-template]'), function (index, attr) {
            $(this).toggleClass('disabled', !(selectedRecordCount === 1))
                .attr('href', (selectedRecordCount === 1) ? $(this).data('href-template').replace('0', selectedRecordIds) : null);
        });
        $.each($('.action-bar [data-selection-allowed=N][data-href-template]'), function (index, attr) {
            $(this).toggleClass('disabled', !(selectedRecordCount >= 1))
                .attr('href', (selectedRecordCount >= 1) ? $(this).data('href-template').replace('0', selectedRecordIds) : null);
        });
    });

    /*
     * Panels
     */
    /* For collapsable panels, clicking the header performs collapse/expand */
    $('.panel_collapsable .panel__header').click(function () {
        $(this).closest('.panel').toggleClass('panel_minimized').find('.panel__body').slideToggle(transitionduration);
    });

    /*
     * Modal windows
     *
     * TODO but is this really needed? Does body scrolling really need to be prevented?
     * Not even currently doing this on all modals, just main help modal.
     */
    $('.lightbox__state').on('modalShown', function() {
        $('body').addClass('lightbox_open');
    }).on('modalHidden', function() {
        $('body').removeClass('lightbox_open');
    });
});

/*
 * Custom logging so that eventually, log levels can be defined. For now, just log the message as is.
 */
function consoleLog(msg) {
    console.log(msg);
}
