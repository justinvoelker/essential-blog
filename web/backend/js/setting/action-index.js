/* Search input */
var $settingSearch = $('#setting-search');
var $panel = $(".panel");

/* Trigger filtering of settings up keyup of search input */
$settingSearch.keyup(function () {
    filterSettings();
});

/* Function for filtering settings */
function filterSettings() {
    /* Search input value */
    var settingSearchValue = $settingSearch.val();

    /* Use positive lookahead to find the search term anywhere in the settings (e.g. "(?=.*audio)(?=.*video).*" ) */
    settingSearchValue = settingSearchValue.replace(/ /g, ")(?=.*");
    settingSearchValue = "(?=.*" + settingSearchValue + ").*";

    /* Create regex with previously set pattern, globally, case-insensitive */
    var regex = new RegExp(settingSearchValue, 'gi');

    /* Loop through each form-group (which represents a single setting) */
    $(".form-group .control-label").each(function () {
        /* Combine label,  additional search terms data attribute, and panel title */
        var haystack = $(this).text();
        haystack += $(this).data("additional-search-terms");
        haystack += $(this).closest(".panel").find(".panel__title").text();
        /* Hide the setting form group if it does not match the searched value */
        $(this).closest('.form-group').toggleClass("hidden", !haystack.match(regex));
    });

    /* Loop through each panel */
    $panel.each(function () {
        /* Hide the panel if all settings within that panel are hidden */
        $(this).toggleClass("hidden", $(this).find(".form-group").not(".hidden").length === 0);
    });

    /* If all panels are hidden, show the "no matching settings" message */
    $("#no-matching-settings").toggleClass("hidden", $panel.not(".hidden").length !== 0);
}
