/**
 * SECTION 1: GLOBAL
 *
 * Anything needed to make the following sections function
 */

/**
 * The following variables are set globally, outside this file. Included here for editor error suppression. This
 * comment only contains details about where the global variables are set. The following individual comments are
 * the actual declarations of those variables (they cannot contain additional text which is why this is here).
 * autosave_seconds Set in page/_form.php
 * page_is_locked_const Set in page/_form.php
 */
/* global autosave_seconds, page_is_locked_const */
var autosave_microseconds = autosave_seconds * 1000;
var pageIsLockedConst = page_is_locked_const;

/**
 * Functions declared elsewhere:
 * updateActionBarButtonText, set in widgets/ActionBar
 * updateActionBarButtonState, set in widgets/ActionBar
 */
/* global updateActionBarButtonText, updateActionBarButtonState */

/**
 * Additional variables to be used throughout the JavaScript
 */
/* Timeout used to trigger saving after given amount of time has passed. */
var timeoutId;
/* SimpleMDE instance. Set in page ready. */
var simplemde;
/* CodeMirror instance. Set after SimpleMDE is initialized to reference CodeMirror well after SimpleMDE initialization */
var cm;
/* Type of save that trigger the form submit (autosave, manual save, ctrl+s save, etc. */
var submitType;

/**
 * jQuery selectors for commonly used items
 */
/* Standard input fields */
var $pageTitle = $('#page-title');
var $pageContent = $('#page-content'); /* May not be used. Replaced with SimpleMDE */
//var $pageTags = $('#page-tags');
//var $pageCategoryId = $('#page-category_id');
var $pageImage = $('#page-image');
var $pageExcerpt = $('#page-excerpt');
var $pageSlug = $('#page-slug');
var $pageScheduledFor = $('#page-scheduled_for');
var $pageIsLocked = $('#page-is_locked');
/* Customized items used in addition to standard input fields */
var $fieldPageSlug = $(".field-page-slug");
var $imageModal = $(".image-modal");
var $imageModalTriggerField = $("#image-modal-trigger-field");
var $linkModal = $(".link-modal");
var $pageImageButtonClear = $("#page-image-clear");
//var $pageScheduledForInputGroup = $('.field-page-scheduled_for .input-group');
var $pageScheduledForButtonClear = $('.field-page-scheduled_for .btn-clear-input');
var $btnSave = $('[data-submit-type="save"]');
var $btnSaveRestorePoint = $('[data-submit-type="saverestorepoint"]');
var $btnPublishLocator = '[data-action="publish"]'; /* Used to update action bar button text/state */
var $btnPublish = $('[data-action="publish"]'); /* Used to set data-id after save */
var $btnUnpublishLocator = '[data-action="unpublish"]'; /* Used to update action bar button text/state */
var $btnUnpublish = $('[data-action="unpublish"]'); /* Used to set data-id after save */


/**
 * SECTION 2: FIELDS
 *
 * Logic triggered via actions to individual form fields.
 *
 * For title, excerpt, and slug, keydown is used (rather than the desirable keyup) because when focus is inside the
 * field, ctrl+s would not work. Although ctrl+s sets submitType to 'save' (manual save) the subsequent release of
 * ctrl+s keys triggered the keyup which changed submitType back to 'auto'. And if, for instance, autosave was disabled,
 * the form would now be submitted with 'auto' even though it was a manual ctrl-s save, and the autosave disabled check
 * would prevent the form from saving because autosave is disabled and now the submitType is auto. Keydown is fine
 * unless someone is holding down a key. In that case the autosave may save a page that is being written to from a
 * long-held keydown.
 */

/**
 * Title
 * Autosave timer can be triggered by keydown, but slug generation needs to happen on key up, after all title is known.
 */
$pageTitle.keydown(function (event) {
    if (!event.ctrlKey && !event.metaKey && !event.shiftKey) {
        resetAutosaveTimer('keydown on #page-title');
    }
});
$pageTitle.keyup(function () {
    generateOrValidateSlug('title');
});

/**
 * Content
 */
$pageContent.change(function () {
    // This change event is actually triggered via the cm.change event
    resetAutosaveTimer('change of #page-content');
});

/**
 * Tags
 * Since tags are within a Pjax container that will be refreshed after new records are created, need to bind the
 * 'on' event to the Pjax container and set the actual field as the selector. This allows the select input to still
 * listen for change events after the Pjax container is refreshed.
 */
$('#create-tag-refresh').on('change', '#page-tags', function () {
    resetAutosaveTimer('change of #page-tags');
});

/**
 * Category
 * Since category is within a Pjax container that will be refreshed after new records are created, need to bind the
 * 'on' event to the Pjax container and set the actual field as the selector. This allows the select input to still
 * listen for change events after the Pjax container is refreshed.
 */
$('#create-category-refresh').on('change', '#page-category_id', function () {
    resetAutosaveTimer('change of #page-category_id');
});

/**
 * Image
 */
$pageImage.change(function () {
    /* Change event triggered by click of .image-modal .modal-accept and click of #page-image-clear */
    resetAutosaveTimer('change of #page-image');
});
$pageImageButtonClear.click(function () {
    /* Trigger change event of the page-image field */
    $pageImage.change();
});

/**
 * Excerpt
 */
$pageExcerpt.keydown(function (event) {
    if (!event.ctrlKey && !event.metaKey && !event.shiftKey) {
        resetAutosaveTimer('keydown on #page-excerpt');
    }
});

/**
 * Slug
 */
$pageSlug.keydown(function (event) {
    if (!event.ctrlKey && !event.metaKey && !event.shiftKey) {
        resetAutosaveTimer('keydown on #page-slug');
    }
});
$pageSlug.keyup(function() {
    generateOrValidateSlug('slug');
});

/**
 * Scheduled At
 */
$pageScheduledFor.change(function () {
    /* Change event is triggered by manual edit, closing datetimepicker widget, or clicking the clear button. */
    resetAutosaveTimer('change of #page-scheduled_for');
    generateOrValidateSlug('scheduled_for');
});



/**
 * SECTION 3.1: EVENTS - FIELD RELATED
 *
 * Events for various form fields
 */

/**
 * Image Modal (from SimpleMDE editor or page image)
 */
/* When the image modal is closed via accept */
$imageModal.find(".modal-accept").click(function () {
    /* Different action depending on what triggered the modal to open */
    if ($imageModalTriggerField.val() === "page-content") {
        /* Triggered from page-content - insert the selected image into the page-content */
        consoleLog('Image modal (triggered from page-content) accepted. Inserting image.');
        var doc = cm.getDoc();
        var cursor = doc.getCursor();
        var imgAlternativeText = $("#image-modal-selected-image-alternative_text").val();
        /* Encoded to protect against spaces (encodeURL does not encode '/' like encodeURIComponent does) */
        var imgPath = encodeURI($("#image-modal-selected-image-path").val());
        doc.replaceRange("![" + imgAlternativeText + "](" + imgPath + ")", cursor);
    } else if ($imageModalTriggerField.val() === "page-image") {
        /* Trigger change event of the page-image field */
        $pageImage.change();
    }
});
/* When the image modal is closed via cancel, close the modal */
$('#image-modal').on('modalHidden', function() {
    /* Different action depending on what triggered the modal to open */
    if ($("#image-modal-trigger-field").val() === "page-content") {
        /* No need to resetAutosaveTimer() because it will be triggered from the SimpleMDE cm.change event */
        consoleLog('Image modal (triggered from page-content) closed. Sending focus to editor.');
        /* Triggered from page-content - send focus back there */
        cm.focus();
    }
});

/**
 * Image Modal (from SimpleMDE editor only - trigger field checks are just for consistency)
 */
/* When the link modal is closed via accept, insert the link */
$linkModal.find(".modal-accept").click(function () {
    /* Different action depending on what triggered the modal to open */
    if ($("#link-modal-trigger-field").val() === "page-content") {
        /* Triggered from page-content - insert the selected link into the page-content */
        consoleLog('Link modal (triggered from page-content) accepted. Inserting link.');
        var doc = cm.getDoc();
        var cursor = doc.getCursor();
        var linkTitle = $("#link-modal-link-title").val();
        /* Encoded to protect against spaces (encodeURL does not encode '/' like encodeURIComponent does) */
        var linkUrl = encodeURI($("#link-modal-link-url").val());
        doc.replaceRange("[" + linkTitle + "](" + linkUrl + ")", cursor);
    }
});
/* When the image modal is closed via cancel, close the modal */
$('#link-modal').on('modalHidden', function() {
    /* Different action depending on what triggered the modal to open */
    if ($("#link-modal-trigger-field").val() === "page-content") {
        /* No need to resetAutosaveTimer() because it will be triggered from the SimpleMDE cm.change event */
        consoleLog('Link modal (triggered from page-content) closed. Sending focus to editor.');
        /* Triggered from page-content - send focus back there */
        cm.focus();
    }
});

/**
 * Scheduled At handling clear button and clicking outside datetime widget
 */
/* On click of scheduled_for clear button ... */
$pageScheduledForButtonClear.click(function() {
    /* ... clear the DateTimePicker ... */
    /* CALWIDGET Uncomment following line (and probably update it) when a calendar widget is added */
    // $pageScheduledForInputGroup.data("DateTimePicker").clear();
    /* ... and trigger the scheduled_for change event */
    $pageScheduledFor.change();
});
/* Clicked outside the datetimepicker, hide it */
$(document).on('mousedown', function(e) {
    /* Hide picker when clicking outside (https://github.com/Eonasdan/bootstrap-datetimepicker/pull/1615) */
    /* CALWIDGET Uncomment following block (and probably update it) when a calendar widget is added */
    // if ($(e.target).closest('.datetimepicker').length === 0) {
    //     $pageScheduledForInputGroup.data("DateTimePicker").hide();
    // }
});



/**
 * SECTION 3.2: EVENTS - GENERAL
 *
 * Events not related to form fields
 */

/*
 * Capture the standard form submit and instead call savePage which will submit the form data via ajax
 */
$('form#page-form').on('beforeSubmit', function () {
    consoleLog('Form beforeSubmit captured.');
    /* Save the page */
    savePage();
    /* Prevent default form submission */
    return false;
});

/*
 * Handling the Save and Save Restore Point buttons
 */
/* When a link/button with submit-type data attribute is clicked, copy the submitType to the form field and submit the form. */
$('[data-submit-type]').click(function () {
    submitType = $(this).data('submit-type');
    consoleLog('Action bar button clicked: ' + submitType);
    /* Submit the form (will be caught by beforeSubmit) */
    $('form#page-form').submit();
});

/*
 * Capture ctrl+s as save
 */
$(window).bind('keydown', function(event) {
    if (event.ctrlKey || event.metaKey) {
        switch (String.fromCharCode(event.which).toLowerCase()) {
            case 's':
                event.preventDefault();
                consoleLog('Keydown combination: ctrl+s');
                if (!$('[data-submit-type=save]').hasClass('disabled')) {
                    submitType = 'save';
                    $('form#page-form').submit();
                } else {
                    consoleLog('Save button is disabled');
                }
                break;
        }
    }
});



/**
 * SECTION 4.1: FUNCTIONS - GENERAL
 *
 * General functions triggered by actions performed on individual fields
 */

function consoleLog(msg) {
    console.log(msg);
}

/**
 * Saving restore points can be enabled or disabled as needed. For example, after a save restore point, disable save restore point to
 * protect against a second, duplicate restore point. After any field changes, re-enable save restore point.
 * Values will be set as follows:
 *  - On page load: TRUE/FALSE. If most recent child is locked FALSE. Else TRUE.
 *  - On any field edit: TRUE. All edits enable all types of save (from resetAutosaveTimer()).
 *  - On form submit - auto: TRUE. Automatic save, restore point can still be saved.
 *  - On form submit - save: TRUE. Manual save, restore point can still be saved.
 *  - On form submit - saverestorepoint: FALSE. Manual restore point save prohibits another restore point save.
 *  - On form submit - ajax error: TRUE. A failed save allows another attempt at saving.
 * @param state
 */
function btnSaveRestorePointEnableDisable(state) {
    $btnSaveRestorePoint.toggleClass('disabled', !state);
}

/**
 * Manual and autosaving can be permitted/prohibited as needed. For example, after a manual save,
 * disable autosave so an autosave timer does not trigger another save.
 * Values will be set as follows:
 *  - On page load: FALSE. Page was just opened so nothing has changed to be saved.
 *  - On any field edit: TRUE. All edits enable all types of save (from resetAutosaveTimer()).
 *  - On form submit - any type: FALSE. Any save prohibits another save.
 *  - On form submit - ajax error: TRUE. A failed save allows another attempt at saving.
 * @param state
 */
function btnSaveEnableDisable(state) {
    $btnSave.toggleClass('disabled', !state);
}

/**
 * Reset the autosave timeout timer, triggering an autosave after the time expired
 * @param trigger Message about what triggered the call to this function
 */
function resetAutosaveTimer(trigger) {
    consoleLog('Autosave timer reset. Triggered via ' + trigger);
    /* Ensure save is enabled */
    btnSaveEnableDisable(true, trigger);
    /* Ensure saverestorepoint is enabled */
    btnSaveRestorePointEnableDisable(true, trigger);
    /* Disable publish and unpublish (will potentially be be enabled after save) */
    updateActionBarButtonState($btnPublishLocator, false);
    updateActionBarButtonState($btnUnpublishLocator, false);
    /* Clear current timeout */
    window.clearTimeout(timeoutId);
    /* Set a new timeout */
    timeoutId = window.setTimeout(function () {
        consoleLog('Autosave timer expired. Page save triggered.');
        submitType = 'auto';
        savePage();
    }, autosave_microseconds);
}

/**
 * Update the actual slug field and the preview containing the full path (including slug)
 */
function updateSlugValue(slug) {
    /* Update the actual slug input with the new slug (may not have changed depending on controller logic) */
    $('#page-slug').val(slug);
}
function updateSlugPreview(preview) {
    /* Update the url preview */
    $('.field-page-slug .hint-block .preview').html(preview);
}



/**
 * SECTION 4.2: FUNCTIONS - SPECIFIC
 *
 * Specific functions used by only a limited set of calling
 */

/**
 * Generate a unique page slug for the given title and scheduled_for (if present). Or, if triggered from the slug field,
 * validate the specified slug. The page ID is needed so that if this is a page being updated, the
 * slug generated is allowed to be slug that it is already assigned. Otherwise, generating a the page slug would
 * conflict with itself. A new slug is only returned if page is in draft status.
 * @param trigger
 */
function generateOrValidateSlug(trigger) {
    consoleLog('generateOrValidateSlug. Triggered via ' + trigger);
    return $.ajax({
        url: '/admin/page/generate-or-validate-slug?primary_id=' + $('#page-primary_id').val() + '&trigger=' + trigger,
        type: 'POST',
        data: $('form#page-form').serialize(),
        /**
         * @param data Data object returned from ajax submission
         * @param data.notify Notification message
         * @param data.notifyType Notification type (success, error, etc)
         * @param data.slug Page slug (without other URL parameters)
         * @param data.url Entire page URL path (including slug)
         */
        success: function (data) {
            /* If triggered via anything other than the slug field, update the value */
            if (trigger !== 'slug') {
                updateSlugValue(data.slug);
            }
            /* Regardless of the trigger, the preview should be updated */
            updateSlugPreview(data.url);
            /* Only display non-success return notifications */
            if (data.notifyType !== 'success') {
                //noinspection JSUnresolvedFunction
                flashAlert({type: data.notifyType, message: data.notify});
            } else {
                /* As a success, no alert is show, but, the result can at least be logged in the console */
                consoleLog(data.notify);
            }
        }
    });
}

/**
 * Save the page to the database (if there are no form errors). After form is saved via ajax, a lot happens:
 * 1) The form action is returned/update. Next save will page to the proper update action.
 * 2) The url is updated via push state. A refreshed page will now appear to be on the update page.
 * 3) The page id is updated. Ensures subsequent submissions are aware a primary exists and so slug won't conflict with its own primary page.
 * $) Save notifications are returned/displayed. Indicates success or error during save.
 * @returns {boolean}
 */
function savePage() {
    consoleLog('Inside savePage() with submitType of ' + submitType);

    /* Only continue if NOT an autosave or IS an autosave and the auto save seconds are not 0 */
    if (submitType !== 'auto' || (submitType === 'auto' && autosave_seconds !== '0')) {

        /* If auto save and the record has not yet been (manually) saved, return message and exit */
        if ((submitType === 'auto') && $('#page-primary_id').val().length === 0) {
            consoleLog('submitType is "'+submitType+'" but page has not yet been manually saved. Exiting savePage().');
            //noinspection JSUnresolvedFunction
            flashAlert({type: 'warning', message: 'Autosave temporarily disable. Manually save the page to enable autosaving.'});
            return false;
        }

        /* Do nothing if this is a manual or auto save and save is disabled */
        if ((submitType === 'save' || submitType === 'auto') && $btnSave.hasClass('disabled')) {
            consoleLog('submitType is "'+submitType+'" but save is disabled. Exiting savePage().');
            return false;
        }

        /* Do nothing if this is a save restore point and save restore point is disabled */
        if (submitType === 'saverestorepoint' && $btnSaveRestorePoint.hasClass('disabled')) {
            consoleLog('submitType is "'+submitType+'" but save restore point is disabled. Exiting savePage().');
            return false;
        }

        /* Do nothing and return false if the form still has validation errors */
        if ($("form#page-form").find(".has-error").length) {
            consoleLog('Form contains errors. Exiting savePage().');
            return false;
        }

        /* If submitType is saverestorepoint, set "is_locked" field to lock this restore point when it is saved. */
        if (submitType === 'saverestorepoint') {
            consoleLog('SubmitType is saverestorepoint. Setting is_locked to "true".');
            $pageIsLocked.val(pageIsLockedConst);
        }

        // Submit the ajax request to save the page
        var form = $('form#page-form');
        $.ajax({
            url: form.attr('action'),
            type: 'POST',
            data: form.serialize(),
            success: function (data) {
                /**
                 * @param data Data object returned from ajax submission
                 * @param data.notify Notification message
                 * @param data.notifyType Notification type (success, error, etc)
                 * @param data.buttons Button text and enabled/disabled state for all of the action bar buttons
                 * @param data.primary_id ID of primary page
                 * @param data.action Form action to be set
                 * @param data.slug Page slug
                 * @param data.url Entire page URL path (which includes additional URL parameters)
                 * @param data.urlPreview Text URL of the page preview link
                 */

                /* Display returned notification */
                //noinspection JSUnresolvedFunction
                flashAlert({type: data.notifyType, message: data.notify});

                /* Update text and enabled/disabled state of action bar buttons */
                var buttons = data.buttons;
                for (var button in buttons) {
                    if (buttons.hasOwnProperty(button)) {
                        updateActionBarButtonText(buttons[button].locator, buttons[button].text);
                        updateActionBarButtonState(buttons[button].locator, buttons[button].enabled);
                    }
                }
                /* Update the data-id for publish and ubpublish buttons */
                $btnPublish.attr('data-id', data.primary_id);
                $btnUnpublish.attr('data-id', data.primary_id);

                /*
                 * Update the primary page ID attribute so it can be used during future form submissions.
                 * First ensure primary_id was returned, since, if there was a validaton error it will not be returned
                 * and without this check it would set the primary_id to empty which would cause the next save to not
                 * include a primary_id which is very bad.
                 */
                if (data.primary_id) {
                    $('#page-primary_id').val(data.primary_id);
                }

                /**
                 * The returned form action will change when a new page becomes an update page. Note, if there was a
                 * validation error, the child will not be saved and the action returned will be the same as a new page.
                 * This logic should only ever be called once (when changing from new to update page)
                 */
                if (form.attr('action') !== data.action) {

                    /* Update the form action with the new action (only ever changes from create to update) */
                    form.attr('action', data.action);

                    /**
                     * Update the current URL via pushState. This is used when a new page is saved for the first time
                     * to update the url to be that of the update page that includes the page id
                     */
                    var obj = {ID: data.primary_id, Title: 'Update', Url: data.action};
                    history.pushState(obj, obj.Title, obj.Url);

                    /* Update the preview page link and remove disabled class */
                    $('a.page-preview-link').prop('href', data.urlPreview).removeClass('disabled');
                }

                /* Update the slug and url. Any time the page is saved, this has the potential to be different. */
                updateSlugValue(data.slug);
                updateSlugPreview(data.url);

                /* Update header to reflect that this is no longer a new page but is now and update */
                $.pjax.reload({container:'#header__body'});
            },
            error: function (jqXHR) {
                //noinspection JSUnresolvedFunction
                flashAlert({type: 'danger', message: jqXHR.responseText});
                /* Log to console */
                consoleLog('savePage() ajax resulted in an error.');
                /* Since something must have gone wrong, ensure all saving is enabled */
                btnSaveEnableDisable(true, 'Ajax error in savePage()');
                btnSaveRestorePointEnableDisable(true, 'Ajax error in savePage()');
            },
            complete: function () {
                /*
                 * Regardless of success of failure, always set "is_locked" back to empty so that subsequent form
                 * submits are not locked unless a restore point is being saved.
                 */
                $pageIsLocked.val('');
            }
        });
    } else {
        consoleLog('Autosave is disabled.');
    }
}



/**
 * SECTION 5: PAGE READY
 *
 * Everything that needs to happen when the page is ready
 */
$(function () {
    /**
     * SimpleMDE - page-content
     */
    simplemde = new SimpleMDE({
        element: document.getElementById("page-content"),
        autoDownloadFontAwesome: false,
        forceSync: true,
        indentWithTabs: false,
        status: false,
        toolbar: [
            'bold', 'italic', 'heading', '|', 'quote', 'unordered-list', 'ordered-list', '|',
            {
                name: "link",
                action: function drawLinkCustom(){
                    consoleLog('Link modal opened from page-content.');
                    $("#link-modal-trigger-field").val('page-content');
                    $('#link-modal').trigger('modalShow'); // Display modal for link insertion
                },
                className: "fa fa-link",
                title: "Insert Link"
            },
            {
                name: "image",
                action: function drawImageCustom(){
                    consoleLog('Image modal opened from page-content.');
                    $("#image-modal-trigger-field").val('page-content');
                    $('#image-modal').trigger('modalShow'); // Display modal for image selection
                },
                className: "fa fa-picture-o",
                title: "Insert Image"
            },
            '|', 'preview', 'side-by-side', 'fullscreen',
            {
                name: "markdown-help",
                action: function showMarkdownGuide(){
                    consoleLog('Markdown Help modal opened from page-content.');
                    $('#markdown-help-modal').trigger('modalShow'); // Display modal for markdown guide
                },
                className: "fa fa-question-circle",
                title: "Markdown Help"
            }
        ]

    });
    // CodeMirror instance for later use in setting content or focus
    cm = simplemde.codemirror;
    // On change of SimpleMDE, trigger change event of page-content
    cm.on("change", function () {
        $pageContent.change();
    });

    /**
     * DateTimePicker - page-scheduled_for
     */
    /* CALWIDGET Uncomment following line (and probably update it) when a calendar widget is added */
    /*$pageScheduledForInputGroup.datetimepicker({
        format: 'YYYY-MM-DD HH:mm',
        focusOnShow: false,
        showClose: true, // Required for this: https://github.com/Eonasdan/bootstrap-datetimepicker/pull/1615
        icons: {
            time: 'btn-toggle-time',
            date: 'btn-toggle-date',
            up: 'fa fa-chevron-up',
            down: 'fa fa-chevron-down',
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            close: 'btn-calendar-close'
        },
        useCurrent: false // Used in conjunction with the dp.show below
    }).on('dp.show', function() {
        // Since a date shows up instantly upon opening, show the clear icon
        $pageScheduledForButtonClear.removeClass('hidden');
        // Fix for time always starting at 12:00 AM ( https://github.com/Eonasdan/bootstrap-datetimepicker/issues/1311)
        if($(this).data("DateTimePicker").date() === null) {
            // This likely needs moment javascript library
            $(this).data("DateTimePicker").date(moment());
        }
    }).on('dp.change', function(e) {
        // Re-evaluate the visibility of the clear icon
        $pageScheduledForButtonClear.toggleClass('hidden', e.date == false);
    }).on('dp.hide', function() {
        // Since dp.change triggers on every date selection within the widget and we only want the final date selected,
        // the scheduled_for change event is only fired when the widget is closed.
        $pageScheduledFor.change();
    });*/
});
