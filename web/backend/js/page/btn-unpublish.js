/**
 * Unpublish a single page via Ajax call.
 */

/**
 * Functions declared elsewhere:
 * updateActionBarButtonText, set in widgets/ActionBar
 * updateActionBarButtonState, set in widgets/ActionBar
 * updateActionBarButtonState, set in widgets/FlashAlert
 */
/* global updateActionBarButtonText, updateActionBarButtonState, flashAlert */

/* Define publish button */
var $btnUnpublish = $('[data-action="unpublish"]');

/* On click of element with data-action attribute set to unpublish, unpublish the page */
$btnUnpublish.click(function () {

    /* Disable the button to prevent multiple submissions. Will be re-enabled later if unpublish failed. */
    $btnUnpublish.addClass('disabled');

    /* Ajax call to publish action */
    return $.ajax({
        url: '/admin/page/unpublish?id=' + $(this).data('id'),
        type: 'POST',

        /* Ajax call was successful */
        success: function (data) {
            /**
             * @param data Data object returned from ajax submission
             * @param data.notify Notification message
             * @param data.notifyType Notification type (success, error, etc)
             * @param data.buttons Button text and enabled/disabled state for all of the action bar buttons
             */

            /* Display returned notification */
            flashAlert({type: data.notifyType, message: data.notify});

            /* Update text and enabled/disabled state of action bar buttons */
            var buttons = data.buttons;
            for (var button in buttons) {
                if (buttons.hasOwnProperty(button)) {
                    updateActionBarButtonText(buttons[button].locator, buttons[button].text);
                    updateActionBarButtonState(buttons[button].locator, buttons[button].enabled);
                }
            }
        },

        /* Ajax call failed (permissions issue, etc.) */
        error: function (jqXHR) {
            flashAlert({type: 'danger', message: jqXHR.responseText});
            /* Re-enable publish button since something failed */
            $btnUnpublish.removeClass('disabled');
        }

    });

});
