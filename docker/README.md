# Docker

General notes from https://github.com/gliderlabs/docker-alpine/blob/master/docs/usage.md

Partially pulled from https://github.com/prooph/docker-files/blob/master/php/7.1-fpm

Imagemagick instructions https://github.com/docker-library/php/issues/105#issuecomment-249716758

Imagemagick 3.4.1 issue fixed in 3.4.3 https://bugs.php.net/bug.php?id=72311

