#!/bin/sh

# Copy sample params file and fill it with environment variables
cp -f /var/www/html/config/params-sample.php /var/www/html/config/params.php
sed -i "s/database_host_here/${MYSQL_HOST}/" /var/www/html/config/params.php
sed -i "s/database_name_here/${MYSQL_DATABASE}/" /var/www/html/config/params.php
sed -i "s/database_username_here/${MYSQL_USER}/" /var/www/html/config/params.php
sed -i "s/database_password_here/${MYSQL_PASSWORD}/" /var/www/html/config/params.php
sed -i "s/database_table_prefix_here/${MYSQL_TABLE_PREFIX}/" /var/www/html/config/params.php
sed -i "s/cookie_validation_key/${COOKIE_VALIDATION_KEY}/" /var/www/html/config/params.php
sed -i "s/encryption_key_here/${ENCRYPTION_KEY}/" /var/www/html/config/params.php

# Update set_real_ip_from in nginx default.conf if set
if [ ! -z ${NGINX_SET_REAL_IP_FROM+x} ]
then
  sed -i "s|set_real_ip_from.*|set_real_ip_from ${NGINX_SET_REAL_IP_FROM};|" /etc/nginx/conf.d/default.conf
fi

# NOTE: Note that we are specifying TERM=dumb below when invoking the PHP
# interpreter. This suppresses an error which looks like this:
#   No entry for terminal type "unknown";
#   using dumb terminal settings.
# This arises from somewhere in the PHP startup machinery if TERM is not
# set to a recognized value.
TERM=dumb php -- <<'EOPHP'
<?php
// Ensure database is up and running before trying to execute the migration

// Open standard error in write mode
$stderr = fopen('php://stderr', 'w');

// Split MYSQL_HOST into a host and socket (or port)
list($host, $socket) = explode(':', getenv('MYSQL_HOST'), 2);

// If the socket/port is numeric, it is a port
$port = 0;
if (is_numeric($socket)) {
	$port = (int) $socket;
	$socket = null;
}

// Get MYSQL_USER, MYSQL_PASSWORD, and MYSQL_DATABASE
$user = getenv('MYSQL_USER');
$pass = getenv('MYSQL_PASSWORD');
$dbName = getenv('MYSQL_DATABASE');

// Try to connect to database every 2 seconds for 30 attempts (1 minute total)
$maxTries = 30;
do {
	$mysql = new mysqli($host, $user, $pass, '', $port, $socket);
	if ($mysql->connect_error) {
		fwrite($stderr, "\n" . 'MySQL Connection Error: (' . $mysql->connect_errno . ') ' . $mysql->connect_error . "\n");
		--$maxTries;
		if ($maxTries <= 0) {
			exit(1);
		}
		sleep(2);
	}
} while ($mysql->connect_error);

// By this point, either the connection was successful or has exited

$mysql->close();
EOPHP

# Remove environment variables to protect against accidentally leaking private information.
unset MYSQL_HOST
unset MYSQL_DATABASE
unset MYSQL_USER
unset MYSQL_PASSWORD
unset MYSQL_TABLE_PREFIX
unset COOKIE_VALIDATION_KEY
unset ENCRYPTION_KEY
unset NGINX_SET_REAL_IP_FROM

# Execute database migration
/var/www/html/yii migrate --interactive=0

# Since entrypoint runs as root, reset all permissions just to be safe.
# Permissions are set here rather than Dockerfile to ensure proper permissions during development as well as production.
chgrp -R www-data /var/www/html/
chmod -R g-w /var/www/html/
chmod -R g+w /var/www/html/runtime/ /var/www/html/web/assets/ /var/www/html/web/uploads/

# Start supervisord and services
/usr/bin/supervisord -c /etc/supervisord.conf

exec "$@"
