server {
    listen 80 default;
    # https://github.com/jwilder/nginx-proxy/issues/130
    real_ip_header X-Forwarded-For;
    # https://github.com/jwilder/nginx-proxy/issues/130
    set_real_ip_from 172.17.0.0/16;
    client_max_body_size 128M;
    error_log /var/log/nginx/error.log;
    root /var/www/html/web;
    index index.php;
    if (!-e $request_filename) {
        rewrite ^.*$ /index.php last;
    }
    location ~ \.php$ {
        client_body_temp_path /tmp;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;

        # If behind a reverse proxy, Yii needs some way for \Yii::$app->getRequest()->getIsSecureConnection() to
        # return true (used by auth clients to build URLs). Setting $_SERVER['HTTPS'] will accomplish this.
        # https://gist.github.com/DaniilTomilow/95a87823eda19ea9b1e283415dc6bd0d
        set $my_https $https;
        if ($http_x_forwarded_proto = 'https') {
            set $my_https 'on';
        }
        fastcgi_param HTTPS $my_https; # set $_SERVER['HTTPS']

        fastcgi_pass 127.0.0.1:9000;
    }
}
